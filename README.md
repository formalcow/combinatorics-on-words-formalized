# Combinatorics on Words Formalized in Isabelle/HOL

This repository contains the formalization of basics of Combinatorics on Words in [Isabelle/HOL](https://isabelle.in.tum.de/).
Note that this is a **work in progress** and future versions may change.
The main motivation for this project is that tedious and repetitive proofs are no exception in Combinatorics on Words, reappearing in many works.
It is desirable to outsource these tasks to a computer. 
Moreover, we hope that systematic use of proof assistants may evetually bring about new results in problems where the main obstacle is an excessive complexity of the proof.

Mature parts are part of [Archive of Formal Proofs](https://www.isa-afp.org/).

Table of contents:

[[_TOC_]]

## Requirements / Dependencies

Compatible with [Isabelle2024](https://isabelle.in.tum.de/).

The following entries from the [Archive of Formal Proofs](https://www.isa-afp.org/) are used and needed to run some of our theories (currently Lyndon/Lyndon_Addition.thy ):
1. [Szpilrajn](https://www.isa-afp.org/entries/Szpilrajn.html) (used in [Lyndon_Addition.thy](#lyndon_additionthy))
1. [Regular sets](https://www.isa-afp.org/entries/Regular-Sets.html) (used in [Infinite_words.thy](#infinite_wordsthy))

<!-- See [using the AFP](https://www.isa-afp.org/using.html). -->

<!-- ## Documentation -->

<!-- See the [automatically generated Isabelle document for the latest version.](/../-/jobs/artifacts/master/raw/document.pdf?job=build) -->

## Installation / Usage

Isabelle/HOL (including an editor called jEdit) may be installed [using the Isabelle installation guide](https://isabelle.in.tum.de/installation.html).

AFP, required in some of our theories, may be installed [following the help page of AFP](https://www.isa-afp.org/help).

There are 3 ways how to use our formalization:
1. [download the current 'draft' version](/../-/jobs/artifacts/master/download?job=build-draft), unzip it and open any *.thy file in the Isabelle/HOL editor jEdit (see below for a short guide)
1. use the mature (but not the most recent) content via the [Archive of Formal Proofs](https://www.isa-afp.org/using.html) using the sessions [Combinatorics_Words](https://www.isa-afp.org/entries/Combinatorics_Words.html), [Combinatorics_Words_Lyndon](https://www.isa-afp.org/entries/Combinatorics_Words_Lyndon.html), [Combinatorics_Words_Graph_Lemma](https://www.isa-afp.org/entries/Combinatorics_Words_Graph_Lemma.html),
[Binary_Code_Imprimitive](https://www.isa-afp.org/entries/Binary_Code_Imprimitive.html),
[Two_Generated_Word_Monoids_Intersection](https://www.isa-afp.org/entries/Two_Generated_Word_Monoids_Intersection.html)
1. clone this repository and point Isabelle to its ROOTS by manually adding your path like /home/myself/Combinatorics-On-Words-Formalized (Linux/Mac) or /cygdrive/c/Combinatorics-On-Words-Formalized to the ROOTS file in the Isabelle_Home_User directory, which is best accessed from jEdit using File/Open/Favorites (top of the pop up window)/$ISABELLE_HOME_USER . If you have AFP installed, this will create a conflict and you need to disable the conflicting entries in the AFP Roots file.

<!--
Add this folder path to your Isabelle ROOTS file (`.isabelle/Isabelle20XX/ROOTS` on Mac/Linux/cygwin installations):

On Windows, add the folder as TODO
-->

### A short guide for Isabelle/HOL beginners - How to formalize your Combinatorics on Words result

Isabelle comes with an editor called jEdit included in the package downloadable from [Isabelle homepage](https://isabelle.in.tum.de/).

First, you need to install Isabelle [using the Isabelle installation guide](https://isabelle.in.tum.de/installation.html) and Archive of Formal Proofs (AFP) [following the help page of AFP](https://www.isa-afp.org/help).
Note that jEdit needs to be restarted so that AFP is made available.


The easiest way to explore our formalization is to [download the current 'draft' version](/../-/jobs/artifacts/master/download?job=build-draft).
Once downloaded and unzipped, a good starting point is ExampleBasic.thy.
It contains comments aimed at Isabelle beginners and introduces a few concepts of our formalization.
You can try to write your lemma and the end of this example file or in Scratch.thy.

The Isabelle's documentation is available directly in the editor, in the Documentation tab next to the File browser tab.
The basic tutorial is prog-prove: Programming and Proving in Isabelle/HOL.

We'd be happy if you use our formalization of Combinatorics on Words foundations to formalize your results.
Besides the very basic concepts in CoW/CoWBasic.thy, such as prefixes, factors, quotients, powers, roots etc., we already cover and plan to cover more elementary concepts.
For instance, we have the Periodicity lemma, the Graph lemma, the Lyndon-Schützenberger theorem, Lyndon words, factor interpretation, ((binary) code, all ready to be used.
See [code organization](#code) for a list of what is formalized and where.
Your results can be built up on our formalization, it suffices to base your theory on ours ( = include the required session).

## Suggestions etc.

We are open to suggestions and opinions!


## <a name="code"></a> Code organization and formalized facts/results

The theory files are in the src folder.

### Basics

A release version is available in the AFP as [Combinatorics_Words](https://www.isa-afp.org/entries/Combinatorics_Words.html).

The development session is called CoW and is in the folder of the same name. It contains the following theories.

#### CoWBasic.thy

1. Essential definitions and notation: concatenation, empty word, nonempty element of, prefix, suffix, left and right quotient, longest common prefix, factor, length
1. Equidivisibility properties
1. Total morphisms, reversed morphism
1. Conjugation
1. Root
1. Commutation of 2 words
1. Period root and numeric period
1. Border, shortest border, relation to period
1. Primitive word and primitive root
1. Primitivity_inspection method
1. List_inspection method
1. Primitivity testing method
1. Various auxiliary facts

#### Submonoids.thy

1. Hull
1. Factorization into generators
1. Basis
1. Code
1. Free hull
1. Marked code
1. Singletons code
1. Mismatch method
1. Binary mismatch

#### PeriodicityLemma.thy

1. Relaxed version of periodicity lemma
1. Periodicity lemma (also known as the Fine and Wilf theorem)
1. Optimality of the Periodicity lemma (construction of the Fine-and-Wilf-word)

#### Morphisms.thy

1. (Total) morphism
1. Morphic extension, core map
1. Non-erasing morphism
1. Code morphism
1. Prefix code morphism
1. Marked morphism
1. Two morphisms, solutions, coincidence pairs
1. Two non-erasing morphisms
1. Two marked morphisms

#### Equations_Basics.thy

1. Factor interpretation
1. Mismatch related lemmas
1. Conjugate words
1. Commuting words
1. Three covers
1. Binary equality words (in progress)

#### Binary_Code_Morphisms.thy

1. Datatype of binary alphabet
1. Morphism over a binary alphabet (marked and unmarked)
1. Two binary morphisms

#### Lyndon_Schutzenberger.thy

1. Lyndon-Schützenberger theorem
1. Full parametric solution of $`x^jy^k = z^\ell`$

### Graph_Lemma

A release version is available in AFP as [Combinatorics_Words_Graph_Lemma](https://www.isa-afp.org/entries/Combinatorics_Words_Graph_Lemma.html).

The development session is called CoW_Graph_Lemma and is in the folder of the same name. It contains the following theories.

#### GluedCodes.thy

1. Letter gluing, glued code, binary glued code

#### GraphLemma.thy

1. Graph lemma
1. Application: two noncommuting words form a code

### Lyndon

A release version is available in AFP as [Combinatorics_Words_Lyndon](https://www.isa-afp.org/entries/Combinatorics_Words_Lyndon.html).

The development session is called CoW_Lyndon and is in the folder of the same name. It contains the following theories.

#### Lyndon.thy

1. Lyndon words, their properties and characterization
1. Longest Lyndon suffix
1. Lyndon factorization

#### Lyndon_Addition.thy

1. Smallest relation guaranteeing lexicographical minimality of a given word (in its conjugacy class)

[Archive of Formal Proofs](https://www.isa-afp.org/) entry [Szpilrajn](https://www.isa-afp.org/entries/Szpilrajn.html) is required in this theory.

### Interpretations

The development session is called CoW_Interpretations and is in the folder of the same name. It contains the following theories.

#### Binary_Square_Interpretation.thy

1. Lemmas for the situation of a square covered by a binary code

#### Binary_Code_Imprimitive.thy

1. Primitivity non-preserving binary code

#### Binary_Imprimitive_Decision.thy

1. Lemma and code equations to test binary imprimitivity

### Two Generated Word Monoids Intersection

A release version is available in AFP as [Two_Generated_Word_Monoids_Intersection](https://www.isa-afp.org/entries/Two_Generated_Word_Monoids_Intersection.html).

The development session is called CoW_Binary_Monoids_Intersection and is in the folder of the same name. It contains the following theories.

#### CoW_Binary_Monoids_Intersection.thy

1. Characterization of intersection of two-generated word monoids

### Equations (**work in progress**)

The development session is called CoW_Equations and is in the folder of the same name.
The published version is empty for now.

### Infinite (**work in progress**)

Session covering infinite words.

The development session is called CoW_Infinite and is in the folder of the same name. It contains the following theories.

#### Languages.thy

1. Factorial languages
1. Factor closure
1. Recurrent languages
1. (Purely) morphic languages

#### Infinite_Words.thy

1. Elementary concepts related to infinite words
1. Language of an infinite word
1. Periodic infinite words
1. Morphic image of an infinite word
1. Fixed points

#### Sturmian_Words.thy

1. Unit circle rotation
1. Lower mechanical words

### Post Correspondence Problem

#### Double_Periodic_GPCP2.thy

1. Algorithm deciding the Post Correspondence Problem for two periodic morphisms

## Article versions and references

The code, or its previous versions, was mentioned and used in the following publications:

 1. Š. Holub, Š. Starosta: [Binary Intersection Formalized](https://doi.org/10.1016/j.tcs.2021.03.002), Theoret. Comput. Sci. 866, pp. 14-24, 2021, archived in the branch [Binary-Intersection-Formalized](../../tree/Binary-Intersection-Formalized)
 1. Š. Holub, Š. Starosta: [Formalization of Basic Combinatorics on Words](https://doi.org/10.4230/LIPIcs.ITP.2021.22), 12th International Conference on Interactive Theorem Proving (ITP 2021), Liron Cohen and Cezary Kaliszyk (Eds.), Article No. 22; pp. 22:1–22:17
 1. Š. Holub, Š. Starosta: [Lyndon words formalized in Isabelle/HOL](https://doi.org/10.1007/978-3-030-81508-0_18), DLT 2021
 1. Š. Holub, M. Raška, Š. Starosta: [Binary codes that do not preserve primitivity](https://doi.org/10.4230/LIPIcs.ITP.2021.22), IJCAR 2022
 1. Š. Holub, M. Raška, Š. Starosta. [Binary Codes that do not Preserve Primitivity](https://doi.org/10.1007/s10817-023-09674-2), J. Autom. Reason. 67, 25 (2023).
 1. V. Halava, Š. Holub. [Binary Generalized PCP for Two Periodic Morphisms is Decidable in Polynomial Time](https://doi.org/10.1142/S0129054123480076), International Journal of Foundations of Computer Science 35, No. 01n02, pp. 129-144 (2024).
 1. K. Klouda, Š. Starosta [The number of primitive words of unbounded exponent in the language of an HD0L-system is finite](https://doi.org/10.1016/j.jcta.2024.105904), Journal of Combinatorial Theory, Series A, 206, 105904 (2024).

## Tentative roadmap

 1. Results on binary equality words
 1. Textbook
 1. Results on derived sequences
 

## jEdit macros

The folder jEdit contains our macro scripts.
See the readme file in the folder.

These macros are very helpful to use our notation, but also ease our work with the jEdit editor in general.

## Avatar
The avatar of the repository features a [painting](http://words2005.lacim.uqam.ca/english/AfficheA.html) by [Michel Mendès-France](https://fr.wikipedia.org/wiki/Michel_Mend%C3%A8s_France). Courtesy of [Srecko Brlek](http://lacim-membre.uqam.ca/~brlek/).
