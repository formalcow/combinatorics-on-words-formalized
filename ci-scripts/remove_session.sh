#!/usr/bin/env bash
# Prepare draft version script - remove session dependency and put all in one archive.

# shopt -s globstar
dirs=(.. ../src/CoW ../src/CoW_Lyndon ../src/CoW_Graph_Lemma ../src/examples ../src/CoW_Equations ../src/CoW_Interpretations ../src/CoW_Binary_Monoids_Intersection ../src/CoW_Infinite ../src/CoW_PCP)
target="../Combinatorics-On-Words-Formalized"

echo "There are ${#dirs[@]} dirs in the current path"
echo "Target is ${target}."

mkdir ${target}

let i=1

for dir in "${dirs[@]}"; do
    echo "$((i++)) $dir"
    # readarray -t dirs < <(find . -maxdepth 1 -type d -printf '%P\n')
    find $dir -name "*.thy" -maxdepth 1 -type f -print0 | while read -d $'\0' file
		do
		    echo "…code using ${file}"
		    cp ${file} "${target}/$(basename $file)"
		    sed -i -E '
s/CoW\.//g
s/CoW_Lyndon\.//g
s/CoW_Graph_Lemma\.//g
s/CoW_Equations\.//g
s/CoW_Interpretations\.//g
s/CoW_Binary_Monoids_Intersection\.//g
s/CoW_Infinite\.//g
s/CoW_PCP\.//g
s/\@\{theory ([^\}]+)\}/\1/g
' "${target}/$(basename $file)"
		done
done
