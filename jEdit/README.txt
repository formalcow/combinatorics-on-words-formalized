Macros are to be placed in the directory
$HOME/.isabelle/Isabelle20XX/jedit/macros

Special character insertion macros
===============================================================
Symbols/Cdot.bsh
Symbols/Empty.bsh
Symbols/Open.bsh
Symbols/Close.bsh

More special character insertion
===============================================================

Symbols/List_Power.bsh
Symbols/Left_Quotient.bsh
Symbols/Right_Quotient.bsh
Symbols/Primitive_Root.bsh
Symbols/Bold_Bar.bsh

Macros that are also able to wrap the selected text with special characters
===============================================================
Wrapping/Generated.bsh
Wrapping/Length.bsh
Wrapping/Quote.bsh
Wrapping/Paste_Literal_Facts.bsh

Attributes
===============================================================
Attributes/Symmetric.bsh
Attributes/Reversed.bsh
Attributes/of.bsh
Attributes/Toggle_OF.bsh

Code editing macros
===============================================================
Edit/Adjust_Selection.bsh
Edit/Smart_Copy.bsh
Edit/Toggle_Line_Comments.bsh
Edit/Flip_Expression.bsh
Edit/Using_Method_Facts.bsh
Edit/Switch_Using_Unfolding.bsh
Edit/Repair_Length.bsh

Configuration declaration
===============================================================
Declare/Declare_Show_Types.bsh
Declare/Declare_Show_Sorts.bsh
Declare/Declare_Simp_Trace.bsh
Declare/Declare_Simp_Trace_New.bsh

Isabelle commands
===============================================================
Commands/Try0.bsh
Commands/Sladgehammer.bsh
Commands/Find_Theorems.bsh

Blocks
===============================================================
Blocks/ML.bsh
Blocks/Notepad.bsh

ML antiquotation
===============================================================
ML/Context.bsh
ML/Thm.bsh
ML/Prop.bsh
ML/Term.bsh
ML/Termp.bsh

Windows/Panel/Tab control
===============================================================
View/Switch_Sledgehammer_Window.bsh
View/Apply_Sledgehammer.bsh
View/Show_Trace.bsh
