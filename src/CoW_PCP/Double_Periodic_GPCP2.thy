(*  Title:      BinaryPCP/Periodic_Binary_PCP.thy
    Author:     Štěpán Holub, Charles University
    Author:     Vesa Halava, University of Turku
*)

theory Double_Periodic_GPCP2
  imports
    CoW.Binary_Code_Morphisms

begin

declare [[coercion_enabled, coercion int]]

section \<open>Arithmetical results\<close>

text \<open>Our  @{verbatim "Algorithm_GPCP_both_periodic"} strongly relies on Combinatorics on Words but also
on solving a Diophantine equation or a pair of Diophantine equations on two variables. For our purposes
only non-negative solutions of such equations or pair of equations are useful, with additional
requirement that variables cannot both have zero value. We need to build a theory for such equations
and solutions.
In the algorithms for  solving the  double periodic GPCP, the Diopahantine equations
@{term "ax+by=c"}  may have  negative integer terms  @{term a}.  @{term b} and @{term c}
but in a solution @{term x} and @{term y} are  natural numbers with additional requirement
 $0 < x+y$. The last condition is due to rejecting the empty word as solution for the GPCP.\<close>

subsection \<open>Positive modular division\<close>

text \<open>In order to characterize solvable solutions of the form @{term "a*x + b*y = c"},
    we define the following modular division.\<close>

definition div_mod :: "int \<Rightarrow> int \<Rightarrow> int \<Rightarrow> int "
  where "div_mod c a b =  (
   let b' = nat(abs (b div (gcd a b)));
       a' = nat(a div (gcd a b) mod int b');
       c' = c div (gcd a b)
   in
  if b = 0 then c div a else (c' * fst (bezw a' b') mod int b')
  )"
text \<open>Intended meaning: @{term "d = div_mod c a b"} means $a * d = c \mod b$. Think of
$\frac c d \mod b$.\<close>

text\<open>The function is constructive:\<close>

value "div_mod 3 4 5"

lemma div_mod_zero: "div_mod c a 0 = c div a"
  unfolding div_mod_def Let_def by simp

lemma div_mod_zero': "div_mod 0 a b = 0"
  unfolding div_mod_def Let_def by simp

lemma div_mod_minus: "div_mod c a (- b) = div_mod c a b"
proof(cases "b = 0")
  assume "b \<noteq> 0"
  hence "gcd a (-b) = gcd a b" "gcd a b \<noteq> 0" and "-b \<noteq> 0"
    by simp_all
  have m: "\<bar>-(b::int) div gcd a b\<bar> = \<bar>b div gcd a b\<bar>"
    by (simp add: dvd_neg_div)
  show ?thesis
    unfolding div_mod_def Let_def if_not_P[OF \<open>b \<noteq> 0\<close>] if_not_P[OF \<open>-b \<noteq> 0\<close>]
      \<open>gcd a (-b) = gcd a b\<close> m..
qed simp

lemma pos_abs_nat: "0 < b \<Longrightarrow> nat \<bar>b\<bar> = nat b" and
  pos_int_nat_abs: "0 < b \<Longrightarrow> int (nat \<bar>b\<bar>) =  b" and
  pos_int_nat: "0 < b \<Longrightarrow> int (nat b) =  b" and
  int_plus: "int a + int b = int (a + b)" and
  int_mult: "int a * int b = int (a * b)"
  by simp_all

lemmas int_simps = int_plus int_mult int_int_eq

lemma pos_div_le: "0 < b \<Longrightarrow> b div gcd a b \<le> (b :: int)"
  using div_by_1 gcd_pos_int[of a b] int_div_less_self[of b "gcd a b"]
  by (cases "gcd a b = 1", force) linarith

lemma eq_inv_conv: "- a * x + - b * y = - c \<longleftrightarrow> a * x + b * y = (c :: int)"
  by force

lemma div_modI_aux0: assumes "0 < b" and "coprime a b"
  shows "a * div_mod c a b mod b = c mod b"
proof-
  note div_mod_def Let_def coprime_imp_gcd_eq_1[OF\<open>coprime a b\<close>]
    div_by_1
  have "int (nat b) = b"
    using \<open>0 < b\<close> by simp
  have "b \<noteq> 0"
    using \<open>0 < b\<close> by simp
  have "int (nat (a mod b)) = a mod b"
    using \<open>0 < b\<close>  by simp
  have "gcd (a mod b) b = gcd a b"
    using  gcd.commute gcd_red_int by metis

  let ?x = "fst (bezw (nat (a mod b)) (nat b))"
  let ?y = "snd (bezw (nat (a mod b)) (nat b))"

  have "1 = ?x * (a mod b) + ?y * b"
    using \<open>gcd (a mod b) b = gcd a b\<close> \<open>gcd a b = 1\<close>
      \<open>int (nat (a mod b)) = a mod b\<close> \<open>int (nat b) = b\<close>  bezw_aux gcd_int_int_eq by metis

  from arg_cong[OF this, of "\<lambda> x. c * x mod b", unfolded mult_1_right int_distrib mult.assoc[symmetric] mult.commute[of _ b]]
  have [symmetric]: "c mod b = c * ?x * (a mod b) mod b"
    unfolding mult.assoc mod_mult_self2.

  thus ?thesis
    using mod_mult_right_eq mult.commute abs_of_nat
    unfolding div_mod_def Let_def coprime_imp_gcd_eq_1[OF \<open>coprime a b\<close>] div_by_1
      if_not_P[OF \<open>b \<noteq> 0\<close>]
    by (metis \<open>int (nat b) = b\<close>)
qed

lemma div_modI_aux: assumes "b \<noteq> 0" and "coprime a b"
  shows "a * div_mod c a b mod b = c mod b"
proof (cases "0 < b")
  assume "0 < b"
  from div_modI_aux0[OF this \<open>coprime a b\<close>]
  show ?thesis.
next
  define d where "d = a * (c div 1 * fst (bezw (nat (a div 1 mod  \<bar>b div 1\<bar>)) (nat \<bar>b div 1\<bar>)) mod \<bar>b div 1\<bar>)"
  assume "\<not> 0 < b"
  hence "0 < - b" and "-b \<noteq> 0" and s: "\<bar>- b div 1\<bar> = \<bar>b div 1\<bar>"
    using \<open>b \<noteq> 0\<close> by simp_all
  have "coprime a (-b)"
    using \<open>coprime a b\<close> by simp
  from mod_minus_cong[OF div_modI_aux0[OF \<open>0 < -b\<close> this, of c]]
  show ?thesis
    unfolding div_mod_def Let_def if_not_P[OF \<open>-b \<noteq> 0\<close>] if_not_P[OF \<open>b \<noteq> 0\<close>] coprime_imp_gcd_eq_1[OF \<open>coprime a b\<close>]
      coprime_imp_gcd_eq_1[OF \<open>coprime a (-b)\<close>] s mod_minus_right d_def[symmetric] add.inverse_inverse neg_equal_iff_equal.
qed

lemma div_mod_reduce:
  assumes "b \<noteq> 0"
  shows "div_mod c a b = div_mod (c div gcd a b) (a div gcd a b) (b div gcd a b)"
proof-
  from div_gcd_coprime assms
  have *: "n div gcd (a div gcd a b) (b div gcd a b) = n" for n
    by (metis coprime_imp_gcd_eq_1 div_by_1)
  have "0 < gcd a b"
    using assms by simp
  from pos_imp_zdiv_pos_iff[OF this, of b] pos_imp_zdiv_nonneg_iff[OF this, of b]
  have "b div gcd a b \<noteq> 0"
    using assms  gcd_le2_int[of b] by force
  show ?thesis
    unfolding div_mod_def * Let_def if_not_P[OF \<open>b \<noteq> 0\<close>] if_not_P[OF \<open>b div gcd a b \<noteq> 0\<close>]..
qed

lemma div_modI: assumes "b \<noteq> 0" and "gcd a b dvd c"
  shows "a * div_mod c a b mod b = c mod b"
  using assms
proof -
  let ?a = "a div gcd a b" and
    ?b = "b div gcd a b" and
    ?c = "c div gcd a b"
  have gcd_b: "b div gcd a b * gcd a b = b" and gcd_a: "d * (a div gcd a b) * gcd a b = d * a" for d
    by force+
  have "coprime ?a ?b"
    by (rule div_gcd_coprime) (use \<open>b \<noteq> 0\<close> in fast)
  hence "gcd ?a ?b dvd ?c"
    using assms is_unit_gcd by blast
  have "?b \<noteq> 0"
    using \<open>b \<noteq> 0\<close> gcd_b by fastforce
  have   "?a * div_mod ?c ?a ?b  mod ?b = ?c mod ?b"
    using div_modI_aux[OF \<open>?b \<noteq> 0\<close> \<open>coprime ?a ?b\<close>] \<open>gcd (a div gcd a b) (b div gcd a b)
          dvd c div gcd a b\<close> by blast
  hence  "gcd a b * (div_mod c a b * ?a mod ?b) = gcd a b * (c div gcd a b mod (b div gcd a b))"
    unfolding div_mod_reduce[OF \<open>b \<noteq> 0\<close>, of c a, symmetric] mult.commute[of "div_mod c a b"]
    by argo
  from this[unfolded mult_mod_right]
  show ?thesis
    unfolding dvd_div_mult_self gcd_b gcd_a mult.commute[of "gcd a b"] gcd_a mult.commute[of a]
    using \<open>gcd a b dvd c\<close> by fastforce
qed

lemma div_mod_bound: assumes "0 < b" shows "div_mod c a b < b div gcd a b"
proof-
  have "b \<noteq> 0"
    using assms by simp
  have "0 < b div gcd a b"
    using \<open>0 < b\<close>  by (simp add: pos_imp_zdiv_pos_iff)
  hence *: "int (nat \<bar>b div gcd a b\<bar>) = b div gcd a b"
    by fastforce
  from pos_mod_bound[OF \<open>0 < b div gcd a b\<close>]
  show ?thesis
    unfolding div_mod_def Let_def * if_not_P[OF \<open>b \<noteq> 0\<close>]
    by blast
qed

lemma div_mod_min0: assumes "0 < b" and "a * x mod b = c mod b" and "0 \<le> x"
  shows "div_mod c a b \<le> x" "gcd a b dvd c"
proof-
  have "b \<noteq> 0"
    using \<open>0 < b\<close> by blast
  show "gcd a b dvd c"
    using dvd_mod_iff[of "gcd a b" a "a*x", OF gcd_dvd1] dvd_mod_iff[of "gcd a b" b "a*x", OF gcd_dvd2]
      dvd_mod_iff[of "gcd a b" b c, OF gcd_dvd2]
    unfolding  mod_mult_self1_is_0[of a x] \<open>a * x mod b = c mod b\<close>
    using  dvd_0_right[of "gcd a b"] by blast
  show "div_mod c a b \<le> x"
  proof -
    have "gcd a b \<noteq> 0"
      using assms(1) by auto
    have "0 < b div gcd a b"
      using \<open>0 < b\<close> dvd_div_eq_0_iff gcd_dvd2 gr0I
      by (simp add: pos_imp_zdiv_pos_iff)
    have "div_mod c a b < b div gcd a b"
      unfolding div_mod_def
      using  \<open>0 < b div gcd a b\<close> nat_less_iff
      by (metis assms(1) div_mod_bound div_mod_def)
    have "a * div_mod c a b mod b = a * x mod b"
      using div_modI[OF \<open>b \<noteq> 0\<close> \<open>gcd a b dvd c\<close>] \<open>a * x mod b = c mod b\<close>
      by presburger
    show ?thesis
    proof (rule ccontr)
      assume "\<not> div_mod c a b \<le> x"
      hence "x < div_mod c a b" by force
      have "b dvd a * (div_mod c a b - x)"
        using \<open>x < div_mod c a b\<close> \<open>a * div_mod c a b mod b = a * x mod b\<close>
        by (simp add: mod_eq_dvd_iff right_diff_distrib')
      hence "b div gcd a b dvd (div_mod c a b - x)"
        unfolding div_dvd_iff_mult[of "gcd a b" b "div_mod c a b - x", OF \<open>gcd a b \<noteq> 0\<close> gcd_dvd2]
        unfolding gcd_mult_distrib[of  "(div_mod c a b - x)" a b] mult.commute[of a]  by simp
      have "b div gcd a b \<le> div_mod c a b - x"
        by (simp add: \<open>b div gcd a b dvd div_mod c a b - x\<close> \<open>x < div_mod c a b\<close> zdvd_imp_le)
      thus False
        using div_mod_bound[OF \<open>0 < b\<close>, of c a] \<open>0 \<le> x\<close>
        by linarith
    qed
  qed
qed

lemma div_mod_pos: assumes "b \<noteq> 0" shows "0 \<le> div_mod c a b"
proof-
  have "0 < gcd a b"
    using assms by simp
  have "b div gcd a b \<noteq> 0"
    unfolding zdiv_eq_0_iff
    by (rule notI, elim disjE, use \<open>b \<noteq> 0\<close> in force)
      (use \<open>b \<noteq> 0\<close> gcd_le2_int linorder_not_less order_le_imp_less_or_eq in blast,
        use \<open>0 < gcd a b\<close> in linarith)
  hence "0 <  \<bar>b div gcd a b\<bar>"
    by linarith
  from pos_mod_sign[OF this]
  show "0 \<le> div_mod c a b"
    unfolding div_mod_def Let_def if_not_P[OF \<open>b \<noteq> 0\<close>]
    using abs_ge_zero int_nat_eq by metis
qed

\<comment> \<open>The modular division by a nonzero integer has intended properties: by @{thm div_mod_pos} it is
always non-negative, and the next theorems shows that if there is a non-negative divisor, then
@{term div_mod} yields the smallest one\<close>

theorem div_mod_correct: assumes "b \<noteq> 0" and "a * x mod b = c mod b" and "0 \<le> x"
  shows "a * div_mod c a b mod b = c mod b" and "div_mod c a b \<le> x"
proof-
  have "a * div_mod c a b mod b = c mod b \<and> div_mod c a b \<le> x"
  proof (cases "0 < b")
    assume "0 < b"
    from div_mod_min0[OF this \<open>a * x mod b = c mod b\<close> \<open>0 \<le> x\<close>] div_modI[OF \<open>b \<noteq> 0\<close>]
    show "a * div_mod c a b mod b = c mod b \<and> div_mod c a b \<le> x"
      by blast
  next
    assume "\<not> 0 < b"
    hence "0 < - b" and "-b \<noteq> 0"
      using \<open>b \<noteq> 0\<close> by simp_all
    have "a * x mod - b = c mod - b"
      using \<open>a * x mod b = c mod b\<close>
      by (metis zmod_zminus2_eq_if)
    from div_mod_min0[OF \<open>0 < -b\<close> this \<open>0 \<le> x\<close>] div_modI[OF \<open>b \<noteq> 0\<close>]
    show "a * div_mod c a b mod b = c mod b \<and> div_mod c a b \<le> x"
      unfolding div_mod_minus  by simp
  qed
  thus "a * div_mod c a b mod b = c mod b" and "div_mod c a b \<le> x"
    by blast+
qed

subsection \<open>Bezout with natural number coefficients\<close>

\<comment> \<open>We begin with Diophantine equations   @{term "a*x + b*y = c"} where coefficients   @{term "b"}
and   @{term "c"} are non-negative.\<close>

lemma bezout_eq_imp_conds: assumes "a*x + b*y = c" and "0 \<le> b" and "0 \<le> c" and "0 \<le> x" and "0 \<le> y"
  shows "(gcd a b) dvd c" and  "a * div_mod c a b \<le> c"
proof-
  show "(gcd a b) dvd c"
    using assms(1) by force
  show "a * div_mod c a b \<le> c"
  proof (cases "b = 0")
    assume "b \<noteq> 0"
    hence "0 < b" using \<open>0 \<le> b\<close> by simp
    have "a*x = c + b*(- y)"
      using assms(1)  by fastforce
    hence "a*x \<le> c"
      using assms(2) assms(5) by force
    have "(a*x) mod b = (c +b*(-y)) mod b"
      using \<open>a*x= c +  b*(-y)\<close>    by presburger
    hence "a*x mod b = c mod b "
      using mod_mult_self2[of "c" "b" "(-y)"] by presburger
    hence "div_mod c a b \<le> x"
      using \<open>b \<noteq> 0\<close> assms(2) assms(3) assms(4) mod_mod_trivial  div_mod_correct(2)[of "b" "a" "x" "c" ]
      by blast
    show  "a*div_mod c a b \<le> c"
    proof (cases)
      assume "a \<le> 0"
      hence "a*div_mod c a b \<le> 0"
        using div_mod_pos[OF \<open>b \<noteq> 0\<close>] mult_nonneg_nonpos2 by blast
      thus ?thesis
        using \<open>0 \<le> c\<close> by auto
    next
      assume "\<not> a \<le> 0"
      hence "0 \<le> a" by auto
      from mult_left_mono[OF \<open>div_mod c a b \<le> x\<close> this]
      show ?thesis
        using \<open>a * x \<le> c\<close> by fastforce
    qed
  next
    assume "b = 0"
    hence "c = a*x"
      using \<open>a*x + b*y = c\<close> by simp
    then show "a * div_mod c a b \<le> c"
    proof (cases "c = 0")
      assume "c = 0"
      show ?thesis
        unfolding div_mod_def Let_def  \<open>b = 0\<close> \<open>c = 0\<close> by simp
    next
      assume "c \<noteq> 0"
      hence "0 < a"
        using \<open>0 \<le> c\<close> \<open>0 \<le> x\<close> \<open>c = a*x\<close>
        by (simp add: zero_le_mult_iff)
      then show ?thesis
        unfolding div_mod_def Let_def \<open>b = 0\<close>
        by (simp add: \<open>c = a * x\<close>)
    qed
  qed
qed

\<comment> \<open>We define a predicate which is intended to characterize "positive" solvability
of the equation @{term "a*x + b*y = c"} in non-negative numbers for non-negative @{term b}
and @{term c}. By a positive solution we mean @{term "0 \<le> x"}, @{term "0 \<le> y"} and @{term "x + y \<noteq> 0"}.\<close>

definition bezout_solvable ::  "int \<Rightarrow> int \<Rightarrow> int \<Rightarrow> bool" where
  "bezout_solvable a b c = (if c = 0 then a*b \<le> 0
                                    else if b = 0 then 0 < a \<and> a dvd c
                                                   else ((gcd a b) dvd c \<and> a * div_mod c a b \<le> c)
                            )"

\<comment> \<open>The predicate is constructive\<close>

value "bezout_solvable 13 17 55"

definition solve_bezout :: "int \<Rightarrow> int \<Rightarrow> int \<Rightarrow> int * int"
  where  "solve_bezout a b c =
  (if c = 0 then  if b = 0 then (0,1)
                           else (b div gcd a b, - (a div gcd a b))
            else  if b = 0 then (c div a, 1)
                           else (div_mod c a b, (c - a*div_mod c a b) div b)
  )"

value "solve_bezout 3 5 19"
value "solve_bezout 3 0 1"

\<comment> \<open> We now prove that if there exists a positive solution to the equation @{term "a*x + b*y = c"}
   with @{term "0 \<le> c"} and @{term "0 \<le> c"}, then one such solution is given by the
@{term "solve_bezout"}. If @{term "(0,0)"} is the only non-negative solution,
then it is never given as a result.\<close>

lemma bezout_nat_solution_test: assumes "0 \<le> b" and "0 \<le> c" and "a*x + b*y = c" "0 \<le> x" "0 \<le> y" "x + y \<noteq> 0"
  shows "bezout_solvable a b c"
proof-
  have pos_or: "0 < x \<or> 0 < y"
    using assms(4-6) by auto
  have "gcd a b dvd c \<and> a * div_mod c a b \<le> c"
  proof
    show "gcd a b dvd c"
      using assms bezout_eq_imp_conds(1) less_imp_le by blast
    show "a * div_mod c a b \<le> c"
      using assms bezout_eq_imp_conds(2) less_imp_le by blast
  qed
  thus "bezout_solvable a b c"
  proof (cases "c = 0")
    assume "c = 0"
    have "a * b \<le> 0"
    proof (rule ccontr, unfold not_le)
      assume "0 < a * b"
      hence "0 < a"
        using \<open>0 \<le> b\<close> zero_less_mult_pos2 by force
      have "0 < b"
        using zero_less_mult_pos[OF \<open>0 < a * b\<close> \<open>0 < a\<close>].
      have "0 < a * x + b * y"
      proof (rule disjE[OF pos_or])
        assume "0 < x"
        thus "0 < a * x + b * y"
          using mult_nonneg_nonneg[OF \<open>0 \<le> b\<close> \<open>0 \<le> y\<close>] mult_pos_pos[OF \<open>0 < a\<close> \<open>0 < x\<close>] by auto
      next
        assume "0 < y"
        thus "0 < a * x + b * y"
          using mult_nonneg_nonneg[OF less_imp_le[OF \<open>0 < a\<close>] \<open>0 \<le> x\<close>] mult_pos_pos[OF \<open>0 < b\<close> \<open>0 < y\<close>] by auto
      qed
      thus False
        using \<open>a * x + b * y = c\<close>[unfolded \<open>c = 0\<close>] by simp
    qed
    thus ?thesis
      unfolding bezout_solvable_def if_P[OF \<open>c = 0\<close>].
  next
    assume "c \<noteq> 0"
    show ?thesis
    proof (cases "b = 0")
      assume "b \<noteq> 0"
      show ?thesis
        unfolding if_not_P[OF \<open>c \<noteq> 0\<close>] if_not_P[OF \<open>b \<noteq> 0\<close>] bezout_solvable_def by fact
    next
      assume "b = 0"
      obtain z where  "c = a * z" and "0 \<le> z"
        using assms[unfolded \<open>b = 0\<close>] by force
      show ?thesis
        unfolding bezout_solvable_def if_P[OF \<open>b = 0\<close>] if_not_P[OF \<open>c \<noteq> 0\<close>]
      proof
        show "a dvd c"
          using \<open>c = a * z\<close> by auto
        show "0 < a"
          using \<open>c = a * z\<close> \<open>0 \<le> c\<close> \<open>c \<noteq> 0\<close> \<open>0 \<le> z\<close>
          by (simp add: zero_le_mult_iff)
      qed
    qed
  qed
qed

lemma solve_bezout_nat_correct: assumes "(gcd a b) dvd c"
  shows "a * fst(solve_bezout a b c) + b * snd(solve_bezout a b c) = c" (is "a * ?x + b * ?y = c")
proof (cases "c = 0")
  assume "c = 0"
  show ?thesis
  proof (cases "b = 0")
    assume "b = 0"
    show ?thesis
      unfolding solve_bezout_def if_P[OF \<open>c = 0\<close>] if_P[OF \<open>b = 0\<close>] \<open>c = 0\<close> \<open>b = 0\<close> by simp_all
  next
    assume "b \<noteq> 0"
    hence xy: "?x = b div gcd a b" "?y = - (a div gcd a b)"
      unfolding solve_bezout_def if_P[OF \<open>c = 0\<close>] if_not_P[OF \<open>b \<noteq> 0\<close>] by simp_all
    show ?thesis
      unfolding xy unfolding \<open>c = 0\<close>
      by (simp add: div_mult_swap mult.commute)
  qed
next
  assume "c \<noteq> 0"
  show ?thesis
  proof (cases "b = 0")
    assume "b \<noteq> 0"
    hence xy: "?x = div_mod c a b" "?y = (c - a * div_mod c a b) div b"
      unfolding solve_bezout_def by (simp_all add: \<open>c \<noteq> 0\<close>)
    show  ?thesis
      unfolding xy
      unfolding dvd_mult_div_cancel[OF div_modI[OF \<open>b \<noteq> 0\<close> \<open>(gcd a b) dvd c\<close>, symmetric, unfolded mod_eq_dvd_iff]]
      by simp
  next
    assume "b = 0"
    then show ?thesis
      unfolding solve_bezout_def
      using \<open>gcd a b dvd c\<close> by auto
  qed
qed

lemma solve_bezout_nat_correct': assumes "(gcd a b) dvd c"
  shows "a*div_mod c a b + b*((c - a*div_mod c a b) div b) = c" (is "a * ?x + b * ?y = c")
proof (cases "c = 0")
  assume "c = 0"
  show ?thesis
    unfolding \<open>c = 0\<close> div_mod_zero' by simp
next
  assume "c \<noteq> 0"
  show ?thesis
  proof (cases "b = 0")
    assume "b \<noteq> 0"
    then show ?thesis
      using solve_bezout_nat_correct[OF \<open>(gcd a b) dvd c\<close>]
      unfolding solve_bezout_def using \<open>c \<noteq> 0\<close> by force
  next
    assume "b = 0"
    have "a * (c div a) = c"
      using \<open>(gcd a b) dvd c\<close>[unfolded \<open>b = 0\<close>] by simp
    then show ?thesis
      using solve_bezout_nat_correct[OF \<open>(gcd a b) dvd c\<close>]
      unfolding solve_bezout_def \<open>b = 0\<close> div_mod_zero by simp
  qed
qed

lemma solve_bezout_pos:
  assumes "a*(fst (solve_bezout a b c)) + b*(snd (solve_bezout a b c)) = c" and
    "0 \<le> fst (solve_bezout a b c)" "0 \<le> snd (solve_bezout a b c)"
  shows "fst (solve_bezout a b c) + snd (solve_bezout a b c) \<noteq> 0"
proof(rule notI)
  assume "fst (solve_bezout a b c) + snd (solve_bezout a b c) = 0"
  hence "fst (solve_bezout a b c) = 0" "snd (solve_bezout a b c) = 0"
    using assms(2-3) by fastforce+
  from assms(1)[unfolded this]
  have "c = 0"
    by auto
  show False
  proof (cases "b = 0")
    assume "b = 0"
    have "solve_bezout a b c = (0,1)"
      unfolding solve_bezout_def if_P[OF \<open>c = 0\<close>] if_P[OF \<open>b = 0\<close>]..
    thus False
      using \<open>snd (solve_bezout a b c) = 0\<close> by simp
  next
    assume "b \<noteq> 0"
    hence 1: "gcd a b \<noteq> 0"
      by force
    have 2: "\<not> (b \<le> 0 \<and> gcd a b < b)"
      using less_le_trans[of "gcd a b" b 0] gcd_pos_int[of a b] \<open>b \<noteq> 0\<close> by linarith
    have 3: "\<not> (0 \<le> b \<and> b < gcd a b)"
      using le_less_trans[of 0 b "gcd a b"] gcd_le2_int[of b a] \<open>b \<noteq> 0\<close> by linarith
    have val: "b div gcd a b = fst (solve_bezout a b c)"
      unfolding solve_bezout_def if_P[OF \<open>c = 0\<close>] if_not_P[OF \<open>b \<noteq> 0\<close>] fst_conv..
    thus False
      unfolding \<open>fst (solve_bezout a b c) = 0\<close> zdiv_eq_0_iff
      using 1 2 3 by blast
  qed
qed

\<comment> \<open>The following theorem together with @{thm bezout_nat_solution_test} shows the above announced properties of
    @{term bezout_solvable} and @{term solve_bezout}\<close>

theorem solve_bezout_solvable : assumes  "0 \<le> b" and "0 \<le> c" and "bezout_solvable a b c"
  shows "a*fst(solve_bezout a b c) + b*snd(solve_bezout a b c) = c" (is "a * ?x + b * ?y = c") and
    "0 \<le> fst(solve_bezout a b c)" (is "0 \<le> ?x") and
    "0 \<le> snd(solve_bezout a b c)" (is "0 \<le> ?y") and
    "fst(solve_bezout a b c) + snd(solve_bezout a b c) \<noteq> 0" (is "?x + ?y \<noteq> 0")
proof-
  note a = assms(3)[unfolded bezout_solvable_def]
  show "a * ?x + b * ?y = c" and "0 \<le> ?x" and "0 \<le> ?y" and "?x + ?y \<noteq> 0"
  proof (atomize(full), cases "c = 0")
    assume "c = 0"
    have "a * b \<le> 0"
      using a[unfolded if_P[OF \<open>c = 0\<close>]].
    show "(a * ?x + b * ?y = c \<and> 0 \<le> ?x) \<and> 0 \<le> ?y \<and> ?x + ?y \<noteq> 0"
    proof (cases "b = 0")
      assume "b = 0"
      show ?thesis
        unfolding solve_bezout_def if_P[OF \<open>c = 0\<close>] if_P[OF \<open>b = 0\<close>] fst_conv snd_conv
        unfolding \<open>c = 0\<close> \<open>b = 0\<close> by force+
    next
      assume "b \<noteq> 0"
      have "a * ?x + b * ?y = c"
        using  \<open>c = 0\<close> solve_bezout_nat_correct
        by simp
      have "a * b \<le> 0"
        using a[unfolded if_P[OF \<open>c = 0\<close>]].
      hence "a \<le> 0"
        using \<open>0 \<le> b\<close> \<open>b \<noteq> 0\<close>
        by (simp add: mult_le_0_iff)
      have "0 < ?x"
        unfolding solve_bezout_def if_P[OF \<open>c = 0\<close>] fst_conv
        using \<open>0 \<le> b\<close> \<open>b \<noteq> 0\<close>
        by (simp add: pos_imp_zdiv_pos_iff)
      have "0 \<le> ?y"
        using \<open>0 \<le> b\<close> \<open>b \<noteq> 0\<close> \<open>a \<le> 0\<close>
        unfolding solve_bezout_def if_P[OF \<open>c = 0\<close>] snd_conv
        by (simp add: div_nonpos_pos_le0)
      show ?thesis
        using \<open>a * ?x + b * ?y = c\<close> less_imp_le[OF \<open>0 < ?x\<close>]
          \<open>0 < ?x\<close> \<open>0 \<le> ?y\<close> by linarith
    qed
  next
    assume "c \<noteq> 0"
    show "(a * ?x + b * ?y = c \<and> 0 \<le> ?x) \<and> 0 \<le> ?y \<and> ?x + ?y \<noteq> 0"
    proof (cases "b = 0")
      assume "b \<noteq> 0"
      from a[unfolded if_not_P[OF \<open>c \<noteq> 0\<close>] if_not_P[OF \<open>b \<noteq> 0\<close>]]
      have "a * ?x + b * ?y = c" "a * div_mod c a b \<le> c"
        using  solve_bezout_nat_correct by blast+
      have "0 \<le> ?x"
        unfolding solve_bezout_def if_not_P[OF \<open>c \<noteq> 0\<close>] if_not_P[OF \<open>b \<noteq> 0\<close>] fst_conv
        using div_mod_pos[OF \<open>b \<noteq> 0\<close>].
      have "0 \<le> ?y"
        unfolding solve_bezout_def if_not_P[OF \<open>c \<noteq> 0\<close>] if_not_P[OF \<open>b \<noteq> 0\<close>] snd_conv
        using \<open>a * div_mod c a b \<le> c\<close> \<open>0 \<le> b\<close> unfolding div_int_pos_iff
        by simp
      have "?x + ?y \<noteq> 0"
        using add_nonneg_eq_0_iff[OF \<open>0 \<le> ?x\<close> \<open>0 \<le> ?y\<close>] \<open>a * ?x + b * ?y = c\<close> \<open>c \<noteq> 0\<close> by force
      show ?thesis
        using \<open>a * ?x + b * ?y = c\<close> \<open>0 \<le> ?x\<close> \<open>0 \<le> ?y\<close> \<open>?x + ?y \<noteq> 0\<close> by linarith
    next
      assume "b = 0"
      from a[unfolded if_not_P[OF \<open>c \<noteq> 0\<close>] if_P[OF \<open>b = 0\<close>]]
      have "0 < a" "a dvd c"
        by blast+
      have "0 \<le> c div a"
        using \<open>0 \<le> c\<close> pos_imp_zdiv_nonneg_iff[OF \<open>0 < a\<close>] by auto
      show ?thesis
        unfolding solve_bezout_def if_not_P[OF \<open>c \<noteq> 0\<close>] if_P[OF \<open>b = 0\<close>] snd_conv fst_conv
        unfolding \<open>b = 0\<close>
        using \<open>a dvd c\<close> \<open>0 \<le> c div a\<close> \<open>c \<noteq> 0\<close>
        by (simp add: dvd_div_eq_0_iff)
    qed
  qed
qed

subsection \<open>Extension for general integers\<close>

\<comment> \<open>We now turn our attention to obtaining positive solutions of Diophantine equations @{term "a*x+b*y=c"} with possibly negative
coefficients.\<close>

function solve_Dio_nat :: "int \<Rightarrow> int \<Rightarrow> int \<Rightarrow> int * int"
  where
    "0 \<le> c \<Longrightarrow> 0 \<le> b \<Longrightarrow>  solve_Dio_nat a b c = (solve_bezout a b c)"|
    "0 \<le> c \<Longrightarrow> \<not> 0 \<le> b \<Longrightarrow>  solve_Dio_nat a b c = prod.swap (solve_bezout b a c)" |
    "\<not> 0 \<le> c \<Longrightarrow> solve_Dio_nat a b c = solve_Dio_nat (-a) (-b) (-c)"
  by (meson prod_cases3) auto
termination by (relation "measure ((\<lambda> x. (if 0 \<le> x then 0 else 1)) \<circ> snd \<circ> snd)")
    simp_all

lemma[code] : "solve_Dio_nat a b c =
    (if 0 \<le> c then
         if 0 \<le> b then (solve_bezout a b c)
         else prod.swap (solve_bezout b a c)
     else solve_Dio_nat (-a) (-b) (-c))"
  by auto

value "solve_Dio_nat 1 1 0"
value "solve_Dio_nat 3 0 1"

definition Dio_nat_solvable :: "int \<Rightarrow> int \<Rightarrow> int \<Rightarrow> bool"
  where  " Dio_nat_solvable a b c =
         (a*fst(solve_Dio_nat a b c) + b*snd(solve_Dio_nat a b c) = c
          \<and> 0 \<le> fst(solve_Dio_nat a b c) \<and> 0 \<le> snd(solve_Dio_nat a b c))"

lemma solve_Dio_nat_pos_aux: assumes "Dio_nat_solvable a b c" and "0 \<le> c"
  shows "fst(solve_Dio_nat a b c) + snd(solve_Dio_nat a b c) \<noteq> 0"
proof (cases "0 \<le> b")
  assume "0 \<le> b"
  have "solve_Dio_nat a b c = solve_bezout a b c"
    using solve_Dio_nat.simps(1)[OF \<open>0 \<le> c\<close> \<open>0 \<le> b\<close>].
  thus ?thesis
    using Dio_nat_solvable_def assms(1) solve_bezout_pos by presburger
next
  assume "\<not> 0 \<le> b"
  have vals: "fst(solve_Dio_nat a b c) = snd (solve_bezout b a c)" "snd(solve_Dio_nat a b c) = fst (solve_bezout b a c)"
    using solve_Dio_nat.simps(2)[OF \<open>0 \<le> c\<close> \<open>\<not> 0 \<le> b\<close>] by auto
  thus ?thesis
    using Dio_nat_solvable_def assms(1) solve_bezout_pos[of b a c, folded vals] by presburger
qed

lemma solve_Dio_nat_pos: assumes "Dio_nat_solvable a b c"
  shows "fst(solve_Dio_nat a b c) + snd(solve_Dio_nat a b c) \<noteq> 0"
proof (cases "0 \<le> c")
  assume "\<not> 0 \<le> c"
  hence "0 \<le> (-c)"
    by simp
  note minus = solve_Dio_nat.simps(3)[of c a b, OF \<open>\<not> 0 \<le> c\<close>]
  have "Dio_nat_solvable (-a) (-b) (-c)"
    using assms(1) unfolding Dio_nat_solvable_def minus by linarith
  from solve_Dio_nat_pos_aux[OF this \<open>0 \<le> (-c)\<close>]
  show ?thesis
    unfolding minus.
qed (simp add: solve_Dio_nat_pos_aux[OF assms])

lemma nosol_aux : "(a::int) \<le> 0 \<Longrightarrow> b \<le> 0 \<Longrightarrow> 0 < c \<Longrightarrow> 0 \<le> x \<Longrightarrow> 0 \<le> y \<Longrightarrow> a*x + b*y \<noteq> c"
  using mult_le_0_iff[of a x] mult_le_0_iff[of b y] by simp

theorem solve_Dio_nat_correct:
  fixes a b c
  assumes "Dio_nat_solvable a b c"
  defines "x \<equiv> fst(solve_Dio_nat a b c)" and
    "y \<equiv> snd(solve_Dio_nat a b c)"
  shows "a*x + b*y = c" and "0 \<le> x" and "0 \<le> y" and "x + y \<noteq> 0"
proof-
  from solve_Dio_nat_pos[OF assms(1)]
  show "a*x + b*y = c" and "0 \<le> x" and "0 \<le> y" and "x + y \<noteq> 0"
    using assms(1) unfolding x_def y_def Dio_nat_solvable_def by blast+
qed

lemma Dio_nat_solvableI_aux: assumes "0 \<le> c" and
  "a*x + b*y = c" "0 \<le> x" "0 \<le> y" "x + y \<noteq> 0"
shows "Dio_nat_solvable a b c"
proof-
  let ?x = "fst(solve_Dio_nat a b c)"
  let ?y = "snd(solve_Dio_nat a b c)"
  let ?x' = "fst(solve_bezout a b c)"
  let ?y' = "snd(solve_bezout a b c)"
  have pos_or: "0 < x \<or> 0 < y"
    using assms by auto
  show "Dio_nat_solvable a b c"
    unfolding Dio_nat_solvable_def
  proof (cases)
    assume "0 \<le> b"
    hence "bezout_solvable a b c"
      using assms bezout_nat_solution_test by metis
    hence pred1:"a*?x' + b*?y' = c  \<and> 0 \<le> ?x' \<and> 0 \<le> ?y' \<and> ?x' + ?y' \<noteq> 0"
      using solve_bezout_solvable[OF \<open>0 \<le> b\<close> \<open>0 \<le> c\<close>, of a] by fast
    have "solve_Dio_nat a b c = solve_bezout a b c"
      using  \<open>0\<le> c\<close> \<open>0\<le> b\<close>   solve_Dio_nat.simps  by simp
    thus "a*?x + b*?y = c \<and> 0 \<le> ?x \<and> 0 \<le> ?y"
      by (simp add: pred1)
  next
    assume "\<not> 0 \<le> b"
    hence "b < 0"
      by simp
    hence "0 \<le> a"
      using  \<open>0 \<le> x\<close> \<open>0 \<le> y\<close> \<open>0 \<le> c\<close> \<open>a*x+b*y=c\<close> \<open>x + y \<noteq> 0\<close>
        assms mult_nonneg_nonpos2[OF ] mult_pos_neg2[OF _ ] by (smt (verit, ccfv_SIG))
    show "a*?x + b*?y = c \<and> 0 \<le> ?x \<and> 0 \<le> ?y"
    proof-
      have pred2:"a*snd(solve_bezout b a c) + b*fst( solve_bezout b a c) = c
             \<and> 0 \<le> snd( solve_bezout b a c) \<and> 0 \<le> fst(solve_bezout b a c)
             \<and> fst( solve_bezout b a c) + snd( solve_bezout b a c) \<noteq>  0"
        using  solve_bezout_solvable[OF \<open>0 \<le> a\<close> assms(1), of b]
          bezout_nat_solution_test[OF \<open>0 \<le> a\<close> \<open>0 \<le> c\<close> _  \<open>0 \<le> y\<close> \<open>0 \<le> x\<close>] \<open>x + y \<noteq> 0\<close> \<open>a*x + b*y = c\<close> by force
      have "solve_Dio_nat a b c = prod.swap (solve_bezout b a c)"
        using  \<open>0\<le> c\<close> \<open>\<not> 0\<le> b\<close>  solve_Dio_nat.simps by presburger
      thus ?thesis
        using pred2 by auto
    qed
  qed
qed

lemma Dio_nat_solvableI [intro] : assumes "a*x + b*y=c" "0 \<le> x" "0 \<le> y" "x + y \<noteq> 0"
  shows "Dio_nat_solvable a b c"
proof(cases)
  assume "0 \<le> c"
  thus ?thesis
    using  Dio_nat_solvableI_aux[OF _ assms] by force
next
  assume " \<not> 0 \<le> c"
  hence "0 \<le> - c"
    by fastforce
  have equ:"Dio_nat_solvable (-a) (-b) (-c)"
    using assms unfolding eq_inv_conv[symmetric, of a _ b _ c]
    using Dio_nat_solvableI_aux[OF \<open>0 \<le> -c\<close>, of "-a" _ "-b"]  by blast
  have inv: "solve_Dio_nat (-a)  (-b) (-c) = solve_Dio_nat a b c"
    using \<open>\<not> 0 \<le> c\<close> solve_Dio_nat.simps by presburger
  thus ?thesis
    using Dio_nat_solvable_def equ inv by simp
qed

\<comment> \<open>The theorem @{thm solve_Dio_nat_correct} and lemma @{thm Dio_nat_solvableI} show the desired properties of
     @{term "Dio_nat_solvable"} and @{term solve_Dio_nat}\<close>

lemma solution_size: assumes "a*x + b*y = c" and "0 < (b::int)" and "0 \<le> c"  and "0 \<le> x" "0 \<le> y"
  shows   "a * (div_mod c a b) + b * ((c - a * div_mod c a b) div b) = c" and
    "0 \<le> div_mod c a b" and
    "0 \<le> (c - a * div_mod c a b) div b" and
    "div_mod c a b + ((c - a * div_mod c a b) div b) \<le> c + b + \<bar>a\<bar>"
proof-
  have "0 \<le> b"
    using \<open>0 < b\<close> by force
  have "(gcd a b) dvd c"
    using assms bezout_eq_imp_conds(1) less_imp_le by blast+
  have "gcd a b dvd c" and "a * div_mod c a b \<le> c"
    using bezout_eq_imp_conds[OF assms(1) \<open>0 \<le> b\<close> assms(3-5)].

  from solve_bezout_nat_correct'[OF \<open>gcd a b dvd c\<close>]
  show sol:"a * div_mod c a b + b * ((c - a*div_mod c a b) div b) = c".
  have b1:"div_mod c a b < b"
    using div_mod_bound[OF \<open>0 < b\<close>] pos_div_le[OF \<open>0 < b\<close>] less_le_trans by blast
  show "0 \<le> div_mod c a b"
    using \<open>0 < b\<close> div_mod_pos by force
  from mult_right_mono[OF abs_ge_self[of "- a"] this]
  have "- a * div_mod c a b \<le> \<bar>a\<bar> * div_mod c a b"
    by auto
  hence "- a * div_mod c a b \<le> \<bar>a\<bar> * b"
    using mult_mono'[OF order.refl less_imp_le[OF b1] _ \<open>0 \<le> div_mod c a b\<close>, of "\<bar>a\<bar>"] by linarith
  have "(c - a * div_mod c a b) div b \<le> (c + \<bar>a\<bar> * div_mod c a b) div b"
    by (rule zdiv_mono1)
      (use \<open>- a * div_mod c a b \<le> \<bar>a\<bar> * div_mod c a b\<close> \<open>0 < b\<close> in fastforce)+
  have  "(c - a * div_mod c a b) div b \<le> (c + abs(a) * b) div b"
    by (rule zdiv_mono1) (use \<open>- a * div_mod c a b \<le> \<bar>a\<bar> * b\<close> \<open>0 < b\<close> in fastforce)+
  have "c \<le> c*b"
    using \<open>0 < b\<close> \<open>0 \<le> c\<close>
    by (simp add: mult_le_cancel_left1)
  show "0 \<le> (c - a * div_mod c a b) div b"
    by (simp add: \<open>a * div_mod c a b \<le> c\<close> \<open>0 < b\<close> pos_imp_zdiv_nonneg_iff)
  have "(c - a * div_mod c a b) div b \<le> (c*b + \<bar>a\<bar> * b) div b"
    by (rule zdiv_mono1)
      (use \<open>c \<le> c*b\<close> \<open>- a * div_mod c a b \<le> \<bar>a\<bar> * b\<close> \<open>0 < b\<close> in simp_all)
  hence "(c - a * div_mod c a b) div b \<le> c + \<bar>a\<bar>"
    using  \<open>0 < b\<close> by force
  thus "div_mod c a b +  (c - a * div_mod c a b) div b \<le> c + b + \<bar>a\<bar>"
    using b1 by linarith
qed

lemma solution_size_bzero: assumes  "b = (0 :: int)" and "0 \<le> c"  and "a*x + b*y = c" and "0 \<le> x" and "0 \<le> y"
  shows "a*(c div a) + b*0 = c"  and "c div a \<le> c"
proof-
  show "a*(c div a) + b*0 = c"
    using assms(1) assms(3) by fastforce
  hence "a*(c div a) = c"
    by simp
  show "c div a \<le> c"
  proof (cases "a = 0")
    assume "a \<noteq> 0"
    show ?thesis
    proof (cases "c = 0")
      assume "c \<noteq> 0"
      show ?thesis
        by (cases "c div a = 0", use \<open>0 \<le> c\<close> in argo)
          (use div_pos_pos_trivial[OF \<open>0 \<le> c\<close>, of "c div a"] \<open>a \<noteq> 0\<close>
            nonzero_mult_div_cancel_right[of "c div a" a, unfolded \<open>a*(c div a) = c\<close>]
            in linarith)
    qed simp
  qed (simp add: \<open>0 \<le> c\<close>)
qed

subsection \<open>Positive solution of a pair of Diophantine equations\<close>

\<comment> \<open>Note that in this case all coefficients in our equations are natural numbers but we use
integers in the definitions. Our main tool is the special case of the Cramer rule for integers. \<close>

lemma cramer_int': assumes eq1: "a*x + b*y = e" and eq2: "c*x + d*(y:: int) = f"
  shows "(a*d - b*c)*x = (e*d - f*b)" and
    "(a*d - b*c)*y = (f*a - e*c)"
proof-
  have aux: "a*x*d = a*d*x" "c*x*b = b*c*x" "b*y*d = b*d*y" "d*y*b = b*d*y"
    "c*x*a = a*c*x" "a*x*c = a*c*x" "b*y*c = b*c*y" "d*y*a = a*d*y"
    by auto
  show "(a*d - b*c)*x = e*d - f*b"
    unfolding left_diff_distrib'
      distrib_right[of "a*x" "b*y" d, unfolded eq1]
      distrib_right[of "c*x" "d*y" b, unfolded eq2]
      add_diff_cancel_left[of "b*d*y" "a*d*x" "b*c*x", symmetric] aux by force
  show "(a*d - b*c)*y = f*a - e*c"
    unfolding left_diff_distrib'
      distrib_right[of "a*x" "b*y" c, unfolded eq1]
      distrib_right[of "c*x" "d*y" a, unfolded eq2]
      add_diff_cancel_left[of "a*c*x" "a*d*y" "b*c*y", symmetric] aux by force
qed

lemma cramer_int: assumes eq1: "a*x + b*y = e" and eq2: "c*x + d*y = f" and "a*d \<noteq> b*(c :: int)"
  shows "x = (e*d - f*b) div (a*d - b*c)"
    "y = (f*a - e*c) div (a*d - b*c)"
proof-
  have "a*d - b*c \<noteq> 0"
    using \<open>a*d \<noteq> b*c\<close> by simp
  from cramer_int'[OF assms(1-2)] assms(3) nonzero_mult_div_cancel_left[OF this]
  show "x = (e*d - f*b) div (a*d - b*c)" "y = (f*a - e*c) div (a*d - b*c)"
    by metis+
qed

\<comment> \<open>The following lemmas deal with dependent equations (one of them nontrivial), to which the Cramer rule does not apply.\<close>

lemma two_eqs_dependent0:
  assumes
    eq1: "a*x +  b*y = e" and eq2: "c*x + d*y = f" and det: "(a::int)*d = b*c" and
    eq1':"a*x' + b*y'= e" and nontriv: "a \<noteq> 0"
  shows "c*x' + d*y' = f"
proof-
  have "f = c*a*x div a + c*b*y div a"
    unfolding mult.commute[of c] det[symmetric] mult.assoc[of a]
      nonzero_mult_div_cancel_left[OF nontriv] using eq2 by force
  also have "... = (c*a*x + c*b*y) div a"
    by (simp add: mult.commute mult.left_commute nontriv)
  also have "... = c*e div a"
    unfolding eq1[symmetric] mult.assoc[of c] add_mult_distrib2
    by (simp add: distrib_left)
  also have "... = (c*a*x' + c*b*y') div a"
    unfolding add_mult_distrib2[symmetric] mult.assoc[of c] eq1'
    by (metis distrib_left eq1')
  ultimately show ?thesis
    unfolding det[symmetric] mult.commute[of c] add_mult_distrib2[symmetric]
      add_mult_distrib2[symmetric]  mult.assoc[of a]
      nonzero_mult_div_cancel_left[OF nontriv]
    by (metis \<open>\<And>b. a * b div a = b\<close> distrib_left)
qed

lemma two_eqs_dependent:
  assumes
    eq1: "a*x +  b*y = e" and eq2: "c*x + d*y = f" and det: "(a::int)*d = b*c" and
    eq1':"a*x' + b*y'= e" and nontriv: "a \<noteq> 0 \<or> b \<noteq> 0"
  shows "c*x' + d*y' = f"
  using two_eqs_dependent0[OF assms(1-2)[unfolded add.commute[of "_*x"]] det[symmetric] assms(4)[unfolded add.commute[of "_*x'"]]]
    two_eqs_dependent0[OF assms(1-4)] \<open>a \<noteq> 0 \<or> b \<noteq> 0\<close> by force

\<comment> \<open>We now can give a constructive solution of a pair of Diophantine equations.
If the equations are independent, we take the only existing solution (possibly) existing. If they are dependent,
we have in fact only one equation, and can use the approach from the previous section. Note we have to be careful when the first
equation is trivial, since then the other one is not equivalent to it, if nontrivial.
.
\<close>

definition solve_two_eqs :: "int \<Rightarrow>int \<Rightarrow> int \<Rightarrow> int \<Rightarrow> int \<Rightarrow> int \<Rightarrow> (int * int)"
  where "solve_two_eqs a b c d e f =
      (if a*d = b*c then (if (a = 0 \<and> b = 0) then (solve_Dio_nat c d f) else (solve_Dio_nat a b e))
       else  ((e* d - f* b) div (a*d -  b*c),
       (f*a - e*c) div (a*d - b*c))
      )"

\<comment> \<open>Theorem showing the correctness of the function @{term solve_two_eqs}.  A positive solution
with for two given equations exists iff @{term solve_two_eqs} yield one.\<close>

theorem two_eqs_sol_iff:
  fixes a b c d e f
  defines "P \<equiv> \<lambda>(x,y). a * x + b * y = e \<and> c * x + d * y = f \<and> 0 \<le> x \<and> 0 \<le> y \<and> x + y \<noteq> 0"
  shows   "(\<exists> z. P z) \<longleftrightarrow> P (solve_two_eqs a b c d e f)"
proof (rule iffI)
  assume "\<exists> z. P z"
  then obtain x y where ex: "a * x + b * y = e" "c * x + d * y = f" "0 \<le> x" "0 \<le> y" "x + y \<noteq> 0"
    unfolding P_def by blast
  let ?x = "fst (solve_two_eqs a b c d e f)" and
    ?y = "snd (solve_two_eqs a b c d e f)"
  show "P (solve_two_eqs a b c d e f)"
  proof (cases)
    assume as1:"a \<noteq> 0 \<or> b \<noteq> 0"
    show "P (solve_two_eqs a b c d e f)"
    proof (cases)
      assume det: "a*d = b*c"
      note pos = solve_Dio_nat_pos[OF Dio_nat_solvableI, OF ex(1,3-)]
      have xy: "?x = fst(solve_Dio_nat a b e)" "?y = snd(solve_Dio_nat a b e)"
        unfolding solve_two_eqs_def using det as1 by auto
      from Dio_nat_solvableI[OF ex(1,3-)]
      have conds: "a*?x + b*?y = e" "0 \<le> ?x"  "0 \<le> ?y"
        unfolding  xy(1) xy(2) Dio_nat_solvable_def by blast+
      have "c*?x + d*?y = f"
        using two_eqs_dependent[OF ex(1-2) det \<open>a*?x + b*?y=e\<close> as1].
      thus ?thesis
        using conds pos unfolding P_def case_prod_beta xy by blast
    next
      assume det: "a*d \<noteq> b*c"
      from ex[unfolded cramer_int[OF ex(1-2) det]]
      show ?thesis
        unfolding solve_two_eqs_def solve_bezout_def
        unfolding P_def if_not_P[OF det] case_prod_unfold snd_conv fst_conv
        by force+
    qed
  next
    assume "\<not> (a \<noteq> 0 \<or> b \<noteq> 0)"
    hence "a = 0 \<and> b = 0"
      by simp
    hence "e = 0" and "a*d=b*c"
      using ex(1) by presburger+
    have xy2: "?x = fst(solve_Dio_nat c d f)" "?y = snd(solve_Dio_nat c d f)"
      using \<open>a*d=b*c\<close> \<open>a=0\<and> b=0\<close> solve_two_eqs_def by force+
    have "\<exists> x y. (c*x+d*y = f \<and> 0 \<le> x \<and> 0 \<le> y \<and> x + y \<noteq> 0)"
      using ex(2) \<open>0 \<le> x\<close> \<open>0 \<le> y\<close> \<open>x + y \<noteq> 0\<close> by blast
    hence "c*?x + d*?y= f \<and> 0 \<le> ?x \<and> 0 \<le> ?y \<and> ?x + ?y \<noteq> 0"
      using solve_Dio_nat_pos Dio_nat_solvableI
      unfolding  xy2(1) xy2(2) Dio_nat_solvable_def by blast
    hence "a*?x + b*?y = e \<and> 0 \<le> ?x \<and> 0 \<le> ?y \<and> ?x + ?y \<noteq> 0"
      using \<open>a=0\<and> b=0\<close> \<open>e=0\<close> by auto
    thus ?thesis
      using \<open>c*?x + d*?y = f \<and> 0 \<le> ?x \<and> 0 \<le> ?y \<and> ?x + ?y \<noteq> 0\<close> unfolding P_def case_prod_beta by blast
  qed
qed (force simp: P_def)

\<comment> \<open>Using the previous lemma, the following predicate characterizes the existence of a
positive solution with non-zero sum of given two equations.\<close>

definition two_eqs_pos_solvable ::  "int \<Rightarrow> int \<Rightarrow> int \<Rightarrow> int \<Rightarrow> int \<Rightarrow> int \<Rightarrow> bool" where
  "two_eqs_pos_solvable a b c d e f = (let (x, y) = solve_two_eqs a b c d e f
        in
          a*x + b*y = e \<and> c*x + d*y = f \<and> 0 \<le> x \<and> 0 \<le> y \<and> x + y \<noteq> 0)"

lemma two_eqs_pos_solvableI:
  assumes "a*x + b*y = e" and  "c*x + d*y = f" and  "0 \<le> x" and "0 \<le> y" and "x + y \<noteq> 0"
  shows  "two_eqs_pos_solvable a b c d e f"
proof-
  let ?x = "fst (solve_two_eqs a b c d e f)" and
    ?y = "snd (solve_two_eqs a b c d e f)"
  have "\<exists>z. a * fst z + b * snd z = e \<and> c * fst z + d * snd z = f \<and> 0 \<le> fst z \<and> 0 \<le> snd z \<and> fst z + snd z \<noteq> 0"
    using assms by auto
  from this
  have conds: "a *?x + b*?y = e" "c*?x + d*?y = f" "0 \<le> ?x" "0 \<le> ?y" "?x + ?y \<noteq> 0"
    using two_eqs_sol_iff[of a b e c d f, unfolded case_prod_beta] by blast+
  then show  "two_eqs_pos_solvable a b c d e f"
    unfolding two_eqs_pos_solvable_def[unfolded Let_def case_prod_beta]
    using conds by presburger
qed

lemma same_pow_eq: "u = p \<cdot> r\<^sup>@k \<cdot> s \<Longrightarrow> v = p \<cdot> r\<^sup>@m \<cdot> s \<Longrightarrow>  \<^bold>|u\<^bold>| = \<^bold>|v\<^bold>| \<Longrightarrow> u = v"
  by (clarify, cases "r = \<epsilon>", unfold lenmorph pow_len) simp_all

section \<open>Setup\<close>

subsection \<open>Presolution record\<close>

\<comment> \<open>A record  presolution contains information on a possible solution of the GPCP. The solutions are
searched systemically using the presolution updating. We call
@{term "pg \<cdot> gr\<^sup>@n \<cdot> sg = ph \<cdot> hr\<^sup>@m \<cdot> sh"} a presolution for an instance of the GPCP
(pg sg h g  pg sg) where @{term "gr"} and  @{term "hr"} are the primitive roots of @{term "g"}
and  @{term "h"}, resp.

In presolution  @{term "X"},
1) count is function giving values of two counters of powers of the primitive roots as we shall see:
@{term "count X True"} is the counter value for powers of the primitive root  @{term "gr"} and
@{term "count X False"} is the counter value for powers of the primitive root  @{term "hr"}.
2) over consist similarly on two overflows; @{term "over X True"} is the left overflow and
@{term "over X False"} is  the right overflow. For the left overflow the idea is that
                     a) @{term "pg \<cdot> gr\<^sup>@e1 \<cdot> word(over X True)  = ph \<cdot> hr\<^sup>@e2 "} if
                        @{term "row( over X True)"} is True, otherwise
                        @{term "pg \<cdot> gr\<^sup>@e1 = ph \<cdot> hr\<^sup>@e2  \<cdot> word(over X True)  "}
Similarly, for right overflow the idea is that
                     b) @{term "word(over X False) \<cdot> gr\<^sup>@j1 \<cdot> sg  =  hr\<^sup>@j2\<cdot> sh "} if
                        @{term "row( over X False)"} is False, otherwise
                       @{term " gr\<^sup>@j1 \<cdot> sg  = word(over X False) \<cdot>  hr\<^sup>@j2\<cdot> sh "}. Note the difference
                       in rows for left and right.
Here @{term "e1+j1= count X True"} and @{term "e2+j3= count X False"}
3) The update of the presolution is based on lengths only, so we need to give book that the
words in a) and b) are comparable. Predicate @{term "valid X"} is True if the words are comparable.

Note that the idea is that if the overflows  @{term "over X True = over X False"} and @{term "valid X"} is Ture,
then we have found a rootsolution. \<close>


record 'a overflow =
  word :: "'a list"
  row :: bool

record 'a presolution =
  count :: "bool \<Rightarrow> nat"
  over :: "bool \<Rightarrow> 'a overflow"
  valid :: bool

lemma presol_eq[intro]: "count X  = count Y \<Longrightarrow> over X = over Y \<Longrightarrow> valid X  = valid Y \<Longrightarrow> (X :: 'a presolution) = Y"
  by simp

section \<open>The main part\<close>

subsection \<open>Main locale\<close>

text \<open>Main locale. Note that we study that instances of the problem  where the morphisms are non-trivial,
that is, neither of them is totally erasing. The trivial cases can be decided simply using the length argument and a check,
see our article.  \<close>

locale binary_GPCP_both_periodic = two_binary_morphisms g h +
  gper: binary_periodic_morphism g +
  hper: binary_periodic_morphism h
  for g h :: "binA list \<Rightarrow> 'a list"
    + fixes pg ph sg sh :: "'a list"

begin

lemma swapGPCPBoth: "binary_GPCP_both_periodic h g" ..

lemma mirror_GPCP_both:  "binary_GPCP_both_periodic (rev_map g) (rev_map h)"
  unfolding binary_GPCP_both_periodic_def
proof
  interpret binary_morphism "(rev_map g)"
    using morphism.morph[OF g.rev_map_morph] by unfold_locales
  interpret binary_morphism "(rev_map h)"
    using morphism.morph[OF h.rev_map_morph] by unfold_locales
  show "two_binary_morphisms (rev_map g) (rev_map h)"..
  show "binary_periodic_morphism (rev_map g) \<and> binary_periodic_morphism (rev_map h)"
  proof (rule, unfold_locales)
    show "rev_map g u \<cdot> rev_map g v = rev_map g v \<cdot> rev_map g u" and "rev_map h u \<cdot> rev_map h v = rev_map h v \<cdot> rev_map h u" for u v
      unfolding comm_rev_iff gper.ims_comm hper.ims_comm rev_map_arg by simp_all
    show "\<not> (\<forall>c. rev_map g [c] = \<epsilon>)" and "\<not> (\<forall>c. rev_map h [c] = \<epsilon>)"
      by (simp_all add: gper.not_triv_emp rev_map_sing hper.not_triv_emp)
  qed
qed

definition nga where "nga \<equiv> gper.im_exp [bina]"
definition ngb where "ngb \<equiv> gper.im_exp [binb]"
definition nha where "nha \<equiv> hper.im_exp [bina]"
definition nhb where "nhb \<equiv> hper.im_exp [binb]"

notation gper.mroot ("gr")
notation hper.mroot ("hr")

lemma g_a: "g \<aa> = gr\<^sup>@nga" and g_b: "g \<bb> = gr\<^sup>@ngb" and h_a: "h \<aa> = hr\<^sup>@nha" and h_b: "h \<bb> = hr\<^sup>@nhb"
  using hper.per_morph_im_exp gper.per_morph_im_exp unfolding nga_def ngb_def nha_def nhb_def by blast+

lemma g_nonerasing: "nga \<noteq> 0 \<or> ngb \<noteq> 0"
proof (rule ccontr)
  assume "\<not> (nga \<noteq> 0 \<or> ngb \<noteq> 0)"
  hence "nga = 0" and "ngb = 0"
    by blast+
  have "g \<aa> = \<epsilon>" and "g \<bb> = \<epsilon>"
    unfolding g_a g_b \<open>nga = 0\<close> \<open>ngb = 0\<close> by simp_all
  hence "g [a] = \<epsilon>" for a
    by (induct a, simp_all)
  with gper.not_triv_emp
  show False
    by blast
qed

lemma h_nonerasing: "nha \<noteq> 0 \<or> nhb \<noteq> 0"
proof (rule ccontr)
  assume "\<not> (nha \<noteq> 0 \<or> nhb \<noteq> 0)"
  hence "nha = 0" and "nhb = 0"
    by blast+
  have "h \<aa> = \<epsilon>" and "h \<bb> = \<epsilon>"
    unfolding h_a h_b \<open>nha = 0\<close> \<open>nhb = 0\<close> by simp_all
  hence "h [a] = \<epsilon>" for a
    by (induct a, simp_all)
  with hper.not_triv_emp
  show False
    by blast
qed

lemma length_nonzero: "0 < \<^bold>|gr\<^bold>|" "0 < \<^bold>|hr\<^bold>|"
  using gper.per_morph_root_prim hper.per_morph_root_prim by fastforce+

subsection \<open>Long solution bound\<close>

\<comment> \<open>Solutions  @{term "w"} of GPCP are non-empty words such that  @{term "pg \<cdot> g w \<cdot> sg = ph \<cdot> h w \<cdot> sh"}.
For double periodic binary  GPCP, the soltuions can be divided in to long once and short once.
Existence of a long solution implies that the primitive roots  @{term "gr"} and  @{term "hr"}
are conjugates. There exist a simple bound for such solutions. We extend the study from the
images to the rootsolution of the form  @{term "pg \<cdot> gr\<^sup>@i \<cdot> sg = ph \<cdot> hr\<^sup>@j \<cdot> sh"}, and further to all
words  @{term "p \<cdot> r\<^sup>@i \<cdot> s = p' \<cdot> r'\<^sup>@j \<cdot> s'"}, where  @{term "i j"} are natural numbers. \<close>


definition  great_length
  where "great_length = (max \<^bold>|pg\<^bold>| \<^bold>|ph\<^bold>|) + (max \<^bold>|sg\<^bold>| \<^bold>|sh\<^bold>|) + \<^bold>|gr\<^bold>| + \<^bold>|hr\<^bold>|"

lemma long_gen_conjug_roots:
  assumes "primitive r" and  "primitive r'"  and eq: "p \<cdot> r\<^sup>@n \<cdot> s  = p' \<cdot> r'\<^sup>@m \<cdot> s'" and
    ineq: "(max \<^bold>|p\<^bold>| \<^bold>|p'\<^bold>|) + (max \<^bold>|s\<^bold>| \<^bold>|s'\<^bold>|) + \<^bold>|r\<^bold>| + \<^bold>|r'\<^bold>| \<le> \<^bold>|p \<cdot> r\<^sup>@n \<cdot> s\<^bold>|"
  shows "r \<sim> r'"
proof-
  \<comment> \<open>auxiliary length facts\<close>
  have aux: "(max \<^bold>|p\<^bold>| \<^bold>|p'\<^bold>|) + (\<^bold>|p \<cdot>  r\<^sup>@n \<cdot> s\<^bold>| - (max \<^bold>|s\<^bold>| \<^bold>|s'\<^bold>|) - (max \<^bold>|p\<^bold>| \<^bold>|p'\<^bold>|)) = \<^bold>|p \<cdot> r\<^sup>@n \<cdot> s\<^bold>| - (max \<^bold>|s\<^bold>| \<^bold>|s'\<^bold>|)"
    using ineq by (meson add_leD1 add_le_imp_le_diff ordered_cancel_comm_monoid_diff_class.add_diff_inverse)
  have aux1: "max \<^bold>|s\<^bold>| \<^bold>|s'\<^bold>| \<le> \<^bold>|p \<cdot>  r\<^sup>@n \<cdot> s\<^bold>|"
    using ineq by linarith
      \<comment> \<open>defining suitable factorization\<close>
  define pmax where "pmax = take (max \<^bold>|p\<^bold>| \<^bold>|p'\<^bold>|) (p \<cdot>  r\<^sup>@n \<cdot> s)"
  define middle where "middle = take (\<^bold>|p \<cdot> r\<^sup>@n \<cdot> s\<^bold>| - (max \<^bold>|s\<^bold>| \<^bold>|s'\<^bold>|) - (max \<^bold>|p\<^bold>| \<^bold>|p'\<^bold>|)) (drop (max \<^bold>|p\<^bold>| \<^bold>|p'\<^bold>|) (p \<cdot>  r\<^sup>@n \<cdot> s))"
  define smax where "smax = drop (\<^bold>|p \<cdot> r\<^sup>@n \<cdot> s\<^bold>| - (max \<^bold>|s\<^bold>| \<^bold>|s'\<^bold>|)) (p \<cdot> r\<^sup>@n \<cdot> s)"
  have eqmax: "pmax \<cdot> middle \<cdot> smax = p \<cdot> r\<^sup>@n \<cdot> s"
    using ineq unfolding pmax_def middle_def smax_def lassoc take_add[symmetric] unfolding rassoc aux
    using append_take_drop_id by blast
      \<comment> \<open>length properties of the factorization\<close>
  have "\<^bold>|pmax\<^bold>| = max \<^bold>|p\<^bold>| \<^bold>|p'\<^bold>|"
    unfolding pmax_def using ineq le_add1 le_trans take_len by metis
  have "\<^bold>|smax\<^bold>| = max \<^bold>|s\<^bold>| \<^bold>|s'\<^bold>|"
    unfolding smax_def length_drop using  diff_diff_cancel[OF aux1].
  have "\<^bold>|r\<^bold>| + \<^bold>|r'\<^bold>| \<le> \<^bold>|middle\<^bold>|"
    using ineq[folded eqmax] unfolding lenmorph  \<open>\<^bold>|pmax\<^bold>| = max \<^bold>|p\<^bold>| \<^bold>|p'\<^bold>|\<close> \<open>\<^bold>|smax\<^bold>| = max \<^bold>|s\<^bold>| \<^bold>|s'\<^bold>|\<close> by simp
      \<comment> \<open>middle is a factor of both images\<close>
  have "middle \<le>f r\<^sup>@n"
  proof-
    have "p \<le>p pmax"
      using eq_le_pref[OF \<open>pmax \<cdot> middle \<cdot> smax = p \<cdot>  r\<^sup>@n \<cdot> s\<close>[symmetric]]
      unfolding \<open>\<^bold>|pmax\<^bold>| = max \<^bold>|p\<^bold>| \<^bold>|p'\<^bold>|\<close>
      using max.cobounded1 by blast
    have "s \<le>s smax"
      using eq_le_pref[reversed, OF \<open>pmax \<cdot> middle \<cdot> smax = p \<cdot>  r\<^sup>@n \<cdot> s\<close>[symmetric, unfolded lassoc]]
      unfolding rassoc \<open>\<^bold>|smax\<^bold>| = max \<^bold>|s\<^bold>| \<^bold>|s'\<^bold>|\<close>
      using max.cobounded1 by blast
    obtain pg' sg' where "pmax = p \<cdot> pg'" and "smax = sg' \<cdot> s"
      using \<open>p \<le>p pmax\<close> \<open>s \<le>s smax\<close>
      unfolding prefix_def suffix_def fac_def by blast
    from \<open>pmax \<cdot> middle \<cdot> smax = p \<cdot> r\<^sup>@n \<cdot> s\<close>[unfolded this rassoc cancel, unfolded lassoc cancel_right]
    show ?thesis
      unfolding fac_def rassoc by metis
  qed

  have "middle \<le>f r'\<^sup>@m"
  proof-
    have "p' \<le>p pmax"
      using eq_le_pref[OF \<open>pmax \<cdot> middle \<cdot> smax = p \<cdot> r\<^sup>@n \<cdot> s\<close>[unfolded eq, symmetric]]
      unfolding \<open>\<^bold>|pmax\<^bold>| = max \<^bold>|p\<^bold>| \<^bold>|p'\<^bold>|\<close>
      using max.cobounded2 by blast
    have "s' \<le>s smax"
      using eq_le_pref[reversed, OF \<open>pmax \<cdot> middle \<cdot> smax = p \<cdot> r\<^sup>@n \<cdot> s\<close>[unfolded eq, symmetric, unfolded lassoc]]
      unfolding rassoc \<open>\<^bold>|smax\<^bold>| = max \<^bold>|s\<^bold>| \<^bold>|s'\<^bold>|\<close>
      using max.cobounded2 by blast
    obtain ph' sh' where "pmax = p' \<cdot> ph'" and "smax = sh' \<cdot> s'"
      using \<open>p' \<le>p pmax\<close> \<open>s' \<le>s smax\<close>
      unfolding prefix_def suffix_def fac_def by blast
    from \<open>pmax \<cdot> middle \<cdot> smax = p \<cdot> r\<^sup>@n \<cdot> s\<close>[unfolded eq this rassoc cancel, unfolded lassoc cancel_right]
    show ?thesis
      unfolding fac_def rassoc by metis
  qed
    \<comment> \<open>finishing the proof using @{thm fac_two_prim_conjug}\<close>
  obtain nn where "middle \<le>f r \<^sup>@ nn"
    using  \<open>middle \<le>f  r\<^sup>@n\<close>  by fastforce
  obtain mm where "middle \<le>f r' \<^sup>@ mm"
    using  \<open>middle \<le>f r'\<^sup>@m\<close>  by fastforce
  show "r \<sim> r' "
    using fac_two_prim_conjug[OF \<open>middle \<le>f r \<^sup>@ n\<close> \<open>middle \<le>f r' \<^sup>@ m\<close> assms(1) assms(2)]
      \<open>\<^bold>|r\<^bold>| + \<^bold>|r'\<^bold>| \<le> \<^bold>|middle\<^bold>|\<close> by linarith
qed

thm long_gen_conjug_roots

lemmas long_presol_conjug_roots = long_gen_conjug_roots[OF gper.per_morph_root_prim hper.per_morph_root_prim,
    of pg _ sg ph _ sh, folded great_length_def]

lemma long_solution_conjug_roots:
  assumes eq: "pg \<cdot> g w \<cdot> sg = ph \<cdot> h w \<cdot> sh" and
    ineq: "great_length \<le> \<^bold>|pg \<cdot> g w \<cdot> sg\<^bold>|"
  shows "gr \<sim> hr"
  using assms long_gen_conjug_roots  gper.per_morph_im_exp  hper.per_morph_im_exp gper.per_morph_root_prim
    hper.per_morph_root_prim unfolding great_length_def
  by metis

lemma long_gen_bound_aux:
  assumes "primitive r" and  "primitive r'" and
    eq: "p \<cdot>  r\<^sup>@n \<cdot> s = p' \<cdot> r'\<^sup>@m \<cdot> s'" and "\<^bold>|p\<^bold>| \<le> \<^bold>|p'\<^bold>|" and
    ineql: "(max \<^bold>|p\<^bold>| \<^bold>|p'\<^bold>|) + (max \<^bold>|s\<^bold>| \<^bold>|s'\<^bold>|) + \<^bold>|r\<^bold>| + \<^bold>|r'\<^bold>| \<le> \<^bold>|p \<cdot>  r\<^sup>@n \<cdot> s\<^bold>|"
  shows "\<exists> u v k. u \<cdot> v = r \<and> v \<cdot> u = r' \<and> v \<noteq> \<epsilon> \<and> p' = p \<cdot> (u \<cdot> v)\<^sup>@k \<cdot> u"
proof-
  have conj: "r \<sim> r'"
    using   long_gen_conjug_roots
    using assms(1-2) eq ineql by metis
  have "r \<noteq> \<epsilon>" "r'\<noteq> \<epsilon>"
    using assms(1-2) by fastforce+
  obtain u v where "u \<cdot> v = r" and "v \<cdot> u = r'" and "v \<noteq> \<epsilon>"
    using conjugE_nemp[OF \<open>r \<sim> r'\<close> \<open>r \<noteq> \<epsilon>\<close>].

  have "r\<^sup>@n \<noteq> \<epsilon>"
    using ineql emp_len[of "r\<^sup>@n"] nemp_len[OF \<open>r \<noteq> \<epsilon>\<close>] unfolding lenmorph by linarith
  hence "r'\<^sup>@m \<noteq> \<epsilon>"
    using ineql[unfolded eq] emp_len[of "r'\<^sup>@m"] nemp_len[OF \<open>r' \<noteq> \<epsilon>\<close>]  unfolding lenmorph by linarith
  have "p \<le>p p'"
    using eq \<open>\<^bold>|p\<^bold>| \<le> \<^bold>|p'\<^bold>|\<close> by force

  have "0 < m"
    using \<open>r'\<^sup>@m \<noteq> \<epsilon>\<close> by blast
  have  "0 < n"
    using \<open>r\<^sup>@n \<noteq> \<epsilon>\<close> by blast

  have "p' \<cdot> r' \<le>p p \<cdot> r\<^sup>@n"
  proof (rule ruler_le)
    show "p' \<cdot> r' \<le>p p' \<cdot> r'\<^sup>@m \<cdot> s'"
      unfolding pow_pos[OF \<open>0 < m\<close>] rassoc by force
    show "p \<cdot> r \<^sup>@ n \<le>p p' \<cdot> r'\<^sup>@m \<cdot> s'"
      using eq[symmetric]  by simp
    show "\<^bold>|p' \<cdot> r'\<^bold>| \<le> \<^bold>|p \<cdot> r \<^sup>@ n\<^bold>|"
      using ineql \<open>\<^bold>|p\<^bold>| \<le> \<^bold>|p'\<^bold>|\<close> max.order_iff by fastforce
  qed
  from this[folded \<open>u \<cdot> v = r\<close> \<open>v \<cdot> u = r'\<close>]
  have "p\<inverse>\<^sup>>p' \<cdot> v \<cdot> u \<le>p (u \<cdot> v)\<^sup>@n"
    using pref_cancel[of p "p\<inverse>\<^sup>>p' \<cdot> v \<cdot> u" "(u \<cdot> v) \<^sup>@ n"]
    unfolding lassoc lq_pref[OF \<open>p \<le>p p'\<close>] by blast
  from prim_conjug_pref[OF assms(1)[folded \<open>u \<cdot> v = r\<close>] this \<open>v \<noteq> \<epsilon>\<close>]
  obtain k where "(u \<cdot> v)\<^sup>@k \<cdot> u = p\<inverse>\<^sup>>p'".
  hence "p' = p \<cdot> (u \<cdot> v) \<^sup>@ k \<cdot> u"
    using lq_pref[OF \<open>p \<le>p p'\<close>] by force
  with \<open>u \<cdot> v = r\<close> \<open>v \<cdot> u = r'\<close> \<open>v \<noteq> \<epsilon>\<close>
  show ?thesis
    by blast
qed

lemmas long_gen_bound_aux_mirror = long_gen_bound_aux[reversed, unfolded rassoc]

lemma long_gen_bound_assymetric:
  assumes "primitive r" and  "primitive r'"  and eq: "p \<cdot> r\<^sup>@n  \<cdot> s = p' \<cdot> r'\<^sup>@m \<cdot> s'" and
    lenass: "\<^bold>|p\<^bold>|\<le>\<^bold>|p'\<^bold>|" and
    ineql: "(max \<^bold>|p\<^bold>| \<^bold>|p'\<^bold>|) + (max \<^bold>|s\<^bold>| \<^bold>|s'\<^bold>|) + \<^bold>|r\<^bold>| + \<^bold>|r'\<^bold>|\<le> \<^bold>|p \<cdot> r\<^sup>@n \<cdot> s\<^bold>| " and
    len_sol: "\<^bold>|p \<cdot> r\<^sup>@i \<cdot> s\<^bold>| = \<^bold>|p' \<cdot> r'\<^sup>@j \<cdot> s'\<^bold>|"
  shows "p \<cdot> r\<^sup>@i \<cdot> s = p' \<cdot> r'\<^sup>@j \<cdot> s' "
proof-
  have "r \<sim> r'"
    using  long_gen_conjug_roots assms(1-2) eq ineql
    by metis
  hence "\<^bold>|r\<^bold>|=\<^bold>|r'\<^bold>|"
    by (simp add: conjug_len)
  from  long_gen_bound_aux[OF assms(1-2) eq lenass ineql]
  obtain  u v k where "u \<cdot> v = r" and "v \<cdot> u = r'" and "v \<noteq> \<epsilon>" "p' = p \<cdot> (u \<cdot> v)\<^sup>@k \<cdot> u"
    by blast
  have "primitive (u \<cdot> v)"
    using  assms(1)  unfolding \<open>u \<cdot> v = r\<close>.
  show "p \<cdot> r\<^sup>@i \<cdot> s = p' \<cdot> r'\<^sup>@j \<cdot> s' "
  proof (cases "\<^bold>|s\<^bold>|" "\<^bold>|s'\<^bold>|" rule: le_cases)
    assume "\<^bold>|s\<^bold>| \<le> \<^bold>|s'\<^bold>|"
    from long_gen_bound_aux_mirror[OF assms(1) assms(2) eq \<open>\<^bold>|s\<^bold>|\<le>\<^bold>|s'\<^bold>|\<close> ]
    obtain  v' u' l  where "u' \<cdot> v' = r" and "v' \<cdot> u' =  r'" and "u' \<noteq> \<epsilon>" and  "s' = v' \<cdot> (u' \<cdot> v')\<^sup>@l \<cdot> s"
      using  ineql[unfolded add.commute[of "max \<^bold>|p\<^bold>| \<^bold>|p'\<^bold>|"]] by blast
    show  "p \<cdot> r\<^sup>@i \<cdot> s = p' \<cdot> r'\<^sup>@j \<cdot> s' "
    proof (cases "v' = \<epsilon>")
      assume "v' \<noteq> \<epsilon>"
      from \<open>u \<cdot> v = r\<close>[folded \<open>u' \<cdot> v' = r\<close>, symmetric] \<open>v \<cdot> u = r'\<close>[folded \<open>v' \<cdot> u' =  r'\<close>, symmetric]
      have  "u' = u" and "v'= v"
        using prim_conjug_unique[OF \<open>primitive (u \<cdot> v)\<close>[folded \<open>u' \<cdot> v' = u \<cdot> v\<close>] \<open>u' \<cdot> v' = u \<cdot> v\<close> \<open>v' \<cdot> u' = v \<cdot> u\<close>]
          comm_not_prim[OF \<open>v' \<noteq> \<epsilon>\<close>  \<open>u' \<noteq> \<epsilon>\<close>] prim_conjug[OF \<open>primitive (u \<cdot> v)\<close>[folded \<open>u' \<cdot> v' = u \<cdot> v\<close>] conjugI']
        by argo+
      show "p \<cdot> r\<^sup>@i \<cdot> s = p' \<cdot> r'\<^sup>@j \<cdot> s' "
      proof (rule same_pow_eq[OF _ _ len_sol])
        show "p' \<cdot> r'\<^sup>@j  \<cdot> s' = p \<cdot> (u \<cdot> v)\<^sup>@(k + j +  Suc l) \<cdot> s" and "p\<cdot> r\<^sup>@i  \<cdot> s = p\<cdot> (u \<cdot> v)\<^sup>@i\<cdot> s"
          unfolding   \<open>s' = v'\<cdot> (u' \<cdot> v')\<^sup>@l \<cdot> s\<close> \<open>p' = p \<cdot> (u \<cdot> v)\<^sup>@k \<cdot> u\<close>
            \<open>v \<cdot> u = r'\<close>[symmetric] \<open>u \<cdot> v = r\<close>[symmetric] \<open>u' = u\<close> \<open>v' = v\<close>
                  by (simp only: shifts, simp only: shifts_rev, simp_all only: pows_comm)
      qed
    next
      assume "v' = \<epsilon>"
      from  \<open>u \<cdot> v = r\<close>[folded \<open>u' \<cdot> v' = r\<close>] \<open>v \<cdot> u = r'\<close>[folded \<open>v' \<cdot> u' = r'\<close>] \<open>v \<noteq> \<epsilon>\<close>
      have "u = \<epsilon>" and "v = u'"
        unfolding \<open>v' = \<epsilon>\<close> exp_simps using \<open>primitive (u \<cdot> v)\<close>
        using comm_not_prim[OF \<open>v \<noteq> \<epsilon>\<close>, of u] by auto
      show "p \<cdot> r\<^sup>@i \<cdot> s = p' \<cdot> r'\<^sup>@j \<cdot> s' "
      proof (rule same_pow_eq[OF _ _ len_sol])
        show "p \<cdot> r\<^sup>@i \<cdot> s = p\<cdot> v\<^sup>@i\<cdot> s" and "p' \<cdot> r'\<^sup>@j \<cdot> s' = p \<cdot> v\<^sup>@(k + j +  l) \<cdot> s"
          unfolding   \<open>s' = v'\<cdot> (u' \<cdot> v')\<^sup>@l \<cdot> s\<close> \<open>p' = p \<cdot> (u \<cdot> v)\<^sup>@k \<cdot> u\<close>
            \<open>v \<cdot> u = r'\<close>[symmetric] \<open>u \<cdot> v = r\<close>[symmetric] \<open>u = \<epsilon>\<close> \<open>v' = \<epsilon>\<close> \<open>v = u'\<close>
                  by (simp_all  only: shifts, simp  only: shifts_rev)
      qed
    qed
  next
    assume "\<^bold>|s'\<^bold>| \<le> \<^bold>|s\<^bold>|"
    from long_gen_bound_aux_mirror[OF assms(2) assms(1) eq[symmetric] \<open>\<^bold>|s'\<^bold>| \<le> \<^bold>|s\<^bold>|\<close>] ineql[unfolded eq]
    obtain  v' u' l  where "u' \<cdot> v' = r'" and "v' \<cdot> u' =  r" and "u' \<noteq> \<epsilon>" and  "s = v'\<cdot> (u' \<cdot> v')\<^sup>@l \<cdot>s'"
      unfolding max.commute[of "\<^bold>|s\<^bold>|"] max.commute[of "\<^bold>|p\<^bold>|"] \<open>\<^bold>|r\<^bold>| = \<^bold>|r'\<^bold>|\<close>
        add.commute[of "max \<^bold>|p'\<^bold>| \<^bold>|p\<^bold>|"] by blast
    show "p \<cdot> r\<^sup>@i \<cdot> s = p' \<cdot> r'\<^sup>@j \<cdot> s' "
    proof (cases "v' = \<epsilon>")
      assume "v' \<noteq> \<epsilon>"
      from \<open>u \<cdot> v = r\<close>[folded \<open>v' \<cdot> u' = r\<close>, symmetric] \<open>v \<cdot> u = r'\<close>[folded \<open>u' \<cdot> v' =  r'\<close>, symmetric]
      have  "v' = u" and "u'= v"
        using comm_not_prim[OF \<open>v' \<noteq> \<epsilon>\<close>  \<open>u' \<noteq> \<epsilon>\<close>] \<open>primitive (u \<cdot> v)\<close>[folded \<open>v' \<cdot> u' = u \<cdot> v\<close>]
        using prim_conjug_unique[OF \<open>primitive (u \<cdot> v)\<close>[folded \<open>v' \<cdot> u' = u \<cdot> v\<close>] \<open>v' \<cdot> u' = u \<cdot> v\<close> \<open>u' \<cdot> v' = v \<cdot> u\<close>]
        by blast+
      show "p \<cdot> r\<^sup>@i \<cdot> s = p' \<cdot> r'\<^sup>@j \<cdot> s' "
      proof (rule same_pow_eq[OF _ _ len_sol])
        show "p' \<cdot> r'\<^sup>@j\<cdot> s' = p \<cdot> (u \<cdot> v)\<^sup>@(k + j)\<cdot> u \<cdot> s'" and "p\<cdot> r\<^sup>@i \<cdot> s = p\<cdot> (u \<cdot> v)\<^sup>@(i + l)\<cdot> u \<cdot> s'"
          unfolding  \<open>s = v'\<cdot> (u' \<cdot> v')\<^sup>@l \<cdot> s'\<close> \<open>p' = p \<cdot> (u \<cdot> v)\<^sup>@k \<cdot> u\<close> \<open>v \<cdot> u = r'\<close>[symmetric]
       \<open>u \<cdot> v = r\<close>[symmetric] \<open>u' = v\<close> \<open>v' = u\<close>
                  by (simp_all only: shifts, simp only: shifts_rev)
        qed
    next
      assume "v'= \<epsilon>"
      from  \<open>u \<cdot> v = r\<close>[folded \<open>v' \<cdot> u' = r\<close>] \<open>v \<cdot> u = r'\<close>[folded \<open>u' \<cdot> v' = r'\<close>]
        \<open>primitive (u \<cdot> v)\<close>
      have "u = \<epsilon>" and "v = u'"
        unfolding \<open>v' = \<epsilon>\<close> exp_simps
        using comm_not_prim[OF _ \<open>v \<noteq> \<epsilon>\<close>, of u] by auto
      show "p \<cdot> r\<^sup>@i \<cdot> s = p' \<cdot> r'\<^sup>@j \<cdot> s' "
      proof (rule same_pow_eq[OF _ _ len_sol])
        show "p \<cdot> r\<^sup>@i \<cdot> s = p\<cdot> v\<^sup>@(i +l)\<cdot> s'" and "p' \<cdot> r'\<^sup>@j \<cdot> s' = p \<cdot> v\<^sup>@(k + j) \<cdot> s'"
          unfolding  \<open>s = v'\<cdot> (u' \<cdot> v')\<^sup>@l \<cdot> s'\<close> \<open>p' = p \<cdot> (u \<cdot> v)\<^sup>@k \<cdot> u\<close>
            \<open>v \<cdot> u = r'\<close>[symmetric] \<open>u \<cdot> v = r\<close>[symmetric] \<open>u = \<epsilon>\<close> \<open>v' = \<epsilon>\<close> \<open>v = u'\<close>
                   by (simp_all only: shifts) (simp only: shifts_rev)
      qed
    qed
  qed
qed

lemma long_gen_bound:
  assumes "primitive r" and  "primitive r'"  and eq: "p \<cdot> r\<^sup>@n  \<cdot> s = p' \<cdot> r'\<^sup>@m \<cdot> s'" and
    ineql: "(max \<^bold>|p\<^bold>| \<^bold>|p'\<^bold>|) + (max \<^bold>|s\<^bold>| \<^bold>|s'\<^bold>|) + \<^bold>|r\<^bold>| + \<^bold>|r'\<^bold>|\<le> \<^bold>|p \<cdot> r\<^sup>@n \<cdot> s\<^bold>| " and
    len_sol: "\<^bold>|p \<cdot> r\<^sup>@i \<cdot> s\<^bold>| = \<^bold>|p' \<cdot> r'\<^sup>@j \<cdot> s'\<^bold>|"
  shows "p \<cdot> r\<^sup>@i \<cdot> s = p' \<cdot> r'\<^sup>@j \<cdot> s' "
proof (cases "\<^bold>|p\<^bold>|" "\<^bold>|p'\<^bold>|" rule: le_cases)
  assume "\<^bold>|p\<^bold>|\<le>\<^bold>|p'\<^bold>|"
  thus ?thesis
    using long_gen_bound_assymetric[OF assms(1) assms(2) eq \<open>\<^bold>|p\<^bold>|\<le>\<^bold>|p'\<^bold>|\<close> ineql  len_sol] by blast
next
  assume "\<^bold>|p'\<^bold>|\<le>\<^bold>|p\<^bold>|"
  have ineql2:"(max \<^bold>|p'\<^bold>| \<^bold>|p\<^bold>|) + (max \<^bold>|s'\<^bold>| \<^bold>|s\<^bold>|) + \<^bold>|r'\<^bold>| + \<^bold>|r\<^bold>|\<le> \<^bold>|p' \<cdot> r'\<^sup>@m \<cdot> s'\<^bold>| "
    using eq ineql unfolding max.commute[of "\<^bold>|s\<^bold>|"] max.commute[of "\<^bold>|p\<^bold>|"] add.commute[of "max \<^bold>|p'\<^bold>| \<^bold>|p\<^bold>|"]
    by presburger
  show ?thesis
    using long_gen_bound_assymetric[OF assms(2) assms(1) eq[symmetric] \<open>\<^bold>|p'\<^bold>|\<le>\<^bold>|p\<^bold>|\<close> ineql2  len_sol[symmetric] ]
    by argo
qed

\<comment> \<open>Next lemma shows that long rootsolution implies that it is enough to test the length condition
for testing a solution.\<close>

lemma long_bound_bound:
  assumes eq: "pg \<cdot> gr\<^sup>@n \<cdot> sg = ph \<cdot> hr\<^sup>@m \<cdot> sh" and
    ineql: "great_length \<le> \<^bold>|pg \<cdot> gr\<^sup>@n \<cdot> sg\<^bold>| " and
    len_sol: "\<^bold>|pg \<cdot>gr\<^sup>@i \<cdot> sg\<^bold>| = \<^bold>|ph \<cdot> hr\<^sup>@j \<cdot> sh\<^bold>|"
  shows "pg \<cdot> gr\<^sup>@i \<cdot> sg = ph \<cdot> hr\<^sup>@j \<cdot> sh"
proof-
  have "primitive gr" and  "primitive hr"
    using  gper.per_morph_root_prim hper.per_morph_root_prim
    by simp+
  show ?thesis
    using long_gen_bound[OF \<open>primitive gr\<close> \<open>primitive hr\<close> eq ineql[unfolded great_length_def] len_sol].
qed

\<comment> \<open>Next theorem shows that existence of a long solution implies that it is enough to test the length condition
for testing a solution.\<close>

theorem long_solution_bound:
  assumes  "pg \<cdot> g w \<cdot> sg = ph \<cdot> h w \<cdot> sh" and
    "great_length \<le> \<^bold>|pg \<cdot> g w \<cdot> sg\<^bold>| " and
    "\<^bold>|pg \<cdot> g v \<cdot> sg\<^bold>| = \<^bold>|ph \<cdot> h v \<cdot> sh\<^bold>|"
  shows "pg \<cdot> g v \<cdot> sg = ph \<cdot> h v \<cdot> sh"
  using assms long_bound_bound  gper.per_morph_rootI  gper.per_morph_im_exp hper.per_morph_im_exp
  by metis

subsection \<open>Setting up for the algorithm\<close>

\<comment> \<open>We define an algorithm deciding the both periodic instances. The basic idea is to find a rootsolution
that is, natural numbers @{term "i j"}
such that  @{term "pg \<cdot> gr\<^sup>@i \<cdot> sg = ph \<cdot> hr\<^sup>@j \<cdot> sh"} and there exists  @{term "w"} such that
@{term "g w = gr\<^sup>@i \<and> h w =  hr\<^sup>@j"}. The possible  numbers  @{term "i"} and @{term "j"} are stored recursively in
presolution counters. We begin with definition for usefull notations. \<close>

definition ff where "ff a = (if a then g else h)"
definition pp where "pp a = (if a then pg else ph)"
definition ss where "ss a = (if a then sg else sh)"
definition rr where "rr a = (if a then gr else hr)"

definition int_count:: "'a presolution \<Rightarrow> bool \<Rightarrow> int"
  where "int_count X a = int (count X a)"

lemmas  gpcp_defs = ff_def pp_def ss_def rr_def and
  gpcp_simps'= ff_def pp_def ss_def rr_def if_True if_False not_True_eq_False not_False_eq_True max.commute[of "\<^bold>|sh\<^bold>|"] max.commute[of "\<^bold>|ph\<^bold>|"]
  add.commute[of "max _\<^bold>|sh\<^bold>|"]  semiring_normalization_rules(23)[of _ "\<^bold>|hr\<^bold>|"] select_convs overflow.select_convs
  and
  gpcp_simps = ff_def pp_def ss_def rr_def if_True if_False not_True_eq_False not_False_eq_True g_a h_a g_b h_b
  nga_def[symmetric] nha_def[symmetric] ngb_def[symmetric] nhb_def[symmetric] Let_def case_prod_beta

lemma great_length_sym: "great_length = (max \<^bold>|pp a\<^bold>| \<^bold>|pp (\<not> a)\<^bold>|) + (max \<^bold>|ss b\<^bold>| \<^bold>|ss (\<not> b)\<^bold>|) + \<^bold>|rr c\<^bold>| + \<^bold>|rr (\<not> c)\<^bold>|"
  by (unfold gpcp_simps' great_length_def, induct a, induct b, (induct c, unfold gpcp_simps', presburger+), induct b, (induct c, unfold gpcp_simps', presburger+))

lemma mroot_prim:  "primitive (rr a)"
  unfolding rr_def using  gper.per_morph_root_prim  hper.per_morph_root_prim by simp

lemma mroot_nemp:  "rr a \<noteq> \<epsilon>"
  using prim_nemp[OF mroot_prim].

lemma mroot_len:  "0 < \<^bold>|rr a\<^bold>|"
  using mroot_nemp by blast

\<comment> \<open>Next lemma states bound for the short solutions. \<close>

lemma short_solution_power_bound:
  assumes eq: "pg \<cdot> g w \<cdot> sg = ph \<cdot> h w \<cdot> sh" and
    ineql: " \<^bold>|pg \<cdot> g w \<cdot> sg\<^bold>| < great_length" and
    bpow: "ff a w = rr a\<^sup>@exp"
  shows "exp*\<^bold>|rr a\<^bold>| < ((max \<^bold>|pg\<^bold>| \<^bold>|ph\<^bold>|) + (max \<^bold>|sg\<^bold>| \<^bold>|sh\<^bold>|)  -\<^bold>|pp a\<^bold>| - \<^bold>|ss a\<^bold>| + \<^bold>|gr\<^bold>| + \<^bold>|hr\<^bold>| ) "
proof-
  have "\<^bold>|pp a \<cdot> ff a w \<cdot> ss a\<^bold>| <  (max \<^bold>|pg\<^bold>| \<^bold>|ph\<^bold>|) + (max \<^bold>|sg\<^bold>| \<^bold>|sh\<^bold>|) + \<^bold>|gr\<^bold>| + \<^bold>|hr\<^bold>|"
    using eq ff_def pp_def ss_def ineql unfolding great_length_def  by presburger
  hence "\<^bold>|pp a\<^bold>| +  \<^bold>|ff a w\<^bold>|  + \<^bold>|ss a\<^bold>| <  ((max \<^bold>|pg\<^bold>| \<^bold>|ph\<^bold>|) + (max \<^bold>|sg\<^bold>| \<^bold>|sh\<^bold>|) + \<^bold>|gr\<^bold>| + \<^bold>|hr\<^bold>| )"
    unfolding lenmorph ab_semigroup_add_class.add_ac(1).
  hence "\<^bold>|ff a  w\<^bold>|  <  ((max \<^bold>|pg\<^bold>| \<^bold>|ph\<^bold>|) + (max \<^bold>|sg\<^bold>| \<^bold>|sh\<^bold>|) + \<^bold>|gr\<^bold>| + \<^bold>|hr\<^bold>| ) - \<^bold>|pp a\<^bold>| -  \<^bold>|ss a\<^bold>|"
    unfolding less_diff_conv by presburger
  thus ?thesis
    unfolding  bpow pow_len by auto
qed

\<comment> \<open>The presolution  @{term "IniPresol pg ph sg sh "}  is the one our algorithm starts with.
but we shall use the following generalized version of the @{term "IniPresol"} in our proofs
fro the algorithm correctness. \<close>


definition IniPresol:: "'a list \<Rightarrow> 'a list \<Rightarrow> 'a list \<Rightarrow> 'a list \<Rightarrow> 'a presolution" where
  "IniPresol p1 p2 s1 s2 =
\<lparr>
count = \<lambda> x. 0,
over = \<lambda> x.
   (if x then \<lparr> word = (if \<^bold>|p2\<^bold>| \<le> \<^bold>|p1\<^bold>| then p2\<inverse>\<^sup>>p1 else p1\<inverse>\<^sup>>p2), row = \<^bold>|p2\<^bold>| \<le> \<^bold>|p1\<^bold>| \<rparr>
         else \<lparr> word = (if \<^bold>|s2\<^bold>| \<le> \<^bold>|s1\<^bold>| then s1\<^sup><\<inverse>s2 else s2\<^sup><\<inverse>s1), row = \<^bold>|s1\<^bold>| \<le> \<^bold>|s2\<^bold>| \<rparr> ),
valid = p1 \<bowtie> p2 \<and> s1 \<bowtie>\<^sub>s s2
\<rparr>"

lemma ini_count: "count (IniPresol a b c d) = (\<lambda> x.0)"
  unfolding IniPresol_def select_convs..

lemma ini_remove_count: "(IniPresol a b c d)\<lparr>count := (\<lambda> x. (if x then 0 else 0)) \<rparr> = IniPresol a b c d"
  "(IniPresol a b c d)\<lparr>count := (\<lambda> _. 0) \<rparr> = IniPresol a b c d"
  using ini_count by auto

lemma ini_valid: assumes  "p1 \<cdot> u \<cdot> s1 = p2 \<cdot> v \<cdot> s2" shows "valid (IniPresol p1 p2 s1 s2)"
  unfolding IniPresol_def select_convs
  using eqd_comp[OF assms] eqd_comp[reversed, OF assms[unfolded lassoc]] by blast

lemma empty_sol: assumes "valid (IniPresol p1 p2 s1 s2)"
  "over (IniPresol p1 p2 s1 s2) True = over (IniPresol p1 p2 s1 s2) False"
shows "p1 \<cdot> s1 = p2 \<cdot> s2"
proof (cases "\<^bold>|p2\<^bold>| \<le> \<^bold>|p1\<^bold>|")
  assume "\<^bold>|p2\<^bold>| \<le> \<^bold>|p1\<^bold>|"
  with assms[unfolded IniPresol_def gpcp_simps' if_P[OF this]]
  have "p1 \<bowtie> p2" and "s1 \<bowtie>\<^sub>s s2" and "\<^bold>|s1\<^bold>| \<le> \<^bold>|s2\<^bold>|" and
    "p2\<inverse>\<^sup>>p1 = (if \<^bold>|s2\<^bold>| \<le> \<^bold>|s1\<^bold>| then s1\<^sup><\<inverse>s2  else s2\<^sup><\<inverse>s1)"
    by blast+
  thus ?thesis
    using \<open>\<^bold>|p2\<^bold>| \<le> \<^bold>|p1\<^bold>|\<close> by (cases "\<^bold>|s1\<^bold>| = \<^bold>|s2\<^bold>|")
      (use comp_shorter eq_len_iff eqd_suf long_suf lq_pref not_pref_comp_sym suf_comp_or in metis,
        use append_assoc comp_shorter lq_pref nle_le not_pref_comp_sym rq_suf suf_comp_or suffix_length_le in metis)
next
  assume "\<not> \<^bold>|p2\<^bold>| \<le> \<^bold>|p1\<^bold>|"
  with assms[unfolded IniPresol_def gpcp_simps' if_not_P[OF this]]
  have "p1 \<bowtie> p2" and "s1 \<bowtie>\<^sub>s s2" and "\<not> \<^bold>|s1\<^bold>| \<le> \<^bold>|s2\<^bold>|" and
    "p1\<inverse>\<^sup>>p2 = (if \<^bold>|s2\<^bold>| \<le> \<^bold>|s1\<^bold>| then s1\<^sup><\<inverse>s2  else s2\<^sup><\<inverse>s1)"
    by blast+
  thus ?thesis
    using \<open>\<not> \<^bold>|p2\<^bold>| \<le> \<^bold>|p1\<^bold>|\<close>
    by (cases "\<^bold>|s1\<^bold>| = \<^bold>|s2\<^bold>|", force)
      (metis append_assoc comp_shorter linorder_not_less lq_pref order_less_imp_le rq_suf suf_comp_or suffix_length_le)
qed

subsection \<open>@{term "NextCand"} function\<close>

\<comment> \<open>@{term "NextCand_indicator"}
detects which of the two overflow words  (left or right) is longer and is  thus "shortened".
\<close>

definition NextCand_indicator ::  "(bool \<Rightarrow> 'a overflow)  \<Rightarrow> bool"
  where
    "NextCand_indicator ovs \<equiv> \<^bold>|word (ovs False)\<^bold>| \<le> \<^bold>|word (ovs True)\<^bold>|
"

\<comment> \<open>Next we define how the new overflow is computed.
Note that bool predicate is used in the valid precidate for the updated presolutuion, and it is
a test of comparability of the words used in the overflow update as the  updated
is length based only.  \<close>

definition left_overflow_update :: "'a overflow \<Rightarrow> ('a overflow \<times> bool)"
  \<comment> \<open>left overflow is shortened from the left\<close>
  where
    "left_overflow_update p  =
 \<comment> \<open>the shorter row is always extended\<close>
 (let new = (
  if \<^bold>|rr (\<not> (row p))\<^bold>| = \<^bold>|word p\<^bold>|
  then \<lparr> word = \<epsilon>, row = True \<rparr>  \<comment> \<open>empty overflow has row True\<close>
  else
  if \<^bold>|rr (\<not> (row p))\<^bold>| < \<^bold>|word p\<^bold>|
  then \<lparr> word = (rr (\<not> (row p)))\<inverse>\<^sup>>(word p), row  = (row p) \<rparr> \<comment> \<open>the extended row remains shorter after extension\<close>
  else \<lparr> word = (word p)\<inverse>\<^sup>>(rr (\<not> (row p))), row  = \<not> (row p) \<rparr> \<comment> \<open>the extended row becomes longer\<close>
 )
in
(new, (rr (\<not> (row p)) \<bowtie> word p))
)
"

definition right_overflow_update :: "'a overflow \<Rightarrow> ('a overflow \<times> bool)"
  \<comment> \<open>right overflow is shortened from the right\<close>
  where
    "right_overflow_update p  =
 \<comment> \<open>the shorter row is always extended\<close>
 (let new = (
  if \<^bold>|rr (row p)\<^bold>| = \<^bold>|word p\<^bold>|
  then \<lparr> word = \<epsilon>, row = True \<rparr>  \<comment> \<open>empty overflow has row True\<close>
  else
  if \<^bold>|rr (row p)\<^bold>| < \<^bold>|word p\<^bold>|
  then \<lparr> word = (word p)\<^sup><\<inverse>(rr (row p)), row  = (row p) \<rparr> \<comment> \<open>the extended row remains shorter after extension\<close>
  else \<lparr> word = (rr (row p))\<^sup><\<inverse>(word p), row  = \<not> (row p) \<rparr> \<comment> \<open>the extended row becomes longer\<close>
 )
in
(new, rr (row p) \<bowtie>\<^sub>s word p)
)
"

lemma left_update_valid: "snd (left_overflow_update p) = (rr (\<not> (row p)) \<bowtie> word p)"
  unfolding left_overflow_update_def Let_def by auto

lemma right_update_valid: "snd (right_overflow_update p) = (rr ((row p)) \<bowtie>\<^sub>s word p)"
  unfolding right_overflow_update_def Let_def by auto

definition NextCand_overflow :: "bool \<Rightarrow> (bool \<Rightarrow> 'a overflow) \<Rightarrow> 'a overflow"
  where "NextCand_overflow a ovs = fst (if a then left_overflow_update (ovs a) else right_overflow_update (ovs a))"

definition NextCand_valid :: "bool \<Rightarrow> (bool \<Rightarrow> 'a overflow) \<Rightarrow> bool"
  where "NextCand_valid a ovs = snd (if a then left_overflow_update (ovs a) else right_overflow_update (ovs a))"

definition next_overflow:: "(bool \<Rightarrow> 'a overflow) \<Rightarrow> bool \<Rightarrow> 'a overflow"
  where "next_overflow ovs a = (let  b = (NextCand_indicator ovs) in
(if b = a then fst(if b then left_overflow_update (ovs a) else right_overflow_update (ovs a))
           else (ovs a)))
"

definition next_valid:: "bool \<Rightarrow>(bool \<Rightarrow> 'a overflow) \<Rightarrow> bool"
  where "next_valid a ovs  = (let  b = (NextCand_indicator ovs) in
(if a  then snd (if b then left_overflow_update (ovs b) else right_overflow_update (ovs b))
           else False))"

\<comment> \<open>Next we define the @{term "NextCand"} update procedure for the presolutions using the above functions.\<close>

print_record "'a presolution"

definition NextCand :: "'a presolution \<Rightarrow> 'a presolution"
  where
    "NextCand X =
   (let a = NextCand_indicator (over X);
        b = ((\<not> row ((over X) a)) = a)
   in
   \<lparr> count =  (count X)(b := Suc (count X b)), \<comment> \<open>the shorter row of the indicated overflow\<close>
     over = (over X)(a := NextCand_overflow a (over X)),
     valid = valid X \<and> NextCand_valid a (over X)
   \<rparr>
)"

lemma Next_next_over: "over (NextCand X) = next_overflow (over X)"
  unfolding NextCand_def Let_def select_convs next_overflow_def NextCand_overflow_def by force

lemma Next_next_over_pow: "over ((NextCand^^k) X) = (next_overflow^^k) (over X)"
  by (induct k, force, auto simp: Next_next_over)

lemma Next_next_valid: "valid (NextCand X) = next_valid (valid X) (over X)"
  unfolding NextCand_def Let_def select_convs next_valid_def NextCand_valid_def by presburger

thm NextCand_def
  NextCand_def[reversed]

print_record "'a presolution"

lemma over_count_irrelevant: "over (X \<lparr>count := f\<rparr>) = over X"
  by simp

lemma over_count_irrelevant_pow: "over ((NextCand^^k) (X\<lparr>count := f\<rparr>)) = over ((NextCand^^k) X)"
  unfolding Next_next_over_pow by simp

lemma valid_count_irrelevant: "valid (X \<lparr>count := f\<rparr>) = valid X"
  by simp

lemma valid_count_irrelevant_pow: "valid ((NextCand^^k) (X \<lparr>count := f\<rparr>)) = valid ((NextCand^^k) X)"
proof (induct k arbitrary: X rule: less_induct)
  case (less x)
  then show ?case
  proof (cases x)
    case 0
    then show ?thesis by simp
  next
    case (Suc m)
    let ?X = "(NextCand ^^ m) (X\<lparr>count := f\<rparr>)"
    have hyp: "valid ?X = valid ((NextCand ^^ m) X)"
      using less.hyps[of m X] unfolding \<open>x = Suc m\<close> by blast
    have drop: "valid (NextCand ?X) = (valid ((NextCand ^^ m) X) \<and> NextCand_valid (NextCand_indicator (over ((NextCand ^^ m) X))) (over ((NextCand ^^ m) X)))"
      unfolding NextCand_def[of "?X"] unfolding hyp over_count_irrelevant_pow valid_count_irrelevant Let_def select_convs..
    show ?thesis
      unfolding \<open>x = Suc m\<close> compow_Suc  unfolding drop NextCand_def[of "(NextCand ^^ m) X"] Let_def select_convs..
  qed
qed

lemmas count_irrelevant = over_count_irrelevant over_count_irrelevant_pow valid_count_irrelevant_pow valid_count_irrelevant

lemma count_additive: "count ((NextCand^^k)((IniPresol p1 p2 s1 s2)\<lparr>count := f\<rparr>))
      = (\<lambda> x. f x + count ((NextCand^^k)(IniPresol p1 p2 s1 s2)) x)"
proof (induct k)
  case (Suc k)
  then show ?case
    unfolding compow_Suc NextCand_def[of "((NextCand ^^ k) ((IniPresol p1 p2 s1 s2)))"]
      NextCand_def[of "((NextCand ^^ k) (IniPresol p1 p2 s1 s2\<lparr>count := f\<rparr>))"] Let_def select_convs
      count_irrelevant by fastforce
qed (simp add: IniPresol_def)

\<comment> \<open>We prove four lemmas of single step updates of  @{term "NextCand"} update procedure using the
@{term "IniPresol"}. The four lemmas are according to the cases of  @{term "NextCand_indicator"} and
 @{term "row"} of the presolution.\<close>

lemma NextCand_step1:
  fixes p1 p2 s1 s2 f
  defines "X \<equiv> (IniPresol p1 p2 s1 s2)\<lparr>count := f\<rparr>"
  assumes
    side: "NextCand_indicator (over X)" (is "NextCand_indicator ?X") and \<comment> \<open>left side is extended\<close>
    row:  "\<not> row (over X (NextCand_indicator (over X)))" (is "\<not> row ((over ?X) ?a)") \<comment> \<open>first row is extended\<close>
  shows "NextCand X = (IniPresol (p1 \<cdot> gr) p2 s1 s2)\<lparr>count := f(True := Suc (f True))\<rparr>"
proof-
  define Y where "Y = (IniPresol (p1 \<cdot> gr) p2 s1 s2)\<lparr>count := f(True := Suc (f True))\<rparr>"
  have rowside': "NextCand_indicator (over X) = True" "row (over X True) = False"
    using row side unfolding X_def by simp_all
  have p1_short: "\<^bold>|p1\<^bold>| < \<^bold>|p2\<^bold>|"
    using \<open>row (over X True) = False\<close>
    unfolding X_def IniPresol_def select_convs if_True overflow.select_convs by force
  hence w1: "word (over X True) = p1\<inverse>\<^sup>>p2"
    unfolding X_def IniPresol_def select_convs if_True by force
  have len1: "\<^bold>|p1\<inverse>\<^sup>>p2\<^bold>| = \<^bold>|p2\<^bold>| - \<^bold>|p1\<^bold>|"
    unfolding left_quotient_def using length_drop.
  show "NextCand X  = Y"
  proof
    show count: "count (NextCand X) = count Y"
      unfolding Y_def X_def NextCand_def Let_def using ini_count assms ini_count[of "p1 \<cdot> gr" p2 s1 s2] by simp
    show valid: "valid (NextCand X) = valid Y"
      unfolding Y_def count_irrelevant
    proof-
      have v1:"valid X = (p1 \<bowtie> p2 \<and> s1 \<bowtie>\<^sub>s s2)"
        unfolding X_def IniPresol_def select_convs count_irrelevant..
      have v2: "NextCand_valid (NextCand_indicator (over X)) (over X) = (gr \<bowtie> p1\<inverse>\<^sup>>p2)"
        unfolding rowside' NextCand_valid_def if_True left_overflow_update_def Let_def snd_conv gpcp_simps w1..
      show "valid (NextCand X) = valid (IniPresol (p1 \<cdot> gr) p2 s1 s2)"
        unfolding NextCand_def Let_def IniPresol_def select_convs v1 v2
        using comp_per_partes by blast
    qed
    show over: "over (NextCand X) = over Y"
    proof
      have "over (NextCand X) True = fst (left_overflow_update (over X True))"
        unfolding NextCand_def Let_def select_convs rowside'
        unfolding NextCand_overflow_def if_True by simp
      hence B: "over (NextCand X) True = (if \<^bold>|p1\<inverse>\<^sup>>p2\<^bold>| \<le> \<^bold>|gr\<^bold>| then \<lparr>word = (p1\<inverse>\<^sup>>p2)\<inverse>\<^sup>>gr, row = True\<rparr>
                           else  \<lparr>word = gr\<inverse>\<^sup>>p1\<inverse>\<^sup>>p2, row = False\<rparr>)"
        unfolding  left_overflow_update_def Let_def bool_simps rowside' rr_def if_True  w1  fst_conv
        by (cases "\<^bold>|gr\<^bold>| = \<^bold>|p1\<inverse>\<^sup>>p2\<^bold>|") (simp add: lq_same_len, force)
      have Y: "(over Y) True = \<lparr>word = if \<^bold>|p2\<^bold>| \<le> \<^bold>|p1 \<cdot> gr\<^bold>| then p2\<inverse>\<^sup>>(p1 \<cdot> gr) else (p1 \<cdot> gr)\<inverse>\<^sup>>p2, row = \<^bold>|p2\<^bold>| \<le> \<^bold>|p1 \<cdot> gr\<^bold>|\<rparr>"
        unfolding Y_def IniPresol_def by auto
      show "over (NextCand X) b = over Y b" for b
      proof (induct b)
        show "over (NextCand X) True = (over Y) True"
        proof (rule disjE[OF linorder_le_less_linear,of "\<^bold>|p2\<^bold>|"])
          assume a1: "\<^bold>|p2\<^bold>| \<le> \<^bold>|p1 \<cdot> gr\<^bold>|"
          hence R: "(over Y) True =  \<lparr>word = p2\<inverse>\<^sup>>(p1 \<cdot> gr), row = True\<rparr>"
            using Y by auto
          have "\<^bold>|p1\<inverse>\<^sup>>p2\<^bold>| \<le> \<^bold>|gr\<^bold>|"
            using a1 p1_short unfolding lenmorph len1 by linarith
          hence L: "over (NextCand X) True =  \<lparr>word = (p1\<inverse>\<^sup>>p2)\<inverse>\<^sup>>gr, row = True\<rparr>"
            using B by simp
          show ?thesis
            unfolding L R using lq_assoc[OF less_imp_le[OF p1_short]] by auto
        next
          assume a2: "\<^bold>|p1 \<cdot> gr\<^bold>| < \<^bold>|p2\<^bold>|"
          hence R: "(over Y) True =  \<lparr>word = (p1 \<cdot> gr)\<inverse>\<^sup>>p2, row = False\<rparr>"
            using Y by auto
          have "\<^bold>|gr\<^bold>| < \<^bold>|p1\<inverse>\<^sup>>p2\<^bold>|"
            using a2 unfolding len1 lenmorph by auto
          hence L: "over (NextCand X) True =   \<lparr>word = gr\<inverse>\<^sup>>(p1\<inverse>\<^sup>>p2), row = False\<rparr>"
            using B a2 by auto
          have assoc: "gr\<inverse>\<^sup>>(p1\<inverse>\<^sup>>p2) = (p1 \<cdot> gr)\<inverse>\<^sup>>p2"
            using lq_assoc'[symmetric] by blast
          show ?thesis
            unfolding L R assoc..
        qed
      next
        show "over (NextCand X) False = (over Y) False"
          unfolding NextCand_def \<open>NextCand_indicator (over X) = True\<close>
          unfolding  X_def Y_def IniPresol_def count_irrelevant select_convs if_False by auto
      qed
    qed
  qed
qed

lemma NextCand_step2:
  fixes p1 p2 s1 s2 f
  defines "X \<equiv> (IniPresol p1 p2 s1 s2)\<lparr>count := f\<rparr>"
  assumes
    side: "NextCand_indicator (over X)" (is "NextCand_indicator ?X") and \<comment> \<open>left side is extended\<close>
    row:  " row (over X (NextCand_indicator (over X)))" (is "row ((over ?X) ?a)") \<comment> \<open>second row is extended\<close>
  shows "NextCand X = (IniPresol p1 (p2  \<cdot> hr) s1 s2)\<lparr>count := f(False := Suc (f False))\<rparr>"
proof-
  define Y where "Y = (IniPresol p1  (p2\<cdot> hr)  s1 s2)\<lparr>count := f(False := Suc (f False))\<rparr>"
  have rowside': "NextCand_indicator (over X) = True" "row (over X True) = True"
    using row side unfolding X_def by simp_all
  have p2_short: "\<^bold>|p2\<^bold>| \<le> \<^bold>|p1\<^bold>|"
    using \<open>row (over X True) = True\<close>
    unfolding X_def IniPresol_def select_convs if_True overflow.select_convs by force
  hence w1: "word (over X True) = p2\<inverse>\<^sup>>p1"
    unfolding X_def IniPresol_def select_convs if_True by force
  have len1: "\<^bold>|p2\<inverse>\<^sup>>p1\<^bold>| = \<^bold>|p1\<^bold>| - \<^bold>|p2\<^bold>|"
    unfolding left_quotient_def using length_drop.
  show "NextCand X  = Y"
  proof
    show count: "count (NextCand X) = count Y"
      unfolding Y_def X_def NextCand_def Let_def using ini_count assms ini_count[of p1 "p2\<cdot>hr" s1 s2] by simp
    show valid: "valid (NextCand X) = valid Y"
      unfolding Y_def count_irrelevant
    proof-
      have v1:"valid X = (p1 \<bowtie> p2 \<and> s1 \<bowtie>\<^sub>s s2)"
        unfolding X_def IniPresol_def select_convs count_irrelevant..
      have v2: "NextCand_valid (NextCand_indicator (over X)) (over X) = (hr \<bowtie> p2\<inverse>\<^sup>>p1)"
        unfolding rowside' NextCand_valid_def if_True left_overflow_update_def Let_def snd_conv gpcp_simps w1..
      show "valid (NextCand X) = valid (IniPresol p1 (p2 \<cdot> hr) s1 s2)"
        unfolding NextCand_def Let_def IniPresol_def select_convs v1 v2
        using comp_per_partes[of p2 p1 hr] pref_comp_sym by blast
    qed
    show over: "over (NextCand X) = over Y"
    proof
      have "over (NextCand X) True = fst (left_overflow_update (over X True))"
        unfolding NextCand_def Let_def select_convs rowside'
        unfolding NextCand_overflow_def if_True by simp
      hence B: "over (NextCand X) True = (if \<^bold>|hr\<^bold>| \<le> \<^bold>|p2\<inverse>\<^sup>>p1\<^bold>| then \<lparr>word = hr\<inverse>\<^sup>>p2\<inverse>\<^sup>>p1, row = True\<rparr>
                           else  \<lparr>word = (p2\<inverse>\<^sup>>p1)\<inverse>\<^sup>>hr, row = False\<rparr>)"
        unfolding  left_overflow_update_def Let_def bool_simps rowside' rr_def if_False  w1  fst_conv
        by (cases "\<^bold>|hr\<^bold>| = \<^bold>|p2\<inverse>\<^sup>>p1\<^bold>|") (simp add: lq_same_len, force)
      have Y: "(over Y) True = \<lparr>word = if \<^bold>|p2\<cdot> hr\<^bold>| \<le> \<^bold>|p1\<^bold>| then (p2\<cdot>hr)\<inverse>\<^sup>>p1 else p1\<inverse>\<^sup>>(p2\<cdot>hr), row = \<^bold>|p2\<cdot>hr\<^bold>| \<le> \<^bold>|p1\<^bold>|\<rparr>"
        unfolding Y_def IniPresol_def by auto
      show "over (NextCand X) b = over Y b" for b
      proof (induct b)
        show "over (NextCand X) True = (over Y) True"
        proof (rule disjE[OF linorder_le_less_linear,of "\<^bold>|p2 \<cdot> hr\<^bold>|"])
          assume a1: "\<^bold>|p2 \<cdot> hr\<^bold>| \<le> \<^bold>|p1\<^bold>|"
          hence R: "(over Y) True =  \<lparr>word = (p2 \<cdot> hr)\<inverse>\<^sup>>p1, row = True\<rparr>"
            using Y by auto
          have "\<^bold>|hr\<^bold>| \<le> \<^bold>|p2\<inverse>\<^sup>>p1\<^bold>|"
            using a1 \<open>\<^bold>|p2\<^bold>| \<le> \<^bold>|p1\<^bold>|\<close> unfolding lenmorph len1 by linarith
          hence L: "over (NextCand X) True =  \<lparr>word = hr\<inverse>\<^sup>>p2\<inverse>\<^sup>>p1, row = True\<rparr>"
            using B by simp
          show ?thesis
            unfolding L R  lq_assoc' by auto
        next
          assume a2: "\<^bold>|p1\<^bold>| < \<^bold>|p2 \<cdot> hr\<^bold>|"
          hence R: "(over Y) True =  \<lparr>word = p1\<inverse>\<^sup>>(p2 \<cdot> hr), row = False\<rparr>"
            using Y by auto
          have "\<not> \<^bold>|hr\<^bold>| \<le> \<^bold>|p2\<inverse>\<^sup>>p1\<^bold>|"
            using a2 p2_short unfolding lenmorph len1 by linarith
          hence L: "over (NextCand X) True =  \<lparr>word = (p2\<inverse>\<^sup>>p1)\<inverse>\<^sup>>hr, row = False\<rparr>"
            using B by simp
          show ?thesis
            unfolding L R using lq_assoc p2_short by auto
        qed
      next
        show "over (NextCand X) False = (over Y) False"
          unfolding NextCand_def \<open>NextCand_indicator (over X) = True\<close>
          unfolding  X_def Y_def IniPresol_def count_irrelevant select_convs if_False by auto
      qed
    qed
  qed
qed

lemma NextCand_step3:
  fixes p1 p2 s1 s2 f
  defines "X \<equiv> (IniPresol p1 p2 s1 s2)\<lparr>count := f\<rparr>"
  assumes
    side: "\<not> NextCand_indicator (over X)" (is "\<not> NextCand_indicator ?X") and \<comment> \<open>right side is extended\<close>
    row:  " row (over X (NextCand_indicator (over X)))" (is "row ((over ?X) ?a)") \<comment> \<open>first row is extended\<close>
  shows "NextCand X = (IniPresol p1 p2 (gr\<cdot> s1) s2)\<lparr>count := f(True := Suc (f True))\<rparr>"
proof-
  define Y where "Y = (IniPresol p1 p2 (gr\<cdot>s1) s2)\<lparr>count := f(True := Suc (f True))\<rparr>"
  have rowside': "row (over X False) = True" "NextCand_indicator (over X) = False"
    using row side unfolding X_def by simp_all
  have "\<^bold>|s1\<^bold>| \<le> \<^bold>|s2\<^bold>|"
    using \<open>row (over X False) = True\<close>
    unfolding X_def IniPresol_def select_convs if_True overflow.select_convs by force
  have len1: "\<^bold>|s2\<^sup><\<inverse>s1\<^bold>| = \<^bold>|s2\<^bold>| - \<^bold>|s1\<^bold>|"
    using  rq_len by blast
  have w1: "word (over X False) = s2\<^sup><\<inverse>s1"
    unfolding X_def IniPresol_def le_if_then_rq[OF \<open>\<^bold>|s1\<^bold>| \<le> \<^bold>|s2\<^bold>|\<close>]by simp
  show "NextCand X  = Y"
  proof
    show count: "count (NextCand X) = count Y"
      unfolding Y_def X_def NextCand_def Let_def using ini_count assms ini_count[of p1 p2 "gr\<cdot>s1" s2] by simp
    show valid: "valid (NextCand X) = valid Y"
      unfolding Y_def count_irrelevant
    proof-
      have v1:"valid X = (p1 \<bowtie> p2 \<and> s1 \<bowtie>\<^sub>s s2)"
        unfolding X_def IniPresol_def select_convs count_irrelevant..
      have v2: "NextCand_valid (NextCand_indicator (over X)) (over X) = (gr \<bowtie>\<^sub>s  s2\<^sup><\<inverse>s1)"
        unfolding rowside' NextCand_valid_def if_True right_overflow_update_def Let_def snd_conv gpcp_simps w1..
      show "valid (NextCand X) = valid (IniPresol p1  p2 (gr \<cdot> s1) s2)"
        using long_suf rq_suf sufI suf_comp_cancel suf_comp_monotone suf_comp_or \<open>\<^bold>|s1\<^bold>| \<le> \<^bold>|s2\<^bold>|\<close>
        unfolding  NextCand_def Let_def IniPresol_def select_convs v1 v2 conj_assoc bool_simps
        by metis
    qed
    show "over (NextCand X) = over Y"
    proof
      have "over (NextCand X) False = fst (right_overflow_update (over X False))"
        unfolding NextCand_def Let_def select_convs rowside'
        unfolding NextCand_overflow_def if_True by simp
      hence B: "over (NextCand X) False =
      (if \<^bold>|gr\<^bold>| \<le> \<^bold>|s2\<^sup><\<inverse>s1\<^bold>| then \<lparr>word = s2\<^sup><\<inverse>s1 \<^sup><\<inverse>gr , row = True\<rparr>
                           else \<lparr>word =gr\<^sup><\<inverse>(s2\<^sup><\<inverse>s1), row = False\<rparr>)"
        unfolding  right_overflow_update_def Let_def bool_simps rowside'
        unfolding rr_def w1 fst_conv  if_True
        by (cases "\<^bold>|gr\<^bold>| = \<^bold>|s2\<inverse>\<^sup>>s1\<^bold>|", metis le_refl nat_less_le rq_same_len)
          (simp add: rq_same_len)
      have Y: "(over Y) False = \<lparr>word =  if \<^bold>|s2\<^bold>| \<le> \<^bold>|gr \<cdot> s1\<^bold>| then (gr \<cdot> s1)\<^sup><\<inverse>s2 else s2\<^sup><\<inverse>(gr \<cdot> s1), row = \<^bold>|gr \<cdot> s1\<^bold>| \<le> \<^bold>|s2\<^bold>| \<rparr> "
        unfolding Y_def IniPresol_def by auto
      show "over (NextCand X) b = over Y b" for b
      proof (induct b)
        show "over (NextCand X) False = (over Y) False"
        proof (rule disjE[OF linorder_le_less_linear,of "\<^bold>|gr \<cdot> s1\<^bold>|"])
          assume a1: "\<^bold>|gr\<cdot> s1\<^bold>| \<le> \<^bold>|s2\<^bold>|"
          hence R: "(over Y) False =  \<lparr>word =  s2\<^sup><\<inverse>(gr \<cdot> s1), row = True\<rparr>"
            using Y unfolding le_if_then_rq[OF a1] by force
          have "\<^bold>|gr\<^bold>| \<le> \<^bold>|s2\<^sup><\<inverse>s1\<^bold>|"
            using a1 unfolding len1 lenmorph by auto
          hence L: "over (NextCand X) False =   \<lparr>word = (s2\<^sup><\<inverse>s1)\<^sup><\<inverse>gr, row = True\<rparr>"
            using B a1 by auto
          show ?thesis
            unfolding L R by (simp add: rq_assoc')
        next
          assume a2: "\<^bold>|s2\<^bold>| < \<^bold>|gr\<cdot> s1\<^bold>|"
          hence R: "(over Y) False =  \<lparr>word = (gr \<cdot> s1)\<^sup><\<inverse>s2, row = False\<rparr>"
            using Y by auto
          have "\<not> \<^bold>|gr\<^bold>| \<le> \<^bold>|s2\<^sup><\<inverse>s1\<^bold>|"
            using a2  unfolding lenmorph len1
            by (simp add: Nat.le_diff_conv2 \<open>\<^bold>|s1\<^bold>| \<le> \<^bold>|s2\<^bold>|\<close>)
          hence L: "over (NextCand X) False =  \<lparr>word =gr\<^sup><\<inverse>(s2\<^sup><\<inverse>s1), row = False\<rparr>"
            using B by simp
          show ?thesis
            unfolding L R using rq_assoc \<open>\<^bold>|s1\<^bold>| \<le> \<^bold>|s2\<^bold>|\<close> by blast
        qed
      next
        show "over (NextCand X) True = (over Y) True"
          unfolding Y_def  NextCand_def  \<open>NextCand_indicator (over X) = False\<close>  Let_def  select_convs
          unfolding X_def  count_irrelevant IniPresol_def  select_convs  if_True by simp

      qed
    qed
  qed
qed

lemma NextCand_step4:
  fixes p1 p2 s1 s2 f
  defines "X \<equiv> (IniPresol p1 p2 s1 s2)\<lparr>count := f\<rparr>"
  assumes
    side: "\<not> NextCand_indicator (over X)" (is "\<not> NextCand_indicator ?X") and \<comment> \<open>right side is extended\<close>
    row:  "\<not>  row (over X (NextCand_indicator (over X)))" (is "\<not> row ((over ?X) ?a)") \<comment> \<open>second row is extended\<close>
  shows "NextCand X = (IniPresol p1 p2 s1 (hr\<cdot> s2))\<lparr>count := f(False := Suc (f False))\<rparr>"
proof-
  define Y where "Y = (IniPresol p1 p2 s1 (hr\<cdot> s2))\<lparr>count := f(False := Suc (f False))\<rparr>"
  have rowside': "row (over X False) = False" "NextCand_indicator (over X) = False"
    using row side unfolding X_def by simp_all
  have "\<^bold>|s2\<^bold>| < \<^bold>|s1\<^bold>|"
    using \<open>row (over X False) = False\<close>
    unfolding X_def IniPresol_def select_convs if_True overflow.select_convs by force
  have len1: "\<^bold>|s1\<^sup><\<inverse>s2\<^bold>| = \<^bold>|s1\<^bold>| - \<^bold>|s2\<^bold>|"
    using  rq_len by blast
  have w1: "word (over X False) = s1\<^sup><\<inverse>s2"
    using  \<open>\<^bold>|s2\<^bold>| < \<^bold>|s1\<^bold>|\<close>  unfolding X_def IniPresol_def by auto
  show "NextCand X  = Y"
  proof
    show count: "count (NextCand X) = count Y"
      unfolding Y_def X_def NextCand_def Let_def using ini_count assms ini_count[of p1 p2 s1 "hr\<cdot>s2"] by simp
    show valid: "valid (NextCand X) = valid Y"
      unfolding Y_def count_irrelevant
    proof-
      have v1:"valid X = (p1 \<bowtie> p2 \<and> s1 \<bowtie>\<^sub>s s2)"
        unfolding X_def IniPresol_def select_convs count_irrelevant..
      have v2: "NextCand_valid (NextCand_indicator (over X)) (over X) = (hr \<bowtie>\<^sub>s  s1\<^sup><\<inverse>s2)"
        unfolding rowside' NextCand_valid_def if_True right_overflow_update_def Let_def snd_conv gpcp_simps w1..
      show "valid (NextCand X) = valid (IniPresol p1  p2  s1 (hr\<cdot>s2))"
        unfolding  NextCand_def Let_def IniPresol_def select_convs v1 v2
        using comp_per_partes[reversed] suf_comp_sym by blast
    qed
    show "over (NextCand X) = over Y"
    proof
      have "over (NextCand X) False = fst (right_overflow_update (over X False))"
        unfolding NextCand_def Let_def select_convs rowside'
        unfolding NextCand_overflow_def if_True by simp
      hence B: "over (NextCand X) False =
      (if \<^bold>|hr\<^bold>| < \<^bold>|s1\<^sup><\<inverse>s2\<^bold>| then \<lparr>word = s1\<^sup><\<inverse>s2 \<^sup><\<inverse>hr , row = False\<rparr>
                           else \<lparr>word =hr\<^sup><\<inverse>(s1\<^sup><\<inverse>s2), row = True\<rparr>)"
        unfolding  right_overflow_update_def Let_def bool_simps rowside'
        unfolding rr_def w1 fst_conv  if_False
        by (cases "\<^bold>|hr\<^bold>| = \<^bold>|s1\<inverse>\<^sup>>s2\<^bold>|", metis  nat_less_le rq_same_len)
          (simp add: rq_same_len)
      have Y: "(over Y) False = \<lparr>word =  if \<^bold>|hr \<cdot> s2\<^bold>| \<le> \<^bold>|s1\<^bold>| then s1\<^sup><\<inverse>(hr\<cdot> s2) else(hr\<cdot> s2)\<^sup><\<inverse>s1, row = \<^bold>|s1\<^bold>| \<le> \<^bold>|hr \<cdot> s2\<^bold>| \<rparr> "
        unfolding Y_def IniPresol_def by auto
      show "over (NextCand X) b = over Y b" for b
      proof (induct b)
        show "over (NextCand X) False = (over Y) False"
        proof (rule disjE[OF linorder_le_less_linear,of "\<^bold>|s1\<^bold>|"])
          assume a1: "\<^bold>|s1\<^bold>| \<le> \<^bold>|hr\<cdot> s2\<^bold>|"
          hence R: "(over Y) False =  \<lparr>word = (hr \<cdot> s2)\<^sup><\<inverse>s1, row = True\<rparr>"
            using Y unfolding le_if_then_rq[OF a1] by force
          have "\<^bold>|s1\<^sup><\<inverse>s2\<^bold>| \<le> \<^bold>|hr\<^bold>|"
            using a1 unfolding len1 lenmorph by auto
          hence L: "over (NextCand X) False =   \<lparr>word = hr\<^sup><\<inverse>(s1\<^sup><\<inverse>s2), row = True\<rparr>"
            using B  by auto
          show ?thesis
            using  \<open>\<^bold>|s2\<^bold>| < \<^bold>|s1\<^bold>|\<close>  unfolding L R
            by (simp add: le_eq_less_or_eq rq_assoc)
        next
          assume a2: "\<^bold>|hr\<cdot>s2\<^bold>| < \<^bold>|s1\<^bold>|"
          hence R: "(over Y) False =  \<lparr>word =s1\<^sup><\<inverse>(hr\<cdot> s2), row = False\<rparr>"
            using Y by auto
          have "\<^bold>|hr\<^bold>| < \<^bold>|s1\<^sup><\<inverse>s2\<^bold>|"
            using a2  unfolding lenmorph len1
            by  (simp add: Nat.le_diff_conv2 \<open>\<^bold>|s2\<^bold>| < \<^bold>|s1\<^bold>|\<close>)
          hence L: "over (NextCand X) False =  \<lparr>word = s1\<^sup><\<inverse>s2 \<^sup><\<inverse>hr, row = False\<rparr>"
            using B by simp
          show ?thesis
            unfolding L R using rq_assoc'  by metis
        qed
      next
        show "over (NextCand X) True = (over Y) True"
          unfolding Y_def  NextCand_def  \<open>NextCand_indicator (over X) = False\<close>  Let_def  select_convs
          unfolding X_def  count_irrelevant IniPresol_def  select_convs  if_True by simp
      qed
    qed
  qed
qed

\<comment> \<open>Next lemmas shows that from a non-empty (that is @{term "0<n +m"})
rootsol  @{term "pg \<cdot> gr\<^sup>@n \<cdot> sg = ph \<cdot> hr\<^sup>@m \<cdot> sh"} one occerunce of the a primitive root can be
separated.\<close>

lemma NextCand_basic:
  fixes f
  assumes     eq:   "(p a) \<cdot> (rr a)\<^sup>@(ee a) \<cdot> (s a) = (p (\<not> a)) \<cdot> rr (\<not> a)\<^sup>@(ee (\<not> a)) \<cdot> s (\<not> a)" and "ee a + ee (\<not> a) \<noteq> 0"
  defines    "c \<equiv> NextCand_indicator (over ((IniPresol (p True) (p False) (s True) (s False))\<lparr>count := f\<rparr>))" \<comment> \<open>side c is extended\<close>
  defines    "b \<equiv> row (over (IniPresol (p True) (p False) (s True) (s False)\<lparr>count := f\<rparr>) c)"
  defines    "d \<equiv> b \<noteq> c"
  shows
    "ee d \<noteq> 0" and
    "(p d \<cdot> rr d) \<cdot> rr d \<^sup>@ (ee d - 1) \<cdot> s d = p (\<not> d) \<cdot> rr (\<not> d) \<^sup>@ ee (\<not> d) \<cdot> s (\<not> d)"
proof-
  define X where "X = IniPresol (p True) (p False) (s True) (s False)\<lparr>count := f\<rparr>"
  let ?l = "NextCand_indicator (over X)" \<comment> \<open>extended side\<close>
  have sum: "ee c + ee (\<not> c) \<noteq> 0" for c
    using \<open>ee a + ee (\<not> a) \<noteq> 0\<close> by (cases "c = a") auto
  have eq_sym: "(p c) \<cdot> (rr c)\<^sup>@(ee c) \<cdot> (s c) = (p (\<not> c)) \<cdot> rr (\<not> c)\<^sup>@(ee (\<not> c)) \<cdot> s (\<not> c)" for c
    using eq by (cases "c = a") auto
  show "ee d \<noteq> 0"
  proof
    assume "ee d = 0"
    hence "ee (\<not> d) \<noteq> 0"
      using sum[of d] by presburger
    show False
    proof (rule bool.exhaust)
      assume c1:"c = True"
      hence "ee (\<not> b) = 0" and "ee b \<noteq> 0"
        using \<open>ee d = 0\<close> \<open>ee (\<not> d) \<noteq> 0\<close> unfolding d_def by auto
      have "\<^bold>|p (\<not> b)\<^bold>| \<le> \<^bold>|p b\<^bold>|"
        unfolding b_def bool_simps c1 unfolding IniPresol_def select_convs overflow.select_convs if_True
          count_irrelevant bool_simps
        by (cases "\<^bold>|p False\<^bold>| \<le> \<^bold>|p True\<^bold>|") simp_all
      hence "p (\<not> b) \<le>p p b"
        using eq_le_pref[OF eq_sym, of "\<not> b"] eq_le_pref[OF eq_sym[reversed,symmetric], of "\<not> b"]
        by simp
      have len1: "\<^bold>|p (\<not> b)\<inverse>\<^sup>>p b\<^bold>| = \<^bold>|p b\<^bold>| - \<^bold>|p (\<not> b)\<^bold>|"
        by (simp add: lq_len)
      have w1: "word (over X True) = (p (\<not> b))\<inverse>\<^sup>>p b"
        unfolding X_def IniPresol_def gpcp_simps'
        using  \<open>p (\<not> b) \<le>p p b\<close> \<open>\<^bold>|p (\<not> b)\<^bold>| \<le> \<^bold>|p b\<^bold>|\<close> long_pref[OF \<open>p (\<not> b) \<le>p p b\<close> ]  by (induct b)  auto
      have "\<^bold>|s b\<^bold>| \<le> \<^bold>|s (\<not> b)\<^bold>|"
        using lenarg[OF eq_sym[symmetric, of b]] pref_len[OF \<open>p (\<not> b) \<le>p p b\<close>]
        unfolding \<open>ee (\<not> b) = 0\<close> lenmorph pow_len by fastforce
      hence "s b \<le>s s (\<not> b)"
        using eq_le_pref[reversed, OF eq_sym[symmetric, unfolded lassoc], of "\<not> b"] by presburger
      have len2: "\<^bold>|s (\<not> b)\<^sup><\<inverse>s b\<^bold>| = \<^bold>|s (\<not> b)\<^bold>| - \<^bold>|s b\<^bold>|"
        unfolding rq_len by fastforce
      have w2: "word (over X False) = s (\<not> b)\<^sup><\<inverse>s b"
        unfolding X_def IniPresol_def gpcp_simps'
        using \<open>\<^bold>|s b\<^bold>| \<le> \<^bold>|s (\<not> b)\<^bold>|\<close> \<open>s b \<le>s s (\<not> b)\<close> long_suf[OF \<open>s b \<le>s s (\<not> b)\<close>]
        by (induct b) auto
      have "\<^bold>|p (\<not> b)\<^bold>| + \<^bold>|s (\<not> b)\<^bold>| = \<^bold>|p b\<^bold>| + ee b * \<^bold>|rr b\<^bold>| + \<^bold>|s b\<^bold>|"
        using lenarg[OF eq_sym[of "\<not> b"]]
        unfolding lenmorph pow_len \<open>ee (\<not> b) = 0\<close> by simp
      hence "\<^bold>|s (\<not> b)\<^bold>| - \<^bold>|s b\<^bold>| = \<^bold>|p b\<^bold>|  + ee b * \<^bold>|rr b\<^bold>| - \<^bold>|p (\<not> b)\<^bold>|"
        by force
      hence "\<^bold>|p (\<not> b)\<inverse>\<^sup>>p b\<^bold>| < \<^bold>|s (\<not> b)\<^sup><\<inverse>s b\<^bold>|"
        unfolding len1 len2 using \<open>ee b \<noteq> 0\<close> mroot_len[of b]
        by (simp add: \<open>\<^bold>|p (\<not> b)\<^bold>| \<le> \<^bold>|p b\<^bold>|\<close> diff_less_mono)
      from this[folded w1 w2]
      show False
        using c1 unfolding c_def NextCand_indicator_def X_def by simp
    next
      assume c2:"c = False"
      hence "ee (\<not> b) \<noteq> 0" and "ee b = 0"
        using \<open>ee d = 0\<close> \<open>ee (\<not> d) \<noteq> 0\<close> d_def by auto
      have "\<^bold>|s b\<^bold>| \<le> \<^bold>|s (\<not> b)\<^bold>|"
        unfolding b_def bool_simps c2 unfolding IniPresol_def select_convs overflow.select_convs if_False
          count_irrelevant bool_simps
        by (cases "\<^bold>|s True\<^bold>| \<le> \<^bold>|s False\<^bold>|") simp_all
      hence "s b \<le>s s (\<not> b)"
        using eq_le_pref[reversed, OF eq_sym[unfolded lassoc], of "b"] by force
      have w1: "word (over X False) = s (\<not> b)\<^sup><\<inverse>s b"
        unfolding X_def IniPresol_def gpcp_simps'
        using  \<open>\<^bold>|s b\<^bold>| \<le> \<^bold>|s (\<not> b)\<^bold>|\<close> \<open>s b \<le>s s (\<not> b)\<close> long_pref[reversed, OF \<open>s b \<le>s s (\<not> b)\<close> ]
        by (induct b)  auto
      have lens: "\<^bold>|s (\<not> b)\<^sup><\<inverse> s b\<^bold>| = \<^bold>|s (\<not> b)\<^bold>| - \<^bold>|s b\<^bold>|" "\<^bold>|p (\<not> b) \<inverse>\<^sup>> p b\<^bold>| = \<^bold>|p b\<^bold>| - \<^bold>|p (\<not> b)\<^bold>|"
        unfolding rq_len lq_len by simp_all
      have "\<^bold>|p (\<not> b)\<^bold>| \<le> \<^bold>|p b\<^bold>|"
        using lenarg[OF eq_sym[of "(\<not> b)"]] suf_len[OF \<open>s b \<le>s s (\<not> b)\<close>]
        unfolding \<open>ee b = 0\<close> lenmorph pow_len bool_simps by force
      hence "p (\<not> b) \<le>p p b"
        using eq_le_pref[OF eq_sym, of "\<not> b", unfolded bool_simps] by blast
      have w2: "word (over X True) = p (\<not> b)\<inverse>\<^sup>> p b"
        unfolding X_def IniPresol_def gpcp_simps'
        using \<open>\<^bold>|p (\<not> b)\<^bold>| \<le> \<^bold>|p b\<^bold>|\<close> \<open>p (\<not> b) \<le>p p b\<close> long_pref[OF \<open>p (\<not> b) \<le>p p b\<close>]
        by (induct b) auto
      have "\<^bold>|p b\<^bold>|  + \<^bold>|s b\<^bold>| = \<^bold>|p (\<not> b)\<^bold>| + ee (\<not> b) * \<^bold>|rr (\<not> b)\<^bold>| + \<^bold>|s (\<not> b)\<^bold>|"
        using lenarg[OF eq_sym[of "\<not> b"]]
        unfolding lenmorph pow_len \<open>ee b = 0\<close> bool_simps by presburger
      hence "\<^bold>|p b\<^bold>| - \<^bold>|p (\<not> b)\<^bold>| = \<^bold>|s (\<not> b)\<^bold>| + ee (\<not> b) * \<^bold>|rr (\<not> b)\<^bold>| - \<^bold>|s b\<^bold>|"
        by force
      hence "\<^bold>|p (\<not> b)\<inverse>\<^sup>>p b\<^bold>| > \<^bold>|s (\<not> b)\<^sup><\<inverse>s b\<^bold>|"
        unfolding lens using \<open>ee (\<not> b) \<noteq> 0\<close> mroot_len[of "\<not> b"]
          \<open>\<^bold>|s b\<^bold>| \<le> \<^bold>|s (\<not> b)\<^bold>|\<close> by (simp add: diff_less_mono mroot_nemp)
      from this[folded w1 w2]
      show False
        using c_def c2 unfolding NextCand_indicator_def X_def by simp
    qed
  qed
  have "p d \<cdot> (rr d \<cdot> rr d \<^sup>@ (ee d - 1)) \<cdot> s d = p (\<not> d) \<cdot> rr (\<not> d) \<^sup>@ ee (\<not> d) \<cdot> s (\<not> d)"
    using eq_sym unfolding rassoc pow_Suc[symmetric] Suc_minus[OF \<open>ee d \<noteq> 0\<close>].
  thus "(p d \<cdot> rr d) \<cdot> rr d \<^sup>@ (ee d - 1) \<cdot> s d = p (\<not> d) \<cdot> rr (\<not> d) \<^sup>@ ee (\<not> d) \<cdot> s (\<not> d)"
    unfolding rassoc.
qed

\<comment> \<open>We prove correctness and useful lemmas for @{term "NextCand"} used in proving correctness and properties of
 fun  @{term "FindSolu"}  in section FindSolu. We begin with lemmas proving that counter values
correspond the iteration of  @{term "NextCand"} \<close>

lemma NextCand_step_count:
  shows "count (NextCand X) True + count (NextCand X) False = Suc (count X True + count X False) "
proof-
  define a where "a = NextCand_indicator (over X)"
  define b where "b = ((\<not> row ((over X) a)) = a)"
  have "count (NextCand X) = (count X)(b := Suc (count X b))"
    unfolding b_def NextCand_def  a_def Let_def case_prod_unfold select_convs by blast
  hence "count (NextCand X) b = Suc (count X b)"
    "count (NextCand X) (\<not> b) = count X (\<not> b)"
    by fastforce+
  hence "count (NextCand X) b + count (NextCand X) (\<not> b) = Suc (count X b)+ count X (\<not> b)"
    by presburger
  thus ?thesis
    by (induct b, simp_all)
qed

lemma NextCand_steps_count:
  shows "count ((NextCand^^k) X) True + count ((NextCand^^k) X) False = (count X True + count X False) + k"
  by (induct k, force, auto simp: NextCand_step_count)

\<comment> \<open>Lemma  @{term "NextCand_rootsol"} shows that  @{term "NextCand"} finds the rootsolutions.
It also implies a test for rootsolution which is  based of checking  overflows equal and
valid True. In other words, we do not have to check equality of words total words, but only
the short overflow words.
 \<close>

lemma NextCand_rootsol:
  fixes p1 p2 s1 s2 e1 e2 f
  defines "X \<equiv> (NextCand^^(e1 + e2)) ((IniPresol p1 p2 s1 s2)\<lparr>count := f\<rparr>)"
  assumes "p1 \<cdot> gr\<^sup>@e1 \<cdot> s1 = p2 \<cdot> hr\<^sup>@e2 \<cdot> s2"
  shows
    "count X = (\<lambda> x. (if x then f True + e1 else f False + e2))"
    "over X True = over X False"
    "valid X"
  using assms
proof (atomize(full), induct "e1 + e2" arbitrary: e1 e2 p1 p2 s1 s2 X f)
  case 0
  show ?case
  proof (unfold conj_assoc[symmetric], (rule conjI)+)
    have "e1 = 0" and "e2 = 0"
      using  \<open>0 = e1 + e2\<close> by force+
    have "p1 \<cdot> s1 = p2 \<cdot> s2"
      using \<open>p1 \<cdot> gr \<^sup>@ e1 \<cdot> s1 = p2 \<cdot> hr \<^sup>@ e2 \<cdot> s2\<close>
      unfolding \<open>e1 = 0\<close> \<open>e2 = 0\<close> cow_simps.
    have equiv: "\<^bold>|p2\<^bold>| \<le> \<^bold>|p1\<^bold>| \<longleftrightarrow> \<^bold>|s1\<^bold>| \<le> \<^bold>|s2\<^bold>|"
      using \<open>p1 \<cdot> s1 = p2 \<cdot> s2\<close>
      by (simp add: eq_len_iff)
    have comps: "p1 \<bowtie> p2 \<and> s1 \<bowtie>\<^sub>s s2"
      using \<open>p1 \<cdot> s1 = p2 \<cdot> s2\<close>
      by (metis eqd_comp ruler_suf'' sufI)
    have ifs: "(if \<^bold>|s1\<^bold>| \<le> \<^bold>|s2\<^bold>| then p2\<inverse>\<^sup>>p1 else p1\<inverse>\<^sup>>p2) = (if \<^bold>|s2\<^bold>| \<le> \<^bold>|s1\<^bold>| then s1\<^sup><\<inverse>s2  else s2\<^sup><\<inverse>s1)"
    proof (cases " \<^bold>|s1\<^bold>| \<le> \<^bold>|s2\<^bold>|")
      assume "\<^bold>|s1\<^bold>| \<le> \<^bold>|s2\<^bold>|"
      with eqd[reversed, OF \<open>p1 \<cdot> s1 = p2 \<cdot> s2\<close> this]
      show ?thesis
        by force
    next
      assume "\<not> \<^bold>|s1\<^bold>| \<le> \<^bold>|s2\<^bold>|"
      hence " \<^bold>|s2\<^bold>| \<le> \<^bold>|s1\<^bold>|"
        by fastforce
      with eqd[reversed, OF \<open>p1 \<cdot> s1 = p2 \<cdot> s2\<close>[symmetric] this]
      show ?thesis
        by force
    qed
    let ?X = "(NextCand ^^ (e1 + e2)) (IniPresol p1 p2 s1 s2 \<lparr>count := f\<rparr>)"
    show "over ?X True = over ?X False" and  "valid  ?X"
      unfolding IniPresol_def select_convs  gpcp_simps equiv comps ifs funpow_0 "0.hyps"(1)[symmetric]
        count_irrelevant
      using comps "0.hyps" by simp_all
    show "count ?X = (\<lambda> x. (if x then f True + e1 else f False + e2))"
      unfolding funpow_0 "0.hyps"(1)[symmetric] unfolding \<open>e1 = 0\<close> \<open>e2 = 0\<close>
      by auto
  qed
next
  case (Suc x)
  define I where "I = (IniPresol p1 p2 s1 s2)\<lparr>count := f\<rparr>"
  have "e1 + e2 \<noteq> 0"
    using Suc.hyps(2) by linarith
  note basics = NextCand_basic[of "\<lambda> x. (if x then p1 else p2)" True "\<lambda> x. (if x then e1 else e2)" "\<lambda> x. (if x then s1 else s2)" f,
      unfolded gpcp_simps, OF Suc.prems this]
  show ?case
  proof (cases "NextCand_indicator (over I)" "row (over I (NextCand_indicator (over I)))" rule: case_split[OF case_split case_split])
    assume side: "NextCand_indicator (over I)" (is ?c) \<comment> \<open>left side is extended\<close> and
      row: "\<not> row (over I (NextCand_indicator (over I)))" (is "\<not> ?b") \<comment> \<open>first row is extended\<close>
    have row': "(?b \<noteq> ?c) = True"
      using row side by simp_all
    from NextCand_step1[OF side[unfolded I_def] row[unfolded I_def], folded I_def]
    have next1: "NextCand I = IniPresol (p1 \<cdot> gr) p2 s1 s2\<lparr>count := f(True := Suc (f True))\<rparr>".
    from basics
    have "e1 \<noteq> 0" "(p1 \<cdot> gr) \<cdot> gr \<^sup>@ (e1 - 1) \<cdot> s1 = p2 \<cdot> hr \<^sup>@ e2 \<cdot> s2"
      unfolding row'[unfolded I_def] gpcp_simps.
    have "x = (e1 - 1) + e2"
      using Suc.hyps(2) \<open>e1 \<noteq> 0\<close> by linarith
    have ind_step: "(NextCand ^^ (e1 + e2)) I = (NextCand ^^ (e1 - 1 + e2)) (NextCand I)"
      unfolding Suc.hyps(2)[symmetric] \<open>x = e1 - 1 + e2\<close>[symmetric] compow_Suc'..
    from "Suc.hyps"(2) Suc.hyps(1)[OF \<open>x = (e1 - 1) + e2\<close>  \<open>(p1 \<cdot> gr) \<cdot> gr \<^sup>@ (e1 - 1) \<cdot> s1 = p2 \<cdot> hr \<^sup>@ e2 \<cdot> s2\<close>,
        of "\<lambda> x. (if x then 0 else 0)"]
    have
      count_hyps: "count ((NextCand ^^ (e1 - 1 + e2)) (IniPresol (p1 \<cdot> gr) p2 s1 s2\<lparr>count := \<lambda> x. (if x then 0 else 0)\<rparr>)) =
       (\<lambda>x. if x then (e1 - 1) else e2)" and
      over_hyps: "over ((NextCand ^^ (e1 - 1 + e2)) (IniPresol (p1 \<cdot> gr) p2 s1 s2)) True =
                  over ((NextCand ^^ (e1 - 1 + e2)) (IniPresol (p1 \<cdot> gr) p2 s1 s2)) False " and
      valid_hyps: "valid ((NextCand ^^ (e1 - 1 + e2)) (IniPresol (p1 \<cdot> gr) p2 s1 s2))"
      unfolding  count_irrelevant ini_remove_count add_0 gpcp_simps by simp_all
    have func: "(f(True := Suc (f True))) x + (if x then e1 - 1 else e2) = (if x then f True + e1 else f False + e2)" for x
      by (induct x, unfold gpcp_simps)
        (use \<open>e1 \<noteq> 0\<close> in force)+
    show ?thesis
      unfolding I_def[symmetric] ind_step  next1  count_irrelevant
    proof (unfold conj_assoc[symmetric], (rule conjI)+)
      show "count ((NextCand ^^ (e1 - 1 + e2)) (IniPresol (p1 \<cdot> gr) p2 s1 s2\<lparr>count := f(True := Suc (f True))\<rparr>)) =
           (\<lambda>x. if x then f True + e1 else f False + e2)"
        using count_additive[of "e1 - 1 + e2" "f(True := Suc (f True))" "p1 \<cdot> gr" p2 s1 s2]
        unfolding count_hyps[unfolded ini_remove_count] func.
    qed fact+
  next
    assume side:"NextCand_indicator (over I)" (is ?c) \<comment> \<open>left side is extended\<close> and
      row: "row (over I (NextCand_indicator (over I)))" (is ?b) \<comment> \<open>second row is extended\<close>
    have row': "(?b \<noteq> ?c) = False"
      using row side by simp
    from NextCand_step2[OF side[unfolded I_def] row[unfolded I_def], folded I_def]
    have next1: "NextCand I = IniPresol p1  (p2\<cdot>hr) s1 s2\<lparr>count := f(False := Suc (f False))\<rparr>".
    from basics
    have "e2 \<noteq> 0" "p1  \<cdot> gr \<^sup>@ e1 \<cdot> s1 = (p2 \<cdot>hr) \<cdot> hr \<^sup>@ (e2 - 1) \<cdot> s2 "
      unfolding row'[unfolded I_def] gpcp_simps by auto
    have "x = e1 + (e2 - 1)"
      using Suc.hyps(2) \<open>e2 \<noteq> 0\<close> by linarith
    have ind_step: "(NextCand ^^ (e1 + e2)) I = (NextCand ^^ (e1 + (e2 - 1))) (NextCand I)"
      unfolding Suc.hyps(2)[symmetric] \<open>x = e1 + (e2 - 1)\<close>[symmetric] compow_Suc'..
    from "Suc.hyps"(2) Suc.hyps(1)[OF \<open>x = e1 + (e2 - 1)\<close>  \<open>p1 \<cdot> gr \<^sup>@ e1 \<cdot> s1 = (p2 \<cdot> hr) \<cdot> hr \<^sup>@ (e2 - 1) \<cdot> s2\<close>,
        of "\<lambda> x. (if x then 0 else 0)"]
    have
      count_hyps: "count ((NextCand ^^ (e1 + (e2 - 1))) (IniPresol p1 (p2\<cdot>hr)  s1 s2\<lparr>count := \<lambda> x. (if x then 0 else 0)\<rparr>)) =
       (\<lambda>x. if x then e1 else e2 - 1)" and
      over_hyps: "over ((NextCand ^^ (e1  + (e2 - 1))) (IniPresol p1 (p2\<cdot>hr) s1 s2)) True =
                  over ((NextCand ^^ (e1 + (e2 - 1))) (IniPresol p1 (p2\<cdot>hr) s1 s2)) False " and
      valid_hyps: "valid ((NextCand ^^ (e1 + (e2 - 1))) (IniPresol p1 (p2\<cdot>hr) s1 s2))"
      unfolding  count_irrelevant ini_remove_count add_0 gpcp_simps by simp_all
    have func: "(f(False := Suc (f False))) x + (if x then e1 else e2 - 1) = (if x then f True + e1 else f False + e2)" for x
      by (induct x, unfold gpcp_simps)
        (use \<open>e2 \<noteq> 0\<close> in force)+
    show ?thesis
      unfolding I_def[symmetric] ind_step  next1  count_irrelevant
    proof (unfold conj_assoc[symmetric], (rule conjI)+)
      show "count ((NextCand ^^ (e1 + (e2 - 1))) (IniPresol p1 (p2\<cdot>hr) s1 s2\<lparr>count := f(False := Suc (f False))\<rparr>)) =
           (\<lambda>x. if x then f True + e1 else f False + e2)"
        using count_additive[of "e1 + (e2 - 1)" "f(False := Suc (f False))" p1 "p2\<cdot>hr" s1 s2]
        unfolding count_hyps[unfolded ini_remove_count] func.
    qed fact+
  next
    assume side:"\<not> NextCand_indicator (over I)"  (is "\<not> ?c")  \<comment> \<open>right side is extended\<close> and
      row:"row (over I (NextCand_indicator (over I)))" (is ?b) \<comment> \<open>first row is extended\<close>
    have row': "(?b \<noteq> ?c) = True"
      using row side by simp_all
    from NextCand_step3[OF side[unfolded I_def] row[unfolded I_def], folded I_def]
    have next1: "NextCand I = IniPresol p1  p2 (gr\<cdot> s1) s2\<lparr>count := f(True := Suc (f True))\<rparr>".
    from basics
    have "0 < e1" "(p1 \<cdot> gr) \<cdot> gr \<^sup>@ (e1 -1) \<cdot> s1 = p2 \<cdot> hr \<^sup>@ e2 \<cdot> s2"
      unfolding row'[unfolded I_def] gpcp_simps by simp_all
    hence "p1 \<cdot> gr \<^sup>@ (e1 - 1) \<cdot> (gr \<cdot> s1) = p2 \<cdot> hr \<^sup>@ e2 \<cdot> s2"
      using lassoc  pow_pos[OF \<open>0 < e1\<close>]  pow_pos'[OF \<open>0 < e1\<close>]
      by metis
    have "x = (e1 - 1) + e2"
      using Suc.hyps(2) \<open>0 < e1\<close> by linarith
    have ind_step: "(NextCand ^^ (e1 + e2)) I = (NextCand ^^ (e1 - 1 + e2)) (NextCand I)"
      unfolding Suc.hyps(2)[symmetric] \<open>x = e1 - 1 + e2\<close>[symmetric] compow_Suc'..
    from "Suc.hyps"(2) Suc.hyps(1)[OF \<open>x = (e1 - 1) + e2\<close>  \<open>p1 \<cdot> gr \<^sup>@ (e1 - 1) \<cdot>(gr \<cdot> s1) = p2 \<cdot> hr \<^sup>@ e2 \<cdot> s2\<close>,
        of "\<lambda> x. (if x then 0 else 0)"]
    have
      count_hyps: "count ((NextCand ^^ (e1 - 1 + e2)) (IniPresol p1 p2 (gr \<cdot> s1) s2\<lparr>count := \<lambda> x. (if x then 0 else 0)\<rparr>)) =
       (\<lambda>x. if x then (e1 - 1) else e2)" and
      over_hyps: "over ((NextCand ^^ (e1 - 1 + e2)) (IniPresol p1 p2 (gr \<cdot> s1) s2)) True =
                  over ((NextCand ^^ (e1 - 1 + e2)) (IniPresol p1  p2 (gr \<cdot> s1) s2)) False " and
      valid_hyps: "valid ((NextCand ^^ (e1 - 1 + e2)) (IniPresol p1 p2 (gr \<cdot> s1) s2))"
      unfolding  count_irrelevant ini_remove_count add_0 gpcp_simps by simp_all
    have func: "(f(True := Suc (f True))) x + (if x then e1 - 1 else e2) = (if x then f True + e1 else f False + e2)" for x
      by (induct x, unfold gpcp_simps)
        (use \<open>0 < e1\<close> in force)+
    show ?thesis
      unfolding I_def[symmetric] ind_step  next1  count_irrelevant
    proof (unfold conj_assoc[symmetric], (rule conjI)+)
      show "count ((NextCand ^^ (e1 - 1 + e2)) (IniPresol p1 p2 (gr \<cdot> s1) s2\<lparr>count := f(True := Suc (f True))\<rparr>)) =
           (\<lambda>x. if x then f True + e1 else f False + e2)"
        using count_additive[of "e1 - 1 + e2" "f(True := Suc (f True))" p1 p2 "gr \<cdot> s1" s2]
        unfolding count_hyps[unfolded ini_remove_count] func.
    qed fact+
  next
    assume side:"\<not> NextCand_indicator (over I)"  (is "\<not> ?c") \<comment> \<open>right side is extended\<close> and
      row:"\<not> row (over I (NextCand_indicator (over I)))" (is "\<not> ?b") \<comment> \<open>second row is extended\<close>
    have row': "(?b \<noteq> ?c) = False"
      using row side by simp
    from NextCand_step4[OF side[unfolded I_def] row[unfolded I_def], folded I_def]
    have next1: "NextCand I = IniPresol p1 p2 s1 (hr\<cdot>s2) \<lparr>count := f(False := Suc (f False))\<rparr>".
    from basics
    have "0 < e2" "p1  \<cdot> gr \<^sup>@ e1 \<cdot> s1 = (p2 \<cdot>hr) \<cdot> hr \<^sup>@ (e2 - 1) \<cdot> s2 "
      unfolding row'[unfolded I_def] gpcp_simps by auto
    hence "p1  \<cdot> gr \<^sup>@ e1 \<cdot> s1 = p2 \<cdot> hr \<^sup>@ (e2 - 1) \<cdot> (hr\<cdot>s2) "
      using lassoc pow_pos[OF \<open>0 < e2\<close>]  pow_pos' by metis
    have "x = e1 + (e2 - 1)"
      using Suc.hyps(2) \<open>0 < e2\<close> by linarith
    have ind_step: "(NextCand ^^ (e1 + e2)) I = (NextCand ^^ (e1 + (e2 - 1))) (NextCand I)"
      unfolding Suc.hyps(2)[symmetric] \<open>x = e1 + (e2 - 1)\<close>[symmetric] compow_Suc'..
    from "Suc.hyps"(2) Suc.hyps(1)[OF \<open>x = e1 + (e2 - 1)\<close>  \<open>p1 \<cdot> gr \<^sup>@ e1 \<cdot> s1 = p2  \<cdot> hr \<^sup>@ (e2 - 1) \<cdot> (hr\<cdot>s2)\<close>,
        of "\<lambda> x. (if x then 0 else 0)"]
    have
      count_hyps: "count ((NextCand ^^ (e1 + (e2 - 1))) (IniPresol p1 p2 s1 (hr\<cdot>s2)\<lparr>count := \<lambda> x. (if x then 0 else 0)\<rparr>)) =
       (\<lambda>x. if x then e1 else e2 - 1)" and
      over_hyps: "over ((NextCand ^^ (e1  + (e2 - 1))) (IniPresol p1 p2 s1 (hr\<cdot>s2))) True =
                  over ((NextCand ^^ (e1 + (e2 - 1))) (IniPresol p1 p2 s1 (hr\<cdot>s2))) False " and
      valid_hyps: "valid ((NextCand ^^ (e1 + (e2 - 1))) (IniPresol p1 p2 s1 (hr\<cdot>s2)))"
      unfolding  count_irrelevant ini_remove_count add_0 gpcp_simps by simp_all
    have func: "(f(False := Suc (f False))) x + (if x then e1 else e2 - 1) = (if x then f True + e1 else f False + e2)" for x
      by (induct x, unfold gpcp_simps)
        (use \<open>0 < e2\<close> in force)+
    show ?thesis
      unfolding I_def[symmetric] ind_step  next1  count_irrelevant
    proof (unfold conj_assoc[symmetric], (rule conjI)+)
      show "count ((NextCand ^^ (e1 + (e2 - 1))) (IniPresol p1 p2 s1 (hr\<cdot>s2)\<lparr>count := f(False := Suc (f False))\<rparr>)) =
           (\<lambda>x. if x then f True + e1 else f False + e2)"
        using count_additive[of "e1 + (e2 - 1)" "f(False := Suc (f False))" p1 p2 s1 "hr\<cdot>s2"]
        unfolding count_hyps[unfolded ini_remove_count] func.
    qed fact+
  qed
qed

lemma NextCand_correct :
  fixes p1 p2 s1 s2 m n
  defines "P \<equiv> (NextCand^^(m+n))(IniPresol p1 p2 s1 s2)"
  assumes "p1\<cdot> gr\<^sup>@m \<cdot>s1 = p2\<cdot> hr\<^sup>@n \<cdot>s2"
  shows "valid P" and "over P True = over P False"
  unfolding P_def
  using NextCand_rootsol(2-3)[OF assms(2), of "\<lambda> _. 0", unfolded select_convs ini_remove_count] by blast+

\<comment> \<open>Lemma  @{term "NextCand_computation_gen"} shows how iteration of the   @{term "NextCand"}
can be expressed using the  @{term "IniPresol"}.
 \<close>

lemma NextCand_computation_gen:
  "\<exists> ep1 ep2 es1 es2. m = ep1 + ep2 + es1 + es2 \<and>
    count ((NextCand^^m) ((IniPresol p1 p2 s1 s2) \<lparr>count := \<lambda>x. if x then c1 else c2\<rparr>) )
                    = (\<lambda> x. (if x then c1+ep1 + es1 else c2+ep2 + es2)) \<and>
    over ((NextCand^^m) ((IniPresol p1 p2 s1 s2) \<lparr>count := \<lambda>x. if x then c1 else c2\<rparr>))
          = over ((IniPresol (p1 \<cdot> gr\<^sup>@ep1) (p2 \<cdot>hr\<^sup>@ep2) (gr\<^sup>@es1 \<cdot> s1) (hr\<^sup>@es2\<cdot> s2)))  \<and>
    valid ((NextCand^^m) ((IniPresol p1 p2 s1 s2) \<lparr>count := \<lambda>x. if x then c1 else c2\<rparr>))
           = ((p1 \<cdot> gr\<^sup>@ep1) \<bowtie> (p2 \<cdot>hr\<^sup>@ep2) \<and> (gr\<^sup>@es1 \<cdot> s1) \<bowtie>\<^sub>s (hr\<^sup>@es2\<cdot> s2)) \<and>
    (NextCand^^m) ((IniPresol p1 p2 s1 s2) \<lparr>count := \<lambda>x. if x then c1 else c2 \<rparr>)
       = (IniPresol (p1 \<cdot> gr\<^sup>@ep1) (p2 \<cdot>hr\<^sup>@ep2) (gr\<^sup>@es1 \<cdot> s1)  (hr\<^sup>@es2\<cdot> s2))\<lparr>count := (\<lambda> x. (if x then c1+ep1 + es1 else c2 + ep2 + es2))\<rparr>"
proof (induct "m" arbitrary: p1 p2 s1 s2 c1 c2)
  case 0
  thus ?case
    using "0.prems"[of 0 0 0 0 "c1" "c2"] unfolding IniPresol_def funpow_0 select_convs by force
next
  case (Suc m)
  define X where "X = (IniPresol p1 p2 s1 s2)\<lparr>count := \<lambda>x. if x then c1 else c2\<rparr>"
  show ?case
  proof (cases "NextCand_indicator (over X)" "row (over X (NextCand_indicator (over X)))" rule: case_split[OF case_split case_split])
    assume case_assm: "NextCand_indicator (over X)"
      "\<not> row (over X (NextCand_indicator (over X)))"
    from Suc.hyps[of "Suc c1" c2 "p1 \<cdot> gr" p2 s1 s2]
    obtain ep1 ep2 es1 es2 where
      "m = ep1 + ep2 + es1 + es2" and
      vals: "count ((NextCand ^^ m) ((IniPresol (p1 \<cdot> gr) p2 s1 s2) \<lparr>count := \<lambda>x. if x then Suc c1 else c2\<rparr>)) =
              (\<lambda>x. if x then Suc c1 + ep1 + es1 else c2 +ep2 + es2)"
      "over ((NextCand ^^ m) ((IniPresol (p1 \<cdot> gr) p2 s1 s2) \<lparr>count := \<lambda>x. if x then Suc c1 else c2\<rparr>)) =
             over ((IniPresol ((p1 \<cdot> gr) \<cdot> gr \<^sup>@ ep1) (p2 \<cdot> hr \<^sup>@ ep2) (gr \<^sup>@ es1 \<cdot> s1) (hr \<^sup>@ es2 \<cdot> s2)))"
      "valid ((NextCand ^^ m) ((IniPresol (p1 \<cdot> gr) p2 s1 s2) \<lparr>count := \<lambda>x. if x then Suc c1 else c2\<rparr>)) =
             ((p1 \<cdot> gr) \<cdot> gr \<^sup>@ ep1 \<bowtie> p2 \<cdot> hr \<^sup>@ ep2 \<and> gr \<^sup>@ es1 \<cdot> s1 \<bowtie>\<^sub>s hr \<^sup>@ es2 \<cdot> s2)"
      "(NextCand ^^ m) ((IniPresol (p1 \<cdot> gr) p2 s1 s2) \<lparr>count := \<lambda>x. if x then Suc c1 else c2\<rparr>) =
            (IniPresol ((p1 \<cdot> gr) \<cdot> gr \<^sup>@ ep1) (p2 \<cdot> hr \<^sup>@ ep2) (gr \<^sup>@ es1 \<cdot> s1) (hr \<^sup>@ es2 \<cdot> s2)
            \<lparr>count := \<lambda>x. if x then Suc c1 + ep1 + es1 else c2 + ep2 + es2\<rparr>)"
      by blast
    show ?thesis
    proof (unfold conj_assoc[symmetric], (rule exI)+, (rule conjI)+)
      show "Suc m = (Suc ep1) + ep2 + es1 + es2"
        using \<open>m = ep1 + ep2 + es1 + es2\<close> by auto
      have "p1 \<cdot>gr\<cdot> gr \<^sup>@ ep1 = p1 \<cdot> gr \<^sup>@ Suc ep1"
        by simp
      have "NextCand ((IniPresol p1 p2 s1 s2) \<lparr>count := \<lambda>x. if x then c1 else c2\<rparr>) = (IniPresol (p1\<cdot>gr) p2 s1 s2)
           \<lparr>count := \<lambda>x. if x then Suc c1 else c2\<rparr> "
        using case_assm NextCand_step1[of "\<lambda>x. if x then c1 else c2" p1 p2 s1 s2 ]  X_def by force
      hence fstep:"(NextCand ^^ Suc m) ((IniPresol p1 p2 s1 s2) \<lparr>count := \<lambda>x. if x then c1 else c2\<rparr>)
          = (NextCand ^^m)((IniPresol (p1\<cdot>gr) p2 s1 s2) \<lparr>count := \<lambda>x. if x then Suc c1 else c2\<rparr>)"
        using compow_Suc' by metis
      thus nsuc:"(NextCand^^Suc m) ((IniPresol p1 p2 s1 s2) \<lparr>count := \<lambda>x. if x then c1 else c2 \<rparr>)
           = (IniPresol (p1 \<cdot> gr\<^sup>@(Suc ep1)) (p2 \<cdot>hr\<^sup>@ep2) (gr\<^sup>@es1 \<cdot> s1)  (hr\<^sup>@es2\<cdot> s2))\<lparr>count := (\<lambda> x. (if x then c1+(Suc ep1) + es1 else c2 + ep2 + es2))\<rparr>"
        using vals(4) unfolding \<open>p1 \<cdot>gr\<cdot> gr \<^sup>@ ep1 = p1 \<cdot> gr \<^sup>@ Suc ep1\<close>
        by fastforce
      thus "count ((NextCand^^Suc m) ((IniPresol p1 p2 s1 s2) \<lparr>count := \<lambda>x. if x then c1 else c2\<rparr>) )
             = (\<lambda> x. (if x then c1+(Suc ep1) + es1 else c2+ep2 + es2))"
        by simp
      show " over ((NextCand^^Suc m) ((IniPresol p1 p2 s1 s2) \<lparr>count := \<lambda>x. if x then c1 else c2\<rparr>))
          = over ((IniPresol (p1 \<cdot> gr\<^sup>@(Suc ep1)) (p2 \<cdot>hr\<^sup>@ep2) (gr\<^sup>@es1 \<cdot> s1) (hr\<^sup>@es2\<cdot> s2)))"
        using nsuc  by simp
      show "valid ((NextCand^^Suc m) ((IniPresol p1 p2 s1 s2) \<lparr>count := \<lambda>x. if x then c1 else c2\<rparr>))
           = ((p1 \<cdot> gr\<^sup>@Suc ep1) \<bowtie> (p2 \<cdot>hr\<^sup>@ep2) \<and> (gr\<^sup>@es1 \<cdot> s1) \<bowtie>\<^sub>s (hr\<^sup>@es2\<cdot> s2))"
        using vals(3) nsuc
        using fstep vals(3) by auto
    qed
  next
    assume case_assm: "NextCand_indicator (over X)"
      "row (over X (NextCand_indicator (over X)))"
    from Suc.hyps[of c1 "Suc c2" p1 "p2\<cdot>hr" s1 s2]
    obtain ep1 ep2 es1 es2 where
      "m = ep1 + ep2 + es1 + es2" and
      vals: "count ((NextCand ^^ m) ((IniPresol p1 (p2\<cdot>hr) s1 s2) \<lparr>count := \<lambda>x. if x then c1 else Suc c2\<rparr>)) =
            (\<lambda>x. if x then  c1 + ep1 + es1 else  Suc c2 +ep2 + es2)"
      "over ((NextCand ^^ m) ((IniPresol p1 (p2\<cdot>hr) s1 s2) \<lparr>count := \<lambda>x. if x then c1 else  Suc c2\<rparr>)) =
            over ((IniPresol (p1 \<cdot> gr \<^sup>@ ep1) ((p2\<cdot>hr) \<cdot> hr \<^sup>@ ep2) (gr \<^sup>@ es1 \<cdot> s1) (hr \<^sup>@ es2 \<cdot> s2)))"
      "valid ((NextCand ^^ m) ((IniPresol p1   (p2 \<cdot> hr) s1 s2) \<lparr>count := \<lambda>x. if x then  c1 else Suc c2\<rparr>)) =
            (p1 \<cdot> gr \<^sup>@ ep1 \<bowtie> (p2\<cdot>hr)  \<cdot> hr \<^sup>@ ep2 \<and> gr \<^sup>@ es1 \<cdot> s1 \<bowtie>\<^sub>s hr \<^sup>@ es2 \<cdot> s2)"
      "(NextCand ^^ m) ((IniPresol p1   (p2 \<cdot> hr) s1 s2) \<lparr>count := \<lambda>x. if x then c1 else  Suc c2\<rparr>) =
            (IniPresol (p1 \<cdot> gr \<^sup>@ ep1) ( (p2\<cdot>hr) \<cdot> hr \<^sup>@ ep2) (gr \<^sup>@ es1 \<cdot> s1) (hr \<^sup>@ es2 \<cdot> s2)
            \<lparr>count := \<lambda>x. if x then c1 + ep1 + es1 else  Suc c2 + ep2 + es2\<rparr>)"
      by blast
    show ?thesis
    proof (unfold conj_assoc[symmetric], (rule exI)+, (rule conjI)+)
      show "Suc m = ep1 + (Suc ep2) + es1 + es2"
        using \<open>m = ep1 + ep2 + es1 + es2\<close> by auto
      have "p2 \<cdot>hr\<cdot> hr \<^sup>@ ep2 = p2 \<cdot> hr \<^sup>@ Suc ep2"
        by simp
      have "NextCand ((IniPresol p1 p2 s1 s2) \<lparr>count := \<lambda>x. if x then c1 else c2\<rparr>) = (IniPresol p1 (p2\<cdot>hr) s1 s2)
            \<lparr>count := \<lambda>x. if x then  c1 else Suc c2\<rparr> "
        using case_assm NextCand_step2[of "\<lambda>x. if x then c1 else c2" p1 p2 s1 s2 ]  X_def by force
      hence fstep:"(NextCand ^^ Suc m) ((IniPresol p1 p2 s1 s2) \<lparr>count := \<lambda>x. if x then c1 else c2\<rparr>)
            = (NextCand ^^m)((IniPresol p1  (p2\<cdot>hr) s1 s2) \<lparr>count := \<lambda>x. if x then c1 else Suc  c2\<rparr>)"
        using compow_Suc' by metis
      thus nsuc:"(NextCand^^Suc m) ((IniPresol p1 p2 s1 s2) \<lparr>count := \<lambda>x. if x then c1 else c2 \<rparr>)
           = (IniPresol (p1 \<cdot> gr\<^sup>@ ep1) (p2 \<cdot>hr\<^sup>@(Suc ep2)) (gr\<^sup>@es1 \<cdot> s1)  (hr\<^sup>@es2\<cdot> s2))\<lparr>count := (\<lambda> x. (if x then c1+ ep1 + es1 else c2 + (Suc ep2) + es2))\<rparr>"
        using vals(4) unfolding \<open>p2 \<cdot>hr\<cdot> hr \<^sup>@ ep2 = p2 \<cdot> hr \<^sup>@ Suc ep2\<close>
        by fastforce
      thus "count ((NextCand^^Suc m) ((IniPresol p1 p2 s1 s2) \<lparr>count := \<lambda>x. if x then c1 else c2\<rparr>) )
                    = (\<lambda> x. (if x then c1+ ep1 + es1 else c2+(Suc ep2) + es2))"
        by simp
      show " over ((NextCand^^Suc m) ((IniPresol p1 p2 s1 s2) \<lparr>count := \<lambda>x. if x then c1 else c2\<rparr>))
          = over ((IniPresol (p1 \<cdot> gr\<^sup>@ ep1) (p2 \<cdot>hr\<^sup>@(Suc ep2)) (gr\<^sup>@es1 \<cdot> s1) (hr\<^sup>@es2\<cdot> s2)))"
        using nsuc  by simp
      show "valid ((NextCand^^Suc m) ((IniPresol p1 p2 s1 s2) \<lparr>count := \<lambda>x. if x then c1 else c2\<rparr>))
           = ((p1 \<cdot> gr\<^sup>@ ep1) \<bowtie> (p2 \<cdot>hr\<^sup>@Suc ep2) \<and> (gr\<^sup>@es1 \<cdot> s1) \<bowtie>\<^sub>s (hr\<^sup>@es2\<cdot> s2))"
        using vals(3) nsuc
        using fstep vals(3) by auto
    qed
  next
    assume case_assm: "\<not> NextCand_indicator (over X)"
      "row (over X (NextCand_indicator (over X)))"
    from Suc.hyps[of "Suc c1" c2 p1 p2 "gr\<cdot>s1" s2]
    obtain ep1 ep2 es1 es2 where
      "m = ep1 + ep2 + es1 + es2" and
      vals: "count ((NextCand ^^ m) ((IniPresol p1 p2 (gr\<cdot>s1) s2) \<lparr>count := \<lambda>x. if x then Suc c1 else c2\<rparr>)) =
            (\<lambda>x. if x then Suc c1 + ep1 + es1 else  c2 +ep2 + es2)"
      "over ((NextCand ^^ m) ((IniPresol p1 p2 (gr\<cdot>s1) s2) \<lparr>count := \<lambda>x. if x then Suc c1 else  c2\<rparr>)) =
            over ((IniPresol (p1 \<cdot> gr \<^sup>@ ep1) (p2\<cdot> hr \<^sup>@ ep2) (  gr \<^sup>@ es1 \<cdot>(gr\<cdot> s1)) (hr \<^sup>@ es2 \<cdot> s2)))"
      "valid ((NextCand ^^ m) ((IniPresol p1 p2   (gr\<cdot>s1) s2) \<lparr>count := \<lambda>x. if x then Suc c1 else c2\<rparr>)) =
            (p1 \<cdot> gr \<^sup>@ ep1 \<bowtie> p2\<cdot>hr \<^sup>@ ep2 \<and> gr \<^sup>@ es1 \<cdot> (gr\<cdot>s1) \<bowtie>\<^sub>s hr \<^sup>@ es2 \<cdot> s2)"
      "(NextCand ^^ m) ((IniPresol p1   p2   (gr\<cdot>s1) s2) \<lparr>count := \<lambda>x. if x then Suc c1 else  c2\<rparr>) =
            (IniPresol (p1 \<cdot> gr \<^sup>@ ep1) ( p2 \<cdot> hr \<^sup>@ ep2) (gr \<^sup>@ es1 \<cdot> (gr \<cdot> s1)) (hr \<^sup>@ es2 \<cdot> s2)
            \<lparr>count := \<lambda>x. if x then Suc c1 + ep1 + es1 else  c2 + ep2 + es2\<rparr>)"
      by blast
    show ?thesis
    proof (unfold conj_assoc[symmetric], (rule exI)+, (rule conjI)+)
      show "Suc m = ep1 + ep2 + (Suc es1) + es2"
        using \<open>m = ep1 + ep2 + es1 + es2\<close> by auto
      have "gr \<^sup>@ es1 \<cdot> (gr\<cdot>s1) = gr\<^sup>@(Suc es1) \<cdot> s1"
        using pow_Suc'[symmetric] by simp
      have "Suc c1 + ep1 + es1 =  c1 + ep1 + Suc es1"
        by simp
      have "NextCand ((IniPresol p1 p2 s1 s2) \<lparr>count := \<lambda>x. if x then c1 else c2\<rparr>) = (IniPresol p1 p2  (gr\<cdot>s1) s2)
            \<lparr>count := \<lambda>x. if x then Suc c1 else c2\<rparr> "
        using case_assm NextCand_step3[of "\<lambda>x. if x then c1 else c2" p1 p2 s1 s2 ]  X_def by force
      hence fstep:"(NextCand ^^ Suc m) ((IniPresol p1 p2 s1 s2) \<lparr>count := \<lambda>x. if x then c1 else c2\<rparr>)
             = (NextCand ^^m)((IniPresol p1  p2  (gr\<cdot>s1) s2) \<lparr>count := \<lambda>x. if x then Suc c1 else  c2\<rparr>)"
        using compow_Suc' by metis
      thus nsuc:"(NextCand^^Suc m) ((IniPresol p1 p2 s1 s2) \<lparr>count := \<lambda>x. if x then c1 else c2 \<rparr>)
                 = (IniPresol (p1 \<cdot> gr\<^sup>@ ep1) (p2 \<cdot>hr\<^sup>@ep2) (gr\<^sup>@(Suc es1) \<cdot> s1)  (hr\<^sup>@es2\<cdot> s2))
                    \<lparr>count := (\<lambda> x. (if x then c1+ ep1 + (Suc es1) else c2 + ep2 + es2))\<rparr>"
         unfolding vals(4) \<open>gr \<^sup>@ es1 \<cdot> (gr\<cdot>s1) = gr\<^sup>@(Suc es1) \<cdot> s1\<close> \<open>Suc c1 + ep1 + es1 =  c1 + ep1 + Suc es1\<close>.
      thus "count ((NextCand^^Suc m) ((IniPresol p1 p2 s1 s2) \<lparr>count := \<lambda>x. if x then c1 else c2\<rparr>) )
                 = (\<lambda> x. (if x then c1+ ep1 + (Suc es1) else c2+ ep2 + es2))"
        by simp
      show " over ((NextCand^^Suc m) ((IniPresol p1 p2 s1 s2) \<lparr>count := \<lambda>x. if x then c1 else c2\<rparr>))
          = over ((IniPresol (p1 \<cdot> gr\<^sup>@ ep1) (p2 \<cdot>hr\<^sup>@ep2) (gr\<^sup>@(Suc es1) \<cdot> s1) (hr\<^sup>@es2\<cdot> s2)))"
        using nsuc  by simp
      show "valid ((NextCand^^Suc m) ((IniPresol p1 p2 s1 s2) \<lparr>count := \<lambda>x. if x then c1 else c2\<rparr>))
           = ((p1 \<cdot> gr\<^sup>@ ep1) \<bowtie> (p2 \<cdot>hr\<^sup>@ep2) \<and> (gr\<^sup>@(Suc es1) \<cdot> s1) \<bowtie>\<^sub>s (hr\<^sup>@es2\<cdot> s2))"
        using fstep vals(3) nsuc \<open>gr \<^sup>@ es1 \<cdot> (gr\<cdot>s1) = gr\<^sup>@(Suc es1) \<cdot> s1\<close> by metis
    qed
  next
    assume case_assm: "\<not> NextCand_indicator (over X)"
      "\<not> row (over X (NextCand_indicator (over X)))"
    from Suc.hyps[of c1 "Suc c2" p1 p2 s1 "hr\<cdot>s2"]
    obtain ep1 ep2 es1 es2 where
      "m = ep1 + ep2 + es1 + es2" and
      vals: "count ((NextCand ^^ m) ((IniPresol p1 p2 s1 (hr\<cdot> s2)) \<lparr>count := \<lambda>x. if x then c1 else Suc c2\<rparr>)) =
            (\<lambda>x. if x then c1 + ep1 + es1 else  Suc c2 +ep2 + es2)"
      "over ((NextCand ^^ m) ((IniPresol p1 p2  s1 (hr\<cdot> s2)) \<lparr>count := \<lambda>x. if x then c1 else  Suc c2\<rparr>)) =
            over ((IniPresol (p1 \<cdot> gr \<^sup>@ ep1) (p2\<cdot> hr \<^sup>@ ep2) (  gr \<^sup>@es1 \<cdot> s1) (hr \<^sup>@ es2 \<cdot>(hr\<cdot> s2))))"
      "valid ((NextCand ^^ m) ((IniPresol p1 p2   s1 (hr\<cdot> s2)) \<lparr>count := \<lambda>x. if x then c1 else Suc c2\<rparr>)) =
            (p1 \<cdot> gr \<^sup>@ ep1 \<bowtie> p2\<cdot>hr \<^sup>@ ep2 \<and> gr \<^sup>@ es1\<cdot>s1 \<bowtie>\<^sub>s hr \<^sup>@ es2 \<cdot>(hr\<cdot> s2))"
      "(NextCand ^^ m) ((IniPresol p1   p2  s1 (hr\<cdot> s2)) \<lparr>count := \<lambda>x. if x then c1 else Suc c2\<rparr>) =
            (IniPresol (p1 \<cdot> gr \<^sup>@ ep1) ( p2 \<cdot> hr \<^sup>@ ep2) (gr \<^sup>@ es1  \<cdot> s1) (hr \<^sup>@ es2 \<cdot>(hr\<cdot> s2))
            \<lparr>count := \<lambda>x. if x then c1 + ep1 + es1 else  Suc c2 + ep2 + es2\<rparr>)"
      by blast
    show ?thesis
    proof (unfold conj_assoc[symmetric], (rule exI)+, (rule conjI)+)
      show "Suc m = ep1 + ep2 + es1 + Suc es2"
        using \<open>m = ep1 + ep2 + es1 + es2\<close> by auto
      have "hr \<^sup>@ es2 \<cdot> (hr\<cdot>s2) = hr\<^sup>@(Suc es2) \<cdot> s2"
        using pow_Suc'[symmetric] by simp
      have "NextCand ((IniPresol p1 p2 s1 s2) \<lparr>count := \<lambda>x. if x then c1 else c2\<rparr>) = (IniPresol p1 p2  s1 (hr\<cdot>s2))
            \<lparr>count := \<lambda>x. if x then c1 else Suc c2\<rparr> "
        using case_assm NextCand_step4[of "\<lambda>x. if x then c1 else c2" p1 p2 s1 s2 ]  X_def by force
      hence fstep:"(NextCand ^^ Suc m) ((IniPresol p1 p2 s1 s2) \<lparr>count := \<lambda>x. if x then c1 else c2\<rparr>)
             = (NextCand ^^m)((IniPresol p1  p2  s1 (hr\<cdot>s2)) \<lparr>count := \<lambda>x. if x then  c1 else Suc c2\<rparr>)"
        using compow_Suc' by metis
      thus nsuc:"(NextCand^^Suc m) ((IniPresol p1 p2 s1 s2) \<lparr>count := \<lambda>x. if x then c1 else c2 \<rparr>)
                 = (IniPresol (p1 \<cdot> gr\<^sup>@ ep1) (p2 \<cdot>hr\<^sup>@ep2) (gr\<^sup>@es1 \<cdot> s1)  (hr\<^sup>@(Suc es2)\<cdot> s2))
                    \<lparr>count := (\<lambda> x. (if x then c1+ ep1 + es1 else c2 + ep2 + Suc es2))\<rparr>"
        using vals(4) unfolding \<open>hr \<^sup>@ es2 \<cdot> (hr\<cdot>s2) = hr\<^sup>@(Suc es2) \<cdot> s2\<close>  by fastforce
      thus "count ((NextCand^^Suc m) ((IniPresol p1 p2 s1 s2) \<lparr>count := \<lambda>x. if x then c1 else c2\<rparr>) )
                 = (\<lambda> x. (if x then c1+ ep1 + es1 else c2+ ep2 + Suc es2))"
        by simp
      show " over ((NextCand^^Suc m) ((IniPresol p1 p2 s1 s2) \<lparr>count := \<lambda>x. if x then c1 else c2\<rparr>))
          = over ((IniPresol (p1 \<cdot> gr\<^sup>@ ep1) (p2 \<cdot>hr\<^sup>@ep2) (gr\<^sup>@es1 \<cdot> s1) (hr\<^sup>@(Suc es2)\<cdot> s2)))"
        using nsuc  by simp
      show "valid ((NextCand^^Suc m) ((IniPresol p1 p2 s1 s2) \<lparr>count := \<lambda>x. if x then c1 else c2\<rparr>))
           = ((p1 \<cdot> gr\<^sup>@ ep1) \<bowtie> (p2 \<cdot>hr\<^sup>@ep2) \<and> (gr\<^sup>@es1 \<cdot> s1) \<bowtie>\<^sub>s (hr\<^sup>@(Suc es2)\<cdot> s2))"
        using fstep vals(3) nsuc \<open>hr \<^sup>@ es2 \<cdot> (hr\<cdot>s2) = hr\<^sup>@(Suc es2) \<cdot> s2\<close> by metis
    qed
  qed
qed

thm NextCand_computation_gen[of _ 0 0, unfolded ini_remove_count]

\<comment> \<open>Lemma  @{term "NextCand_computation"} shows how iteration of the   @{term "NextCand"}
can be expressed using the  @{term "IniPresol"} with the initial counter values 0 and 0.
This lemma is very useful in what follows. \<close>

lemma NextCand_computation:
  "\<exists> ep1 ep2 es1 es2. m = ep1 + ep2 + es1 + es2 \<and>
    count ((NextCand^^m) ((IniPresol p1 p2 s1 s2)))
                    = (\<lambda> x. (if x then ep1 + es1 else ep2 + es2)) \<and>
    over ((NextCand^^m) (IniPresol p1 p2 s1 s2))
          = over ((IniPresol (p1 \<cdot> gr\<^sup>@ep1) (p2 \<cdot>hr\<^sup>@ep2) (gr\<^sup>@es1 \<cdot> s1) (hr\<^sup>@es2\<cdot> s2)))
  \<and>
    valid ((NextCand^^m) (IniPresol p1 p2 s1 s2))
           = ((p1 \<cdot> gr\<^sup>@ep1) \<bowtie> (p2 \<cdot>hr\<^sup>@ep2) \<and> (gr\<^sup>@es1 \<cdot> s1) \<bowtie>\<^sub>s (hr\<^sup>@es2\<cdot> s2)) \<and>
    (NextCand^^m) (IniPresol p1 p2 s1 s2)
       = (IniPresol (p1 \<cdot> gr\<^sup>@ep1) (p2 \<cdot>hr\<^sup>@ep2) (gr\<^sup>@es1 \<cdot> s1)  (hr\<^sup>@es2\<cdot> s2))\<lparr> count := \<lambda> x. (if x then ep1 + es1 else ep2 + es2)\<rparr>"
  using  NextCand_computation_gen[of m 0 0 p1 p2 s1 s2, unfolded ini_remove_count add_0].

\<comment> \<open>Lemma  @{term "NextCand_sol_aux"} shows that equal overflows and predicate valid being True imply a rootsolution
defined by the presolution.
 \<close>

lemma NextCand_sol_aux : assumes "valid ((NextCand^^m) (IniPresol p1 p2 s1 s2))"
  and "over ((NextCand^^m) (IniPresol p1 p2 s1 s2)) True = over ((NextCand^^m) (IniPresol p1 p2 s1 s2)) False"
shows  "p1\<cdot> gr\<^sup>@(count ((NextCand^^m) (IniPresol p1 p2 s1 s2)) True) \<cdot>s1 = p2\<cdot> hr\<^sup>@(count ((NextCand^^m) (IniPresol p1 p2 s1 s2)) False) \<cdot>s2"
proof-
  from NextCand_computation[of m p1 p2 s1 s2]
  obtain ep1 ep2 es1 es2 where
    vals: "m = ep1 + ep2 + es1 + es2"
    "count ((NextCand ^^ m) (IniPresol p1 p2 s1 s2)) = (\<lambda>x. if x then ep1 + es1 else ep2 + es2)"
    "over ((NextCand ^^ m) (IniPresol p1 p2 s1 s2)) = over (IniPresol (p1 \<cdot> gr \<^sup>@ ep1) (p2 \<cdot> hr \<^sup>@ ep2) (gr \<^sup>@ es1 \<cdot> s1) (hr \<^sup>@ es2 \<cdot> s2))"
    "valid ((NextCand ^^ m) (IniPresol p1 p2 s1 s2)) = (p1 \<cdot> gr \<^sup>@ ep1 \<bowtie> p2 \<cdot> hr \<^sup>@ ep2 \<and> gr \<^sup>@ es1 \<cdot> s1 \<bowtie>\<^sub>s hr \<^sup>@ es2 \<cdot> s2)"
    "(NextCand ^^ m) (IniPresol p1 p2 s1 s2) = IniPresol (p1 \<cdot> gr \<^sup>@ ep1) (p2 \<cdot> hr \<^sup>@ ep2) (gr \<^sup>@ es1 \<cdot> s1) (hr \<^sup>@ es2 \<cdot> s2)
     \<lparr>count := \<lambda>x. if x then ep1 + es1 else ep2 + es2\<rparr> "
    by blast
  from empty_sol[OF assms(1-2)[unfolded vals(5) count_irrelevant]]
  have "p1 \<cdot> gr \<^sup>@ (ep1 + es1) \<cdot> s1 = p2 \<cdot> hr \<^sup>@ (ep2 + es2) \<cdot> s2"
    unfolding rassoc add_exps.
  thus ?thesis
    unfolding vals(2) by simp
qed


\<comment> \<open>A presolution @{term "pg \<cdot> gr\<^sup>@n \<cdot> sg = ph \<cdot> hr\<^sup>@m \<cdot> sh"} implies  a solution, if there exists a
word @{term "w"} such that @{term "g w =  gr\<^sup>@n  "} and @{term "h w =  hr\<^sup>@m "}. Existence of such word
can be tested solving the pair of Diophantine equations. Note that if there exists a solution to
the length equation pair such that  @{term "x"} and  @{term "y"} are natural numbers and
@{term "0<x+y"}, then  @{term "x"} corresponds to the number of letter  @{term "\<aa>"} in  @{term "w"},
and  @{term "y"} to the number of letters  @{term "\<bb>"} in  @{term "w"}
\<close>

definition length_correct :: "(bool \<Rightarrow> nat) \<Rightarrow> bool"
  where "length_correct p =
(two_eqs_pos_solvable nga ngb nha nhb (p True) (p False))
"

\<comment> \<open>Two numbers are "length correct" if they are possible exponents in images of a nonempty word.\<close>

lemma g_exp_count: "g w = gr \<^sup>@ exp \<longleftrightarrow> exp = nga * (count_list w bina) + ngb * (count_list w binb)" (is "?L \<longleftrightarrow> ?R")
proof
  assume "g w = gr \<^sup>@ exp"
  show ?R
    using lenarg[OF gper.bin_per_morph_expI[of w, unfolded \<open>g w = gr\<^sup>@exp\<close>], unfolded pow_len]
    unfolding nga_def ngb_def mult_cancel2 using length_nonzero(1) by fastforce
next
  assume "?R"
  show "g w = gr \<^sup>@ exp"
    unfolding \<open>?R\<close>  gper.sorted_image[of w bina, unfolded binA_simps] g_a g_b shifts exp_simps..
qed

lemma h_exp_count: "h w = hr \<^sup>@ exp \<longleftrightarrow> exp = nha * (count_list w bina) + nhb * (count_list w binb)" (is "?L \<longleftrightarrow> ?R")
proof
  assume "h w = hr \<^sup>@ exp"
  show ?R
    using lenarg[OF hper.bin_per_morph_expI[of w, unfolded \<open>h w = hr\<^sup>@exp\<close>], unfolded pow_len]
    unfolding nha_def nhb_def mult_cancel2 using length_nonzero(2)  by fastforce
next
  assume "?R"
  show "h w = hr \<^sup>@ exp"
    unfolding \<open>?R\<close>  hper.sorted_image[of w bina, unfolded binA_simps] h_a h_b
        add_exps pow_mult..
qed

lemma length_correctI:
  assumes "w \<noteq> \<epsilon>" and "g w = gr\<^sup>@eg" and "h w = hr\<^sup>@eh"
  shows "length_correct (\<lambda> x. if x then eg else eh)"
  unfolding length_correct_def
proof (rule two_eqs_pos_solvableI, unfold gpcp_simps)
  show "int nga * (count_list w bina) + int ngb * (count_list w binb) = int eg"
    using \<open>g w = gr\<^sup>@eg\<close> g_exp_count by force
  show "int nha * int (count_list w bina) + int nhb * int (count_list w binb) = int eh"
    using h_exp_count  \<open>h w = hr\<^sup>@eh\<close> by force
  show "int (count_list w bina) + int (count_list w binb) \<noteq> 0"
    using nemp_len[OF \<open>w \<noteq> \<epsilon>\<close>] unfolding bin_len_count' by presburger
qed simp_all

lemma length_correctD:
  assumes "length_correct (\<lambda> x. if x then eg else eh)"
  obtains w where "w \<noteq> \<epsilon>" and "g w = gr\<^sup>@eg" and "h w = hr\<^sup>@eh"
proof-
  from assms[unfolded length_correct_def gpcp_simps two_eqs_pos_solvable_def Let_def case_prod_beta]
  obtain x y where xy: "int nga*x + int ngb*y = int eg" "int nha*x + int nhb*y = int eh" "0 \<le> x" "0 \<le> y" "x + y \<noteq> 0"
    by meson
  hence "\<aa>\<^sup>@(nat x) \<cdot> \<bb>\<^sup>@(nat y) \<noteq> \<epsilon>"
    by simp
  have eq1: "nga*(nat x) + ngb*(nat y) = eg"
    using arg_cong[OF xy(1), of nat, unfolded nat_int]
    by (simp add: nat_add_distrib nat_mult_distrib xy(3) xy(4))
  have eq2: "nha*(nat x) + nhb*(nat y) = eh"
    using arg_cong[OF xy(2), of nat, unfolded nat_int]
    by (simp add: nat_add_distrib nat_mult_distrib xy(3) xy(4))
  let ?w = "\<aa>\<^sup>@(nat x) \<cdot> \<bb>\<^sup>@(nat y)"
  have or: "nat x \<noteq> 0 \<or> nat y \<noteq> 0"
    using xy(3-5) by simp
  have countxy: "nat x = count_list ?w bina" "nat y = count_list ?w binb"
    unfolding count_list_append count_sing_exp using count_sing_distinct[of binb bina] by simp_all
  have "g (\<aa>\<^sup>@(nat x) \<cdot> \<bb>\<^sup>@(nat y)) = gr\<^sup>@eg" "h (\<aa>\<^sup>@(nat x) \<cdot> \<bb>\<^sup>@(nat y)) = hr\<^sup>@eh"
    using eq1 eq2 unfolding g_exp_count h_exp_count countxy[symmetric] by blast+
  from that[OF _ this]
  show thesis
    using  pref_nemp[of "\<aa> \<^sup>@ nat x"] suf_nemp[of "\<bb> \<^sup>@ nat y"]  or
    unfolding sing_pow_emp_conv[of bina "nat x"] sing_pow_emp_conv[of binb "nat y"] by blast
qed

\<comment> \<open>Based on above, a presolution gives a solution if it implies a rootsolution with
counter values being @{term "length_correct"}. Combining these we define predicate
@{term "solution_test"}
\<close>

definition solution_test :: "'a presolution \<Rightarrow> bool"
  where "solution_test X \<equiv> valid X \<and> (over X True = over X False)
         \<and> length_correct (count X)"

\<comment> \<open>We show that @{term "NextCand"} and  @{term "solution_test"} find a solution.
\<close>

lemma solution_test_NextCandI [intro]:
  assumes "p1 \<cdot> g w \<cdot> s1 = p2 \<cdot> h w \<cdot> s2" and "w \<noteq> \<epsilon>" and
    "g w = gr\<^sup>@e1" and "h w = hr\<^sup>@e2"
  shows "solution_test ((NextCand^^(e1+e2)) (IniPresol p1 p2 s1 s2))"
  unfolding solution_test_def
proof (unfold conj_assoc[symmetric], (rule conjI)+)
  let ?X = "(NextCand ^^ (e1+e2)) (IniPresol p1 p2 s1 s2)"
  note eq = assms(1)[unfolded assms(3-4)]
  from NextCand_rootsol[OF this]  two_eqs_pos_solvableI
  show "valid ?X" and "over ?X True = over ?X False"
    unfolding count_irrelevant by blast+
  from NextCand_rootsol(1)[OF eq, of "\<lambda> _.0"]
  have counts: "count ?X True = e1" "count ?X False = e2"
    unfolding count_additive[of  "e1 + e2"  "\<lambda> _.0" p1 p2 s1 s2] by force+
  have or: "(count_list w bina) \<noteq> 0 \<or> (count_list w binb) \<noteq> 0"
    using nemp_len[OF \<open>w \<noteq> \<epsilon>\<close>] bin_len_count[of w bina, unfolded binA_simps] by presburger
  have e1: "e1 = (nga*count_list w bina + ngb*count_list w binb)" and
    e2: "e2 = (nha*count_list w bina + nhb*count_list w binb)"
    using assms(3-4) unfolding g_exp_count h_exp_count.
  show "length_correct (count ?X)"
    unfolding length_correct_def counts
  proof (rule two_eqs_pos_solvableI)
    show "int nga * count_list w bina + int ngb * count_list w binb = e1" and
      "int nha * count_list w bina + int nhb * count_list w binb = int e2"
      unfolding e1 e2 int_plus int_simps(2) by auto
    show "0 \<le> int (count_list w bina)" "0 \<le> int (count_list w binb)"
      by force+
    thus "int (count_list w bina) + int (count_list w binb) \<noteq> 0"
      using nemp_len[OF \<open>w \<noteq> \<epsilon>\<close>] unfolding bin_len_count'
      by presburger
  qed
qed

\<comment> \<open>For any word   @{term "w"}, the image   @{term "g w"} for a periodic morphisms   @{term "g"} is
completely determined by the  number of letters  @{term "\<aa>"} in  @{term "w"},
and  number of letters  @{term "\<bb>"} in  @{term "w"}. Therefore, if we know these numbers,
we can define one simple solution.
\<close>

definition get_solution :: "'a presolution \<Rightarrow> binA list" \<comment> \<open>The candidate w for a given solution\<close>
  where " get_solution X =
          (let a = nat (fst (solve_two_eqs (int nga) (int ngb) (int nha) (int nhb) (int (count X True)) (int (count X False))));
               b = nat (snd (solve_two_eqs (int nga) (int ngb) (int nha) (int nhb) (int (count X True)) (int (count X False))))
          in
          \<aa>\<^sup>@a \<cdot> \<bb>\<^sup>@b)"

value "solve_two_eqs 3 1 0 1 1 0"

lemma bin_count_standard: "count_list (\<aa>\<^sup>@a\<cdot>\<bb>\<^sup>@b) bina = a" "count_list (\<aa>\<^sup>@a\<cdot>\<bb>\<^sup>@b) binb = b"
  by simp_all

lemma solution_testD:
  fixes m p1 p2 s1 s2
  defines "X \<equiv> (NextCand^^m) (IniPresol p1 p2 s1 s2)"
  defines "w \<equiv> get_solution X"
  assumes "solution_test X"
  shows "p1 \<cdot> g w \<cdot> s1 = p2 \<cdot> h w \<cdot> s2"
    "w \<noteq> \<epsilon>"
proof-
  let ?a = "nat (fst (solve_two_eqs (int nga) (int ngb) (int nha) (int nhb) (int (count X True)) (int (count X False))))" and
    ?b = "nat (snd (solve_two_eqs (int nga) (int ngb) (int nha) (int nhb) (int (count X True)) (int (count X False))))"
  have w: "w = \<aa>\<^sup>@?a \<cdot> \<bb>\<^sup>@?b"
    unfolding w_def get_solution_def Let_def..

  from assms(3)[unfolded solution_test_def]
  have "valid X" "(over X) True = (over X) False" "length_correct (count X)"
    unfolding X_def by blast+

  from \<open>length_correct (count X)\<close>[unfolded length_correct_def two_eqs_pos_solvable_def Let_def case_prod_beta]
  have "0 \<le> ?a" "0 \<le> ?b" "?a + ?b \<noteq> 0"
    by auto
  thus "w \<noteq> \<epsilon>"
    unfolding w by simp

  from NextCand_computation[of  m p1 p2 s1 s2]
  obtain ep1 ep2 es1 es2 where
    "m = ep1 + ep2 + es1 + es2"
    "count X = (\<lambda>x. if x then ep1 + es1 else ep2 + es2) \<and>
           over X = over (IniPresol (p1 \<cdot> gr \<^sup>@ ep1) (p2 \<cdot> hr \<^sup>@ ep2) (gr \<^sup>@ es1 \<cdot> s1) (hr \<^sup>@ es2 \<cdot> s2)) \<and>
           valid X = (p1 \<cdot> gr \<^sup>@ ep1 \<bowtie> p2 \<cdot> hr \<^sup>@ ep2 \<and> gr \<^sup>@ es1 \<cdot> s1 \<bowtie>\<^sub>s hr \<^sup>@ es2 \<cdot> s2)"
    unfolding X_def by blast
  hence valcount: "count X = (\<lambda>x. if x then ep1 + es1 else ep2 + es2)" and
    valover: "over X = over (IniPresol (p1 \<cdot> gr \<^sup>@ ep1) (p2 \<cdot> hr \<^sup>@ ep2) (gr \<^sup>@ es1 \<cdot> s1) (hr \<^sup>@ es2 \<cdot> s2))" and
    valvalid: "valid X = (p1 \<cdot> gr \<^sup>@ ep1 \<bowtie> p2 \<cdot> hr \<^sup>@ ep2 \<and> gr \<^sup>@ es1 \<cdot> s1 \<bowtie>\<^sub>s hr \<^sup>@ es2 \<cdot> s2)"
    by blast+
  from assms(3)[unfolded solution_test_def X_def]
  have isvalid: "valid X" and
    overeq: "over X True = over X False" and
    lencor:  "length_correct (count X)"
    unfolding X_def by blast+
  from   isvalid[unfolded valvalid]
  have comps: "p1 \<cdot> gr \<^sup>@ ep1 \<bowtie> p2 \<cdot> hr \<^sup>@ ep2" "gr \<^sup>@ es1 \<cdot> s1 \<bowtie>\<^sub>s hr \<^sup>@ es2 \<cdot> s2 "
    by blast+
  have "valid (IniPresol (p1 \<cdot> gr \<^sup>@ ep1) (p2 \<cdot> hr \<^sup>@ ep2) (gr \<^sup>@ es1 \<cdot> s1) (hr \<^sup>@ es2 \<cdot> s2))"
    unfolding IniPresol_def select_convs using isvalid[unfolded valvalid].
  from empty_sol[OF this overeq[unfolded valover]]
  have eq: "p1 \<cdot> gr \<^sup>@ (ep1 + es1) \<cdot> s1 = p2 \<cdot> hr \<^sup>@ (ep2 + es2) \<cdot> s2"
    by comparison

  define a where  "a =fst (solve_two_eqs (int nga) (int ngb) (int nha) (int nhb) (int (ep1 + es1)) (int (ep2 + es2)))"
  define b where  "b = snd (solve_two_eqs (int nga) (int ngb) (int nha) (int nhb) (int (ep1 + es1)) (int (ep2 + es2)))"
  from \<open>length_correct (count X)\<close>[unfolded length_correct_def two_eqs_pos_solvable_def
      Let_def case_prod_beta valcount gpcp_simps, folded a_def b_def]
  have "int nga* int(nat a) + int ngb* int(nat b) = int ep1 + int es1" "int nha* int(nat a) + int nhb* int(nat b) = int ep2 + int es2"
    by auto
  hence [symmetric]: "nga*nat(a) + ngb*nat(b) = ep1 + es1" "nha*nat(a) + nhb*nat(b) = ep2 + es2"
    unfolding int_simps.
  have ims: "g w = gr \<^sup>@ (ep1 + es1)" "h w = hr \<^sup>@ (ep2 + es2)"
    unfolding w_def get_solution_def Let_def valcount gpcp_simps
    unfolding g_exp_count h_exp_count
    unfolding gper.bin_per_morph_expI[of "\<aa>\<^sup>@_\<cdot>_"] gpcp_simps bin_count_standard
    unfolding a_def[symmetric] b_def[symmetric]
    by fact+
  from eq[folded ims]
  show "p1 \<cdot> g w \<cdot> s1 = p2 \<cdot> h w \<cdot> s2".
qed

text \<open>Properties of valid\<close>

lemma not_valid_next_stable: "\<not> valid X \<Longrightarrow> \<not> valid (NextCand X)"
  unfolding NextCand_def Let_def case_prod_unfold select_convs by blast

lemma valid_stable:
  assumes "valid ((NextCand^^m) (IniPresol p1 p2 s1 s2))"
    and "n < m"
  shows "valid ((NextCand^^n) (IniPresol p1 p2 s1 s2))"
proof-
  have mn: "(m-n) + n = m"
    using \<open>n < m\<close> by auto
  have "\<not> valid ((NextCand^^k) X)" if "\<not> valid X" for X k
    using not_valid_next_stable that
    by (induct k)  auto
  from this[of "(NextCand^^n) (IniPresol p1 p2 s1 s2)" "m - n"] assms
  show ?thesis
    using funpow_add[of _ _ NextCand, symmetric, unfolded comp_apply] mn
    by metis
qed

subsection \<open>Long solution revisited\<close>

\<comment> \<open>We translate results "Long solution bound" section into a symmetric form using the defined tools.\<close>

lemma max_pp_sym: "(max \<^bold>|pg\<^bold>| \<^bold>|ph\<^bold>|)  = max \<^bold>|pp c\<^bold>| \<^bold>|pp (\<not> c)\<^bold>|" for c
  unfolding pp_def by (induct c, simp) (simp add: max.commute[of "\<^bold>|pp c\<^bold>|"])

lemma max_ss_sym: "(max \<^bold>|sg\<^bold>| \<^bold>|sh\<^bold>|)  = max \<^bold>|ss c\<^bold>| \<^bold>|ss (\<not> c)\<^bold>|" for c
  unfolding ss_def by (induct c, simp) (simp add: max.commute[of "\<^bold>|ss c\<^bold>|"])

lemma long_solution_sym: assumes "pg \<cdot> g w \<cdot> sg = ph \<cdot> h w \<cdot> sh" and "great_length \<le> \<^bold>|pg \<cdot> g w \<cdot> sg\<^bold>|"
  shows "pp a \<cdot> (ff a) w \<cdot> ss a = pp (\<not> a) \<cdot> (ff (\<not> a)) w \<cdot> ss (\<not> a)" and
    "(max \<^bold>|pp a\<^bold>| \<^bold>|pp (\<not> a)\<^bold>|) + (max \<^bold>|ss a\<^bold>| \<^bold>|ss (\<not> a)\<^bold>|) + \<^bold>|rr a\<^bold>| + \<^bold>|rr  (\<not> a)\<^bold>| \<le> \<^bold>|pp b \<cdot> (ff b) w \<cdot> ss b\<^bold>|"
proof-
  from assms(1)
  show "pp a \<cdot> (ff a) w \<cdot> ss a = pp (\<not> a) \<cdot> (ff (\<not> a)) w \<cdot> ss (\<not> a)"
    unfolding pp_def ff_def ss_def by (induct a) argo+
  from assms
  show "(max \<^bold>|pp a\<^bold>| \<^bold>|pp (\<not> a)\<^bold>|) + (max \<^bold>|ss a\<^bold>| \<^bold>|ss (\<not> a)\<^bold>|) + \<^bold>|rr a\<^bold>| + \<^bold>|rr  (\<not> a)\<^bold>| \<le> \<^bold>|pp b \<cdot> (ff b) w \<cdot> ss b\<^bold>|"
    unfolding pp_def ff_def ss_def rr_def great_length_def  by (induct a) presburger+
qed

lemma long_presol_bound_symmetric: assumes eq: "pg \<cdot> gr\<^sup>@e1 \<cdot> sg = ph \<cdot> hr\<^sup>@e2 \<cdot> sh" and
  ineq: "great_length \<le> \<^bold>|pg \<cdot> gr\<^sup>@e1 \<cdot> sg\<^bold>|"
obtains r s n m a where "r \<cdot> s = rr a" and "s \<cdot> r = rr (\<not> a)" "pp (\<not> a) = pp a \<cdot> (r \<cdot> s) \<^sup>@ n \<cdot> r" and
  "ss a = r \<cdot> (s \<cdot> r) \<^sup>@ m \<cdot> ss (\<not> a) \<or> ss (\<not> a) = s \<cdot> (r \<cdot> s) \<^sup>@ m \<cdot> ss a"
  using long_gen_bound_aux long_gen_bound_aux_mirror
proof-
  note hprim = hper.per_morph_root_prim and
    gprim = gper.per_morph_root_prim
  obtain r s n a where "r \<cdot> s = rr a" and "s \<cdot> r = rr (\<not> a)" and "s \<noteq> \<epsilon>" and  "pp (\<not> a) = pp a \<cdot> (r \<cdot> s) \<^sup>@ n \<cdot> r"
  proof (cases "\<^bold>|pg\<^bold>|" "\<^bold>|ph\<^bold>|" rule: le_cases)
    assume "\<^bold>|pg\<^bold>| \<le> \<^bold>|ph\<^bold>|"
    from long_gen_bound_aux[OF gprim hprim  assms(1)  this assms(2)[unfolded great_length_def]] that[of _ _ True]
    show thesis
      unfolding gpcp_simps by blast
  next
    assume "\<^bold>|ph\<^bold>| \<le> \<^bold>|pg\<^bold>|"
    from long_gen_bound_aux[OF hprim gprim assms(1)[symmetric] this]
      ineq[unfolded great_length_sym[of False False False] eq]
      that[of _ _ True]
    obtain r s n where  "s \<cdot> r = gr" "r \<cdot> s = hr" "s \<noteq> \<epsilon>" "pg = ph \<cdot> (r \<cdot> s) \<^sup>@ n \<cdot> r"
      unfolding gpcp_simps' by blast
    with that[of r s False]
    show thesis
      unfolding gpcp_simps by blast
  qed
  obtain r' s' m b where "s' \<cdot> r' = rr b" and "r' \<cdot> s' = rr (\<not> b)" and "s' \<noteq> \<epsilon>"
    "ss (\<not> b) = r' \<cdot> (s' \<cdot> r') \<^sup>@ m \<cdot> ss b"
  proof (cases "\<^bold>|sg\<^bold>|" "\<^bold>|sh\<^bold>|" rule: le_cases)
    assume "\<^bold>|sg\<^bold>| \<le> \<^bold>|sh\<^bold>|"
    from long_gen_bound_aux_mirror[OF gprim hprim assms(1) this ]
      ineq[unfolded great_length_def]
      that[of _ _ True]
    show thesis
      unfolding  gpcp_simps' by blast
  next
    assume "\<^bold>|sh\<^bold>| \<le> \<^bold>|sg\<^bold>|"
    from long_gen_bound_aux_mirror[OF hprim gprim assms(1)[symmetric] this]
      ineq[unfolded great_length_sym[of False False False] eq]
    obtain r' s' m where  "s' \<cdot> r' = hr" "r' \<cdot> s' = gr" "s' \<noteq> \<epsilon>" "sg = r' \<cdot> (s' \<cdot> r') \<^sup>@ m \<cdot> sh"
      unfolding gpcp_simps' by blast
    with that[of s' r' False]
    show thesis
      unfolding rr_def ss_def if_True if_False bool_simps by blast
  qed
  have conjug: "rr c \<sim> rr (\<not> c)" for c
    using long_presol_conjug_roots[OF eq] ineq[unfolded great_length_def] unfolding rr_def
    unfolding gpcp_simps great_length_def by (simp add: conjug_swap)
  note that' = that[OF \<open>r \<cdot> s = rr a\<close> \<open>s \<cdot> r = rr (\<not>a)\<close> \<open>pp (\<not> a) = pp a \<cdot> (r \<cdot> s) \<^sup>@ n \<cdot> r \<close>]
  show thesis
  proof (cases "s \<cdot> r = r \<cdot> s")
    assume "s \<cdot> r \<noteq> r \<cdot> s"
    from mroot_prim[of "\<not> a", folded \<open>s \<cdot> r = rr (\<not> a)\<close>]
    show thesis
      using prim_conjug_unique[OF \<open>primitive (s \<cdot> r)\<close> _ _  \<open>s \<cdot> r \<noteq> r \<cdot> s\<close>]
    proof (cases "a = b")
      assume "a = b"
      from \<open>r \<cdot> s = rr a\<close>[folded \<open>s' \<cdot> r' = rr b\<close>[folded \<open>a = b\<close>]] mroot_prim[of a, folded \<open>s \<cdot> r = rr (\<not> a)\<close>]
        \<open>s \<cdot> r = rr (\<not> a)\<close>[folded \<open>r' \<cdot> s' = rr (\<not> b)\<close>[folded \<open>a = b\<close>]]
      have "s = r'" and "r = s'"
        using prim_conjug_unique[OF \<open>primitive (s \<cdot> r)\<close> \<open>s \<cdot> r = r' \<cdot> s'\<close> \<open>r \<cdot> s = s' \<cdot> r'\<close> \<open>s \<cdot> r \<noteq> r \<cdot> s\<close>]
        by blast+
      from that'[unfolded this] \<open>ss (\<not> b) = r' \<cdot> (s' \<cdot> r') \<^sup>@ m \<cdot> ss b\<close>[folded \<open>a = b\<close>]
      show thesis
        by blast
    next
      assume "a \<noteq> b"
      hence "a = (\<not> b)" and "(\<not> a) = b"
        by blast+
      from \<open>r \<cdot> s = rr a\<close>[folded \<open>r' \<cdot> s' = rr (\<not> b)\<close>[folded \<open>a = (\<not> b)\<close>]] mroot_prim[of a, folded \<open>r \<cdot> s = rr a\<close>]
        \<open>s \<cdot> r = rr (\<not> a)\<close>[folded \<open>s' \<cdot> r' = rr b\<close>[folded \<open>(\<not> a) = b\<close>]]
      have "r = r'" and "s = s'"
        using prim_conjug_unique[OF \<open>primitive (r\<cdot>s)\<close> \<open>r \<cdot> s = r' \<cdot> s'\<close> \<open>s \<cdot> r = s' \<cdot> r'\<close> \<open>s \<cdot> r \<noteq> r \<cdot> s\<close>[symmetric]]
        by blast+
      from that'[unfolded this] \<open>ss (\<not> b) = r' \<cdot> (s' \<cdot> r') \<^sup>@ m \<cdot> ss b\<close>[folded \<open>(\<not> a) = b\<close>]
      show thesis
        by force
    qed
  next
    assume "s \<cdot> r = r \<cdot> s"
    hence "r = \<epsilon>"
      using \<open>s \<noteq> \<epsilon>\<close> mroot_prim[of a, folded \<open>r \<cdot> s = rr a\<close>]
        comm_not_prim by metis
    hence alls: "rr c = s" for c
      using \<open>s \<cdot> r = rr (\<not> a)\<close> \<open>r \<cdot> s = rr a\<close> unfolding \<open>r =\<epsilon>\<close> rr_def by force+
    define ee where "ee = (\<lambda> a. if a then e1 else e2)"
    have "pp a \<cdot> rr a\<^sup>@ee a \<cdot> ss a = pp (\<not>a) \<cdot> rr (\<not>a)\<^sup>@ee (\<not> a) \<cdot> ss (\<not>a)"
      by (induct a, unfold gpcp_simps ee_def) (use eq in force)+
    from this[unfolded \<open>pp (\<not> a) = pp a \<cdot> (r \<cdot> s) \<^sup>@ n \<cdot> r\<close> \<open>r = \<epsilon>\<close> emp_simps alls rassoc cancel,
        unfolded lassoc add_exps[symmetric]]
    have s_eq: "s \<^sup>@ ee a \<cdot> ss a = s \<^sup>@ (n + ee (\<not> a)) \<cdot> ss (\<not> a)".
    have "ss a = s \<^sup>@ (n + ee (\<not> a) - ee a) \<cdot> ss (\<not> a) \<or> ss (\<not> a) = s \<cdot> s\<^sup>@ (ee a - Suc (n + ee (\<not> a))) \<cdot> ss a"
    proof (rule le_less_cases)
      assume le: "ee a \<le> n + ee (\<not> a)"
      show ?thesis
        using s_eq unfolding pop_pow[OF le, of s, symmetric] rassoc cancel by blast
    next
      assume "n + ee (\<not> a) < ee a"
      hence le_Suc: "Suc (n + ee (\<not> a)) \<le> ee a"
        by force
      show ?thesis
        using s_eq[symmetric]
        unfolding pop_pow[OF le_Suc, of s, symmetric] pow_Suc' rassoc cancel by blast
    qed
    with that[OF _ _ \<open>pp (\<not> a) = pp a \<cdot> (r \<cdot> s) \<^sup>@ n \<cdot> r\<close>, unfolded \<open>r = \<epsilon>\<close> emp_simps, OF alls[symmetric]
        alls[symmetric]]
    show thesis
      by blast
  qed
qed


lemma long_presolution_correct: assumes
  eq: "pg \<cdot> g w \<cdot> sg = ph \<cdot> h w \<cdot> sh" and
  ineql: "great_length \<le> \<^bold>|pg \<cdot> g w \<cdot> sg\<^bold>|" and
  eql: "\<^bold>|pp a \<cdot> (rr a)\<^sup>@i \<cdot> ss a\<^bold>| = \<^bold>|pp (\<not> a) \<cdot> (rr (\<not> a))\<^sup>@j \<cdot> ss (\<not> a)\<^bold>|"
shows "pp a \<cdot> (rr a)\<^sup>@i \<cdot> ss a = pp (\<not> a) \<cdot> (rr (\<not> a))\<^sup>@j \<cdot> ss (\<not> a)"
proof-
  obtain n where "g w = gr\<^sup>@n"
    using gper.per_morph_im_exp by blast
  obtain m where "h w = hr\<^sup>@m"
    using hper.per_morph_im_exp by blast
  show ?thesis
  proof (cases a)
    assume a
    hence "\<^bold>|pg \<cdot> gr\<^sup>@i \<cdot>sg\<^bold>|=\<^bold>|ph \<cdot> hr\<^sup>@j \<cdot>sh\<^bold>|"
      using eql pp_def rr_def ss_def by force
    thus ?thesis
      using \<open>a\<close> long_bound_bound  eq ineql \<open>\<^bold>|pg \<cdot> gr\<^sup>@i \<cdot>sg\<^bold>|=\<^bold>|ph \<cdot> hr\<^sup>@j \<cdot>sh\<^bold>|\<close> unfolding \<open>g w = gr\<^sup>@n\<close>\<open>h w = hr\<^sup>@m\<close> pp_def rr_def ss_def gpcp_simps
        great_length_def
      by presburger
  next
    assume "\<not> a"
    hence "\<^bold>|pg \<cdot> gr\<^sup>@j \<cdot>sg\<^bold>|=\<^bold>|ph \<cdot> hr\<^sup>@i \<cdot>sh\<^bold>|"
      using eql[symmetric] unfolding  pp_def rr_def ss_def by argo
    hence  "pg \<cdot> gr\<^sup>@j \<cdot>sg =ph \<cdot> hr\<^sup>@i \<cdot>sh"
      using long_bound_bound  eq ineql  unfolding \<open>g w = gr\<^sup>@n\<close>\<open>h w = hr\<^sup>@m\<close> great_length_def
      by blast
    thus ?thesis
      using \<open>\<not> a\<close> pp_def rr_def ss_def
      by presburger
  qed
qed

subsection \<open>Roundbound\<close>

\<comment> \<open>With the  roundbound numbers we define the how many rounds of iuteration is needed for the
@{term "NextCand"}. It will also happen that this maximal number of rounds gives us a way to
decide the existence of long solutions, no matter how long they are, in the case where no short
solution exists.
We also prove needed lemma for the @{term "Least"}
\<close>

definition block_part :: "bool \<Rightarrow> nat"
  where "block_part a = (max \<^bold>|pp a\<^bold>| \<^bold>|pp (\<not> a)\<^bold>|) + (max \<^bold>|ss a\<^bold>| \<^bold>|ss (\<not> a)\<^bold>|) - \<^bold>|pp a\<^bold>| - \<^bold>|ss a\<^bold>|"

lemma block_part_alt: "block_part a = nat (int \<^bold>|pp (\<not> a)\<^bold>| - (int \<^bold>|pp a\<^bold>|)) + nat (int \<^bold>|ss (\<not> a)\<^bold>| - (int \<^bold>|ss a\<^bold>|))"
proof-
  have hint: "nat (int b - int a)  = max a b - a" for a b
    by linarith
  show ?thesis
    unfolding max_pp_sym[of a] max_ss_sym[of a] block_part_def hint by simp
qed

lemma block_part': "block_part (\<not> a) = (max \<^bold>|pp a\<^bold>| \<^bold>|pp (\<not> a)\<^bold>|) + (max \<^bold>|ss a\<^bold>| \<^bold>|ss (\<not> a)\<^bold>|) - \<^bold>|pp (\<not> a)\<^bold>| - \<^bold>|ss (\<not> a)\<^bold>|"
  unfolding block_part_def bool_simps using max.commute by presburger

\<comment> \<open>The smallest number of roots guaranteeing that the overlap of roots is at least the sum of both roots lengths\<close>
definition roundbound :: "bool \<Rightarrow> nat"
  where "roundbound a =  (LEAST k. block_part a + \<^bold>|rr a\<^bold>| + \<^bold>|rr (\<not> a)\<^bold>| \<le> k * \<^bold>|rr a\<^bold>|)"

lemma roundboundI:  "block_part a + \<^bold>|rr a\<^bold>| + \<^bold>|rr (\<not> a)\<^bold>| \<le> roundbound a * \<^bold>|rr a\<^bold>|"
  \<comment> \<open>The expected property must be proved: in general, such a number may not exist\<close>
  unfolding roundbound_def
  by (rule LeastI[of "\<lambda> x. block_part a + \<^bold>|rr a\<^bold>| + \<^bold>|rr (\<not> a)\<^bold>| \<le> x * \<^bold>|rr a\<^bold>|" "block_part a + \<^bold>|rr a\<^bold>| + \<^bold>|rr (\<not> a)\<^bold>|"])
    (metis mroot_len[of a ] add_gr_0 div_greater_zero_iff div_mult_self1_is_m)

lemma roundboundI':  "(max \<^bold>|pp a\<^bold>| \<^bold>|pp (\<not> a)\<^bold>|) + (max \<^bold>|ss a\<^bold>| \<^bold>|ss (\<not> a)\<^bold>|) - \<^bold>|pp a\<^bold>| - \<^bold>|ss a\<^bold>| + \<^bold>|rr a\<^bold>| + \<^bold>|rr (\<not> a)\<^bold>| \<le> roundbound a * \<^bold>|rr a\<^bold>|"
  using roundboundI[of a] unfolding block_part_def.

lemma roundbound_long':  "(max \<^bold>|pp a\<^bold>| \<^bold>|pp (\<not> a)\<^bold>|) + (max \<^bold>|ss a\<^bold>| \<^bold>|ss (\<not> a)\<^bold>|) + \<^bold>|rr a\<^bold>| + \<^bold>|rr (\<not> a)\<^bold>| \<le> \<^bold>|pp a \<cdot> rr a\<^sup>@roundbound a \<cdot> ss a\<^bold>|"
proof-
  let ?mp = "(max \<^bold>|pp a\<^bold>| \<^bold>|pp (\<not> a)\<^bold>|)"
  let ?ms = "(max \<^bold>|ss a\<^bold>| \<^bold>|ss (\<not> a)\<^bold>|)"
  have eq: "?mp + ?ms - \<^bold>|pp a\<^bold>| - \<^bold>|ss a\<^bold>| + \<^bold>|rr a\<^bold>| + \<^bold>|rr (\<not> a)\<^bold>| = ?mp + ?ms - \<^bold>|pp a\<^bold>| - \<^bold>|ss a\<^bold>| + (\<^bold>|rr a\<^bold>| + \<^bold>|rr (\<not> a)\<^bold>|)"
    unfolding add.assoc by blast
  have ineq: "\<^bold>|pp a\<^bold>| + \<^bold>|ss a\<^bold>| \<le> (max \<^bold>|pp a\<^bold>| \<^bold>|pp (\<not> a)\<^bold>|) + (max \<^bold>|ss a\<^bold>| \<^bold>|ss (\<not> a)\<^bold>|)"
    by (meson add_mono max.cobounded1)
  have eq2: "?mp + ?ms - \<^bold>|pp a\<^bold>| - \<^bold>|ss a\<^bold>| + \<^bold>|rr a\<^bold>| + \<^bold>|rr (\<not> a)\<^bold>| =
         ?mp + ?ms + \<^bold>|rr a\<^bold>| + \<^bold>|rr (\<not> a)\<^bold>| - (\<^bold>|pp a\<^bold>| + \<^bold>|ss a\<^bold>|)"
    unfolding eq unfolding diff_diff_left add_diff_assoc2[OF ineq] by presburger
  show ?thesis
    unfolding  lenmorph pow_len  using roundboundI'[of a, unfolded eq2 le_diff_conv] by presburger
qed

lemma roundbound_sum_ge1: "1<roundbound True + roundbound False"
proof-
  have "0< roundbound True" and "0< roundbound False"
    using roundbound_def rr_def length_nonzero add_leE
      bot_nat_0.extremum_uniqueI mult_is_0 roundboundI zero_less_iff_neq_zero
    by metis+
  thus ?thesis
    by force
qed

lemma roundbound_long: "(max \<^bold>|pp a\<^bold>| \<^bold>|pp (\<not> a)\<^bold>|) + (max \<^bold>|ss a\<^bold>| \<^bold>|ss (\<not> a)\<^bold>|) + \<^bold>|rr a\<^bold>| + \<^bold>|rr (\<not> a)\<^bold>| \<le>
                         \<^bold>|pp b \<cdot> rr b\<^sup>@roundbound b \<cdot> ss b\<^bold>|"
proof (cases "b = a", use roundbound_long' in blast)
  assume "b \<noteq> a" hence "b = (\<not> a)"
    by simp
  from roundbound_long'[of "\<not> a", unfolded bool_simps]
  show ?thesis
    unfolding \<open>b = (\<not> a)\<close> max.commute[of "\<^bold>| _ (\<not> a)\<^bold>|"] by simp
qed

lemma roundbound_long_pref: "(max \<^bold>|pp a\<^bold>| \<^bold>|pp (\<not> a)\<^bold>|) + \<^bold>|rr a\<^bold>| + \<^bold>|rr (\<not> a)\<^bold>| \<le>
                         \<^bold>|pp b \<cdot> rr b\<^sup>@roundbound b\<^bold>|"
proof-
  have "\<^bold>|ss b\<^bold>| \<le> max \<^bold>|ss a\<^bold>| \<^bold>|ss (\<not> a)\<^bold>|"
    by (cases "b = a", simp_all)
  thus ?thesis
    using roundbound_long[of a b] unfolding lenmorph by fastforce
qed

\<comment> \<open>Rootsolution with roundbound powers is long.
\<close>

lemma great_length_roundbound : "great_length \<le> \<^bold>|pp b \<cdot> rr b\<^sup>@roundbound b  \<cdot> ss b\<^bold>| "
  using roundbound_long great_length_def  great_length_sym
  by metis

lemma roundbound_great_length : assumes "\<^bold>|pp b \<cdot> rr b\<^sup>@j \<cdot> ss b\<^bold>| < great_length"
  shows "j< roundbound b"
proof-
  have "great_length \<le> \<^bold>|pp b \<cdot> rr b\<^sup>@roundbound b  \<cdot> ss b\<^bold>| "
    using roundbound_long great_length_def  great_length_sym
    by metis
  thus ?thesis
    using assms(1)  order_less_le_trans
    unfolding pow_len lenmorph by fastforce
qed

lemma roundbound_presol_conjug_roots:
  assumes eq: "pp a \<cdot> (rr a)\<^sup>@roundbound a \<cdot> ss a = pp (\<not> a) \<cdot> (rr (\<not> a))\<^sup>@roundbound (\<not> a) \<cdot> ss (\<not> a)"
  shows "rr a \<sim> rr (\<not> a)"
proof-
  have eq': "pg \<cdot> gr \<^sup>@ roundbound True \<cdot> sg = ph \<cdot> hr \<^sup>@ roundbound False \<cdot> sh"
    using eq by (induct a, unfold gpcp_simps) force+
  have "max \<^bold>|pg\<^bold>| \<^bold>|ph\<^bold>| + (max \<^bold>|sg\<^bold>| \<^bold>|sh\<^bold>|) + \<^bold>|gr\<^bold>| + \<^bold>|hr\<^bold>| \<le> \<^bold>|pg \<cdot> gr \<^sup>@ (roundbound True) \<cdot> sg\<^bold>|"
    using roundbound_long[of a a] eq by (induct a) (unfold gpcp_simps', force+)
  from long_presol_conjug_roots[unfolded great_length_def, OF eq' this]
  show ?thesis
    by (induct a, unfold gpcp_simps) (simp_all only: conjug_sym)
qed

lemma Least_multiple: "n \<noteq> (0 :: nat) \<Longrightarrow> d \<le> n \<Longrightarrow> d \<noteq> 0 \<Longrightarrow> (LEAST k. m*n + d \<le> k*n) = Suc m"
  by (rule Least_equality, fastforce)
    (metis add.commute add_lessD2 add_mono_thms_linordered_field(1) leD leI less_eq_nat.simps(1) mult_right_le_imp_le not_less_eq_eq verit_la_disequality)

lemma Least_addition : assumes  "n \<noteq> (0 :: nat)" shows " (LEAST k. m + l*n \<le> k*n) = (LEAST k. m \<le> k*n) + l"
  (is "Least ?Q = Least?P + l")
proof (rule Least_equality)
  show "m + l * n \<le> (Least ?P + l) * n"
    unfolding nat_distrib using LeastI[of ?P m] \<open>n \<noteq> 0\<close> by force
  show "(LEAST k. m \<le> k * n) + l \<le> y" if "m + l * n \<le> y * n"  for y
    using Least_le[of ?P "y - l"] that \<open>n \<noteq> 0\<close> unfolding nat_distrib
  proof-
    have "l \<le> y"
      using mult_right_le_imp_le[OF add_leD2[OF that]] using \<open>n \<noteq> 0\<close> by blast
    from Least_le[of ?P "y - l", unfolded nat_distrib]
    show ?thesis
      unfolding le_diff_conv2[OF \<open>l \<le> y\<close>, symmetric] using that by auto
  qed
qed

lemma roundbound_correct_aux_asym: assumes "(0 :: nat) < r" and
  "(a + c) mod r = (b + d) mod r" and "b + d \<le> a + c"
shows
  "a + (((LEAST k. max a b + max c d - a - c \<le> k * r) + l) * r + c) =
  b + (((LEAST k. max a b + max c d - b - d \<le> k * r) + l) * r + d)"
proof-
  let ?P = "(\<lambda> k. max a b + max c d - a - c \<le> k * r)"
  let ?Q = "(\<lambda> k. max a b + max c d - b - d \<le> k * r)"

  have "a + c \<le> max a b + max c d"
    by auto
  have max_smallQ: "max a b + max c d - (b + d) \<le> Least ?Q * r"
    using \<open>0 < r\<close> LeastI[of ?Q "max a b + max c d - b - d"]
    by simp
  hence ineq: "a + c \<le> Least ?Q * r + b + d"
    unfolding le_diff_conv using max.cobounded1[of a b] max.cobounded1[of c d] by linarith
  have max_smallP: "max a b + max c d - a - c \<le> Least ?P * r"
    using \<open>0 < r\<close> LeastI[of ?P "max a b + max c d - a - c"]
    by fastforce
  have le_y: "b + d \<le> y + (a + c)" for y
    using assms(3) by simp

  have LeastP_le: "Least ?P * r \<le> y" if "max a b + max c d - a - c \<le> y \<and> r dvd y" for y
  proof-
    have "max a b + max c d - a - c \<le> y" and "r dvd y"
      using that by blast+
    from dvdE[OF \<open>r dvd y\<close>]
    obtain l where "y = r * l".
    hence "?P l"
      by (metis \<open>max a b + max c d - a - c \<le> y\<close> mult.commute)
    from Least_le[of ?P, OF this]
    have "Least ?P \<le> l".
    thus ?thesis
      using \<open>0 < r\<close> by (metis \<open>y = r * l\<close> mult.commute mult_le_mono1)
  qed

  have PtoR: "(LEAST n. (max a b + max c d - a - c \<le> n \<and> r dvd n)) =  Least ?P * r" (is "Least ?R = Least ?P * r")
  proof (rule Least_equality)
    show "max a b + max c d - a - c \<le> Least ?P * r \<and> r dvd (Least ?P * r)"
      using \<open>max a b + max c d - a - c \<le> Least ?P * r\<close> by simp
    show "Least ?P * r \<le> y" if "max a b + max c d - a - c \<le> y \<and> r dvd y" for y
      using LeastP_le[OF that].
  qed

  have thesis1:  "Least ?P * r = Least ?Q * r + b + d - (a + c)"
    unfolding PtoR[symmetric]
  proof (rule Least_equality, rule)
    show "max a b + max c d - a - c \<le> Least ?Q * r + b + d - (a + c)"
      unfolding diff_diff_left[of _ a] by (rule diff_le_mono) (use le_diff_conv max_smallQ in presburger)
    show "r dvd Least ?Q * r + b + d - (a + c)"
      unfolding mod_eq_dvd_iff_nat[OF ineq, of r, symmetric] unfolding add.assoc mod_mult_self4[of r, unfolded mult.commute[of r]]
      using assms(2)[symmetric].
    from dvdE[OF this]
    obtain m where m: "Least ?Q * r = r * m"
      using assms(2)  by (meson mult.commute)

    show "Least ?Q * r + b + d - (a + c) \<le> y" if "max a b + max c d - a - c \<le> y \<and> r dvd y" for y
    proof-
      have "max a b + max c d - a - c \<le> y" and "r dvd y"
        using that by blast+
      hence Qy: "max a b + max c d - b - d \<le> y + (a + c) - (b + d)"
        by (metis diff_diff_left diff_le_mono le_diff_conv)
      have "r dvd y + (a + c) - (b + d)"
        using assms(2)[unfolded mod_eq_dvd_iff_nat[OF assms(3)]] dvd_add_right_iff[OF \<open>r dvd y\<close>]
        using add_diff_assoc2[OF assms(3), of y, unfolded add.commute[of _ y]] by metis
      from dvdE[OF this]
      obtain n where n: "y + (a + c) - (b + d) = r * n".
      hence "?Q n"
        using Qy unfolding mult.commute[of n] by argo
      from Least_le[of ?Q, OF this]
      have "Least ?Q \<le> n".
      hence "Least ?Q * r \<le> y + (a + c) - (b + d)"
        unfolding n mult.commute[of r] using mult_le_mono1 by blast
      from this[unfolded le_diff_conv2[of "(b + d)" "y + (a + c)", OF le_y]]
      have "Least ?Q * r + b + d \<le> y + (a + c)"
        unfolding add.assoc.
      thus ?thesis
        using le_diff_conv by blast
    qed
  qed
  thus ?thesis
  proof-
    have "Least ?P * r + (a + c) = Least ?Q * r + b + d"
      using thesis1 ineq
      by (metis le_add_diff_inverse2)
    thus ?thesis
      unfolding add.assoc unfolding add.commute[of a] add.commute[of b]  unfolding add.assoc[symmetric]
      unfolding nat_distrib(1) by algebra
  qed
qed

lemma roundbound_correct_aux: assumes "(0 :: nat) < r " and
  "(a + c) mod r = (b + d) mod r"
shows
  "a + (((LEAST k. max a b + max c d - a - c \<le> k * r) + l) * r + c) =
   b + (((LEAST k. max a b + max c d - b - d \<le> k * r) + l) * r + d)"
  using roundbound_correct_aux_asym[OF assms]  roundbound_correct_aux_asym[of r b d a c, OF \<open>0 < r\<close> assms(2)[symmetric],
      symmetric, unfolded max.commute[of b] max.commute[of d]] by (cases rule: le_cases)

lemma roundbound_suits: assumes "\<^bold>|rr (\<not> a)\<^bold>| = \<^bold>|rr a\<^bold>|"
  shows "roundbound a = (LEAST k. block_part a  \<le> k * \<^bold>|rr a\<^bold>|) + 2"
  using Least_addition[OF mroot_len[folded neq0_conv], of "block_part a" 2 a]
  unfolding roundbound_def assms  mult_2 add.assoc.

lemma  roundbound_correct: assumes eq: "pg \<cdot> g w \<cdot> sg = ph \<cdot> h w \<cdot> sh" and
  ineql: "great_length \<le> \<^bold>|pg \<cdot> g w \<cdot> sg\<^bold>|"
shows "pp a \<cdot> (rr a)\<^sup>@(roundbound a) \<cdot> ss a = pp (\<not> a) \<cdot> (rr (\<not> a))\<^sup>@(roundbound (\<not> a)) \<cdot> ss (\<not> a)"
proof (rule long_presolution_correct[OF assms, unfolded lenmorph pow_len])
  from gper.per_morph_im_exp
  have gw: "g w = gr\<^sup>@(gper.im_exp w)".
  from hper.per_morph_im_exp
  have hw: "h w = hr\<^sup>@(hper.im_exp w)".
  from long_presol_bound_symmetric[OF assms(1-2)[unfolded gw hw]]
  obtain r s b where "r \<cdot> s = rr b" "s \<cdot> r = rr (\<not> b)"
    by metis
  have rra: "\<^bold>|rr (\<not> a)\<^bold>| = \<^bold>|rr a\<^bold>|"
    using lenarg[OF \<open>r \<cdot> s = rr b\<close>] lenarg[OF \<open>s \<cdot> r = rr (\<not> b)\<close>, unfolded swap_len] by (cases "a = b", simp_all)
  show "\<^bold>|pp a\<^bold>| + (roundbound a * \<^bold>|rr a\<^bold>| + \<^bold>|ss a\<^bold>|) = \<^bold>|pp (\<not> a)\<^bold>| + (roundbound (\<not> a) * \<^bold>|rr (\<not> a)\<^bold>| + \<^bold>|ss (\<not> a)\<^bold>|)"
    unfolding roundbound_suits[OF rra] roundbound_suits[of "\<not> a", unfolded bool_simps, OF rra[symmetric]] bool_simps block_part_def rra
      max.commute[of "\<^bold>|_ (\<not> a)\<^bold>|"]
  proof (rule  roundbound_correct_aux)
    show "0 < \<^bold>|rr a\<^bold>|"
      using mroot_len by auto
    show "(\<^bold>|pp a\<^bold>| + \<^bold>|ss a\<^bold>|) mod \<^bold>|rr a\<^bold>| = (\<^bold>|pp (\<not> a)\<^bold>| + \<^bold>|ss (\<not> a)\<^bold>|) mod \<^bold>|rr a\<^bold>|"
    proof-
      from gper.per_morph_im_exp
      obtain e1 where gw: "g w = gr\<^sup>@e1"
        by blast
      from hper.per_morph_im_exp
      obtain e2 where hw: "h w = hr\<^sup>@e2"
        by blast
      have eq': "pp a \<cdot> (rr a)\<^sup>@(if a then e1 else e2) \<cdot> ss a = pp (\<not> a) \<cdot> (rr (\<not> a))\<^sup>@(if a then e2 else e1) \<cdot> ss (\<not> a)"
        using eq unfolding gw hw pp_def rr_def ss_def by (induct a) simp_all
      from arg_cong[OF lenarg[OF this, unfolded lenmorph pow_len rra], of "\<lambda> x. x mod \<^bold>|rr a\<^bold>|"]
      show  "(\<^bold>|pp a\<^bold>| + \<^bold>|ss a\<^bold>|) mod \<^bold>|rr a\<^bold>| = (\<^bold>|pp (\<not> a)\<^bold>| + \<^bold>|ss (\<not> a)\<^bold>|) mod \<^bold>|rr a\<^bold>|"
        unfolding add.commute[of "\<^bold>|pp _\<^bold>|"] add.assoc mod_mult_self3.
    qed
  qed
qed

\<comment> \<open>If there exists a rootsolution with roundbound powers then that equal lengths of rows imply a
solution .
\<close>

lemma  roundbound_sol : assumes eq: "pg \<cdot> gr\<^sup>@(roundbound True) \<cdot> sg = ph \<cdot> hr\<^sup>@(roundbound False) \<cdot> sh"
  and len: "\<^bold>|pg \<cdot> g v \<cdot> sg\<^bold>| = \<^bold>|ph \<cdot> h v \<cdot> sh\<^bold>|"
shows "pg \<cdot> g v \<cdot> sg = ph \<cdot> h v \<cdot> sh"
proof-
  obtain e1 where gv[symmetric]: "g v  = gr\<^sup>@e1"
    using gper.per_morph_im_exp by blast
  obtain e2 where hv[symmetric]: "h v  = hr\<^sup>@e2"
    using hper.per_morph_im_exp by blast
  define ee where "ee = (\<lambda> x. (if x then e1 else e2))"
  from long_presol_bound_symmetric[OF eq roundbound_long'[folded great_length_sym, of True, unfolded gpcp_simps]]
  obtain r s n m a where "r \<cdot> s = rr a" "s \<cdot> r = rr (\<not> a)" "pp (\<not> a) = pp a \<cdot> (r \<cdot> s) \<^sup>@ n \<cdot> r"
    "ss a = r \<cdot> (s \<cdot> r) \<^sup>@ m \<cdot> ss (\<not> a) \<or> ss (\<not> a) = s \<cdot> (r \<cdot> s) \<^sup>@ m \<cdot> ss a".

  have len': "\<^bold>|pp a \<cdot> (rr a)\<^sup>@ ee a \<cdot> ss a\<^bold>| = \<^bold>|pp (\<not> a) \<cdot> (rr (\<not> a))\<^sup>@ee (\<not> a) \<cdot> ss (\<not> a)\<^bold>|"
    by (induct a, unfold gpcp_simps ee_def gv hv, simp_all only: len)
  from disjE[OF \<open>ss a = r \<cdot> (s \<cdot> r) \<^sup>@ m \<cdot> ss (\<not> a) \<or> ss (\<not> a) = s \<cdot> (r \<cdot> s) \<^sup>@ m \<cdot> ss a\<close>]
  have "pp a \<cdot> (rr a)\<^sup>@ ee a \<cdot> ss a = pp (\<not> a) \<cdot> (rr (\<not> a))\<^sup>@ee (\<not> a) \<cdot> ss (\<not> a)"
  proof (cases)
    assume "ss a = r \<cdot> (s \<cdot> r) \<^sup>@ m \<cdot> ss (\<not> a)"
    note rsimp = \<open>r \<cdot> s = rr a\<close>[symmetric] \<open>s \<cdot> r = rr (\<not> a)\<close>[symmetric] \<open>pp (\<not> a) = pp a \<cdot> (r \<cdot> s) \<^sup>@ n \<cdot> r\<close> this
    from len'[unfolded rsimp shifts]
    have "(ee a +  m)* \<^bold>|s \<cdot> r\<^bold>| = (n + ee (\<not> a))*\<^bold>|s \<cdot> r\<^bold>|"
      unfolding lenmorph pow_len nat_distrib by algebra
    hence "ee a +  m = n + ee (\<not> a)"
      using mroot_len[of "\<not> a", folded \<open>s \<cdot> r = rr (\<not> a)\<close>] by force
    thus ?thesis
      unfolding rsimp lassoc cancel_right unfolding rassoc cancel shifts
      unfolding add_exps[symmetric]  by (simp add: add.commute)
  next
    assume "ss (\<not> a) = s \<cdot> (r \<cdot> s) \<^sup>@ m \<cdot> ss a"
    note rsimp = \<open>r \<cdot> s = rr a\<close>[symmetric] \<open>s \<cdot> r = rr (\<not> a)\<close>[symmetric] \<open>pp (\<not> a) = pp a \<cdot> (r \<cdot> s) \<^sup>@ n \<cdot> r\<close> this
    from len'[unfolded rsimp shifts]
    have "ee a* \<^bold>|r \<cdot> s\<^bold>| = (n + ee (\<not> a) + m + 1)*\<^bold>|r \<cdot> s\<^bold>|"
      unfolding lenmorph pow_len nat_distrib by algebra
    hence *: "ee a = Suc(n + ee (\<not> a) + m)"
      unfolding  mult_right_cancel[OF  gr_implies_not0[OF mroot_len[of a, folded \<open>r \<cdot> s = rr a\<close>]]] by simp
    thus ?thesis
      unfolding rsimp lassoc cancel_right unfolding rassoc cancel shifts
      unfolding add_exps[symmetric] * pow_Suc lassoc cancel add.assoc
      by (simp add: add.commute)
  qed
  thus "pg \<cdot> g v \<cdot> sg = ph \<cdot> h v \<cdot> sh"
    by (induct a) (unfold gpcp_simps ee_def gv hv, force+)
qed

subsection \<open>Finding "Long solution" using lengths.\<close>

\<comment> \<open>Similarly to @{term "get_solution"}, we define a word  @{term "LengthSol_cand"} using the
Diophantine equation on length of the images.  @{term "length_admissible"} corresponds to
 @{term "length_correct"} predicate.
\<close>

definition block_dif where "block_dif \<equiv> (int \<^bold>|ph\<^bold>|) - (int \<^bold>|pg\<^bold>|) + (int \<^bold>|sh\<^bold>|) - (int \<^bold>|sg\<^bold>|)"
definition a_dif where "a_dif \<equiv> (int \<^bold>|g \<aa>\<^bold>|) - int (\<^bold>|h \<aa>\<^bold>|)"
definition b_dif where "b_dif \<equiv> (int \<^bold>|g \<bb>\<^bold>|) - int (\<^bold>|h \<bb>\<^bold>|)"

definition length_admissible :: "bool"
  where "length_admissible = Dio_nat_solvable a_dif b_dif block_dif"

lemma length_admissibleI:
  assumes "pg \<cdot> g w \<cdot> sg = ph \<cdot> h w \<cdot> sh" and "w \<noteq> \<epsilon>"
  shows "length_admissible"
  unfolding length_admissible_def
proof
  show "a_dif*(count_list w bina) + b_dif*(count_list w binb) = block_dif"
    unfolding a_dif_def b_dif_def block_dif_def
    using arg_cong[OF lenarg[OF assms(1)], of int]
    unfolding gper.bin_per_morph_expI[of w] hper.bin_per_morph_expI[of w]
      lenmorph pow_len gpcp_simps  ring_distribs int_ops(5,7) mult.commute[of "int \<^bold>|_\<^bold>|"] mult.assoc
    unfolding int_ops(7)[symmetric] by algebra
  show "0 \<le> int (count_list w bina)" "0 \<le> int (count_list w binb)"
    by simp_all
  thus "int (count_list w bina) + int (count_list w binb) \<noteq> 0"
    using bin_len_count'[of w] nemp_len[OF \<open>w \<noteq> \<epsilon>\<close>] by presburger
qed

definition LengthSol_cand :: "binA list"
  where "LengthSol_cand =
(
let sol = solve_Dio_nat a_dif b_dif block_dif
in
  \<aa>\<^sup>@nat (fst sol) \<cdot> \<bb>\<^sup>@ nat (snd sol)
)
"

lemma LengthSol_cand_nemp: assumes length_admissible
  shows "LengthSol_cand \<noteq> \<epsilon>"
proof-
  let ?s = block_dif and
    ?a = a_dif and
    ?b = b_dif
  from assms[unfolded length_admissible_def Let_def Dio_nat_solvable_def]
  have nonneg: "0 \<le> fst(solve_Dio_nat ?a ?b ?s)" "0 \<le> snd(solve_Dio_nat ?a ?b ?s)"
    by blast+
  from solve_Dio_nat_pos assms[unfolded length_admissible_def]
  have "fst (solve_Dio_nat ?a ?b ?s) + snd (solve_Dio_nat ?a ?b ?s) \<noteq> 0"
    by presburger
  thus ?thesis
    unfolding LengthSol_cand_def Let_def using nonneg
    by simp
qed

subsection \<open>FindSolu\<close>

\<comment> \<open>Function @{term "FindSolu"} is the main part of our algorithm for the double periodic binary GPCP.
For a presolution  @{term "X"} and number  @{term "n"}, it calls @{term "NextCand X"}
and natural number  @{term "n-1"}.
\<close>

fun FindSolu::"'a presolution \<Rightarrow>  nat  \<Rightarrow> (bool \<times> binA list)"
  where "FindSolu  X 0 = ((valid X \<and> (over X True = over X False)) , LengthSol_cand)" |
    "FindSolu  X (Suc n) =
         (if valid X then
             if  solution_test X then (True, get_solution X)
             else FindSolu (NextCand X) n
                  else (False, undefined))"
    \<comment> \<open>The function terminates if one of the following conditions is met:
- n = 0 : the presolution is already too long
- the presolution X is not valid (a mismatch was encountered)
- the presolution X is a solution

We need to prove that for the presolution @{term "(IniPresol pg ph sg sh)"} @{term "FindSolu"} works
correctly, that is, it finds solutions. We start by showing the FindSolu return predicate True
when there exists a solution. \<close>

lemma FindSolu_recursion:  assumes "valid ((NextCand^^n) (IniPresol pg ph sg sh))" and
  "\<And> s. s \<le> n \<Longrightarrow> \<not> solution_test ((NextCand^^s) (IniPresol pg ph sg sh))"
  and "k \<le> n"
shows "FindSolu (IniPresol pg ph sg sh) (k+ Suc l) = FindSolu ((NextCand^^(Suc k)) (IniPresol pg ph sg sh)) l"
proof-
  have vd:" valid ((NextCand^^s) (IniPresol pg ph sg sh))" if "s<n" for s
    using assms(1)  valid_stable[of n pg ph sg sh s] that by blast
  have it:"FindSolu ((NextCand^^s) (IniPresol pg ph sg sh)) (Suc k) = FindSolu( (NextCand^^(Suc s))
                                             (IniPresol pg ph sg sh)) k"  if "s< n" for s
    using assms(2) vd FindSolu.simps(2) that by force
  show ?thesis
  proof (cases)
    assume "n=0"
    thus ?thesis
      using assms(1) assms(2) assms(3) by auto
  next
    assume "\<not> n=0"
    hence "0<n"
      by fast
    show ?thesis
      using assms
    proof (induct k arbitrary:  l)
      case 0
      show ?case
        using  it[OF \<open>0<n\<close>]  "0.prems"(3) assms(2) vd
          \<open>0 < n\<close> by fastforce
    next
      case (Suc k)
      then show ?case
      proof-
        have  "valid ((NextCand^^ ((Suc k))) (IniPresol pg ph sg sh))"
        proof(cases)
          assume "Suc k <n"
          thus ?thesis
            using  vd by fast
        next
          assume "\<not> Suc k <n"
          hence "Suc k=n"
            using Suc.prems(3) by simp
          thus ?thesis
            using assms(1) by auto
        qed
        hence   "\<not> solution_test ((NextCand^^ (Suc k)) (IniPresol pg ph sg sh))"
          using Suc.prems(2)  Suc.prems(3) by blast
        hence  "FindSolu (IniPresol pg ph sg sh) (k+ (Suc (Suc(l)))) = FindSolu ((NextCand^^(Suc k)) (IniPresol pg ph sg sh)) (Suc l)"
          using Suc.hyps   FindSolu.simps(2)
          using Suc.prems(1) Suc.prems(3) Suc_lessD assms(2)
          using Suc_leD by blast
        hence eq1:"FindSolu (IniPresol pg ph sg sh) ((Suc k)+ (Suc(l))) = FindSolu ((NextCand^^(Suc k)) (IniPresol pg ph sg sh)) (Suc l)"
          by fastforce
        have "FindSolu ((NextCand^^(Suc k)) (IniPresol pg ph sg sh)) (Suc l) =
            FindSolu ((NextCand^^(Suc(Suc k))) (IniPresol pg ph sg sh)) l"
          using \<open>\<not> solution_test ((NextCand^^ (Suc k)) (IniPresol pg ph sg sh))\<close>
            Suc.prems FindSolu.simps(2)
          using \<open>valid ((NextCand ^^ (Suc k)) (IniPresol pg ph sg sh))\<close> not_valid_next_stable by auto
        thus ?case
          using eq1 by argo
      qed
    qed
  qed
qed

lemma FindSolu_recursion_out1:  assumes "solution_test ((NextCand^^(Suc n)) (IniPresol pg ph sg sh))" and
  "\<And> s. s \<le> n \<Longrightarrow> \<not> solution_test ((NextCand^^s) (IniPresol pg ph sg sh))"
shows "FindSolu (IniPresol pg ph sg sh) ((Suc n)+ (Suc l)) = (True, get_solution ((NextCand^^(Suc n)) (IniPresol pg ph sg sh))) "
proof-
  have vd: "valid ((NextCand^^(Suc n)) (IniPresol pg ph sg sh))"
    using solution_test_def assms(1) by blast
  hence "valid ((NextCand^^n) (IniPresol pg ph sg sh))"
    using  valid_stable[of "Suc n" pg ph sg sh n] by blast
  hence "FindSolu (IniPresol pg ph sg sh) (n+ Suc(Suc l)) = FindSolu ((NextCand^^(Suc n)) (IniPresol pg ph sg sh)) (Suc l)"
    using assms(2) FindSolu_recursion by blast
  hence "FindSolu (IniPresol pg ph sg sh) ((Suc n)+ (Suc l)) = FindSolu ((NextCand^^(Suc n)) (IniPresol pg ph sg sh)) (Suc l)"
    by fastforce
  thus ?thesis
    using vd assms(1) FindSolu.cases
    by simp
qed

lemma FindSolu_recursion_out2:  assumes "valid ((NextCand^^(Suc n)) (IniPresol pg ph sg sh))" and
  "\<And> s. s \<le> n \<Longrightarrow> \<not> solution_test ((NextCand^^s) (IniPresol pg ph sg sh))"
shows "FindSolu (IniPresol pg ph sg sh) ((Suc n)) = ((valid ((NextCand^^(Suc n)) (IniPresol pg ph sg sh))
                        \<and> over ((NextCand^^(Suc n)) (IniPresol pg ph sg sh)) True
                       = over ((NextCand^^(Suc n)) (IniPresol pg ph sg sh))False )  , LengthSol_cand) "
proof-
  have "valid ((NextCand^^n) (IniPresol pg ph sg sh))"
    using assms(1) valid_stable[of "Suc n" pg ph sg sh n]  by blast
  hence "FindSolu (IniPresol pg ph sg sh) (n + (Suc 0)) = FindSolu ((NextCand^^(Suc n)) (IniPresol pg ph sg sh)) 0"
    using assms(2) FindSolu_recursion by blast
  hence ss:"FindSolu (IniPresol pg ph sg sh) ((Suc n)+ 0) = FindSolu ((NextCand^^(Suc n)) (IniPresol pg ph sg sh)) 0"
    by fastforce
  thus ?thesis
    using  assms(1) FindSolu.elims(1)
    by simp
qed

lemma FindSolu_nonvalid: assumes "\<not> valid ((NextCand^^(Suc(Suc n))) (IniPresol pg ph sg sh))"
  and  "\<And> s. s \<le> Suc n \<Longrightarrow> \<not> solution_test ((NextCand^^s) (IniPresol pg ph sg sh))"
shows "fst (FindSolu (IniPresol pg ph sg sh) (Suc(Suc n)+l)) = False"
proof-
  obtain m where md: "m = (LEAST k. \<not> valid ((NextCand^^k) (IniPresol pg ph sg sh)))"
    using assms(1) by blast
  hence "\<not> valid ((NextCand^^m) (IniPresol pg ph sg sh))"
    using assms(1) wellorder_Least_lemma(1) by metis+
  have  "m \<le> Suc(Suc n)"
    using md assms(1) Least_le by metis+
  from this
  obtain k where "m+k=Suc(Suc n)"
    by (metis le_Suc_ex)
  show ?thesis
  proof (cases)
    assume "m=0"
    hence "\<not> valid (IniPresol pg ph sg sh)"
      using \<open>\<not> valid ((NextCand^^m) (IniPresol pg ph sg sh))\<close> by simp
    thus ?thesis
      using FindSolu.elims assms(2)
      unfolding prod.inject surjective_pairing[symmetric] by force
  next
    assume "\<not> m=0"
    hence "0<m"
      by blast
    from this
    obtain m1::nat where "m=Suc m1"
      using gr0_implies_Suc by presburger
    hence "valid ((NextCand^^m1) (IniPresol pg ph sg sh))"
      using md  lessI not_less_Least by metis
    have "\<And> s. s\<le> m1 \<Longrightarrow> \<not> solution_test ((NextCand^^s) (IniPresol pg ph sg sh))"
      using assms(2) \<open>m\<le> Suc(Suc n)\<close> unfolding \<open>m=Suc m1\<close> by force
    hence  "FindSolu (IniPresol pg ph sg sh) (m1 + Suc (k +l)) = FindSolu ((NextCand^^(Suc m1)) (IniPresol pg ph sg sh)) (k+l)"
      using FindSolu_recursion[OF \<open>valid ((NextCand^^m1) (IniPresol pg ph sg sh))\<close> ] by blast
    hence rec:"FindSolu (IniPresol pg ph sg sh) (Suc(Suc n) +l) = FindSolu ((NextCand^^(m)) (IniPresol pg ph sg sh)) (k+l)"
      using \<open>m+k=Suc(Suc n)\<close> \<open>m=Suc m1\<close>  add.assoc add_Suc_shift  by metis
    show ?thesis
    proof (cases)
      assume "k+l = 0"
      thus ?thesis
        using rec FindSolu.simps(1) \<open>\<not> valid ((NextCand^^m) (IniPresol pg ph sg sh))\<close> by force
    next
      assume "\<not> (k+l = 0)"
      from this
      obtain t where "Suc t =k +l"
        by (metis not0_implies_Suc)
      thus ?thesis
        using rec FindSolu.simps(2) \<open>\<not> valid ((NextCand^^m) (IniPresol pg ph sg sh))\<close> unfolding \<open>Suc t =k +l\<close>[symmetric]
        by force
    qed
  qed
qed

\<comment> \<open>
We need to prove that for the presolution @{term "(IniPresol pg ph sg sh)"} @{term "FindSolu"} works
correctly, that is, it finds solutions when called for@{term "roundbound True + roundbound False"}.
Note also that long solutions are found with that number of rounds. \<close>

lemma FindSolu_correct: assumes "pg \<cdot> g w \<cdot> sg = ph \<cdot> h w \<cdot> sh"  "w \<noteq> \<epsilon>"
  defines "output \<equiv> FindSolu (IniPresol pg ph sg sh) (roundbound True + roundbound False)"
  shows "fst(output)" (is "?A")
proof-
  obtain e1 where e1: "g w = gr\<^sup>@e1"
    using gper.per_morph_im_exp by blast
  obtain e2 where e2: "h w = hr\<^sup>@e2"
    using hper.per_morph_im_exp by blast
  have  "pg \<cdot> gr\<^sup>@e1 \<cdot>sg = ph \<cdot> hr\<^sup>@e2 \<cdot> sh"
    using assms(1) unfolding e1 e2 by argo
  hence "solution_test ((NextCand^^(e1+e2))(IniPresol pg ph sg sh))"
    using solution_test_NextCandI[OF assms(1) assms(2) e1 e2]
    by blast
  obtain m where md:"m = (LEAST k. solution_test ((NextCand^^k)(IniPresol pg ph sg sh)))"
    by blast
  hence "m\<le> e1 +e2" and  st:"solution_test ((NextCand^^m)(IniPresol pg ph sg sh))"
    and vm:"valid  ((NextCand^^m)(IniPresol pg ph sg sh))"
    using solution_test_def \<open>solution_test ((NextCand^^(e1+e2))(IniPresol pg ph sg sh))\<close>
      wellorder_Least_lemma(2) wellorder_Least_lemma(1)
    by meson+
  have "1 < (roundbound True + roundbound False)"
    using roundbound_sum_ge1 by blast
  have "0\<le>m"
    by simp
  let ?X= "((NextCand^^m) (IniPresol pg ph sg sh))"
  have non:"\<not> solution_test ((NextCand^^n) (IniPresol pg ph sg sh))" if "n < m" for n
    using \<open>n < m\<close> md not_less_Least by blast
  have vd:" valid ((NextCand^^n) (IniPresol pg ph sg sh))" if "n<m" for n
    using vm  valid_stable[of m pg ph sg sh n] that by blast
  show ?thesis
  proof (cases)
    assume "m=0"
    hence "solution_test (IniPresol pg ph sg sh)"
      using st by force
    hence "output
             = (True, get_solution (IniPresol pg ph sg sh))"
      using \<open>1<(roundbound True + roundbound False)\<close> FindSolu.simps(2) \<open>solution_test (IniPresol pg ph sg sh)\<close>
      unfolding output_def
      by (metis FindSolu.elims \<open>m = 0\<close> funpow_0 less_nat_zero_code vm)
    thus ?thesis
      by force
  next
    assume "\<not> m=0"
    hence "0<m"
      by blast
    obtain n1 where n1: "m=Suc n1"
      using \<open>0 < m\<close> not0_implies_Suc by blast
    show ?thesis
    proof (cases)
      assume sh: "m< (roundbound True)+  (roundbound False)"
      obtain l where l1:"m + (Suc l)= (roundbound True)+  (roundbound False)" and "0<Suc l"
        using sh less_iff_Suc_add by force
      have  "\<And> s. (s\<le>n1) \<Longrightarrow> \<not> solution_test ((NextCand ^^ s) (IniPresol pg ph sg sh))"
        using non n1 by simp
      thus ?thesis
        using FindSolu_recursion_out1[OF st[unfolded \<open>m=Suc n1\<close>], of l ] output_def  l1[unfolded  \<open>m=Suc n1\<close>]
        by fastforce
    next
      assume "\<not> m < (roundbound True)+  (roundbound False)"
      hence " (roundbound True)+  (roundbound False)\<le> m"
        by force
      hence "(roundbound True)+  (roundbound False)\<le> e1 + e2"
        using \<open>m\<le> e1 +e2\<close> by force
      hence "roundbound True \<le> e1 \<or> roundbound False\<le> e2"
        by fastforce
      have " great_length \<le> \<^bold>|pg \<cdot> g w \<cdot> sg\<^bold>|"
      proof (cases)
        assume "roundbound True \<le> e1"
        hence "\<^bold>|pg \<cdot> gr\<^sup>@(roundbound True) \<cdot> sg\<^bold>|\<le> \<^bold>|pg \<cdot> g w \<cdot> sg\<^bold>| "
          unfolding pow_len lenmorph  e1 by force
        have "great_length\<le>\<^bold>|pg \<cdot> gr\<^sup>@(roundbound True) \<cdot> sg\<^bold>|"
          using great_length_roundbound  pp_def ss_def rr_def
          by metis
        thus ?thesis
          using \<open>\<^bold>|pg \<cdot> gr\<^sup>@(roundbound True) \<cdot> sg\<^bold>|\<le> \<^bold>|pg \<cdot> g w \<cdot> sg\<^bold>|\<close> le_trans
          by blast
      next
        assume "\<not> roundbound True \<le> e1"
        hence   " roundbound False\<le> e2"
          using \<open>roundbound True \<le> e1 \<or> roundbound False\<le> e2\<close>
          by blast
        hence "\<^bold>|ph \<cdot> hr\<^sup>@(roundbound False) \<cdot> sh\<^bold>|\<le> \<^bold>|ph \<cdot> h w \<cdot> sh\<^bold>| "
          unfolding pow_len lenmorph  e2 by force
        have "great_length\<le>\<^bold>|ph \<cdot> hr\<^sup>@(roundbound False) \<cdot> sh\<^bold>|"
          using great_length_roundbound  pp_def ss_def rr_def
          by metis
        thus ?thesis
          using \<open>\<^bold>|ph \<cdot> hr\<^sup>@(roundbound False) \<cdot> sh\<^bold>|\<le> \<^bold>|ph \<cdot> h w \<cdot> sh\<^bold>|\<close> assms(1) le_trans
          by auto
      qed
      hence rb:"pg \<cdot> gr\<^sup>@(roundbound True) \<cdot> sg = ph \<cdot> hr\<^sup>@(roundbound False) \<cdot> sh"
        using roundbound_correct[OF assms(1)] unfolding  pp_def rr_def ss_def
        by presburger
      hence vsum:"valid ((NextCand^^(roundbound True + roundbound False)) (IniPresol pg ph sg sh))"
        using NextCand_rootsol[OF rb]
        using \<open>roundbound True + roundbound False \<le> m\<close> le_neq_implies_less vd vm by blast
      have eps:"over  ((NextCand^^(roundbound True + roundbound False)) (IniPresol pg ph sg sh)) True
               =  over  ((NextCand^^(roundbound True + roundbound False)) (IniPresol pg ph sg sh)) False "
        using NextCand_rootsol[OF rb]  Next_next_over_pow
        by simp
      obtain n2 where n2:"(Suc n2)=(roundbound True + roundbound False)"
        using   \<open>1<(roundbound True + roundbound False)\<close>  One_nat_def  add_Suc_shift less_imp_Suc_add
        by metis
      have  n2b: "\<And> s. (s\<le>n2) \<Longrightarrow> \<not> solution_test ((NextCand ^^s) (IniPresol pg ph sg sh))"
        using non n2  \<open>\<not> m < roundbound True + roundbound False\<close> by fastforce
      thus ?thesis
        using  eps vsum FindSolu_recursion_out2[OF vsum[unfolded n2[symmetric]] n2b ] unfolding n2
          output_def by simp
    qed
  qed
qed

subsection \<open>Complete algorithm\<close>

\<comment> \<open> We begin by showing that the @{term "length_admissible"} works correctly. Note that
we need to avoid \<epsilon> from the lengths too. If @{term "\<^bold>|pg\<^bold>| + \<^bold>|sg\<^bold>| = \<^bold>|ph\<^bold>| + \<^bold>|sh\<^bold>|"} then (0,0) is a
solution to Diophantine.  \<close>

lemma length_admissible_cand: assumes "length_admissible"
  shows " (\<^bold>|pg \<cdot> g LengthSol_cand  \<cdot>sg\<^bold>| = \<^bold>|ph \<cdot> h LengthSol_cand \<cdot>sh\<^bold>| \<and> LengthSol_cand \<noteq> \<epsilon>)"
proof-
  let ?s = block_dif and
    ?a = a_dif and
    ?b = b_dif
  let ?x = "fst(solve_Dio_nat ?a ?b ?s)" and
    ?y = "snd(solve_Dio_nat ?a ?b ?s)"
  have xy:"LengthSol_cand= \<aa>\<^sup>@(nat ?x) \<cdot> \<bb>\<^sup>@(nat ?y)"
    unfolding LengthSol_cand_def Let_def..
  have "LengthSol_cand \<noteq> \<epsilon>"
    using assms(1) LengthSol_cand_nemp by blast
  have "Dio_nat_solvable ?a ?b ?s"
    using \<open>length_admissible\<close> length_admissible_def by presburger
  from this[unfolded Dio_nat_solvable_def]
  have "?a*?x  + ?b*?y = ?s" and "0 \<le> ?x"  and "0 \<le> ?y"
    by blast+
  hence "\<^bold>|pg\<^bold>| + \<^bold>|g \<aa>\<^bold>|*?x + \<^bold>|g \<bb>\<^bold>|*?y + \<^bold>|sg\<^bold>|
         = \<^bold>|ph\<^bold>| + \<^bold>|h \<aa>\<^bold>|*?x + \<^bold>|h \<bb>\<^bold>|*?y + \<^bold>|sh\<^bold>|"
    using nat_0_le[OF \<open>0 \<le> ?x\<close>] nat_0_le[OF \<open>0 \<le> ?y\<close>]
    unfolding a_dif_def b_dif_def block_dif_def int_simps by algebra
  hence ll:" \<^bold>|pg\<^bold>| + \<^bold>|g \<aa>\<^bold>|*(nat ?x) + \<^bold>|g \<bb>\<^bold>|*(nat ?y) + \<^bold>|sg\<^bold>|
            =  \<^bold>|ph\<^bold>| + \<^bold>|h \<aa>\<^bold>|*(nat ?x) + \<^bold>|h \<bb>\<^bold>|*(nat ?y) + \<^bold>|sh\<^bold>| "
    unfolding int_simps[symmetric] nat_0_le[OF \<open>0 \<le> ?x\<close>] nat_0_le[OF \<open>0 \<le> ?y\<close>].
  have gl:"\<^bold>|g (\<aa>\<^sup>@(nat ?x)\<cdot> \<bb>\<^sup>@(nat ?y))\<^bold>| =  \<^bold>|g \<aa>\<^bold>|*(nat ?x) +  \<^bold>|g \<bb>\<^bold>|*(nat ?y)"
    by (simp add: g.morph g.pow_morph pow_len)
  have hl:"\<^bold>|h (\<aa>\<^sup>@(nat ?x)\<cdot> \<bb>\<^sup>@(nat ?y))\<^bold>| =  \<^bold>|h \<aa>\<^bold>|*(nat ?x) +  \<^bold>|h \<bb>\<^bold>|*(nat ?y)"
    unfolding h.morph h.pow_morph pow_len lenmorph by algebra
  have "\<^bold>|pg\<^bold>| +  \<^bold>|g (\<aa>\<^sup>@(nat ?x)\<cdot> \<bb>\<^sup>@(nat ?y))\<^bold>|  +  \<^bold>|sg\<^bold>|
        =  \<^bold>|ph\<^bold>| + \<^bold>|h  (\<aa>\<^sup>@(nat ?x)\<cdot> \<bb>\<^sup>@(nat ?y))\<^bold>| + \<^bold>|sh\<^bold>| "
    using ll unfolding gl hl by presburger
  thus ?thesis
    using \<open>LengthSol_cand \<noteq> \<epsilon>\<close> unfolding xy lenmorph add.assoc by blast
qed

lemma length_admissible_correct: assumes "\<^bold>|pg \<cdot> g w  \<cdot>sg\<^bold>| = \<^bold>|ph \<cdot> h w \<cdot>sh\<^bold>|" "w \<noteq> \<epsilon>"
  shows length_admissible
proof-
  let ?e0 = "count_list w bina" and
    ?e1 = "count_list w binb" and
    ?s  = block_dif and
    ?a  = a_dif and
    ?b  = b_dif
  have  "\<^bold>|pg\<^bold>| + ?e0*\<^bold>|g \<aa>\<^bold>| + ?e1*\<^bold>|g \<bb>\<^bold>| + \<^bold>|sg\<^bold>|
       = \<^bold>|ph\<^bold>| + ?e0*\<^bold>|h \<aa>\<^bold>| + ?e1*\<^bold>|h \<bb>\<^bold>| + \<^bold>|sh\<^bold>|"
    using assms unfolding
      gper.sorted_image[of w bina] hper.sorted_image[of w bina] lenmorph pow_len binA_simps by simp
  hence "(int \<^bold>|pg\<^bold>|) + (int \<^bold>|g \<aa>\<^bold>|)*int ?e0 + (int \<^bold>|g \<bb>\<^bold>|)*int ?e1 + int \<^bold>|sg\<^bold>|
        =  (int \<^bold>|ph\<^bold>|) + (int \<^bold>|h \<aa>\<^bold>|)*int ?e0 + (int \<^bold>|h \<bb>\<^bold>|)*int ?e1 + int \<^bold>|sh\<^bold>|"
    unfolding int_simps mult.commute[of "count_list _ _"].
  hence ds:" ?a*int ?e0  + ?b*int ?e1 =  ?s"
    unfolding a_dif_def b_dif_def block_dif_def by algebra
  have "0 \<le> int(?e0) \<and> 0 \<le> int(?e1)"
    by simp
  have "\<^bold>|w\<^bold>| = count_list w bina + count_list w binb"
    using bin_len_count[of w bina, unfolded binA_simps].
  hence "int(?e0) + int(?e1) \<noteq> 0"
    using nemp_len[OF \<open>w \<noteq> \<epsilon>\<close>] by presburger
  hence "Dio_nat_solvable ?a ?b ?s"
    using Dio_nat_solvableI[of ?a _ ?b _ block_dif] ds \<open>0 \<le> int(?e0) \<and> 0 \<le> int(?e1)\<close>
    by blast
  thus "length_admissible"
    using length_admissible_def by presburger
qed

\<comment> \<open> Clearly, there can exists a solution only if the  @{term "length_admissible"} is True, that is,
there exists a word @{term "w"} such that  @{term "\<^bold>|pg \<cdot> g w  \<cdot>sg\<^bold>| = \<^bold>|ph \<cdot> h w \<cdot>sh\<^bold>|"} and @{term "w \<noteq> \<epsilon>"}.
If @{term "length_admissible"} then the existence of the solution is given by the predicate
@{term "fst(FindSolu (IniPresol pg ph sg sh) (roundbound True + roundbound False))"} according to
lemma @{term "FindSolu_correct"}.
\<close>

definition Algorithm_GPCP_both_periodic :: "bool \<times> binA list"
  where "Algorithm_GPCP_both_periodic =
(if length_admissible
then  FindSolu (IniPresol pg ph sg sh) (roundbound True + roundbound False)
else (False, undefined))"

\<comment> \<open> What is left is to show that when
@{term "fst(FindSolu (IniPresol pg ph sg sh) (roundbound True + roundbound False))"} is True,
then there exists a solution, and that
@{term "snd(FindSolu (IniPresol pg ph sg sh) (roundbound True + roundbound False))"} is a solution.

We begin with a lemma which corresponds  the case of long solutions only.
\<close>

lemma Algorithm_GPCP_both_periodic_fullrounds:
  defines "bound \<equiv> roundbound True + roundbound False" and
    "I \<equiv> IniPresol pg ph sg sh"
  defines "output \<equiv> FindSolu I bound"
  assumes "length_admissible" and
    "FindSolu I bound = (True, LengthSol_cand)" and
    "valid ((NextCand^^bound) I)" and
    "over ((NextCand^^bound) I) True = over ((NextCand^^bound) I) False"
  shows "pg \<cdot> g LengthSol_cand \<cdot> sg = ph \<cdot> h LengthSol_cand \<cdot> sh" and "LengthSol_cand \<noteq> \<epsilon>"
proof-
  show "LengthSol_cand \<noteq> \<epsilon>"
    using LengthSol_cand_nemp[OF assms(4)].

  let ?X="(NextCand^^bound) I"
  obtain ep1 ep2 es1 es2 where meq: "bound = ep1 + ep2 + es1 + es2" and
    cx:" count ?X = (\<lambda> x. (if x then ep1 + es1 else ep2 + es2))"  and
    ox:" over ?X  = over ((IniPresol (pg \<cdot> gr\<^sup>@ep1) (ph \<cdot>hr\<^sup>@ep2) (gr\<^sup>@es1 \<cdot> sg) (hr\<^sup>@es2\<cdot> sh)))" and
    vx:"valid ?X = ((pg \<cdot> gr\<^sup>@ep1) \<bowtie> (ph \<cdot>hr\<^sup>@ep2) \<and> (gr\<^sup>@es1 \<cdot> sg) \<bowtie>\<^sub>s (hr\<^sup>@es2\<cdot> sh))" and
    xini:"?X = (IniPresol (pg \<cdot> gr\<^sup>@ep1) (ph \<cdot>hr\<^sup>@ep2) (gr\<^sup>@es1 \<cdot> sg)  (hr\<^sup>@es2\<cdot> sh))\<lparr>count := (\<lambda> x. (if x then ep1 + es1 else ep2 + es2))\<rparr>"
    using NextCand_computation[of bound pg ph sg sh] unfolding I_def  by blast
  hence comp:"(pg \<cdot> gr\<^sup>@ep1) \<bowtie> (ph \<cdot>hr\<^sup>@ep2)" and scomp:" (gr\<^sup>@es1 \<cdot> sg) \<bowtie>\<^sub>s (hr\<^sup>@es2\<cdot> sh)"
    using assms by blast+
  have "over ?X True = over ?X False"
    using assms by argo
  hence "pg \<cdot> gr\<^sup>@(ep1+es1)\<cdot> sg = ph \<cdot> hr\<^sup>@(ep2+es2)\<cdot> sh"
    using NextCand_sol_aux[of bound pg ph sg sh, folded bound_def I_def, OF assms(6-7)] unfolding cx
    by argo
  have lsc:" \<^bold>|pg \<cdot> g LengthSol_cand  \<cdot>sg\<^bold>| = \<^bold>|ph \<cdot> h LengthSol_cand \<cdot>sh\<^bold>|" and " LengthSol_cand \<noteq> \<epsilon>"
    using length_admissible_cand[OF \<open>length_admissible\<close>] by blast+
  have "roundbound True \<le> (ep1+es1) \<or> roundbound False \<le> (ep2 + es2)"
    using meq unfolding bound_def by fastforce
  show "pg \<cdot> g LengthSol_cand \<cdot> sg = ph \<cdot> h LengthSol_cand \<cdot> sh"
  proof (cases)
    assume "roundbound True \<le> (ep1+es1)"
    hence "great_length\<le> \<^bold>|pg \<cdot> gr\<^sup>@(ep1+es1)\<cdot> sg\<^bold>|"
      using  roundbound_great_length  linorder_not_less pp_def rr_def ss_def unfolding lenmorph pow_len
      by metis
    thus ?thesis
      using long_bound_bound[OF \<open>pg \<cdot> gr\<^sup>@(ep1+es1)\<cdot> sg = ph \<cdot> hr\<^sup>@(ep2+es2)\<cdot> sh\<close> \<open>great_length\<le> \<^bold>|pg \<cdot> gr\<^sup>@(ep1+es1)\<cdot> sg\<^bold>|\<close>]
        lsc gper.per_morph_im_exp[of LengthSol_cand] hper.per_morph_im_exp[of LengthSol_cand] by metis
  next
    assume "\<not> roundbound True \<le> (ep1+es1)"
    hence  "roundbound False \<le> (ep2 + es2)"
      using \<open>roundbound True \<le> (ep1+es1) \<or> roundbound False \<le> (ep2 + es2)\<close>
      by blast
    hence "great_length\<le> \<^bold>|ph \<cdot> hr\<^sup>@(ep2+es2)\<cdot> sh\<^bold>|"
      using  roundbound_great_length  linorder_not_less pp_def rr_def ss_def unfolding lenmorph pow_len
      by metis
    hence "great_length\<le> \<^bold>|pg \<cdot> gr\<^sup>@(ep1+es1)\<cdot> sg\<^bold>|"
      using \<open>pg \<cdot> gr\<^sup>@(ep1+es1)\<cdot> sg = ph \<cdot> hr\<^sup>@(ep2+es2)\<cdot> sh\<close> by argo
    thus ?thesis
      using long_bound_bound[OF \<open>pg \<cdot> gr\<^sup>@(ep1+es1)\<cdot> sg = ph \<cdot> hr\<^sup>@(ep2+es2)\<cdot> sh\<close> \<open>great_length\<le> \<^bold>|pg \<cdot> gr\<^sup>@(ep1+es1)\<cdot> sg\<^bold>|\<close>]  lsc
        gper.per_morph_im_exp hper.per_morph_im_exp by metis
  qed
qed

\<comment> \<open> Next we prove that
@{term "fst(FindSolu (IniPresol pg ph sg sh) (roundbound True + roundbound False))"} implies
@{term "snd(FindSolu (IniPresol pg ph sg sh) (roundbound True + roundbound False))"} is a solution.
\<close>

theorem Algorithm_GPCP_both_periodic_correct_solution:
  assumes "fst(Algorithm_GPCP_both_periodic)"
  shows " (pg \<cdot> g (snd(Algorithm_GPCP_both_periodic)) \<cdot> sg = ph \<cdot> h (snd(Algorithm_GPCP_both_periodic)) \<cdot> sh) \<and>
 snd(Algorithm_GPCP_both_periodic) \<noteq> \<epsilon>"
proof-
  have "length_admissible"
    using Algorithm_GPCP_both_periodic_def assms(1)
    by (metis fst_conv)
  hence agl1:"(Algorithm_GPCP_both_periodic) = FindSolu (IniPresol pg ph sg sh) (roundbound True + roundbound False) "
    using  Algorithm_GPCP_both_periodic_def by argo
  let ?X= "((NextCand^^(roundbound True + roundbound False)) (IniPresol pg ph sg sh))"
  show ?thesis
  proof (cases)
    assume sh:" \<exists> n. (solution_test ((NextCand^^n)(IniPresol pg ph sg sh)) \<and> n \<le> roundbound True + roundbound False )"
    obtain m where md:"m = (LEAST k. solution_test ((NextCand^^k)(IniPresol pg ph sg sh)))"
      by blast
    have "m \<le> roundbound True + roundbound False"
      using md wellorder_class.Least_le  dual_order.trans sh
      by meson
    have stm: "solution_test ((NextCand^^m)(IniPresol pg ph sg sh))"
      using md LeastI sh by fast
    show ?thesis
    proof (cases "m=0")
      assume "m=0"
      hence "solution_test (IniPresol pg ph sg sh)"
        using stm funpow_0  wellorder_Least_lemma(1) by fastforce
      have  "(Algorithm_GPCP_both_periodic) = (True, get_solution  (IniPresol pg ph sg sh))"
        using agl1 FindSolu.simps(2) FindSolu.elims \<open>solution_test (IniPresol pg ph sg sh)\<close>
          solution_test_def  nat_less_le  less_nat_zero_code roundbound_sum_ge1
        unfolding  \<open>m = 0\<close>
        by metis
      thus ?thesis
        using \<open>solution_test (IniPresol pg ph sg sh)\<close> solution_testD[of 0 pg ph sg sh] by auto
    next
      assume "m\<noteq> 0"
      hence "0<m"
        by simp
      from this
      obtain m1 where "m=Suc m1"
        using gr0_implies_Suc by presburger
      from this
      obtain k where "(Suc m1)+ k = roundbound True + roundbound False"
        using \<open>m \<le> roundbound True + roundbound False\<close> less_iff_Suc_add   Suc_le_eq by auto
      hence kk: "\<And> s.  s\<le> m1\<Longrightarrow> \<not> solution_test ((NextCand ^^s) (IniPresol pg ph sg sh))"
        using md stm  Least_le not_less_eq_eq  \<open>m = Suc m1\<close> by metis
      show ?thesis
      proof (cases)
        assume "0<k"
        from this
        obtain l where "Suc l =k"
          using gr0_implies_Suc by blast
        hence "(Suc m1 + Suc l) = roundbound True + roundbound False"
          using \<open>Suc m1 + k = roundbound True + roundbound False\<close>
          by blast
        hence "(Algorithm_GPCP_both_periodic) = (True, get_solution ((NextCand^^m) (IniPresol pg ph sg sh)))"
          using agl1 FindSolu_recursion_out1[OF stm[unfolded \<open>m=Suc m1\<close> ] kk  ]
            \<open>(Suc m1 + Suc l) = roundbound True + roundbound False\<close>
          unfolding \<open>m=Suc m1\<close> by metis
        thus ?thesis
          using stm solution_testD[of m pg ph sg sh] by auto
      next
        assume "\<not> 0<k"
        hence "k=0" and "Suc m1 = roundbound True + roundbound False"
          using \<open>(Suc m1)+ k = roundbound True + roundbound False\<close> by simp+
        have "valid  ((NextCand^^(Suc m1)) (IniPresol pg ph sg sh))"
          using solution_test_def stm  unfolding \<open>m=Suc m1\<close>
          by blast
        hence call:"FindSolu (IniPresol pg ph sg sh) (roundbound True + roundbound False) = (
                   (valid ?X  \<and> over ?X True = over ?X False ) , LengthSol_cand)"
          using   FindSolu_recursion_out2[OF \<open>valid  ((NextCand^^(Suc m1)) (IniPresol pg ph sg sh))\<close>  kk  ]
          unfolding \<open>Suc m1 = roundbound True + roundbound False\<close>
          by presburger
        hence "over ?X True = over ?X False"
          using assms(1) agl1 by force
        thus ?thesis
          using call \<open>length_admissible\<close> agl1  Algorithm_GPCP_both_periodic_fullrounds \<open>valid  ((NextCand^^(Suc m1)) (IniPresol pg ph sg sh))\<close>
          unfolding  \<open>Suc m1 = roundbound True + roundbound False\<close> by simp
      qed
    qed
  next
    assume "\<not> (\<exists> n. (solution_test ((NextCand^^n)(IniPresol pg ph sg sh)) \<and> n \<le> roundbound True + roundbound False ))"
    hence non: "\<And> s.  (s\<le> roundbound True + roundbound False) \<Longrightarrow>   \<not> (solution_test ((NextCand^^s)(IniPresol pg ph sg sh)))"
      using  add_leD1
      by metis
    obtain t where "Suc(Suc t) = roundbound True + roundbound False"
      using roundbound_sum_ge1
      by (metis One_nat_def add_Suc_right add_Suc_shift less_imp_Suc_add)
    have non2: "\<And> s. s \<le> Suc t \<Longrightarrow> \<not> solution_test ((NextCand^^s) (IniPresol pg ph sg sh))"
      using non\<open>Suc(Suc t) = roundbound True + roundbound False\<close> by simp
    have vfull: "valid ?X"
    proof (rule ccontr)
      assume "\<not> valid ?X"
      hence "\<not> valid ((NextCand^^(Suc(Suc t)))(IniPresol pg ph sg sh))"
        using \<open>Suc(Suc t) = roundbound True + roundbound False\<close> by argo
      thus  False
        using FindSolu_nonvalid[OF \<open>\<not> valid ((NextCand^^(Suc(Suc t)))(IniPresol pg ph sg sh))\<close>
            non2, of 0 ] assms(1) agl1
        unfolding \<open>Suc(Suc t) = roundbound True + roundbound False \<close> by force
    qed
    hence call:"FindSolu (IniPresol pg ph sg sh) (roundbound True + roundbound False) =
          ((valid ?X  \<and> over ?X True  = over ?X False ) , LengthSol_cand)"
      using   FindSolu_recursion_out2[OF vfull[unfolded \<open>Suc(Suc t) = roundbound True + roundbound False\<close>[symmetric]] non2 ]
      unfolding \<open>Suc(Suc t) = roundbound True + roundbound False\<close>
      by blast
    hence "over ?X True  = over ?X False"
      using assms(1) agl1 by force
    thus ?thesis
      using  Algorithm_GPCP_both_periodic_fullrounds agl1 \<open>length_admissible\<close>
        vfull call by simp
  qed
qed

\<comment> \<open> Finally, we prove the correctness of our algorithm\<close>

theorem Algorithm_GPCP_both_periodic_correct_decision:
  "(\<exists> w. pg \<cdot> g w \<cdot> sg = ph \<cdot> h w \<cdot> sh \<and> w \<noteq> \<epsilon>) \<longleftrightarrow>  fst(Algorithm_GPCP_both_periodic)"
proof (rule iffI)
  assume "\<exists> w. (pg \<cdot> g w \<cdot> sg = ph \<cdot> h w \<cdot> sh \<and> w \<noteq> \<epsilon>)"
  from exE[OF this]
  have "length_admissible"
    using length_admissible_correct by metis
  hence fs2: "Algorithm_GPCP_both_periodic =
      FindSolu (IniPresol pg  ph sg sh)  (roundbound True + roundbound False)"
    using  Algorithm_GPCP_both_periodic_def by argo
  show "fst(Algorithm_GPCP_both_periodic)"
    using \<open>\<exists> w. (pg \<cdot> g w \<cdot> sg = ph \<cdot> h w \<cdot> sh \<and> w \<noteq> \<epsilon>)\<close>FindSolu_correct  unfolding fs2
    by blast
next
  assume "fst(Algorithm_GPCP_both_periodic)"
  thus " (\<exists> w. pg \<cdot> g w \<cdot> sg = ph \<cdot> h w \<cdot> sh \<and> w \<noteq> \<epsilon>)"
    using  Algorithm_GPCP_both_periodic_correct_solution by blast
qed

end

end
