theory ExampleMethods
  imports CoW.Submonoids
begin

section "Methods"

help

text\<open>This is an example theory which server to demonstrate various sever methods that we define
in our package.\<close>

subsection "List inspection method"

lemma "\<^bold>|w\<^bold>| \<le> 2 \<Longrightarrow> w \<in> lists {x,y,z} \<Longrightarrow> P w"
  apply(list_inspection)
  oops

lemma "\<^bold>|w\<^bold>| = 2 \<and> w \<in> lists {x,y,z} \<Longrightarrow> P w"
  apply(list_inspection)
  oops

text\<open>The required premise as 2 separate assumptions.\<close>

lemma "\<^bold>|w\<^bold>| \<le> 2 \<Longrightarrow> w \<in> lists {a,b} \<Longrightarrow> hd w = a \<Longrightarrow> w \<noteq> \<epsilon> \<Longrightarrow>  w = [a, b] \<or> w = [a, a] \<or> w = [a]"
  by list_inspection

lemma "\<^bold>|w\<^bold>| = 2 \<Longrightarrow> w \<in> lists {a,b} \<Longrightarrow> hd w = a \<Longrightarrow>  w = [a, b] \<or> w = [a, a]"
  by list_inspection

lemma "\<^bold>|w\<^bold>| = 2 \<and> w \<in> lists {a,b} \<Longrightarrow> hd w = a \<Longrightarrow>  w = [a, b] \<or> w = [a, a]"
  by list_inspection

lemma "w \<in> lists {a,b} \<and> \<^bold>|w\<^bold>| = 2 \<Longrightarrow> hd w = a \<Longrightarrow>  w = [a, b] \<or> w = [a, a]"
  by list_inspection

text\<open>To a reasonable extent, the method works independently on the order of assumptions
and with additional assumptions.\<close>
lemma "A \<Longrightarrow> w \<in> lists {a,b} \<Longrightarrow> \<^bold>|w\<^bold>| = 2 \<Longrightarrow> hd w = a \<Longrightarrow>  w = [a, b] \<or> w = [a, a]"
  by list_inspection

text\<open>However, the order of all premises matters - the first matching premise is taken into account.\<close>
lemma " w \<in> lists {a,b,c} \<Longrightarrow> w \<in> lists {a,b} \<Longrightarrow> \<^bold>|w\<^bold>| = 2 \<Longrightarrow> hd w = a \<Longrightarrow>  w = [a, b] \<or> w = [a, a]"
  apply(list_inspection)
  oops

subsection "Primitivity inspection"

lemma "x \<noteq> y \<Longrightarrow> primitive [x,y,x,x,y,x,x,y,y,x,y,x,x,y,x,x,y,y,x]"
  by primitivity_inspection

lemma "\<not> primitive [x,y,x,y]"
  by primitivity_inspection

lemma "x \<noteq> y \<Longrightarrow> primitive (([x,y,x,y]\<^sup>@6)\<cdot>[x])"
  by primitivity_inspection

lemma "x \<noteq> y \<Longrightarrow> primitive ([x]\<cdot>([x,y,x,y]\<^sup>@6)\<cdot>[x])"
  by primitivity_inspection

lemma "\<not> primitive ([x,y,x,y]\<cdot>[x,y]\<^sup>@7)"
  by primitivity_inspection

lemma "x \<noteq> y \<Longrightarrow> n \<noteq> 0 \<Longrightarrow> primitive (([x,y,x,y]\<^sup>@n)\<cdot>[x])"
  oops \<comment> \<open>this is out of scope of the method\<close>

subsection "Mismatch"

lemma "x\<cdot>y\<cdot>x = y\<cdot>x\<cdot>y \<Longrightarrow> x\<cdot>y = y\<cdot>x"
  by mismatch

lemma "j \<noteq> 0 \<Longrightarrow> x\<^sup>@j\<cdot>y\<cdot>x = y\<cdot>x\<cdot>y \<Longrightarrow> x\<cdot>y = y\<cdot>x"
  by mismatch

lemma "j \<noteq> 0 \<Longrightarrow> k \<noteq> 0 \<Longrightarrow> x\<cdot>x\<cdot>y\<cdot>x = x\<cdot>y\<cdot>x\<cdot>y \<Longrightarrow> x\<cdot>y = y\<cdot>x"
  by mismatch

lemma "s \<in> \<langle>{y,x}\<rangle> \<Longrightarrow> x\<cdot>y\<cdot>x \<le>p y\<cdot>s\<cdot>y\<cdot>x \<Longrightarrow> x\<cdot>y = y\<cdot>x"
  by mismatch

subsection "Inlists"

lemma "a \<in> lists A \<Longrightarrow> b \<in> lists A \<Longrightarrow> a\<cdot>b \<in> lists A"
  by inlists

end
