theory ExampleInfinite
  imports CoW_Infinite.Infinite_Words
begin

section "Infinite words"


subsection "Thue--Morse word"

text\<open>The Thue--Morse word is defined on a binary alphabet.
For binary alphabet, we use the type @{typ binA}.
The two letters are @{term bina} and @{term binb}.
We define the word as a fixed point of the Thue--Morse morphism.\<close>

locale ThueMorse_endomorphism = endomorphism f
  for f::"binA list \<Rightarrow> binA list" +
  assumes
    image0: "f\<^sup>\<C> bina = [bina, binb]" and
    image1: "f\<^sup>\<C> binb = [binb, bina]"
begin

lemma image: "f\<^sup>\<C> a = [a,1-a]"
proof(cases rule: two_elem_cases[OF a_in_bin_basis[of a]])
  case 1
  hence a: "a = bina" by simp
  show ?thesis using image0 unfolding core_sing a binsimp.
next
  case 2
  hence a: "a = binb" by simp
  show ?thesis using image1 unfolding core_sing a binsimp.
qed

lemma image2: "(f^^2) [a] = [a,1-a,1-a,a]"
  unfolding Suc_1[symmetric]
  unfolding funpow.simps comp_apply funpow_1
  unfolding image[unfolded core_sing]
  unfolding shifts
  unfolding morph
  unfolding image[unfolded core_sing]
  by simp

lemma image_pow: "(f ^^ Suc i) [a] = ((f^^i) [a]) \<cdot> ((f^^i) [1-a])"
  unfolding funpow_Suc_right
  unfolding comp_apply
  unfolding image[unfolded core_sing]
  unfolding shifts
  unfolding pow_morph..

sublocale nonerasing_morphism
  using image nonerI by force

lemma spref_sing: "(f ^^ i) [a] <p (f ^^ Suc i) [a]"
  unfolding image_pow
  by (simp add: core_nemp funpow_nonerasing_morphism nonerI nonerasing_morphism.sing_to_nemp)

lemma image_rev_sing[reversal_rule]: "rev (f [1-a]) = f [a]"
  unfolding image[unfolded core_sing] binsimp
  by auto

lemma pow2_image_rev_sing[reversal_rule]: "rev ((f^^(Suc (2*i))) [1-a]) = (f^^(Suc (2*i))) [a]"
proof(induction i arbitrary:a)
  case 0
  then show ?case
    using image_rev_sing
    by force
next
  case (Suc i)
  have sc: "2 * Suc i = Suc (Suc (2*i))"
    by simp
  have p2: "f(f w) = (f^^2) w" for w
    by (simp add: numeral_2_eq_2)
  have he: "[a, b, c, d] = [a]\<cdot>[b]\<cdot>[c]\<cdot>[d]" for a b c d by simp
  show ?case
    unfolding sc
    unfolding funpow_Suc_right[of "Suc k" for k] comp_apply
    unfolding p2
    unfolding image2 binsimp
    unfolding he
    unfolding pow_morph
    unfolding rev_append
    unfolding Suc Suc[of "1-a" for a, unfolded binsimp]
    by force
qed

corollary pow2_image_rev [reversal_rule]: "(f^^(Suc (2*i))) (rev w) = rev ((f^^(Suc (2*i))) (map (\<lambda>a.1-a) w))"
proof(induction w)
  case Nil
  then show ?case
    by (simp add: pow_emp_to_emp)
next
  case (Cons a w)
  show ?case
    unfolding map_hd
    unfolding hd_word[of a w]
    unfolding pow_morph
    unfolding rev_append
    unfolding Cons[symmetric]
    unfolding pow2_image_rev_sing
    unfolding rev_sing pow_morph..
qed

lemma fac_sing_exch: "w \<le>f (f ^^ k) [a] \<Longrightarrow>  w \<le>f (f ^^ (Suc k)) [1-a]"
  unfolding funpow_Suc_right comp_apply
  unfolding image[unfolded core_sing] binsimp
  unfolding shifts
  unfolding pow_morph
  by (simp add: fac_ext_pref)

text\<open>The Thue-Morse morphism has indeed two fixed points.\<close>

definition \<uu>0 where "\<uu>0 = fixed_point_of f bina"
definition \<uu>1 where "\<uu>1 = fixed_point_of f binb"

sublocale fp0: fixed_point f \<uu>0
  unfolding \<uu>0_def
  using spref_seq_fixed_point spref_sing by blast

sublocale fp1: fixed_point f \<uu>1
  unfolding \<uu>1_def
  using spref_seq_fixed_point spref_sing by blast

text\<open>They have the same language.
(Which in general follows from the fact that the generation morphism is primitive.)\<close>

lemma fp_lan_same: "\<F> \<uu>0 = \<F> \<uu>1"
  unfolding set_eq_iff
  unfolding fp0.fac_fac_pow_eq fp1.fac_fac_pow_eq
  unfolding fp0.fp_of_shd[OF \<uu>0_def] fp1.fp_of_shd[OF \<uu>1_def]   using fac_sing_exch[of _ _ bina] fac_sing_exch[of _ _ binb]
  by (smt (verit, del_insts) bin_neq_swap)

text\<open>The language of the Thue--Morse word is closed under reversal.\<close>

lemma fp_lan_closed_rev: assumes "w \<in> (\<F> \<uu>0)"
  shows "(rev w) \<in> (\<F> \<uu>0)"
proof-
  obtain k where
    "w \<le>f (f ^^ k) [shd \<uu>0]"
    using fp0.fac_fac_pow[OF assms].
  have "w \<le>f (f ^^ Suc (2 * Suc k)) [bina]"
    using fp0.fac_pow_ge[of _ "Suc (2*(Suc k))", OF _ \<open>w \<le>f (f ^^ k) [shd \<uu>0]\<close>]
    unfolding fp0.fp_of_shd[OF \<uu>0_def]
    unfolding mult_2 add_Suc_right
    by linarith
  from this[folded pow2_image_rev_sing[of _ bina]]
  have "(rev w) \<in> (\<F> \<uu>1)"
    unfolding binsimp fp1.fac_fac_pow_eq fp1.fp_of_shd[OF \<uu>1_def]
    unfolding sublist_rev_right by blast
  thus "(rev w) \<in> (\<F> \<uu>0)"
    unfolding fp_lan_same.
qed

end

end
