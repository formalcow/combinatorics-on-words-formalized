(*  Title:      Border Array Example
    Author:     Štěpán Holub, Charles University

Part of Combinatorics on Words Formalized. See https://gitlab.com/formalcow/combinatorics-on-words-formalized/
*)

theory ExampleBorderArray

imports
 CoW.Border_Array CoW_Interpretations.Binary_Code_Imprimitive

begin

section "Border array"

text \<open>An illustration of the possible use of the border array evaluation. The border array is used to establish whether
      a given word is primitive. This is currently more easy using the method @{method primitivity_inspection}\<close>

subsection \<open>Encoding\<close>

fun bin_encode :: "'b \<Rightarrow> 'b \<Rightarrow> 'b \<Rightarrow>  Enum.finite_2"
  where "bin_encode x y = (\<lambda> z. (if z = x then Enum.finite_2.a\<^sub>1 else Enum.finite_2.a\<^sub>2))"

fun bin_decode :: "'b \<Rightarrow> 'b \<Rightarrow> Enum.finite_2 \<Rightarrow> 'b"
  where "bin_decode x y = (\<lambda> z. (if z = Enum.finite_2.a\<^sub>1 then x else y))"

lemma bin_encode_eval: assumes "x \<noteq> y"
  shows "(bin_encode x y) y = Enum.finite_2.a\<^sub>2" and
    "(bin_encode x y) x = Enum.finite_2.a\<^sub>1"    and
    "(bin_decode x y) Enum.finite_2.a\<^sub>1 = x" and
    "(bin_decode x y) Enum.finite_2.a\<^sub>2 = y"
  using assms by simp_all

lemma bin_enc_inv: "x \<noteq> y \<Longrightarrow> ws \<in> lists {x,y} \<Longrightarrow> map (bin_decode x y) (map (bin_encode x y) ws) = ws"
  by (induct ws, simp, auto)

lemma map_pow: "map f (r\<^sup>@k) = (map f r)\<^sup>@k"
  by (induct k, simp_all)

lemma prim_map_prim: "primitive (map f ws) \<Longrightarrow> primitive ws"
  unfolding primitive_def using map_pow  by metis

lemma prim_bin_enc_iff: assumes "x \<noteq> y" and "ws \<in> lists {x,y}"
  shows "primitive ws \<longleftrightarrow> primitive (map (bin_encode x y) ws)"
  using  prim_map_prim[of "bin_decode x y" "map (bin_encode x y) ws", unfolded bin_enc_inv[OF assms]]
         prim_map_prim by auto

subsection \<open>Proof of concept\<close>

lemma spehner_example:
  assumes "x \<noteq> y" and "\<not> primitive (concat [x,y,x,y,y])"
  shows "x \<cdot> y = y \<cdot> x"
proof-
  have facts: "[x,y,x,y,y] \<in> lists {x,y}" "2 \<le> count_list [x, y, x, y, y] x" "2 \<le> count_list [x, y, x, y, y] y"  by simp_all
  have "primitive [x,y,x,y,y]"
    unfolding prim_bin_enc_iff[OF \<open>x \<noteq> y\<close> \<open>[x,y,x,y,y] \<in> lists {x,y}\<close>] list.map bin_encode_eval[OF \<open>x \<noteq> y\<close>]
    by eval
  from  bin_imprim_both_twice[OF _ facts this assms(2)]
  show ?thesis
    by blast
qed

text \<open>An alternative using the method @{method primitivity_inspection}\<close>

lemma spehner_example':
  assumes "x \<noteq> y" and "\<not> primitive (concat [x,y,x,y,y,x,y])"
  shows "x \<cdot> y = y \<cdot> x"
proof-
  have facts: "[x,y,x,y,y,x,y] \<in> lists {x,y}" "2 \<le> count_list [x, y, x, y, y, x, y] x" "2 \<le> count_list [x, y, x, y, y, x, y] y"  by simp_all
  have "primitive [x,y,x,y,y,x,y]"
    using \<open>x \<noteq> y\<close> by primitivity_inspection
  from  bin_imprim_both_twice[OF _ facts this assms(2)]
  show ?thesis
    by blast
qed

end
