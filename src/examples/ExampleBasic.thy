theory ExampleBasic
  imports CoW.CoWBasic CoW.Periodicity_Lemma CoW.Submonoids
begin

section "Elementaries"
\<comment> \<open>\<close>
lemma "a = b" \<comment> \<open> Isabelle evaluates the current line and shows its output in the Output pane,
 which is by default in the lower part of the editor view
 it should says there is a counterexample to this lemma \<close>
  oops \<comment> \<open> = abandon proof \<close>


text\<open> an example of a readable very simple proof \<close>
lemma example_1: assumes "p \<cdot> s = w"   shows "\<^bold>|p\<^bold>| \<le> \<^bold>|w\<^bold>|"
proof-
  have "\<^bold>|p \<cdot> s\<^bold>| =  \<^bold>|w\<^bold>|"
    using \<open>p \<cdot> s = w\<close>
    by simp
  have "\<^bold>|p\<^bold>| + \<^bold>|s\<^bold>| =  \<^bold>|w\<^bold>|"
    using \<open>\<^bold>|p \<cdot> s\<^bold>| = \<^bold>|w\<^bold>|\<close>
    by auto
  show "\<^bold>|p\<^bold>| \<le> \<^bold>|w\<^bold>|" \<comment> \<open> the keyword show means we are proving the current proof goal \<close>
    using \<open>\<^bold>|p\<^bold>| + \<^bold>|s\<^bold>| =  \<^bold>|w\<^bold>|\<close>
    by linarith
qed

text\<open> note that the length of a word is either \<close>
term "length w"

text\<open> or in a more usual notation in combinatorics on words \<close>
term "\<^bold>|w\<^bold>|" \<comment> \<open> which is \\bold |, rather than just |; \\bold requires to hit a Tab to be transformed into the pretty symbol \<close>

text\<open> we can reuse our proven lemma called @{thm example_1} to prove the next lemma  \<close>
lemma example_2: assumes "p \<cdot> s = u \<cdot> v"
  shows "\<^bold>|p\<^bold>| \<le> \<^bold>|u \<cdot> v\<^bold>|"
  using \<open>p \<cdot> s = u \<cdot> v\<close> example_1 by blast

text\<open> to see the full proof, we can use the following command (and see the output pane);
  the Logic session needs to be set to HOL-Proofs in order to see all the details (it can be changed in Theories pane, by default on the right \<close>
full_prf example_1


text\<open> Isabelle contains the Sledgehammer tool, which is able to suggest a proof based on known (proven) facts;
 just navigate to the end of the next line, go the the Sledgehammer pane, on the botton by default,
and hit Apply; if a proof is found, you can click it to copy it here \<close>
lemma "primitive (u\<cdot>v) \<Longrightarrow> primitive (v\<cdot>u)"
  oops \<comment> \<open> = abandon proof, replace with your proof \<close>

text\<open> hit Ctrl+click on the theorems used in the proof in order to go where they are stated and proven \<close>


text\<open> another example: proof by induction
  we induce on the word w:
    the first case is w empty
    the second case is 1) the claim holds for w 2) we show it for aw, where a is a letter
\<close>
lemma example_3: "\<^bold>|w\<cdot>v\<^bold>| = \<^bold>|w\<^bold>| + \<^bold>|v\<^bold>|"
proof(induct w)
  have "\<epsilon> \<cdot> v = v"
    by simp
  have "\<^bold>|\<epsilon>\<^bold>| = 0"
    by simp
  show "\<^bold>|\<epsilon> \<cdot> v\<^bold>| = \<^bold>|\<epsilon>\<^bold>| + \<^bold>|v\<^bold>|"
    using \<open>\<epsilon> \<cdot> v = v\<close> \<open>\<^bold>|\<epsilon>\<^bold>| = 0\<close>
    by simp
next
  fix a w
  assume "\<^bold>|w \<cdot> v\<^bold>| = \<^bold>|w\<^bold>| + \<^bold>|v\<^bold>|"
  have h1: "(a # w) \<cdot> v = a # (w \<cdot> v)" \<comment> \<open> note that @{term "(#)"} is not concatenation, it prepends the letter a to the word w, or concatenates the word [a] (of length 1) to the word w \<close>
    by simp
  have h2: "\<^bold>|a # (w \<cdot> v)\<^bold>| = 1 + \<^bold>|w \<cdot> v\<^bold>|"
    by simp
  have h3: "\<^bold>|a # w\<^bold>| = 1 + \<^bold>|w\<^bold>|"
    by simp
  show "\<^bold>|(a # w) \<cdot> v\<^bold>| = \<^bold>|a # w\<^bold>| + \<^bold>|v\<^bold>|"
    using h1 h2 h3
    by simp
qed


text\<open> of course, Isabelle knows the lemma already (provided by the Main library, in List theory)...
  see the output of this line \<close>
thm lenmorph

subsection "Elementary notation"

text \<open> our formalization introduces many elementary concepts along with notations:
  hit Ctrl+click on the notation to go the definition \<close>

term "\<epsilon>" \<comment> \<open> the empty word \<close>
term "u\<cdot>v" \<comment> \<open> concatenation \<close>
term "p \<le>p w"
term "p <p w"
term "s \<le>s w"
term "s <s w"
term "f \<le>f w"
term "f <f w"
term "\<^bold>|w\<^bold>|"
term "p\<inverse>\<^sup>>w" \<comment> \<open>well defined if p is prefix of w \<close>
term "w\<^sup><\<inverse>s" \<comment> \<open>well defined if s is suffix of w \<close>
term "w \<bowtie> v"
term "w \<bowtie>\<^sub>s v"
term "u \<and>\<^sub>p v"
term "u\<^sup>@k" \<comment> \<open> word power \<close>
term "primitive w"
term "\<rho> w" \<comment> \<open> primitive root \<close>
term "u \<sim> v" \<comment> \<open> conjugation \<close>
term "period w n" \<comment> \<open> word having period n \<close>
term "\<langle>G\<rangle>" \<comment> \<open> hull \<close>
term "decompose G u"
term "\<BB> L" \<comment> \<open> basis \<close>
term "\<BB>\<^sub>F L" \<comment> \<open> free basis \<close>



text\<open> formalized results example 1: binary code \<close>
theorem assumes "u \<cdot> v \<noteq> v \<cdot> u" shows "code {u,v}"
  using assms bin_code_code by blast

text\<open> formalized results example 2: periodicity lemma \<close>
theorem assumes "period w p" and "period w q" and  "p + q - gcd p q \<le> \<^bold>|w\<^bold>|"
  shows  "period w (gcd p q)"
  using assms per_lemma by blast

text\<open> your lemma goes here: \<close>
lemma "w = w"
  oops


end
