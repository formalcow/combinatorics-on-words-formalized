(*  Title:      Sturmian_Words
    File:       CoW_Infinite.Sturmian_Words
    Author:     Štěpán Starosta, Czech Technical University in Prague

Part of Combinatorics on Words Formalized. See https://gitlab.com/formalcow/combinatorics-on-words-formalized/
*)

theory Sturmian_Words
  imports
    Infinite_Words
    "HOL.Real"
begin

section "Sturmian words"

text\<open>Sturmian words are infinite words that form a very popular subject of study in combinatorics on words.
They were studied in the article of 1940 by Morse and Hedlund \cite{ HeMo}, and they have many equivalent characterizations (c.f \cite{ BaPeSta2}).
In this formalization, we choose a constructive characterization which allows to parametrize the set of all Sturmian words.
More precisely, we choose the concept of lower mechanical words with irrational slope.

One may also refer to the classical books \cite{ Lo83} and \cite{ Fogg}.\<close>

no_notation min_image_length  ("\<lfloor>_\<rfloor>" )

subsection "Preliminaries"

lemma frac_def': "floor x + frac x = x"
  by (simp add: frac_def)

lemma frac_int_real: "frac ((n::int) * (x::real) + (y::real)) = frac ( n*(frac x) + y)"
  unfolding arg_cong[OF frac_def', of "\<lambda>z. n*z + y" x, symmetric]
  unfolding distrib_left
  unfolding add.assoc
  unfolding frac_add[of "real_of_int n * real_of_int \<lfloor>x\<rfloor>"]
  using add_0 frac_lt_1 frac_of_int of_int_mult by metis

definition of_bin :: "int \<Rightarrow> binA"
  where "of_bin n \<equiv> (if n = 0 then bina else (if n = 1 then binb else undefined))"

lemma of_bin_simps[simp]: "of_bin 0 = bina" "of_bin 1 = binb"
  unfolding of_bin_def
  by simp_all



subsection "Rotation"

definition circle_rotation :: "real \<Rightarrow> real \<Rightarrow> real"
  where "circle_rotation \<alpha> \<beta> \<equiv> (frac (\<alpha>+\<beta>))"

abbreviation crot
  where "crot \<alpha> \<beta> n \<equiv> (((circle_rotation \<alpha>)^^n) (frac \<beta>))"

lemma crot_frac:
  shows "crot \<alpha> \<beta> n = (frac (n*\<alpha> + \<beta>))"
proof(induction n rule:nat_induct)
  case (Suc n)
  have he: "real (1 + n) = 1 + real n"
    by simp
  show ?case
    unfolding funpow.simps
    unfolding comp_apply
    unfolding Suc.IH
    unfolding circle_rotation_def
    unfolding Suc_eq_plus1_left he
    unfolding distrib_right[of 1 n \<alpha>]
    unfolding mult_1  add.assoc
    unfolding frac_add[of \<alpha>]
    by auto
qed simp


lemma crot_frac_frac:
  shows "crot \<alpha> \<beta> n = crot (frac \<alpha>) \<beta> n"
proof-
  have aux: "real n * \<alpha> + \<beta> = n*(real_of_int \<lfloor>\<alpha>\<rfloor>) + (n * frac \<alpha> + \<beta>)"
    using frac_def'
    by (metis add.commute distrib_left group_cancel.add2)
  have aux2: "frac (n*(real_of_int \<lfloor>\<alpha>\<rfloor>)) = 0"
    by simp
  show ?thesis
    unfolding crot_frac
    unfolding aux
    unfolding frac_add[of "n*(real_of_int \<lfloor>\<alpha>\<rfloor>)"]
    unfolding aux2 add_0 if_P[OF frac_lt_1]..
qed


lemma frac_diff_lin_cases:
  "frac y < frac x \<Longrightarrow> frac (x - y) = frac x - frac y"
  "frac x < frac (y :: real) \<Longrightarrow> frac (x - y) = 1 - (frac y - frac x)"
  "frac x = frac (y :: real) \<Longrightarrow> frac (x - y) = 0"
  unfolding diff_conv_add_uminus frac_add frac_neg
  by ((cases "y \<in> \<int>"; simp_all add: frac_lt_1),
      (cases "y \<in> \<int>"; simp_all add: frac_lt_1 less_le_not_le flip: frac_eq_0_iff),
      (cases "y \<in> \<int>"; simp_all add: frac_lt_1 less_le_not_le flip: frac_eq_0_iff))

definition circle_distance :: "real \<Rightarrow> real \<Rightarrow> real" ("dRot")
  where "circle_distance x y \<equiv> (min (abs ((frac x)-(frac y))) (1-(abs ((frac x)-(frac y)))))"

lemma dRot_def': "dRot x y = min (frac (x - y)) (frac (y - x))"
  unfolding circle_distance_def
  by (cases "frac y" "frac x" rule: linorder_cases) (unfold frac_diff_lin_cases, simp_all)

lemma dRot_if: "dRot x y = (if (abs ((frac x)-(frac y))) \<le> 1/2 then (abs ((frac x)-(frac y))) else (1-(abs ((frac x)-(frac y)))))"
  unfolding circle_distance_def
  by fastforce

lemma dRot_max: "dRot x y \<le> 1/2"
  unfolding circle_distance_def
  by argo

lemma dRot_min: "0 \<le> dRot x y"
  unfolding circle_distance_def
  using frac_ge_0[of x] frac_ge_0[of y] frac_lt_1[of x] frac_lt_1[of y]
  by linarith

lemma dRot_sym: "dRot x y = dRot y x"
  by (simp add: circle_distance_def)

lemma dRot_frac_frac: "dRot (frac x) (frac y) = dRot x y"
  by (simp add: dRot_if)

lemma dRot_triang:
  shows "dRot x y \<le> (dRot x z) + (dRot z y)"
proof(cases "\<bar>frac x - frac z\<bar> \<le> 1 / 2")
  case True
  then show ?thesis
    unfolding dRot_if
    by (smt (verit) field_sum_of_halves)
next
  case False
  then show ?thesis
    unfolding dRot_if
    by (smt (verit, best) field_sum_of_halves frac_ge_0 frac_lt_1)
qed

lemma crot_iso':
  shows "dRot x y = dRot (circle_rotation \<alpha> x) (circle_rotation \<alpha> y)"
        unfolding circle_rotation_def dRot_frac_frac unfolding dRot_def' by argo

lemma crot_iso:
  shows "dRot x y = dRot (crot \<alpha> x n) (crot \<alpha> y n)"
  using circle_rotation_def crot_frac crot_iso' by presburger

lemma dRot_shift: "dRot x y = dRot (x+z) (y+z)"
  by (metis add.commute circle_distance_def circle_rotation_def crot_iso' frac_frac)

    lemma crot_inv: assumes "i \<le> n"
  shows "crot \<alpha> \<beta> (n-i) = crot (-\<alpha>) (crot \<alpha> \<beta> n) i"
  unfolding crot_frac
  unfolding of_nat_diff[OF \<open>i \<le> n\<close>]
  unfolding left_diff_distrib'
  unfolding uminus_add_conv_diff[symmetric]
  unfolding mult.commute[of "real i"]
  unfolding mult_minus_left[symmetric]
  unfolding frac_add[of "- \<alpha> * real i"]
  unfolding frac_frac
  by (metis (mono_tags, opaque_lifting) ab_semigroup_add_class.add_ac(1) frac_add)

lemma crot_mult:
  shows "(crot (n*\<alpha>) 0 t) = frac (t*(crot \<alpha> 0 n))"
proof(induction t)
  case (Suc t)
  define z where "z = crot \<alpha> 0 n"
  with crot_frac frac_lt_1
  have "0 \<le> z" "z < 1"
    by simp_all
  have a1: "frac (real n * \<alpha>) = z"
    by (simp add: crot_frac z_def)
  have a2: "frac (real n * \<alpha> + frac (real t * z)) = frac (frac (real n * \<alpha>) + frac (real t * z))"
    by (simp add: frac_add)
  show ?case
    unfolding funpow.simps(2) comp_apply
    unfolding Suc.IH
    unfolding z_def[symmetric]
    unfolding circle_rotation_def
    unfolding a2
    unfolding a1
    by (simp add: frac_add mult.commute ring_class.ring_distribs(1))
qed simp

lemma crot_mult':
  shows "(crot (n*\<alpha>) \<beta> t) = (crot \<alpha> \<beta> (t*n))"
  unfolding crot_frac
  by (simp add: mult.assoc)

lemma crot_add: "crot \<alpha> \<beta> (n+m) = frac ((crot \<alpha> \<beta> n) + (crot \<alpha> 0 m))"
  using add.assoc add.commute  crot_frac crot_frac_frac distrib_left mult.commute mult_1 of_nat_1 of_nat_add
  ab_semigroup_add_class.add_ac(1) circle_rotation_def frac_frac funpow_1  ring_class.ring_distribs(1) verit_sum_simplify by metis

lemma dRot_closest_div: assumes "0 < p"
  obtains q::nat where "q < p" "dRot (q/p) x \<le> 1/2/p"
proof-
  define q where "q = (round ((frac x)*p))"
  hence "0 \<le> q"
    by (metis frac_ge_0 mult_nonneg_nonneg of_nat_0_le_iff round_0 round_mono)
  have "q \<le> p"
    unfolding q_def
    using frac_lt_1[of x]
    by (metis leI mult.commute mult_1 of_nat_0_le_iff order_less_imp_triv ordered_comm_semiring_class.comm_mult_left_mono round_mono round_of_nat)
  have fa: "q - (frac x)*p = (q/p- (frac x))*p"
    by (simp add: assms mult.commute right_diff_distrib')
  have "abs (q - (frac x)*p) \<le> 1/2"
    using of_int_round_abs_le q_def by blast
  hence "abs (q/p- (frac x)) \<le> 1/2/p"
    unfolding fa
    unfolding Rings.idom_abs_sgn_class.abs_mult
    using \<open>0 < p\<close>
    by (simp add: le_divide_eq)

  obtain q'::nat where "q' = (if q=p then (0::nat) else q)"
    using \<open>0 \<le> q\<close> nat_0_le
    by metis
  have "dRot ((q')/p) x \<le> 1/2/p"
  proof(cases "q=p")
    case True
    then show ?thesis
      by (smt (verit, del_insts) \<open>\<bar>real_of_int q / real p - frac x\<bar> \<le> 1 / 2 / real p\<close> \<open>int q' = (if q = int p then int 0 else q)\<close> dRot_if diff_divide_distrib divide_self_if field_sum_of_halves frac_eq of_int_of_nat_eq of_nat_0)
  next
    case False
    then show ?thesis
      by (smt (verit) \<open>\<bar>real_of_int q / real p - frac x\<bar> \<le> 1 / 2 / real p\<close> \<open>int q' = (if q = int p then int 0 else q)\<close> \<open>q \<le> int p\<close> circle_distance_def divide_less_eq_1_pos divide_nonneg_nonneg frac_eq less_imp_of_nat_less nat_int_comparison(2) of_int_of_nat_eq of_nat_0_le_iff)
  qed
  from that[OF _ this]
  show thesis
    by (smt (verit, ccfv_SIG) \<open>int q' = (if q = int p then int 0 else q)\<close> \<open>q \<le> int p\<close> assms nat_int_comparison(3) not_le_imp_less order_le_less_trans)
qed

lemma frac_mult_coin_rat: assumes "frac ((real n)*(\<alpha>::real)) = frac ((real m)*\<alpha>)" "n > m"
  shows "\<alpha> \<in> \<rat>"
proof-
  have "(real n)*\<alpha> = (real (n-m))*\<alpha> + (real m)*\<alpha>"
    by (metis Nat.le_imp_diff_is_add assms(2) distrib_left less_le_not_le mult.commute of_nat_add)
  from arg_cong[OF this, of frac]
  have "frac (real (n - m) * \<alpha>) = 0"
    by (smt (verit, ccfv_SIG) assms(1) frac_1 frac_add frac_frac)
  thus "\<alpha> \<in> \<rat>"
    by (smt (verit) Rats_eq_int_div_nat assms(2) frac_def mem_Collect_eq nonzero_mult_div_cancel_left of_nat_0_eq_iff zero_less_diff')
qed

corollary dRot_rat: assumes "dRot (crot \<alpha> \<beta> n) \<beta> = 0" "0 < n"
  shows "\<alpha> \<in> \<rat>"
  using frac_mult_coin_rat[of n \<alpha> 0, OF _ \<open>0 < n\<close>] assms(1)
  unfolding dRot_if
  unfolding crot_frac
  by (smt (verit, best) frac_add frac_eq frac_frac left_diff_distrib of_nat_0)

lemma rat_crot_coin:
  fixes q::int
  assumes "(p::nat) \<noteq> 0"
  shows "crot (q/p) \<beta> (n+p) = crot (q/p) \<beta> n"
proof-
  let ?\<alpha> = "q/p"
  from nonzero_mult_div_cancel_left[OF \<open>p \<noteq> 0\<close>]
  have aux: "(n+p)*?\<alpha> = q + n*?\<alpha>"
    unfolding of_nat_add
    unfolding distrib_right
    unfolding times_divide_eq_right
    by (simp add: \<open>p \<noteq> 0\<close>)
  show ?thesis
    unfolding crot_frac
    unfolding aux
    unfolding add.assoc
    unfolding frac_add[of "real_of_int q"]
    by (simp add: frac_lt_1)
qed

 \<comment> \<open>See Kurka Proposition 1.32 (concerns the next lemma too)\<close>
lemma crot_dense':  assumes "(\<alpha>::real) \<notin> \<rat>" "(\<tau>::real) > 0"
  obtains n::nat where "0 < dRot (crot \<alpha> \<beta> n) \<beta>" "dRot (crot \<alpha> \<beta> n) \<beta> < \<tau>" "0 < n"
proof-
  obtain p::nat where "1 < \<tau>*(real p)"
    by (metis assms(2) ex_less_of_nat_mult mult.commute)
  have "0 < p"
    using \<open>1 < \<tau> * real p\<close> zero_less_iff_neq_zero
    by force

  let ?P = "\<lambda>q n. (q < p \<and> (dRot (q/p) (crot \<alpha> \<beta> n) \<le> 1/2/p))"
  define oq where "oq = (\<lambda>n. SOME q. ?P q n)"
  have oq_ok: "?P (oq n) n" for n
  proof-
    from dRot_closest_div[OF \<open>0 < p\<close>, of "(crot \<alpha> \<beta> n)"]
    obtain q where
      "q < p" and
      "dRot (real q / real p) (crot \<alpha> \<beta> n) \<le> 1 / 2 / real p"
      by blast
    hence "?P q n"
      by blast
    from someI[of "\<lambda>q'. ?P q' n" q, OF this]
    show ?thesis
      using oq_def by blast
  qed

  have "card (oq`{0..<(Suc p)}) \<le> p"
  proof-
    have "oq ` {0..<Suc p} \<subseteq> {0..<p}"
      unfolding subset_iff
      using conjunct1[OF oq_ok] atLeastLessThan_iff by blast
    thus ?thesis
      using subset_eq_atLeast0_lessThan_card by presburger
  qed
  hence "card (oq`{0..<(Suc p)}) < card {0..<(Suc p)}"
    by force
  from pigeonhole[OF this]
  obtain i j where "i < j" "oq i = oq j"
    by (metis linorder_inj_onI nle_le)
  have "(dRot (crot \<alpha> \<beta> i) (crot \<alpha> \<beta> j) \<le> 1/p)"
    using conjunct2[OF oq_ok, of i] conjunct2[OF oq_ok, of j]
    unfolding dRot_sym[of _ "(crot \<alpha> \<beta> i)"]
    unfolding \<open>oq i = oq j\<close>
    using dRot_triang
    by (smt (verit, ccfv_SIG) add_divide_distrib field_sum_of_halves)
  hence "(dRot (crot \<alpha> \<beta> i) (crot \<alpha> \<beta> j) < \<tau>)"
    by (metis \<open>0 < p\<close> \<open>1 < \<tau> * real p\<close> assms(2) divide_divide_eq_left divide_less_eq_1_pos mult_of_nat_commute mult_pos_pos of_nat_0_less_iff order_le_less_trans)
  hence "(dRot (crot \<alpha> \<beta> 0) (crot \<alpha> \<beta> (j-i)) < \<tau>)"
    by (smt (verit, ccfv_threshold) \<open>i < j\<close> add_diff_inverse_nat crot_frac crot_iso funpow_0 funpow_add'' order_less_imp_triv)
  thus ?thesis
    using that dRot_rat[of "j-i" \<alpha> \<beta>]
    by (metis \<open>i < j\<close> assms(1) dRot_frac_frac dRot_min dRot_sym frac_frac funpow_0 less_eq_real_def zero_less_diff)
qed



lemma crot_dense: assumes "(\<alpha>::real) \<notin> \<rat>" "(\<tau>::real) > 0"
  obtains n::nat where "dRot (crot \<alpha> \<beta> n) \<gamma> < \<tau>"
proof(cases)
  assume "\<tau> \<le> 1/2"

  from crot_dense'[OF \<open>(\<alpha>::real) \<notin> \<rat>\<close> \<open>(\<tau>::real) > 0\<close>, of 0]
  obtain n::nat where
    "0 < dRot (crot \<alpha> 0 n) 0" and
    "dRot (crot \<alpha> 0 n) 0 < \<tau>" and
    "0 < n"
    by blast

  have "0 \<le> crot \<alpha> 0 n"
    unfolding crot_frac
    using frac_ge_0.
  hence "0 < crot \<alpha> 0 n"
    using \<open>0 < dRot (crot \<alpha> 0 n) 0\<close>
    unfolding circle_distance_def
    by (smt (verit, best))

  define step where "step = (if crot \<alpha> 0 n < 1/2 then crot \<alpha> 0 n else (1-crot \<alpha> 0 n))"
  have "step \<le> 1/2"
    by (simp add: step_def)
  have "0 < step"
    using \<open>0 < crot \<alpha> 0 n\<close> crot_frac frac_lt_1 step_def by auto

  define goal where "goal = (if crot \<alpha> 0 n < 1/2 then \<gamma>-\<beta> else (1- (\<gamma>-\<beta>)))"

  define q::int where "q = floor ((frac goal)/step)"
  hence "q*step \<le> (frac goal)"
    using \<open>0 < step\<close> floor_divide_lower by blast
  hence "q*step < 1"
    by (simp add: frac_lt_1 order_le_less_trans)

  have "0 \<le> q"
    by (simp add: \<open>0 < step\<close> mult_imp_le_div_pos q_def)

  have "step < \<tau>"
    using \<open>dRot (crot \<alpha> 0 n) 0 < \<tau>\<close> \<open>\<tau> \<le> 1/2\<close>
    unfolding step_def
    by (smt (verit, best) circle_distance_def crot_frac field_sum_of_halves frac_1 frac_frac)

  have "dRot (q*step) goal  < \<tau>"
    by (smt (verit, best) \<open>0 < step\<close> \<open>real_of_int q * step < 1\<close> \<open>real_of_int q * step \<le> frac goal\<close> \<open>step < \<tau>\<close> circle_distance_def divide_diff_eq_iff divide_less_eq_1_pos floor_divide_upper frac_eq frac_ge_0 le_divide_eq mult.commute mult_nonneg_nonneg of_int_1 of_int_add of_int_less_0_iff q_def zero_less_mult_pos)




  have "((crot (n*\<alpha>) 0 (nat q))) = (crot \<alpha> 0 ((nat q)*n))"
    by (simp add: crot_frac mult.assoc)
  hence "frac (q*(crot \<alpha> 0 n)) = (crot \<alpha> 0 ((nat q)*n))"
    using crot_mult
    using \<open>0 < step\<close> q_def by auto

  have "dRot (crot \<alpha> 0 ((nat q)*n)) (\<gamma>-\<beta>) < \<tau>"
  proof(cases "crot \<alpha> 0 n < 1 / 2")
    case True
    show ?thesis
      using \<open>dRot (q*step) goal  < \<tau>\<close>
      unfolding step_def goal_def
      unfolding if_P[OF True]
      unfolding \<open>frac (q*(crot \<alpha> 0 n)) = (crot \<alpha> 0 ((nat q)*n))\<close>[symmetric]
      by (metis dRot_frac_frac frac_frac)
  next
    case False
    have ax1: "frac (real_of_int q * (1 - crot \<alpha> 0 n)) = 1 - frac (q*(crot \<alpha> 0 n))" if "q \<noteq> 0"
          proof-
      have a1: "real_of_int q * (1 - crot \<alpha> 0 n) = real_of_int q + - (real_of_int q * crot \<alpha> 0 n)"
        by (simp add: right_diff_distrib)
      have a2: "frac (real_of_int q) = 0"
        by simp
      have a3: "\<not> real_of_int q * crot \<alpha> 0 n \<in> \<int>"
        by (metis \<open>0 < n\<close> \<open>0 \<le> q\<close> \<open>frac (real_of_int q * crot \<alpha> 0 n) = crot \<alpha> 0 (nat q * n)\<close> abs_zero assms(1) cancel_comm_monoid_add_class.diff_cancel circle_distance_def dRot_rat diff_0_right frac_eq_0_iff min_0_1(1) mult_pos_pos order_less_le that zero_less_nat_eq)
      show ?thesis
        unfolding a1
        unfolding frac_add
        unfolding frac_neg a2 add_0
        unfolding if_not_P[OF a3]
        by (simp add: a3)
    qed
    have "frac (real_of_int q * (1 - crot \<alpha> 0 n)) = 0" if "q = 0"
      by (simp add: that)
    have fr: "frac (1 - (\<gamma> - \<beta>)) = (if \<gamma> - \<beta> \<in> \<int> then 0 else 1 - frac (\<gamma> - \<beta>))"
      unfolding minus_real_def
      unfolding frac_add frac_neg
      unfolding frac_1 add_0
      by (smt (verit, best) frac_add frac_gt_0_iff frac_neg)

    have ?thesis if "q \<noteq> 0"
    proof(cases "\<bar>frac (real_of_int q * (1 - crot \<alpha> 0 n)) - frac (1 - (\<gamma> - \<beta>))\<bar>
      \<le> 1 / 2")
      case True
      show ?thesis
        using \<open>dRot (q*step) goal  < \<tau>\<close>
        unfolding step_def goal_def
        unfolding if_not_P[OF \<open>\<not> crot \<alpha> 0 n < 1 / 2\<close>]
        unfolding \<open>frac (q*(crot \<alpha> 0 n)) = (crot \<alpha> 0 ((nat q)*n))\<close>[symmetric]
        unfolding dRot_if frac_frac
        unfolding if_P[OF True]
        unfolding ax1[OF that]
        by (smt (z3) field_sum_of_halves frac_1 frac_add)
    next
      case False
      show ?thesis
        using \<open>dRot (q*step) goal  < \<tau>\<close>
        unfolding step_def goal_def
        unfolding if_not_P[OF \<open>\<not> crot \<alpha> 0 n < 1 / 2\<close>]
        unfolding \<open>frac (q*(crot \<alpha> 0 n)) = (crot \<alpha> 0 ((nat q)*n))\<close>[symmetric]
        unfolding dRot_if frac_frac
        unfolding if_not_P[OF False]
        unfolding ax1[OF that]
        unfolding frac_add
        unfolding fr
        by (smt (verit, ccfv_threshold) field_sum_of_halves frac_eq_0_iff frac_gt_0_iff)
    qed
    moreover have ?thesis if "q = 0"
      unfolding \<open>frac (real_of_int q * crot \<alpha> 0 n) = crot \<alpha> 0 (nat q * n)\<close>[symmetric]
      using \<open>dRot (real_of_int q * step) goal < \<tau>\<close>
      unfolding goal_def dRot_if that
      by (smt (verit) field_sum_of_halves fr frac_1 frac_eq_0_iff frac_frac frac_ge_0 mult_cancel_left2 of_int_0)
    ultimately show ?thesis
      by blast
  qed

  have "dRot (crot \<alpha> 0 t) (\<gamma>-\<beta>) = dRot (crot \<alpha> \<beta> t) \<gamma>" for t
    using dRot_shift[of "(crot \<alpha> 0 t)" "(\<gamma>-\<beta>)" \<beta>]
    unfolding dRot_frac_frac[symmetric, of "crot \<alpha> 0 t + \<beta>"]
    by (smt (verit) circle_distance_def crot_frac crot_iso dRot_shift dRot_sym frac_add frac_frac funpow_add'' gr_zeroI zero_eq_add_iff_both_eq_0)
  hence "dRot (crot \<alpha> \<beta> (nat q * n)) \<gamma> < \<tau>"
    using \<open>dRot (crot \<alpha> 0 (nat q * n)) (\<gamma> - \<beta>) < \<tau>\<close> by presburger
  from that[OF this]
  show thesis.
next
  assume "\<not> \<tau> \<le> 1/2"
  then show thesis
    using that[of 0] dRot_max
    by (meson linorder_not_le order_le_less_trans)
qed

corollary crot_dense_infin: assumes "(\<alpha>::real) \<notin> \<rat>" "(\<tau>::real) > 0"
obtains n::nat where "dRot (crot \<alpha> \<beta> n) \<gamma> < \<tau>" "m < n"
proof(cases)
  assume "\<tau> \<le> 1/2"
  have "(m+1) * \<alpha> \<notin> \<rat>"
    by (metis Rats_divide Rats_of_nat add_gr_0 assms(1) less_numeral_extra(1) less_numeral_extra(3) nonzero_mult_div_cancel_left of_nat_eq_0_iff)

  define goal where "goal = \<beta>+\<tau>"
  from crot_dense[OF \<open>(m+1) * \<alpha> \<notin> \<rat>\<close> \<open>0 < \<tau>\<close>, of \<beta> goal]
  obtain n where
    "dRot (crot (real (m+1) * \<alpha>) \<beta> n) goal < \<tau>".
  have "n \<noteq> 0"
  proof
    assume "n = 0"
    hence "dRot \<beta> goal < \<tau>"
      by (metis \<open>dRot (crot (real (m+1) * \<alpha>) \<beta> n) goal < \<tau>\<close> dRot_frac_frac frac_frac funpow_0)
    thus False
      unfolding goal_def
      by (smt (verit) \<open>\<tau> \<le> 1 / 2\<close> dRot_if field_sum_of_halves frac_add frac_eq frac_frac)
  qed

  from crot_dense[OF \<open>\<alpha> \<notin> \<rat>\<close> \<open>0 < \<tau>\<close>, of "(crot (real (m+1) * \<alpha>) \<beta> n)" \<gamma>]
  obtain t where
    "dRot (crot \<alpha> (crot (real (m+1) * \<alpha>) \<beta> n) t) \<gamma> < \<tau>".
  hence
    "dRot (crot \<alpha> \<beta> (n*(m+1)+t)) \<gamma> < \<tau>"
    unfolding crot_mult' crot_add
    by (metis add.commute comm_monoid_mult_class.mult_1 crot_frac crot_frac_frac group_cancel.rule0 of_nat_1)
  from that[OF this]
  show thesis
    by (meson \<open>n \<noteq> 0\<close> less_add_one order_less_le_trans quotient_smaller trans_less_add1)
next
  assume "\<not> \<tau> \<le> 1/2"
  then show thesis
    using that[of "m+1"]
    by (meson dRot_max less_add_one linorder_not_le order_le_less_trans)
qed











subsection "Lower Mechanical word"

definition lower_mechanical_word_n :: "real \<Rightarrow> real \<Rightarrow> int \<Rightarrow> int"  where
  "lower_mechanical_word_n \<alpha> \<beta> n \<equiv> \<lfloor>(n+1)*\<alpha>+\<beta>\<rfloor> - \<lfloor>n*\<alpha>+\<beta>\<rfloor>"

lemma "(\<lfloor>\<alpha>+\<gamma>\<rfloor> - \<lfloor>\<gamma>::real\<rfloor>) \<in> {\<lfloor>\<alpha>\<rfloor>,\<lfloor>\<alpha>\<rfloor>+1}"
  by (simp add: floor_add)

lemma lmwn_add:
  shows "lower_mechanical_word_n \<alpha> \<beta> n = (if frac \<alpha> + frac (real_of_int n * \<alpha> + \<beta>) < 1
     then \<lfloor>\<alpha>\<rfloor>
     else \<lfloor>\<alpha>\<rfloor> + 1)"
proof-
  have he: "(n + 1) * \<alpha> = \<alpha> + n * \<alpha>"
    by (simp add: distrib_left mult.commute)
  show ?thesis
    unfolding lower_mechanical_word_n_def
    unfolding he
    unfolding add.assoc[of \<alpha>]
    unfolding floor_add[of \<alpha> "n * \<alpha> + \<beta>"]
    by simp
qed

corollary lmwn_ran:
  shows "lower_mechanical_word_n \<alpha> \<beta> n \<in> {\<lfloor>\<alpha>\<rfloor>,\<lfloor>\<alpha>\<rfloor>+1}"
  by (simp add: lmwn_add)

lemma lmwn_ran':
  assumes "0 < \<alpha>" "\<alpha> < 1"
  shows "lower_mechanical_word_n \<alpha> \<beta> n \<in> {0,1}"
  using lmwn_ran[of \<alpha> \<beta> n]
  unfolding floor_eq[of 0 \<alpha>, unfolded of_int_0 add_0, OF assms]
  unfolding add_0.

lemma lmwn_crot:
  assumes "0 \<le> \<alpha>" "\<alpha> < 1"
  shows "lower_mechanical_word_n \<alpha> \<beta> n = (if crot \<alpha> \<beta> n < 1 - \<alpha>
     then 0
     else 1)"
  unfolding crot_frac lmwn_add
  by (smt (verit) assms(1) assms(2) floor_le_zero frac_eq nat_int of_nat_0_le_iff of_nat_nat zero_le_floor)

lemma lmwn_frac:
  shows "lower_mechanical_word_n \<alpha> \<beta> n = \<lfloor>\<alpha>\<rfloor> + lower_mechanical_word_n (frac \<alpha>) \<beta> n"
  unfolding lmwn_add frac_frac
  unfolding frac_int_real[of n \<alpha> \<beta>]
  by force

lemma lmwn_fac_balanced:
  shows "(\<Sum>i \<in> {0..l1}. lower_mechanical_word_n \<alpha> \<beta> (n+int i)) \<in> {\<lfloor>(l1+1)*\<alpha>\<rfloor>,\<lfloor>(l1+1)*\<alpha>\<rfloor>+1}"
proof-
  have un1: "(\<Sum>i \<in> {0..l}. lower_mechanical_word_n \<alpha> \<beta> (n+i)) = \<lfloor>(n+(Suc l))*\<alpha>+\<beta>\<rfloor> - \<lfloor>n*\<alpha>+\<beta>\<rfloor>" for l::nat
  proof(induction l)
    case 0
    have un0: "(\<Sum>i = 0..0. lower_mechanical_word_n \<alpha> \<beta> (n + int i)) = lower_mechanical_word_n \<alpha> \<beta> n"
      by simp
    show ?case
      unfolding un0
      unfolding One_nat_def[symmetric] int_ops(2)
      unfolding lower_mechanical_word_n_def..
  next
    case (Suc l)
    have "(\<Sum>i \<in> {0..(Suc l)}. lower_mechanical_word_n \<alpha> \<beta> (n+int i)) = (\<Sum>i \<in> {0..l}. lower_mechanical_word_n \<alpha> \<beta> (n+i)) + lower_mechanical_word_n \<alpha> \<beta> (n+Suc l)"
      using sum.atLeast0_atMost_Suc by blast
    then show ?case
      unfolding Suc.IH
      unfolding lower_mechanical_word_n_def[of _ _ "n + int (Suc l)"]
      unfolding diff_conv_add_uminus
      unfolding add.commute[of "\<lfloor>real_of_int (n + int (Suc l)) * \<alpha> + \<beta>\<rfloor>"] add.assoc
      unfolding left_minus add_0_right
      unfolding add.commute[of "- \<lfloor>real_of_int n * \<alpha> + \<beta>\<rfloor>"] int_Suc.
  qed
  have un2: "\<lfloor>real_of_int (n + int (Suc l1)) * \<alpha> + \<beta>\<rfloor> = \<lfloor>(Suc l1) * \<alpha> + (n * \<alpha> + \<beta>)\<rfloor>"
    by (simp add: add.commute add.left_commute mult.commute ring_class.ring_distribs(1))
  show ?thesis
    unfolding un1[of "l1"]
    unfolding un2
    unfolding floor_add[of "(Suc l1) * \<alpha>"]
    by force
qed


definition lower_mechanical_word :: "real \<Rightarrow> real \<Rightarrow> int stream"  where
  "lower_mechanical_word \<alpha> \<beta> \<equiv> snth_stream (lower_mechanical_word_n \<alpha> \<beta>)"

lemma lmw_rat_per: assumes "\<alpha> \<in> \<rat>"
  shows "purely_periodic (lower_mechanical_word \<alpha> \<beta>)"
proof-
  obtain q::int and p::nat where "frac \<alpha> = q/p" "p \<noteq> 0"
    using Rats_eq_int_div_nat assms by fastforce
  from rat_crot_coin[OF \<open>p \<noteq> 0\<close>, of _ q \<beta>, folded \<open>frac \<alpha> = q/p\<close>]
  have "(lower_mechanical_word \<alpha> \<beta>)!!(n+p) = (lower_mechanical_word \<alpha> \<beta>)!!(n)" for n
        unfolding lower_mechanical_word_def snth_snth_stream
    unfolding lmwn_frac[of \<alpha>]
    unfolding lmwn_crot[OF frac_ge_0 frac_lt_1]
    by auto
  thus "purely_periodic (lower_mechanical_word \<alpha> \<beta>)"
    by (metis \<open>p \<noteq> 0\<close> pperiodic_period zero_less_iff_neq_zero)
qed

lemma lmw_per_rat: assumes "periodic (lower_mechanical_word \<alpha> \<beta>)"
  shows "\<alpha> \<in> \<rat>"
proof(rule ccontr)
  assume "\<alpha> \<notin> \<rat>"
  obtain q p where
    "0 < p" and
            per1: "n \<ge> q \<Longrightarrow> \<lfloor>\<alpha>\<rfloor> + (if crot (frac \<alpha>) \<beta> n < 1 - frac \<alpha> then 0 else 1) =
                \<lfloor>\<alpha>\<rfloor> + (if crot (frac \<alpha>) \<beta> (n + p) < 1 - frac \<alpha> then 0 else 1)" for n
    using assms
    unfolding periodic_period
    unfolding lower_mechanical_word_def snth_snth_stream
    unfolding lmwn_frac[of \<alpha>]
    unfolding lmwn_crot[OF frac_ge_0 frac_lt_1]
    by blast
  have per2: "crot (frac \<alpha>) \<beta> n < 1 - frac \<alpha> \<longleftrightarrow> crot (frac \<alpha>) \<beta> (n+p) < 1 - frac \<alpha>" if "n \<ge> q" for n
    using per1[OF that] linorder_not_le by fastforce
  have per3: "crot (frac \<alpha>) \<beta> n < 1 - frac \<alpha> \<longleftrightarrow> crot (frac \<alpha>) \<beta> (n+l*p) < 1 - frac \<alpha>" if "n \<ge> q" for n l
  proof(induction l)
    case (Suc l)
    have "q \<le> n + l * p"
      using that
      by simp
    have "n + l * p + p = n + (Suc l) * p"
      by simp
    show ?case
      using Suc.IH
      unfolding per2[OF \<open>q \<le> n + l * p\<close>]
      unfolding \<open>n + l * p + p = n + (Suc l) * p\<close>.
  qed simp
  hence per3': "crot (frac \<alpha>) \<beta> n \<ge> 1 - frac \<alpha> \<longleftrightarrow> crot (frac \<alpha>) \<beta> (n+l*p) \<ge> 1 - frac \<alpha>" if "n \<ge> q" for n l
    by (smt (verit, best) that)

  define \<tau> where "\<tau> = (min (1-frac \<alpha>) (frac \<alpha>))/2"
  have "0 < \<tau>"
    using \<open>\<alpha> \<notin> \<rat>\<close> unfolding \<tau>_def
    by (metis Rats_0 diff_gt_0_iff_gt frac_ge_0 frac_in_Rats_iff frac_lt_1 half_gt_zero leI min.commute min.orderE nle_le)
  have "\<tau> < 1/2"
    by (simp add: \<tau>_def)

  from crot_dense_infin[OF \<open>\<alpha> \<notin> \<rat>\<close> \<open>0 < \<tau>\<close>, of \<beta> "(1 - frac \<alpha>) + (frac \<alpha>)/2" q]
  obtain n where
    "dRot (crot \<alpha> \<beta> n) ((1 - frac \<alpha>) + (frac \<alpha>)/2) < \<tau>"
    "q < n".
  from this(1)
  have di1: "abs ((crot \<alpha> \<beta> n) - ((1 - frac \<alpha>) + (frac \<alpha>)/2)) < \<tau>"
    unfolding dRot_if \<tau>_def
    unfolding crot_frac frac_frac
    using field_sum_of_halves frac_eq  frac_lt_1 frac_ge_0
    by (smt (z3))

  have "p*\<alpha> \<notin> \<rat>"
    by (metis Rats_divide Rats_of_nat \<open>0 < p\<close> \<open>\<alpha> \<notin> \<rat>\<close> nonzero_mult_div_cancel_left not_gr_zero of_nat_eq_0_iff)
  from crot_dense[OF this \<open>0 < \<tau>\<close>, of "(crot \<alpha> \<beta> n)" "(frac \<alpha>)/2"]
  obtain l where
    "dRot (crot (real p * \<alpha>) (crot \<alpha> \<beta> n) l) (frac \<alpha> / 2) < \<tau>".
  hence
    "dRot (crot \<alpha> \<beta> (n+l*p)) (frac \<alpha> / 2) < \<tau>"
    unfolding crot_mult'
    unfolding funpow_add' comp_apply
    by (simp add: crot_frac)
  hence di2: "abs ((crot \<alpha> \<beta> (n+l*p)) - (frac \<alpha> / 2)) < \<tau>"
    unfolding circle_distance_def \<tau>_def
    by (smt (verit, del_insts) crot_frac field_sum_of_halves frac_eq frac_frac)

  from crot_dense[OF \<open>p*\<alpha> \<notin> \<rat>\<close> \<open>0 < \<tau>\<close>, of "(crot \<alpha> \<beta> n)" "(1- frac \<alpha>)/2"]
  obtain l' where
    "dRot (crot (real p * \<alpha>) (crot \<alpha> \<beta> n) l') ((1 - frac \<alpha>) / 2) < \<tau>".
  hence
    "dRot (crot \<alpha> \<beta> (n+l'*p)) ((1 - frac \<alpha>) / 2) < \<tau>"
    unfolding crot_mult'
    unfolding funpow_add' comp_apply
    by (simp add: crot_frac)
  hence di3: "abs ((crot \<alpha> \<beta> (n+l'*p)) - ((1 - frac \<alpha>) / 2)) < \<tau>"
    unfolding circle_distance_def \<tau>_def
    by (smt (z3) crot_frac field_sum_of_halves frac_eq frac_ge_0 frac_lt_1)


  consider "frac \<alpha> < 1/2" | "1/2 < frac \<alpha>"
    by (metis Rats_1 Rats_divide Rats_number_of \<open>\<alpha> \<notin> \<rat>\<close> frac_in_Rats_iff linorder_neqE_linordered_idom)
  thus False
  proof(cases)
    case 1
    hence "\<tau> = (frac \<alpha>)/2"
      unfolding \<tau>_def by simp

    have "crot \<alpha> \<beta> (n+l*p) < 1 - frac \<alpha>"
      using di2
      unfolding \<open>\<tau> = (frac \<alpha>)/2\<close>
      using "1" by linarith
    moreover have "crot \<alpha> \<beta> n > 1 - frac \<alpha>"
      using di1
      unfolding \<open>\<tau> = (frac \<alpha>)/2\<close>
      by linarith
    ultimately show False
      using per3'[OF less_imp_le[OF \<open>q < n\<close>], of l]
      using crot_frac_frac by auto
  next
    case 2
    hence "\<tau> = (1-frac \<alpha>)/2"
      unfolding \<tau>_def by simp

    have "crot \<alpha> \<beta> (n+l'*p) < 1 - frac \<alpha>"
      using di3
      unfolding \<open>\<tau> = (1-frac \<alpha>)/2\<close>
      by (smt (z3) field_sum_of_halves)

    moreover have "crot \<alpha> \<beta> n > 1-frac \<alpha>"
      using di1
      unfolding \<open>\<tau> = (1-frac \<alpha>)/2\<close>
      by (smt (z3) "2" field_sum_of_halves)

    ultimately show ?thesis
      using per3'[OF less_imp_le[OF \<open>q < n\<close>], of l']
      using crot_frac_frac by auto
  qed
qed



end
