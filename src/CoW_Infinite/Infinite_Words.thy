(*  Title:      Infinite_Words
    File:       CoW_Infinite.Infinite_Words
    Author:     Štěpán Starosta, Czech Technical University in Prague

Part of Combinatorics on Words Formalized. See https://gitlab.com/formalcow/combinatorics-on-words-formalized/
*)

theory Infinite_Words
  imports
    "Regular-Sets.Regular_Set"
    Languages
    "HOL-Library.Stream"
    "HOL.Real"
begin


text\<open>An infinite word is a sequence of symbols from a given set of letters, usually called alphabet. Here, we deal with one-sided infinite words which seamlessly coincide with Isabelle's datatype @{typ "'a stream"}.

The reader may refer to the classical literature on the matter \cite{ Lo83}.
\<close>

section "Infinite words basics"

notation shift (infixr "\<^bold>\<cdot>" 65) \<comment> \<open>the cdot is bold\<close>

subsection "Additions to stream"

lemma stream_all_stake: "stream_all P ww \<Longrightarrow> list_all P (stake n ww)"
  by (metis stake_sdrop stream_all_shift)

lemma cycle_decomp: "cycle u = u @- cycle u"
proof(cases "u = []")
  assume "u \<noteq> []"
  show ?thesis
    using \<open>u \<noteq> []\<close>
  proof (coinduction arbitrary: u)
    case Eq_stream then show ?case using stream.collapse[of "cycle u"]
      by (auto intro!: exI[of _ "tl u @ [hd u]"])
  qed
qed simp

lemma cycle_period:
  shows "\<And>n. (cycle u)!!n = (cycle u)!!(n+\<^bold>|u\<^bold>|)"
  by (metis Nat.add_0_right add.commute list.size(3) sdrop_cycle_eq sdrop_snth)

lemma cycle_snth_mod: "(cycle w) !! i = (cycle w) !! (i mod \<^bold>|w\<^bold>|)" if "w \<noteq> \<epsilon>"
  using rotate_conv_mod sdrop_cycle sdrop_simps(1) that by metis


lemmas scancel = shift_left_inj

lemma stl_sdrop: "(stl^^i) uu = sdrop i uu"
  by(induction i, simp_all)



lemma conc_fin_card:
  assumes "finite L" "finite K"
  shows "finite (L@@K)"
        "card(L@@K) \<le> card L * card K"
proof-
  let ?f = "\<lambda> (f,s). f\<cdot>s"

  have "?f`(L \<times> K) = L@@K"
    unfolding set_eq_iff image_iff
    by fastforce

  note fc = finite_cartesian_product[OF \<open>finite L\<close> \<open>finite K\<close>]

  show "finite (L @@ K)"
    using finite_imageI[OF fc, of ?f]
    unfolding \<open>?f`(L \<times> K) = L@@K\<close>.

  show "card(L@@K) \<le> card L * card K"
    using card_image_le[OF fc, of  ?f]
    unfolding \<open>?f`(L \<times> K) = L@@K\<close> card_cartesian_product.
qed

lemma stake_mono_pref: "i \<le> j \<Longrightarrow> stake i uu \<le>p stake j uu"
  by (metis le_add_diff_inverse prefI stake_add)


lemma sing_scons: "[a] \<^bold>\<cdot> uu = a ## uu"
  by simp

subsection "Prefix of an infinite word"

definition sprefix:: "'a list \<Rightarrow> 'a stream \<Rightarrow> bool"
  where "sprefix p uu \<equiv> (\<exists>vv. uu = p\<^bold>\<cdot>vv)"

lemma sprefixI [intro?]: "uu = p\<^bold>\<cdot>vv \<Longrightarrow> sprefix p uu"
  unfolding sprefix_def by blast

lemma sprefix_emp: "sprefix \<epsilon> uu"
  by (simp add: sprefix_def)

lemma sprefixE [elim?]:
  assumes "sprefix p uu"
  obtains vv where "uu = p\<^bold>\<cdot>vv"
  using assms unfolding sprefix_def by blast

lemma sprefix_stake: "sprefix p uu \<longleftrightarrow> p = (stake \<^bold>|p\<^bold>| uu)"
  by (metis append.right_neutral cancel_comm_monoid_add_class.diff_cancel sprefix_def stake.simps(1) stake_sdrop stake_shift take_self)

lemma "w \<noteq> \<epsilon> \<Longrightarrow> sprefix w (cycle w)"
  using cycle_decomp sprefix_def by blast

lemma sprefix_cycle: "w \<noteq> \<epsilon> \<Longrightarrow> sprefix (w\<^sup>@k) (cycle w)"
  unfolding sprefix_def
proof(induction k, simp)
  case (Suc k)
  then show ?case
    unfolding power_Suc shift_append
    using cycle_decomp
    by (metis list_power.pow_Suc shift_append)
qed

lemma prefix_sprefix: "u \<le>p v \<Longrightarrow> sprefix v ww \<Longrightarrow> sprefix u ww"
  using prefD sprefix_def shift_append by metis

lemma sprefix_sdrop: assumes "sprefix w uu"
  shows "w \<^bold>\<cdot> (sdrop \<^bold>|w\<^bold>| uu) = uu"
  by (metis assms sprefix_stake stake_sdrop)


lemma assumes "sprefix u ww" "sprefix v ww"
  shows "u \<bowtie> v"
  proof-
  obtain uw where "ww = u \<^bold>\<cdot> uw"
    using sprefixE[OF \<open>sprefix u ww\<close>] by blast
  obtain vw where "ww = v \<^bold>\<cdot> vw"
    using sprefixE[OF \<open>sprefix v ww\<close>] by blast
  show "u \<bowtie> v"
    using \<open>ww = u \<^bold>\<cdot> uw\<close> \<open>ww = v \<^bold>\<cdot> vw\<close>
    unfolding prefix_comparable_def
  proof(induct arbitrary: ww rule:list_induct2', simp, simp, simp )
    case (4 x xs y ys)
    then show ?case
      by (simp add: prefix_comparable_def)
  qed
qed

lemma sprefix_comp: assumes "sprefix u ww" "sprefix v ww"
  shows "u \<bowtie> v"
  using assms
  unfolding prefix_comparable_def
proof(induct u v arbitrary:ww rule:list_induct2')
  case 1
  then show ?case
    by simp
next
  case (2 x xs)
  then show ?case
    by simp
next
  case (3 y ys)
  then show ?case
    by simp
next
  case (4 x xs y ys)
  obtain xw where "ww =(x#xs) \<^bold>\<cdot> xw"
    using sprefixE[OF \<open>sprefix (x # xs) ww\<close>] by blast
  obtain yw where "ww =(y#ys) \<^bold>\<cdot> yw"
    using sprefixE[OF \<open>sprefix (y # ys) ww\<close>] by blast
  have "x = y"
    using \<open>ww =(x#xs) \<^bold>\<cdot> xw\<close> \<open>ww =(y#ys) \<^bold>\<cdot> yw\<close>
     list.simps(3)
    shift_simps(1)[of "x # xs" xw] shift_simps(1)[of "y # ys" yw]
    unfolding list.sel(1)
    by force
  have "sprefix xs (stl ww)"
    by (simp add: \<open>ww = (x # xs) \<^bold>\<cdot> xw\<close> sprefix_def)
  moreover have "sprefix ys (stl ww)"
    by (simp add: \<open>ww = (y # ys) \<^bold>\<cdot> yw\<close> sprefix_def)
  ultimately have "xs \<bowtie> ys"
    using "4.hyps" by blast
  thus ?case
    by (simp add: \<open>x = y\<close> prefix_comparable_def)
qed

lemma cycle_sprefix: "w \<noteq> \<epsilon> \<Longrightarrow> \<forall>k. sprefix (w\<^sup>@k) uu \<Longrightarrow> uu = cycle w"
proof (coinduction arbitrary: w uu)
  case Eq_stream
  have "shd uu = shd (cycle w)"
    using Eq_stream(1) Eq_stream(2) cycle.simps(1) sprefix_def shift_simps(1) pow_one by metis
  have "(\<exists>w' uu'. stl uu = uu' \<and> stl (cycle w) = cycle w' \<and> w' \<noteq> \<epsilon> \<and> (\<forall>k. sprefix (w' \<^sup>@ k) uu'))"
  proof-
    have "stl (cycle w) = cycle (rotate1 w)"
      by (simp add: Eq_stream(1) rotate1_hd_tl)
    have "rotate1 w \<noteq> \<epsilon>"
      by (simp add: Eq_stream(1))
    have "sprefix ((rotate1 w) \<^sup>@ k) (stl uu)" for k
    proof-
      have "sprefix (w\<^sup>@Suc k) uu"
        using Eq_stream(2) by blast
      hence "sprefix (tl (w\<^sup>@Suc k)) (stl uu)"
        by (metis Eq_stream(1) nemp_Suc_pow_nemp sprefix_def shift_simps(2))
      have "tl (w\<^sup>@Suc k) = (rotate1 w)\<^sup>@k \<cdot> tl w"
        by (simp add: Eq_stream(1) rotate1_hd_tl shift_pow)
      thus ?thesis
        by (metis \<open>sprefix (tl (w \<^sup>@ Suc k)) (stl uu)\<close> prefix_sprefix triv_pref)
    qed
    thus ?thesis
      using \<open>rotate1 w \<noteq> \<epsilon>\<close> \<open>stl (cycle w) = cycle (rotate1 w)\<close> by blast
  qed
  then show ?case
    using \<open>shd uu = shd (cycle w)\<close> by blast
qed

corollary cycle_sprefix_eq: assumes "w \<noteq> \<epsilon>" shows "(\<forall>k. sprefix (w\<^sup>@k) uu) \<longleftrightarrow> uu = cycle w"
  using assms cycle_sprefix sprefix_cycle by blast

lemma cycle_pow: assumes "k > 0"
  shows "cycle (w\<^sup>@k) = cycle w"
proof(cases)
  assume "w \<noteq> \<epsilon>"
  show ?thesis
    using cycle_sprefix_eq[OF \<open>k > 0\<close>[folded nemp_pow_nemp_pos_conv[OF \<open>w \<noteq> \<epsilon>\<close>]],  THEN iffD1, rule_format, folded pow_mult, OF sprefix_cycle[OF \<open>w \<noteq> \<epsilon>\<close>], symmetric].
qed simp

lemma prefix_cycle: assumes "u \<noteq> \<epsilon>" "uu = u \<^bold>\<cdot> uu"
  shows "uu = cycle u"
proof-
  have "sprefix (u\<^sup>@k) uu" for k
  proof(induction k)
    case 0
    then show ?case
      by (simp add: sprefix_def)
  next
    case (Suc k)
    then show ?case
      by (metis assms(2) list_power.pow_Suc sprefix_def shift_append)
  qed
  thus ?thesis
    using assms(1) cycle_sprefix by blast
qed

lemma cycle_primroot:
  shows "cycle w = cycle (\<rho> w)"
  using primroot_expE cycle_pow emp_pow gr0_implies_Suc by metis


subsubsection "Prefixes"

inductive_set
  sprefixes :: "'a stream \<Rightarrow> 'a list set"
  for uu :: "'a stream"
  where
  "\<epsilon> \<in> (sprefixes uu)" |
  "p \<in> (sprefixes uu) \<Longrightarrow> stake (Suc \<^bold>|p\<^bold>|) uu \<in> (sprefixes (uu))"

lemma sprefixes_sprefix: "p \<in> sprefixes uu \<longleftrightarrow> sprefix p uu"
  unfolding sprefixes.simps[of p]
proof(induction "\<^bold>|p\<^bold>|" arbitrary: p)
  case 0
  then show ?case
    by (simp add: sprefix_emp)
next
  case (Suc x)
  then show ?case
    by (metis Zero_not_Suc length_stake list.size(3) sprefix_stake sprefixes.intros(1) sprefixes.intros(2))
qed

lemma sprefixes_SCons: "sprefixes (a##uu) = (((\<lambda>v. a#v)`(sprefixes uu)) \<union> {\<epsilon>})"
  unfolding set_eq_iff Un_iff
  unfolding image_iff
  unfolding sprefixes_sprefix
  using sprefixes_sprefix
  by (metis neq_Nil_conv shift.simps(2) singleton_iff sprefixE sprefixI sprefix_emp stream.inject)

subsection "Factor of an infinite word"

definition sfactor:: "'a list \<Rightarrow> 'a stream \<Rightarrow> bool"
  where "sfactor f uu \<equiv> (\<exists>p vv. uu = p\<^bold>\<cdot>f\<^bold>\<cdot>vv)"

lemma sfactorI [intro?]: "uu = p\<^bold>\<cdot>f\<^bold>\<cdot>vv \<Longrightarrow> sfactor f uu"
  unfolding sfactor_def by blast

lemma sfactorE [elim?]:
  assumes "sfactor f uu"
  obtains p vv where "uu = p\<^bold>\<cdot>f\<^bold>\<cdot>vv"
  using assms unfolding sfactor_def by blast

lemma sfactor_reassoc: "sfactor f uu \<longleftrightarrow> (\<exists>p vv. uu = (p\<cdot>f)\<^bold>\<cdot>vv)"
  unfolding sfactor_def
  by simp

lemma sfactor_stake_sdrop: "sfactor f uu \<longleftrightarrow> (\<exists>i . uu = (stake i uu)\<^bold>\<cdot>f\<^bold>\<cdot>(sdrop (i+\<^bold>|f\<^bold>|) uu))"
  unfolding sfactor_def
  by (metis length_append shift_append sprefixI sprefix_stake stake_sdrop)

lemma snth_stake_sdrop: "(\<forall>j \<in> {0..<\<^bold>|f\<^bold>|}. uu!!(i+j) = f!j) \<longleftrightarrow> uu = (stake i uu)\<^bold>\<cdot>f\<^bold>\<cdot>(sdrop (i+\<^bold>|f\<^bold>|) uu)"
proof(induction "\<^bold>|f\<^bold>|" arbitrary: f)
  case 0
  then show ?case
    by (simp add: stake_sdrop)
next
  case (Suc x)
  have hp0: "x = \<^bold>|butlast f\<^bold>|"
    using Suc.hyps(2) by auto
  note hyp = Suc.hyps(1)[OF this, unfolded hp0[symmetric]]

  have "f = (butlast f)\<cdot>[last f]"
    by (metis Suc.hyps(2) append_butlast_last_id list.size(3) nat.simps(3))
  hence un1: "(stake i uu)\<^bold>\<cdot>f\<^bold>\<cdot>(sdrop (i+\<^bold>|f\<^bold>|) uu) = (stake i uu \<cdot> butlast f) \<^bold>\<cdot> ([last f]\<^bold>\<cdot> (sdrop (i+\<^bold>|f\<^bold>|) uu))"
    by (metis shift_append)

  have un2: "\<^bold>|stake i uu \<cdot> butlast f\<^bold>| = (i+\<^bold>|f\<^bold>|)-1"
    using Suc.hyps(2) by auto

  have spl: "(\<forall>j\<in>{0..<Suc x}. P j) \<longleftrightarrow> (P x \<and> (\<forall>j\<in>{0..<x}. P j))" for P
    unfolding atLeast0_lessThan_Suc
    unfolding ball_simps(7)[of x]
    by blast

  have f_but: "(\<forall>j\<in>{0..<x}. f ! j = (butlast f) ! j)"
    using Suc.hyps(2) atLeastLessThan_iff diff_Suc_1 length_butlast nth_butlast by metis


  show ?case
  proof
    show "uu = stake i uu \<^bold>\<cdot> f \<^bold>\<cdot> sdrop (i + \<^bold>|f\<^bold>|) uu" if "\<forall>j\<in>{0..<\<^bold>|f\<^bold>|}. uu !! (i + j) = f ! j"
    proof-
      have "uu !! (i + x) = f ! x" "(\<forall>j\<in>{0..<x}. uu !! (i + j) = f ! j)"
        using that
        unfolding Suc.hyps(2)[symmetric]
        unfolding spl
        by blast+
      from this(2)
      have "(\<forall>j\<in>{0..<x}. uu !! (i + j) = (butlast f) ! j)"
        using f_but
        by fastforce
      thus ?thesis
        unfolding hyp
        using \<open>uu !! (i + x) = f ! x\<close>
        by (metis Suc.hyps(2) \<open>f = butlast f \<cdot> [last f]\<close> add_Suc_right hp0 id_stake_snth_sdrop nth_append_length scancel shift.simps(1) shift.simps(2) shift_append stake_sdrop)
    qed
  next
    show "\<forall>j\<in>{0..<\<^bold>|f\<^bold>|}. uu !! (i + j) = f ! j" if "uu = stake i uu \<^bold>\<cdot> f \<^bold>\<cdot> sdrop (i + \<^bold>|f\<^bold>|) uu"
    proof-
      have ri1: "uu = stake i uu \<^bold>\<cdot> butlast f \<^bold>\<cdot> sdrop (i + x) uu"
        using that
        by (smt (verit, del_insts) hp0 length_append length_stake shift_append sprefixI sprefix_stake stake_sdrop un1)
      have "stake (i + \<^bold>|f\<^bold>| - 1) uu \<^bold>\<cdot> sdrop (i + \<^bold>|f\<^bold>| - 1) uu = (stake i uu)\<^bold>\<cdot>f\<^bold>\<cdot>(sdrop (i+\<^bold>|f\<^bold>|) uu)"
        using that
        unfolding stake_sdrop.
      hence "uu!!(i+\<^bold>|f\<^bold>|-1) = last f"
        unfolding un1
        unfolding sprefixI[OF that[unfolded un1], unfolded sprefix_stake un2]
        unfolding scancel
        unfolding sdrop_simps(1)[symmetric]
        by simp
      thus ?thesis
        unfolding spl
        using ri1[folded hyp] f_but
        by (metis \<open>stake (i + \<^bold>|f\<^bold>| - 1) uu \<^bold>\<cdot> sdrop (i + \<^bold>|f\<^bold>| - 1) uu = stake i uu \<^bold>\<cdot> f \<^bold>\<cdot> sdrop (i + \<^bold>|f\<^bold>|) uu\<close> lessThan_atLeast0 lessThan_iff scancel sdrop_snth shift_snth_less stake_sdrop)
    qed
  qed
qed

corollary sfactor_occ: "sfactor f uu \<longleftrightarrow> (\<exists>i. \<forall>j \<in> {0..<\<^bold>|f\<^bold>|}. uu!!(i+j) = f!j)"
  unfolding sfactor_stake_sdrop
  using snth_stake_sdrop
  by blast

lemma factor_sfactor: "v \<le>f f \<Longrightarrow> sfactor f uu \<Longrightarrow> sfactor v uu"
  by (metis fac_suf sfactor_reassoc shift_append suffix_def)

lemma sprefix_sfactor: "sprefix p uu \<Longrightarrow> sfactor p uu"
  by (metis append_Nil sfactor_reassoc sprefix_stake stake_sdrop)

lemma sfactor_cases[case_names sprefix sfactor]: assumes "sfactor f uu"
  obtains "sprefix f uu" | "sfactor f (stl uu)"
  using assms
  by (metis sfactor_def shift.simps(1) shift_simps(2) sprefix_def)

lemma sfactor_set: "sfactor f uu \<Longrightarrow> set f \<subseteq> sset uu"
  by (metis Un_iff sfactorE sset_shift subsetI)

subsection "Subsequence from list of indices"

primrec snth_list :: "'a stream \<Rightarrow> nat list \<Rightarrow> 'a list" (infixl \<open>!#\<close> 100) where
  "s !# [] = []"
| "s !# (i#is) = s !! i # s!#is"

lemma snth_list_sing: "uu!#[i] = [uu!!i]"
  by simp

lemma snth_list_len: "\<^bold>|uu!#is\<^bold>| = \<^bold>|is\<^bold>|"
  by(induction "is", simp_all)

lemma snth_list_len': "\<^bold>|uu!#[i..<j]\<^bold>| = j - i"
  by (simp add: snth_list_len)

lemma snth_list_append_sing: "uu !# (is \<cdot> [i]) = uu!#is \<cdot> [uu!!i]"
  by(induction "is", simp_all)

lemma snth_list_shd: "uu !# [0] = [shd uu]"
  by simp

lemma snth_list_append: "uu !# (is \<cdot> js) = uu!#is \<cdot> uu!#js"
  by(induction "is", simp_all)

lemma snth_list_rev[reversal_rule]: "rev (uu!#(rev is)) = uu!#is"
proof(induction "is" rule:rev_induct)
  case Nil
  then show ?case
    by simp
next
  case (snoc x xs)
  have r1: "rev (xs \<cdot> [x]) = x#(rev xs)"
    by auto
  show ?case
    unfolding r1 snth_list_append
    by (simp add: snoc)
qed

lemma snth_list_0_stake: "uu!#[0..<j] = stake j uu"
proof(induction j)
  case (Suc j)
  then show ?case
    by (metis snth_list_append_sing stake_Suc upt.simps(2) zero_le)
qed simp

lemma snth_list_shift: "uu!#is = (p\<^bold>\<cdot>uu)!#(map (\<lambda>i. i + \<^bold>|p\<^bold>|) is)"
  by(induction "is", simp_all)

lemma snth_list_stake_sdrop: "uu!#[i..<j] = stake (j-i) (sdrop i uu)" if "i \<le> j"
  using snth_list_shift[of "sdrop i uu" "[0..<(j-i)]" "stake i uu"]
  unfolding length_stake snth_list_0_stake[of "(sdrop i uu)" "j-i"]
  unfolding stake_sdrop
  by (simp add: map_add_upt that)

lemma snth_list_sfactor: "sfactor (uu!#[i..<j]) uu"
  using snth_list_stake_sdrop
  by (metis nat_le_linear sfactorI stake_sdrop upt_conv_Nil)

lemma sfactor_snth_list:  "sfactor f uu \<Longrightarrow> \<exists>i. f = uu !# [i..<i + \<^bold>|f\<^bold>|]"
  by (metis add.commute add_diff_cancel_left' le_add_same_cancel2 length_greater_0_conv less_le_not_le list.size(3) nat_le_linear scancel sfactor_stake_sdrop snth_list_stake_sdrop sprefixI sprefix_stake stake_sdrop)

lemma cycle_snth_list_mod: "(cycle w) !# is = (cycle w) !# (map (\<lambda>i. i mod \<^bold>|w\<^bold>|) is)" if "w \<noteq> \<epsilon>"
proof(induction "is")
  case Nil
  then show ?case
    by simp
next
  case (Cons a "is")
  then show ?case
    unfolding snth_list.simps
    using cycle_snth_mod that by auto
qed

lemma cycle_snth_list_shift: "(cycle w) !# is = (cycle w) !# (map (\<lambda>i. i + k*\<^bold>|w\<^bold>|) is)"
proof(induction k)
  case 0
  then show ?case
    by simp
next
  case (Suc k)
  then show ?case
    using Infinite_Words.cycle_decomp snth_list_shift
    by (metis add_0 dvd_eq_mod_eq_0 dvd_triv_left length_stake list.size(3) map_add_upt mult.commute sdrop_cycle stake_sdrop times_nat.simps(2))
qed


subsection "Set of factors (language of an infinite word)"

definition word_language ("\<F>")
  where "word_language uu \<equiv> {w. sfactor w uu}"

lemma wlan_n: "(\<F> uu)\<^bsub>n\<^esub> = {f. sfactor f uu \<and> \<^bold>|f\<^bold>| = n}"
  by (simp add: length_subset_def word_language_def)

lemma wlan_n_snth_list: "f \<in> (\<F> uu)\<^bsub>n\<^esub> \<longleftrightarrow> (\<exists>i. f = uu!#[i..<(i+n)])"
  by (metis add_diff_cancel_left' length_upt mem_Collect_eq mem_length_subset sfactor_snth_list snth_list_len snth_list_sfactor word_language_def)

corollary "(\<F> uu)\<^bsub>n\<^esub> = range (\<lambda>i. uu!#[i..<(i+n)])"
  unfolding set_eq_iff wlan_n_snth_list
  by blast

lemma wlan_snth_list: "f \<in> word_language uu \<longleftrightarrow> (\<exists>i. f = uu!#[i..<(i+\<^bold>|f\<^bold>|)])"
  unfolding word_language_def mem_Collect_eq
  using sfactor_snth_list snth_list_sfactor
  by metis

lemma sfactors_0: "(\<F> uu)\<^bsub>0\<^esub> = {\<epsilon>}"
  by (metis (no_types, opaque_lifting) add.commute add_0 all_not_in_conv cancel_comm_monoid_add_class.diff_cancel is_singletonI' is_singleton_def length_0_conv length_upt singleton_iff snth_list.simps(1) wlan_n_snth_list)

lemma sfactors_1: "(\<F> uu)\<^bsub>1\<^esub> = sings (sset uu)"
proof-
  have he: "[i..<i + 1] = [i]" for i
    by simp
  show ?thesis
    unfolding set_eq_iff wlan_n_snth_list
    unfolding he
    unfolding snth_list_sing
    unfolding mem_Collect_eq
    using sset_range
    by fast
qed

lemma wlan_sub: "(\<F> uu)\<^bsub>n+m\<^esub> \<subseteq> ((\<F> uu)\<^bsub>n\<^esub>)@@((\<F> uu)\<^bsub>m\<^esub>)"
proof
  fix f
  assume "f \<in> \<F> uu\<^bsub>n + m\<^esub>"
  then obtain i where
    "f = uu !# [i..<i + (n + m)]"
    unfolding wlan_n_snth_list by blast
  have "uu !# [i..<i + (n + m)] = uu !# [i..<i + n] \<cdot> uu !# [(i + n)..<i + (n + m)]"
    using snth_list_append
    by (metis add.assoc le_add1 upt_add_eq_append)
  moreover have "uu !# [i..<i + n] \<in> ((\<F> uu)\<^bsub>n\<^esub>)"
    using wlan_n_snth_list by blast
  moreover have "uu !# [(i + n)..<i + (n + m)] \<in> ((\<F> uu)\<^bsub>m\<^esub>)"
    using wlan_n_snth_list
    by (metis add.assoc)
  ultimately show "f  \<in> (\<F> uu\<^bsub>n\<^esub>) @@ (\<F> uu\<^bsub>m\<^esub>)"
    by (metis \<open>f = uu !# [i..<i + (n + m)]\<close> concI)
qed

lemma wlan_n_finite: assumes "finite (sset uu)"
  shows "finite (\<F> uu\<^bsub>n\<^esub>)"
proof(induction n)
  case 0
  then show ?case
    by (simp add: sfactors_0)
next
  case (Suc n)
  have "finite (\<F> uu\<^bsub>1\<^esub>)"
    using \<open>finite (sset uu)\<close>
    by (metis finite_imageI sfactors_1 sings_image)
  then show ?case
    using wlan_sub[of uu n 1] conc_fin_card(1)[OF Suc.IH \<open>finite (\<F> uu\<^bsub>1\<^esub>)\<close>]
    by (metis add.commute finite_subset plus_1_eq_Suc)
qed

lemma wlan_n_sing:
  shows "\<F> (a##uu)\<^bsub>n\<^esub> = insert ((a##uu) !# [0..<n]) (\<F> uu\<^bsub>n\<^esub>) "
proof-
  have mp: "(map (\<lambda>i. i + 1) [i..<i + n]) = [(i+1)..<((i+1)+n)]" for i
    using Suc_eq_plus1 add_0 add_Suc drop_map drop_upt map_add_upt by metis

  show ?thesis
    unfolding set_eq_iff insert_iff
    unfolding wlan_n_snth_list
    unfolding snth_list_shift[of uu "[i..<i + n]" "[a]" for i]
    unfolding sing_len sing_scons
    unfolding mp
    unfolding Suc_eq_plus1[symmetric]
    by (metis add_0 not0_implies_Suc)
qed

subsection "Factor complexity"

definition factor_complexity
  where "factor_complexity uu \<equiv> (\<lambda>i. card ((\<F> uu)\<^bsub>i\<^esub>))"

lemma fac_com_0: "factor_complexity uu 0 = 1"
  unfolding factor_complexity_def sfactors_0
  by auto

lemma fac_com_1: "factor_complexity uu 1 = card (sset uu)"
  unfolding factor_complexity_def sfactors_1
  unfolding sings_card[symmetric]..


lemma fac_com_sub: assumes "finite (sset uu)"
  shows "factor_complexity uu (n+m) \<le> factor_complexity uu n * factor_complexity uu m"
  using conc_fin_card[OF wlan_n_finite[OF assms] wlan_n_finite[OF assms], of n m] wlan_sub[of uu n m]
  unfolding factor_complexity_def
  by (meson card_mono dual_order.trans)

corollary fac_com_max: assumes "finite (sset uu)"
  shows "factor_complexity uu n \<le> (card (sset uu))^n"
proof(induction n)
  case 0
  then show ?case
    by (simp add: fac_com_0)
next
  case (Suc n)
  then show ?case
    using fac_com_sub[OF \<open>finite (sset uu)\<close>, of n 1]
    unfolding fac_com_1 Suc_eq_plus1
    using order.trans by force
qed

lemma fac_comp_sing:
  shows "factor_complexity (a##uu) n \<le> factor_complexity uu n + 1"
  using wlan_n_sing
  unfolding factor_complexity_def
  using add_diff_cancel_right' card_insert_le_m1 le_add2 order.strict_trans2 order_refl zero_less_one by metis



subsection "Recurrent words"

term recurrent
term uniformly_recurrent



subsection "Stream defined by its elements"

primcorec snth_stream :: "(nat \<Rightarrow> 'a) \<Rightarrow> 'a stream" where
  "shd (snth_stream s) = (s 0)"
| "stl (snth_stream s) = snth_stream (\<lambda>i. s (Suc i))"

lemma sdrop_snth_stream: "sdrop n (snth_stream f) = (snth_stream (\<lambda>i. f (i+n)))"
proof(induction n arbitrary: f)
  case (Suc n)
  then show ?case
    unfolding sdrop.simps(2)
    by simp
qed simp

lemma snth_snth_stream: "(snth_stream f)!!n = f n"
  using arg_cong[OF stake_sdrop[of n "snth_stream f"], of "\<lambda>s. snth s n"]
  by (simp add: sdrop_snth_stream)

subsection "Morphic image of an infinite word"

definition morph_stream::"('a list \<Rightarrow> 'b list) \<Rightarrow> 'a stream \<Rightarrow> 'b stream"
  where "morph_stream f uu \<equiv> flat (sfilter (\<lambda>w. w \<noteq> \<epsilon>) (smap f\<^sup>\<C> uu))"
    \<comment> \<open>@{term flat} needs non empty words as elements to work well.\<close>

lemma morph_stream_id: "morph_stream (\<lambda>w. w) uu = uu"
proof(rule stream.coinduct)
  let ?R = "\<lambda> uu vv. morph_stream (\<lambda>w. w) vv = uu"

  show "?R stream stream' \<Longrightarrow> shd stream = shd stream' \<and> ?R (stl stream) (stl stream')" for stream stream'::"'a stream"
  proof-
    fix stream stream'::"'a stream"
    assume "?R stream stream'"
    hence sp: "morph_stream (\<lambda>w. w) ((shd stream')##(stl stream')) = (shd stream)##(stl stream)"
      by auto
    hence "shd stream = shd stream'" "morph_stream (\<lambda>w. w) (stl stream') = stl stream"
      unfolding morph_stream_def
      unfolding stream.map
      unfolding core_sing
      unfolding sfilter_Stream
      by fastforce+
    thus "shd stream = shd stream' \<and> ?R (stl stream) (stl stream')"
      by blast
  qed

  show "?R (morph_stream (\<lambda>w. w) uu) uu"
    by simp
qed

lemma (in morphism) morph_stream_pref: "morph_stream f (p\<^bold>\<cdot>uu) = (f p)\<^bold>\<cdot>(morph_stream f uu)"
proof(induction p, simp)
  case (Cons a p)
  show ?case
    unfolding pop_hd[of a p]
    unfolding shift_append
    unfolding Cons[symmetric]
    unfolding morph_stream_def
    unfolding shift.simps(2)
    unfolding smap_shift
    unfolding stream.map
    unfolding core_sing[of _ a]
    unfolding smap_shift[symmetric]
    unfolding sfilter_Stream
    using flat_Stream
    by simp
qed

lemma stream_stake_eq: assumes "(\<And>n. stake n uu = stake n vv)"
  shows "uu = vv"
proof(rule stream.coinduct)
  let ?R = "\<lambda> uu vv. (\<forall>n. stake n uu = stake n vv)"

  show "?R stream stream' \<Longrightarrow> shd stream = shd stream' \<and> ?R (stl stream) (stl stream')" for stream stream'::"'a stream"
    by (metis list.sel(1) list.sel(3) stake.simps(2))

  show "?R uu vv"
    using assms by blast
qed


lemma morph_stream_comp: assumes "nonerasing_morphism f" "nonerasing_morphism g"
  shows "morph_stream (f o g) uu = morph_stream f (morph_stream g uu)"
proof(rule stream_stake_eq)
  show "stake n (morph_stream (f \<circ> g) uu) = stake n (morph_stream f (morph_stream g uu))" for n
  proof(induction n arbitrary: uu rule:less_induct)
    case (less x)

    have t1: "morph_stream (f \<circ> g) uu = (f o g) [shd uu] \<^bold>\<cdot> morph_stream (f \<circ> g) (stl uu)"
      by (metis assms(1) assms(2) morph_compose morphism.morph_stream_pref nonerasing_morphism.axioms(1) shift.simps(1) shift.simps(2) stream.exhaust_sel)

    have t2: "(morph_stream f (morph_stream g uu)) = (f o g) [shd uu] \<^bold>\<cdot> (morph_stream f (morph_stream g (stl uu)))"
      by (metis assms(1) assms(2) comp_apply morphism.morph_stream_pref nonerasing_morphism.axioms(1) shift.simps(1) shift.simps(2) stream.exhaust_sel)

    show ?case
    proof(cases "x = 0")
      assume "x \<noteq> 0"

      have "0 < \<^bold>|f (g [shd uu])\<^bold>|"
        by (simp add: assms(1) assms(2) nonerasing_morphism.nemp_to_nemp)
      hence le: "x - \<^bold>|f (g [shd uu])\<^bold>| < x"
        using diff_less neq0_conv \<open>x \<noteq> 0\<close> by presburger

      show ?case
        unfolding t1 t2
        using less[OF le]
        unfolding comp_apply stake_shift
        unfolding cancel.
    qed simp
  qed
qed


lemma (in morphism) morph_stream_fac: assumes "sfactor w uu"
  shows "sfactor (f w) (morph_stream f uu)"
  by (metis assms morph_stream_pref sfactorE sfactorI)

lemma (in morphism) assumes "f w \<noteq> \<epsilon>"
  shows "morph_stream f (cycle w) = cycle (f w)"
  unfolding cycle_sprefix_eq[OF assms, symmetric]
proof
  show "sprefix (f w \<^sup>@ k) (morph_stream f (cycle w))" for k
  proof(induction k)
    case 0
    then show ?case
      by (simp add: sprefix_def)
  next
    case (Suc k)
    have "morph_stream f (cycle w) = (f w)\<^bold>\<cdot> morph_stream f (cycle w)"
      by (metis cycle_decomp morph_stream_pref shift.simps(1))
    then show ?case
      by (metis CoWBasic.pow_Suc Suc sprefixE sprefixI shift_append)
  qed
qed

lemma noner_endom_growing:  assumes "endomorphism f" "nonerasing_morphism f" "[shd \<uu>] \<le>f f [shd \<uu>]" "\<^bold>|f [shd \<uu>]\<^bold>| > 1"
  shows "growing f [shd \<uu>]"
proof-
  have shd_k: "[shd \<uu>] \<le>f (f^^k) [shd \<uu>]" for k
  proof(induction k)
    case (Suc k)
    then show ?case
      unfolding funpow.simps comp_apply
      using assms(1) assms(3) endomorphism_def morphism.fac_mono by blast
  qed simp

  have "k \<le> \<^bold>|(f ^^ k) [shd \<uu>]\<^bold>|" for k
  proof(induction k)
    case (Suc k)
    then show ?case
      unfolding funpow.simps comp_apply
      using shd_k
      by (metis (no_types, lifting) assms(2) assms(4) le_less_Suc_eq linorder_le_less_linear nonerasing_morphism.im_len_le nonerasing_morphism.im_len_less order.strict_trans1 order_less_imp_not_eq2 sing_fac_set)
  qed auto
  thus ?thesis
    by blast
qed

lemma (in morphism) spref_morph_stream:  assumes "sprefix w uu"
  shows "sprefix (f w) (morph_stream f uu)"
  by (metis assms morph_stream_pref sprefix_def)

subsection "Infinite word defined by a sequence of its prefixes"

lemma pref_seq_stl: assumes "\<And>i. prefs!!i \<le>p prefs!!(Suc i)"
  shows "\<And>i. (stl prefs)!!i \<le>p (stl prefs)!!(Suc i)"
  by (metis assms snth.simps(2))

lemma pref_seq_lq: assumes "\<And>i. prefs!!i \<le>p prefs!!(Suc i)"
  shows "\<And>i. (smap (\<lambda>w. (shd prefs)\<inverse>\<^sup>>w) (prefs))!!i \<le>p (smap (\<lambda>w. (shd prefs)\<inverse>\<^sup>>w) (prefs))!!(Suc i)"
  by (metis assms pref_lq snth_smap)

lemma pref_seq_lq_stl: assumes "\<And>i. prefs!!i \<le>p prefs!!(Suc i)"
  shows "\<And>i. (smap (\<lambda>w. (shd prefs)\<inverse>\<^sup>>w) (stl prefs))!!i \<le>p (smap (\<lambda>w. (shd prefs)\<inverse>\<^sup>>w) (stl prefs))!!(Suc i)"
  by (metis assms pref_lq pref_seq_stl snth_smap)

lemma pref_seq_strong: assumes "\<And>i. prefs!!i \<le>p prefs!!(Suc i)"
  shows "\<And>i j. i \<le> j \<Longrightarrow> prefs!!i \<le>p prefs!!j"
  using assms prefix_order.lift_Suc_mono_le by blast

lemma spref_seq: assumes "\<And>i. prefs!!i <p prefs!!(Suc i)"
  shows
      "\<And>i. prefs!!i \<le>p prefs!!(Suc i)"
      "\<And>i j. i < j \<Longrightarrow> prefs!!i <p prefs!!j"
  using assms prefix_order.lift_Suc_mono_less by blast+



primcorec split_prefix_seq :: "'a list stream \<Rightarrow> 'a list stream"
  where
 "shd (split_prefix_seq prefs) = shd prefs" |
 "stl (split_prefix_seq prefs) = split_prefix_seq (smap (\<lambda>w. (shd prefs)\<inverse>\<^sup>>w) (stl prefs))"

lemma split_pref_seq_inv: assumes "\<And>i. prefs!!i \<le>p prefs!!(Suc i)"
  shows
      "sdrop (Suc k) (split_prefix_seq prefs) = split_prefix_seq (smap (\<lambda>w. (prefs!!k)\<inverse>\<^sup>>w) (sdrop (Suc k) prefs))"
      "(split_prefix_seq prefs)!!(Suc k) = (prefs!!k)\<inverse>\<^sup>>(prefs!!(Suc k))"
      "prefs!!k = concat ((split_prefix_seq prefs)!#[0..<(Suc k)])"
  using assms
proof(induction k arbitrary: prefs)
  case 0
  fix prefs::"'a list stream"
  have s0: "split_prefix_seq prefs !! Suc 0 =  shd (stl (split_prefix_seq prefs))"
    by simp
  show "split_prefix_seq prefs !! Suc 0 = prefs !! 0\<inverse>\<^sup>>prefs !! Suc 0"
    unfolding s0
    unfolding split_prefix_seq.simps(2)
    unfolding split_prefix_seq.simps(1)
    by simp
  show "sdrop (Suc 0) (split_prefix_seq prefs) =
    split_prefix_seq (smap (left_quotient (prefs !! 0)) (sdrop (Suc 0) prefs))"
    unfolding sdrop.simps
    unfolding split_prefix_seq.simps
    by simp
  show "prefs !! 0 = concat (split_prefix_seq prefs !# [0..<Suc 0])"
    unfolding upt_Suc
    using snth_list_shd split_prefix_seq.simps(1)
    by simp
next
  case (Suc k)
  fix prefs::"'a list stream"
  assume ps: "(\<And>i. prefs !! i \<le>p prefs !! Suc i)"
  have he1: "smap (left_quotient (shd prefs)) (stl prefs) !! k = (shd prefs)\<inverse>\<^sup>>(prefs!!(Suc k))"
    by simp
  have he2: "shd prefs \<le>p prefs !! Suc k"
    using pref_seq_strong[OF ps, OF zero_less_Suc[unfolded less_Suc_eq_le]]
    unfolding snth.simps(1)[symmetric].
  have he3: "(\<lambda>x. (shd prefs\<inverse>\<^sup>>prefs !! Suc k)\<inverse>\<^sup>>shd prefs\<inverse>\<^sup>>x) = left_quotient (prefs !! Suc k)"
    unfolding left_quotient_def
    unfolding drop_drop
    using add.commute he2 left_quotient_def length_append lq_pref by metis
  have he4: " (smap (left_quotient (smap (left_quotient (shd prefs)) (stl prefs) !! k))
       (sdrop (Suc k) (smap (left_quotient (shd prefs)) (stl prefs)))) = (smap (left_quotient (prefs !! Suc k)) (sdrop (Suc k) (stl prefs)))"
    unfolding he1
    unfolding sdrop_smap
    unfolding stream.map_comp
    unfolding comp_apply
    unfolding he3..
  show "sdrop (Suc (Suc k)) (split_prefix_seq prefs) =
         split_prefix_seq (smap (left_quotient (prefs !! Suc k)) (sdrop (Suc (Suc k)) prefs))"
    unfolding sdrop.simps(2)[of "(Suc k)"]
            unfolding split_prefix_seq.simps
    unfolding Suc.IH(1)[OF pref_seq_lq_stl[OF ps]]
    unfolding he4..
  thus "split_prefix_seq prefs !! Suc (Suc k) = prefs !! Suc k\<inverse>\<^sup>>prefs !! Suc (Suc k)"
    by (metis sdrop_simps(1) split_prefix_seq.simps(1) stream.map_sel(1))
  have q1: "split_prefix_seq prefs !# [0..<Suc (Suc k)] = split_prefix_seq prefs !# [0..<(Suc k)] \<cdot> [(split_prefix_seq prefs)!!(Suc k)]"
    by (simp add: snth_list_append)
  show "prefs !! Suc k = concat (split_prefix_seq prefs !# [0..<Suc (Suc k)])"
    unfolding q1
    unfolding concat_append
    unfolding Suc.IH(3)[OF ps, symmetric]
    unfolding Suc.IH(2)[OF ps]
    using ps by force
qed


lemma split_spref_seq_nemp: assumes "\<And>i. prefs!!i <p prefs!!(Suc i)"
  shows
      "(split_prefix_seq prefs)!!(Suc k) \<noteq> \<epsilon>"
  by (metis assms lq_spref_nemp split_pref_seq_inv(2) sprefD)



definition stream_from_prefixes :: "('a list stream) \<Rightarrow> 'a stream"
  where
 "(stream_from_prefixes prefs) = flat (split_prefix_seq prefs)"

lemma concat_flat_flat: assumes "\<And>k. uu!!k \<noteq> \<epsilon>"
  shows "(concat (uu!#[0..<k])) \<^bold>\<cdot> flat (sdrop k uu) = flat uu"
proof(induction k )
  case 0
  then show ?case
    by simp
next
  case (Suc k)
  have h1: "concat (uu !# [0..<Suc k]) = (concat (uu !# [0..<k])) \<cdot> (uu)!!k"
    by (metis concat_morph concat_sing' snth_list_0_stake stake_Suc)
  have a1: "sdrop k uu = uu!!k ## sdrop (Suc k) uu"
    by (metis id_stake_snth_sdrop scancel stake_sdrop)
  have h2: "(uu !! k)\<^bold>\<cdot> flat (sdrop (Suc k) (uu)) = flat (sdrop k (uu))"
    unfolding a1
    unfolding flat_Stream[OF assms]..
  show ?case
    unfolding h1
    unfolding sdrop.simps
    using h2
    by (simp add: Suc)
qed

lemma spref_stream_from_pref: assumes "\<And>i. prefs!!i <p prefs!!(Suc i)" "prefs!!0 \<noteq> \<epsilon>"
  shows "sprefix (prefs!!k) (stream_from_prefixes prefs)"
proof(induction k)
  case 0
  then show ?case
    unfolding stream_from_prefixes_def
    by (smt (verit, ccfv_SIG) flat_unfold sfilter_Stream snth.simps(1) split_prefix_seq.code sprefixI sprefixes.simps sprefixes_sprefix stream.sel(1))
next
  case (Suc k)
  have nemp: "(split_prefix_seq prefs)!!i \<noteq> \<epsilon>" for i
    using split_spref_seq_nemp[OF assms(1)] assms(2)
    by (metis list_decode.cases snth.simps(1) split_prefix_seq.simps(1))
  show ?case
    unfolding stream_from_prefixes_def
    using split_pref_seq_inv(3)[OF spref_seq(1)[OF assms(1)]] concat_flat_flat[OF nemp] sprefixI by metis
qed

definition fixed_point_of::"('a list \<Rightarrow> 'a list) \<Rightarrow> 'a \<Rightarrow> 'a stream"
  where "fixed_point_of f a \<equiv> stream_from_prefixes (snth_stream (\<lambda>i. (f^^i) [a]))"

lemma (in endomorphism) spref_seq_fixed_point_of:
  assumes "\<And>i. (f^^i) [a] <p (f^^(Suc i)) [a]"
  shows "morph_stream f (fixed_point_of f a) = fixed_point_of f a" (is "?mf = ?fp")
proof-
  let ?prefs = "snth_stream (\<lambda>i. (f^^i) [a])"
  have a1: "\<And>i. ?prefs!!i <p ?prefs!!(Suc i)"
    unfolding snth_snth_stream
    using assms.
  have a2: "?prefs!!0 \<noteq> \<epsilon>"
    unfolding snth_snth_stream funpow_0
    by simp
  note p1 = spref_stream_from_pref[OF a1 a2, unfolded snth_snth_stream]
  have p2: "sprefix ((f^^i) [a]) ?mf" for i
    unfolding fixed_point_of_def
    using spref_morph_stream[OF p1] assms prefix_sprefix by fastforce
  show ?thesis
  proof(rule stream_stake_eq)
    fix n
    have "\<^bold>|(f^^i) [a]\<^bold>| < \<^bold>|(f^^(Suc i)) [a]\<^bold>|" for i
      using assms prefix_length_less by blast
    hence "strictly_growing_seq (\<lambda>i. (f^^i) [a])"
      by blast
    then obtain q where "\<^bold>|(f^^q) [a]\<^bold>| \<ge> n"
      using sgrowing_growing growing_def by blast
    show "stake n ?mf = stake n (?fp)"
      using arg_cong[OF sprefix_sdrop[OF p1[of q]], of "stake n"]
        arg_cong[OF sprefix_sdrop[OF p2[of q]], of "stake n"]
      unfolding fixed_point_of_def
      by (simp add: \<open>n \<le> \<^bold>|(f ^^ q) [a]\<^bold>|\<close> stake_shift)
  qed
qed

subsection "Fixed point"

locale fixed_point = endomorphism +
  fixes \<uu>
  assumes
  fixed_point: "(morph_stream f \<uu>) = \<uu>" and
  shd_grow: "growing f [shd \<uu>]" begin

lemma shd_pref: "sprefix ((f^^k) [shd \<uu>]) \<uu>"
proof(induction k)
  case 0
  then show ?case
    by (simp add: sprefix_stake)
next
  case (Suc k)
  then show ?case
    by (metis compow_Suc fixed_point morph_stream_pref sprefix_def)
qed

lemma shd_im: "(hd (f [shd \<uu>])) = shd \<uu>" "tl (f [shd \<uu>]) \<noteq> \<epsilon>" "f [shd \<uu>] \<noteq> \<epsilon>"
proof-
  have "(f ^^ k) [shd \<uu>] \<noteq> \<epsilon>" for k
    using growing_immortal[OF shd_grow]
    unfolding immortal_def
    by blast
  from this[of 1]
  show "(hd (f [shd \<uu>])) = shd \<uu>"
    using shd_pref[of 1]
    unfolding sprefix_def funpow_1
    using shift_simps(1)
    by metis
  show "tl (f [shd \<uu>]) \<noteq> \<epsilon>"
  proof(rule ccontr)
    assume "\<not> tl (f [shd \<uu>]) \<noteq> \<epsilon>"
    hence "f [shd \<uu>] = [shd \<uu>]"
      using \<open>\<And>k. (f ^^ k) [shd \<uu>] \<noteq> \<epsilon>\<close>[of 1, THEN list.collapse]
      unfolding funpow_1
      unfolding \<open>hd (f [shd \<uu>]) = shd \<uu>\<close>
      by argo
    hence "(f^^k) [shd \<uu>] = [shd \<uu>]" for k
      by(induction k, simp_all)
    thus False
      using shd_grow
      unfolding growing_def
      by auto
  qed
  show "f [shd \<uu>] \<noteq> \<epsilon>"
    using \<open>tl (f [shd \<uu>]) \<noteq> \<epsilon>\<close> by force
qed

lemma shd_it_suf_pref: "sprefix ([shd \<uu>]\<cdot>(iterated_suffix (tl (f [shd \<uu>]))) k) \<uu>"
  using image_iterated_suffix'[of "[shd \<uu>]" "tl (f [shd \<uu>])"]
  by (metis hd_tl shd_im(1) shd_im(2) shd_pref tl_Nil)

lemma pow_fixed_point: assumes "nonerasing_morphism f" \<comment> \<open>assumption required to use @{thm morph_stream_comp}, however, can be dropped using a more elaborate argument following from @{thm fixed_point} \<close>
  shows "(morph_stream (f^^k) \<uu>) = \<uu>"
proof(induction k)
  case 0
  then show ?case
    unfolding funpow_0 morph_stream_id..
next
  case (Suc k)
  have nepow: "nonerasing_morphism (f ^^ k)"
    by (simp add: assms funpow_nonerasing_morphism)
  show ?case
    unfolding funpow.simps
    unfolding morph_stream_comp[OF \<open>nonerasing_morphism f\<close> nepow]
    unfolding Suc
    using fixed_point.
qed

lemma wlan_pmor: "\<F> \<uu> = purely_morphic_language f ([shd \<uu>])"
proof
  show "\<F> \<uu> \<subseteq> \<L> [shd \<uu>]"
  proof
    fix x
    assume " x \<in> \<F> \<uu>"
    then obtain i where "x = \<uu> !# [i..<i + \<^bold>|x\<^bold>|]"
      unfolding wlan_snth_list
      by blast
    hence "x \<le>f stake (i + \<^bold>|x\<^bold>|) \<uu>"
      by (metis le_add1 length_upt snth_list_len snth_list_stake_sdrop stake_add sublist_append_leftI)

    obtain n where n_def: "\<^bold>|(f^^n) [shd \<uu>]\<^bold>| \<ge> i + \<^bold>|x\<^bold>|"
      using growingE shd_grow by blast
    from shd_pref[of n, unfolded sprefix_stake]
    have "x \<le>f (f^^n) [shd \<uu>]"
      using fac_trans[OF \<open>x \<le>f stake (i + \<^bold>|x\<^bold>|) \<uu>\<close> stake_mono_pref[OF n_def, THEN prefix_imp_sublist]]
      by argo
    thus "x \<in> \<L> [shd \<uu>]"
      using pmor_lan_fac_pow by blast
  qed
  show "\<L> [shd \<uu>] \<subseteq> \<F> \<uu>"
    unfolding subset_iff
    unfolding pmor_lan_fac_pow wlan_snth_list
    using shd_pref factor_sfactor sfactor_snth_list sprefix_sfactor by metis
qed

corollary fac_fac_pow: assumes "w \<in> \<F> \<uu>"
  obtains k where "w \<le>f (f^^k) [shd \<uu>]"
  using assms pmor_lan_fac_pow wlan_pmor by blast

corollary fac_fac_pow_eq:
  shows "w \<in> \<F> \<uu> \<longleftrightarrow> (\<exists>k. w \<le>f (f^^k) [shd \<uu>])"
  using pmor_lan_fac_pow wlan_pmor by presburger

lemma fp_of_shd: assumes "\<uu> = fixed_point_of f a"
  shows "shd (\<uu>) = a"
  using shd_pref shd_im assms
  unfolding fixed_point_of_def stream_from_prefixes_def
  by simp

lemma fac_pow_ge: "k \<le> l \<Longrightarrow> w \<le>f (f^^k) [shd \<uu>] \<Longrightarrow> w \<le>f (f^^l) [shd \<uu>]"
proof(induction "l-k" arbitrary:l)
  case 0
  then show ?case
    by simp
next
  case (Suc x)
  have km:"k \<le> l - 1"
    using Suc.hyps(2) by linarith
  have x: "x = (l-1)-k"
    by (metis Suc.hyps(2) diff_Suc_1 diff_right_commute)
  have fl: "((f ^^ l) w) = (f ^^ (l - 1)) (f w)" for w
    by (metis Suc.hyps(2) Suc_minus compow_Suc' lessI less_nat_zero_code zero_diff)
  show ?case
    using Suc.hyps(1)[OF x km \<open>w \<le>f (f ^^ k) [shd \<uu>]\<close>]
    unfolding fl
    unfolding morphism.pop_hd_nemp[OF endomorphism.pow_morphism, OF endomorphism_axioms, OF shd_im(3)]
    unfolding shd_im(1)
    by blast
qed

end

lemma (in endomorphism) spref_seq_fixed_point:
  assumes "\<And>i. (f^^i) [a] <p (f^^(Suc i)) [a]"
  shows "fixed_point f (fixed_point_of f a)"
proof(unfold_locales)
  show "morph_stream f (fixed_point_of f a) = fixed_point_of f a"
    using spref_seq_fixed_point_of[OF assms].

  let ?prefs = "snth_stream (\<lambda>i. (f^^i) [a])"
  have a1: "\<And>i. ?prefs!!i <p ?prefs!!(Suc i)"
    unfolding snth_snth_stream
    using assms.
  have a2: "shd (fixed_point_of f a) = a"
    unfolding fixed_point_of_def
    using spref_stream_from_pref[OF a1]
    using funpow_0 list.distinct(1) shift.simps(2) snth_snth_stream sprefix_def stream.sel(1) by metis
  show "growing f [shd (fixed_point_of f a)]"
    unfolding a2 growing_def
    using prefix_length_less[OF assms]
    using sgrowingI[THEN sgrowing_len]
    by fast
qed

section "Periodic infinite words"

definition purely_periodic where
  "purely_periodic uu \<equiv> \<exists>u. uu = cycle u"

definition periodic where
  "periodic uu \<equiv> \<exists>u s. uu = s \<^bold>\<cdot> cycle u"

lemma pperiodicI: "uu = cycle u \<Longrightarrow> purely_periodic uu"
  using purely_periodic_def by blast

lemma pperiodicD: assumes "purely_periodic uu"
  obtains u where "uu = cycle u" "u \<noteq> \<epsilon>"
  using assms cycle.ctr cycle_Cons list.distinct(1) purely_periodic_def by metis

lemma periodicI: "uu = s \<^bold>\<cdot> cycle u \<Longrightarrow> periodic uu"
  using periodic_def by blast

lemma periodicD: assumes "periodic uu"
  obtains s u where "uu = s \<^bold>\<cdot> cycle u" "u \<noteq> \<epsilon>"
  using assms cycle.ctr cycle_Cons list.distinct(1) periodic_def
  by metis

lemma pperiodic_prefix: "purely_periodic uu \<longleftrightarrow> (\<exists>u. u \<noteq> \<epsilon> \<and> uu = u \<^bold>\<cdot> uu)"
  using cycle_decomp prefix_cycle pperiodicD pperiodicI by metis

lemma pperiodic_period: "purely_periodic uu \<longleftrightarrow> (\<exists>p. \<forall>n. 0 < p \<and> uu!!n = uu!!(n+p))"
proof
  assume "purely_periodic uu"
  from pperiodicD[OF this]
  obtain u where
    "uu = cycle u" and
    "u \<noteq> \<epsilon>"
    by blast
  from cycle_period[of u, folded \<open>uu = cycle u\<close>]
  show "\<exists>p. \<forall>n. 0 < p \<and> uu !! n = uu !! (n + p)"
    using \<open>u \<noteq> \<epsilon>\<close> by blast
next
  assume "\<exists>p. \<forall>n. 0 < p \<and> uu !! n = uu !! (n + p)"
  then obtain p where "0 < p" "\<And>n. uu !! n = uu !! (n + p)"
    by blast
  have "uu!!n = ((stake p uu) \<^bold>\<cdot> uu)!!n" for n
    using \<open>\<And>n. uu !! n = uu !! (n + p)\<close> le_add_diff_inverse2 length_stake not_less shift_snth stake_nth by metis
  hence "uu = (stake p uu) \<^bold>\<cdot> uu"
    using stream_smap_nats[of uu] stream_smap_nats[of "(stake p uu) \<^bold>\<cdot> uu"]
    by presburger
  moreover have "stake p uu \<noteq> \<epsilon>"
    using \<open>0 < p\<close> by blast
  ultimately show "purely_periodic uu"
    unfolding pperiodic_prefix by blast
qed

lemma periodic_suffix: "periodic uu \<longleftrightarrow> (\<exists>s vv. uu = s \<^bold>\<cdot> vv \<and> purely_periodic vv)"
  by (metis periodic_def purely_periodic_def)

lemma periodic_period: "periodic uu \<longleftrightarrow> (\<exists>p q. 0 < p \<and> (\<forall>n. q \<le> n \<longrightarrow> uu!!n = uu!!(n+p)))"
proof
  assume "periodic uu"
  then obtain s vv where "uu = s \<^bold>\<cdot> vv" "purely_periodic vv"
    using periodic_suffix by blast
  from this(2)[unfolded pperiodic_period]
  obtain p where "0 < p" "\<And>n. vv !! n = vv !! (n + p)"
    by blast
  hence "uu !! n = uu !! (n + p)" if "n \<ge> \<^bold>|s\<^bold>|" for n
    unfolding \<open>uu = s \<^bold>\<cdot> vv\<close>
    using nat_le_iff_add that by auto
  thus "\<exists>p q. 0 < p \<and> (\<forall>n\<ge>q. uu !! n = uu !! (n + p))"
    using \<open>0 < p\<close> by blast
next
  assume "\<exists>p q. 0 < p \<and> (\<forall>n\<ge>q. uu !! n = uu !! (n + p))"
  then obtain p q where "0 < p" "\<And>n. n\<ge>q \<Longrightarrow> uu !! n = uu !! (n + p)" by blast
  have "\<And>n. uu !! (q+n) = uu !! (q + (n + p))"
    using \<open>\<And>n. q \<le> n \<Longrightarrow> uu !! n = uu !! (n + p)\<close> le_add1 add.assoc[of q]
    by metis
  hence "purely_periodic (sdrop q uu)"
    unfolding sdrop_snth[of q uu, symmetric]
    unfolding pperiodic_period
    using \<open>0 < p\<close> by blast
  thus "periodic uu"
    unfolding periodic_suffix
    using stake_sdrop[of q uu, symmetric]
    by blast
qed

subsection "Factor complexity of periodic words"

lemma pperiodic_fac_comp: assumes "w \<noteq> \<epsilon>"
  shows "factor_complexity (cycle w) n \<le> \<^bold>|\<rho> w\<^bold>|"
        "n \<ge> \<^bold>|\<rho> w\<^bold>| \<Longrightarrow> factor_complexity (cycle w) n = \<^bold>|\<rho> w\<^bold>|"
proof-
    have crw: "cycle w = cycle (\<rho> w)"
    using assms cycle_primroot by blast
  define F where "F = {(cycle w) !# [i..<i + n] |i. i < \<^bold>|\<rho> w\<^bold>|}"
  have "f \<in> F" if "f \<in> (\<F> (cycle w))\<^bsub>n\<^esub>" for f
  proof-
    obtain i where "f = cycle (\<rho> w) !# [i..<i + n]"
      using \<open>f \<in> \<F> (cycle w)\<^bsub>n\<^esub>\<close>
      unfolding wlan_n_snth_list crw by blast
    have ma: "map (\<lambda>ia. ia + i div \<^bold>|\<rho> w\<^bold>| * \<^bold>|\<rho> w\<^bold>|) [i mod \<^bold>|\<rho> w\<^bold>|..<i mod \<^bold>|\<rho> w\<^bold>| + n] = [i..<i+n]" for n
    proof(induction n)
      case (Suc n)
      have he: "i mod \<^bold>|\<rho> w\<^bold>| \<le> (i mod \<^bold>|\<rho> w\<^bold>| + n)"
        by linarith
      show ?case
        using mod_div_mult_eq[of i "\<^bold>|\<rho> w\<^bold>|"]
        unfolding add_Suc_right
        unfolding upt_Suc_append[OF he]
        using Suc by auto
    qed simp

    show ?thesis
      using cycle_snth_list_shift[of "\<rho> w" "[(i mod \<^bold>|\<rho> w\<^bold>|)..<(i mod \<^bold>|\<rho> w\<^bold>|)+n]" "i div \<^bold>|\<rho> w\<^bold>|"]
      unfolding F_def mem_Collect_eq crw
      unfolding ma \<open>f = cycle (\<rho>  w) !# [i..<i + n]\<close>
      using primroot_nemp[OF \<open>w \<noteq> \<epsilon>\<close>, THEN nemp_len, THEN mod_less_divisor]  by metis
  qed
  hence sub: "(\<F> (cycle w))\<^bsub>n\<^esub> = F"
    by (smt (verit, ccfv_SIG) Collect_cong F_def mem_Collect_eq wlan_n wlan_n_snth_list)

  let ?f = "(\<lambda>i. (cycle w)!#[i..<i+n])"
  have "?f`{0..<\<^bold>|\<rho> w\<^bold>|} = F"
    unfolding F_def set_eq_iff image_iff
    using atLeast0LessThan by blast
  hence "card F \<le> \<^bold>|\<rho> w\<^bold>|"
    using atLeast0LessThan card_image_le card_lessThan finite_lessThan by metis

  have "card F = \<^bold>|\<rho> w\<^bold>|" if "n \<ge> \<^bold>|\<rho> w\<^bold>|"
  proof-
    have "inj_on ?f {0..<\<^bold>|\<rho> w\<^bold>|}"
    proof(rule ccontr)
      assume "\<not> inj_on ?f {0..<\<^bold>|\<rho> w\<^bold>|}"
      then obtain i j where "i < j" "(cycle w)!#[i..<i+n] = (cycle w)!#[j..<j+n]" "j < \<^bold>|\<rho> w\<^bold>|"
        unfolding inj_on_def
        by (metis atLeastLessThan_iff linorder_neqE_nat)
      have "rotate i (\<rho> w) = rotate j (\<rho> w)"
      proof-
        have he:"cycle w !# [i..<i + n] = (sdrop i (cycle w)) !# [0..< n]" for i
          by (simp add: snth_list_stake_sdrop)
        have a1: "i mod \<^bold>|\<rho> w\<^bold>| = i"
          using \<open>i < j\<close> \<open>j < \<^bold>|\<rho> w\<^bold>|\<close> by auto
        have a2: "j mod \<^bold>|\<rho> w\<^bold>| = j"
          by (simp add: \<open>j < \<^bold>|\<rho> w\<^bold>|\<close>)
                have "stake n (cycle (rotate i (\<rho> w))) = stake n (cycle (rotate j (\<rho> w)))"
          using \<open>(cycle w)!#[i..<i+n] = (cycle w)!#[j..<j+n]\<close>
          unfolding he
          unfolding crw
          unfolding sdrop_cycle[OF primroot_nemp[OF \<open>w \<noteq> \<epsilon>\<close>]]
          unfolding a1 a2
          unfolding snth_list_0_stake.
        from arg_cong[OF this, of "\<lambda>t. take (\<^bold>|\<rho> w\<^bold>|) t"]
        have "stake (\<^bold>|\<rho> w\<^bold>|) (cycle (rotate i (\<rho> w))) = stake (\<^bold>|\<rho> w\<^bold>|) (cycle (rotate j (\<rho> w)))"
          by (metis min_absorb1 take_stake that)
        thus ?thesis
          by (metis length_rotate rotate_is_Nil_conv stake_cycle_eq)
      qed
      thus False
        by (metis \<open>i < j\<close> \<open>j < \<^bold>|\<rho> w\<^bold>|\<close> add_diff_cancel_left' add_lessD2 assms less_imp_add_positive prim_iff_rotate primroot_nemp primroot_prim rotate_back')
    qed
    from card_image[OF this]
    show ?thesis
      unfolding \<open>?f`{0..<\<^bold>|\<rho> w\<^bold>|} = F\<close>
      by force
  qed

  show "factor_complexity (cycle w) n \<le> \<^bold>|\<rho> w\<^bold>|"
    by (simp add: \<open>card F \<le> \<^bold>|\<rho> w\<^bold>|\<close> factor_complexity_def sub)
  show "\<^bold>|\<rho> w\<^bold>| \<le> n \<Longrightarrow> factor_complexity (cycle w) n = \<^bold>|\<rho> w\<^bold>|"
    by (simp add: \<open>\<^bold>|\<rho> w\<^bold>| \<le> n \<Longrightarrow> card F = \<^bold>|\<rho> w\<^bold>|\<close> factor_complexity_def sub)
qed


lemma periodic_fac_comp: assumes "periodic uu"
  obtains c where "\<And>n. factor_complexity uu n \<le> c"
proof-
  from periodicD[OF assms]
  obtain s u where
    "uu = s \<^bold>\<cdot> cycle u" and
    "u \<noteq> \<epsilon>"
    by blast

  have "(\<And>c. (\<And>n. factor_complexity (s \<^bold>\<cdot> cycle u) n \<le> c) \<Longrightarrow> thesis) \<Longrightarrow> thesis" if "u \<noteq> \<epsilon>" for s u::"'a list" and thesis
    using that
  proof(induction s)
    case Nil
    then show ?case
      using pperiodic_fac_comp(1)
      by auto
  next
    case (Cons a s)
    then show ?case
      using fac_comp_sing
      by (metis (no_types, opaque_lifting) add.commute dual_order.trans nat_add_left_cancel_le shift.simps(2))
  qed
  thus thesis
    using \<open>u \<noteq> \<epsilon>\<close> \<open>uu = s \<^bold>\<cdot> cycle u\<close> that by blast
qed


section "infinite factor of a language"


definition infinite_factor
  where "infinite_factor L ww \<equiv> (\<forall>n. stake n ww \<in> L)"

lemma inf_facI[intro]: "(\<And>n. stake n ww \<in> L) \<Longrightarrow> infinite_factor L ww"
  unfolding infinite_factor_def
  by simp

lemma inf_facI'[intro]: assumes "\<forall>n. \<exists>i. k i \<ge> n" "(\<And>n. stake (k n) ww \<in> L)"
"factorial L"
  shows "infinite_factor L ww"
  unfolding infinite_factor_def
proof
  fix n
  obtain l where "(k l) \<ge> n"
    using assms(1) by blast
  hence "stake (k l) ww \<in> L"
    using assms(2) by auto
  hence "take n (stake (k l) ww) \<in> L"
    using  assms(3) factorialD by blast
  have "stake n ww = take n (stake (k l) ww)"
    by (simp add: \<open>n \<le> k l\<close> take_stake)
  thus "stake n ww \<in> L"
    using \<open>take n (stake (k l) ww) \<in> L\<close> by presburger
qed

lemma inf_facE: assumes "infinite_factor L ww"
  shows "stake n ww \<in> L"
  using assms infinite_factor_def by blast





section "infinite words in purely morphic languages"

context endomorphism
begin


lemma inf_fac_pushy: assumes "infinite_factor (\<L> axiom) ww" "stream_all (\<lambda>a. bounded [a]) ww"
  shows "pushy axiom"
  using inf_facE[OF \<open>infinite_factor (\<L> axiom) ww\<close>] stream_all_stake[OF \<open>stream_all (\<lambda>a. bounded [a]) ww\<close>] pushyI
  unfolding bounded_list_all[symmetric]
  using impossible_Cons length_stake less_le_not_le nle_le
  by metis


lemma inf_fac_long: assumes "infinite_factor (\<L> axiom) ww"
  obtains n where "\<^bold>|(f^^n) axiom\<^bold>| \<ge> n"
  using inf_facE[OF \<open>infinite_factor (\<L> axiom) ww\<close>]
  unfolding pmor_lan_fac_pow
  by blast













end
end
