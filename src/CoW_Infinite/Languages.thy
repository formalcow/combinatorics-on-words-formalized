(*  Title:      Languages
    File:       CoW_Infinite.Languages
    Author:     Štěpán Starosta, Czech Technical University in Prague

Part of Combinatorics on Words Formalized. See https://gitlab.com/formalcow/combinatorics-on-words-formalized/
*)

theory Languages
  imports
    CoW.Morphisms
    CoW.Equations_Basic
begin


section "Preliminaries"

subsection "Funpow and funpow image"

lemma funpow_add': "f ^^ (m + n) = f ^^ n \<circ> f ^^ m"
  unfolding add.commute[of m]
  using funpow_add.

lemma funpow_add'': "(f^^k) ((f^^e) w) = (f^^(k+e)) w"
  unfolding funpow_add comp_apply..

lemma funpow_1: "f^^1 = (f::'a \<Rightarrow> 'a)"
  by simp

abbreviation funpow_image ("(_)^`(_)`(_)" )
  where "funpow_image g I A \<equiv> \<Union> {(g ^^ i) ` A |i. i \<in> I}"

lemma funpow_image_sing:
  "(f^`I`{x}) = {(f^^i) x | i. i \<in> I}"
  unfolding Union_eq Bex_def mem_Collect_eq
  by blast

lemma funpow_image_Un:
  "(f^`I`(A \<union> B)) = (f^`I`A) \<union> (f^`I`B)"
  "(f^`(I \<union> J)`A) = (f^`I`A) \<union> (f^`J`A)"
  unfolding image_Un Union_Un_distrib[symmetric]
  unfolding Union_eq Bex_def
  unfolding Un_iff
  unfolding mem_Collect_eq
  by blast+


lemma funpow_image_rev_map: "((rev_map ((f::'a list \<Rightarrow>'a list)))^`I`(rev ` A)) = rev`(f^`I`A)"
  unfolding Union_eq Bex_def
  unfolding rev_map_funpow
  unfolding rev_map_def
  unfolding image_comp[symmetric]
  unfolding rev_rev_image_eq
  by (standard, blast+)

lemma funpow_image_fin_fin_fin: assumes "finite I" "finite A"
  shows "finite (f^`I`A)"
  using assms(2)
proof(induct rule: finite_induct[OF \<open>finite I\<close>])
  case 1
  then show ?case
    by auto
next
  case (2 x S)
  then show ?case
    unfolding insert_is_Un[of x S]
    unfolding funpow_image_Un
    unfolding funpow_image_sing finite_Un
    by fastforce
qed

subsection "Comparability"

lemma pref_comp_takeWhile: assumes "u \<bowtie> v" and "\<not> list_all P u" and "\<not> list_all P v"
  shows "takeWhile P u = takeWhile P v"
  by (rule pref_compE[OF \<open>u \<bowtie> v\<close>, unfolded prefix_def]) (use assms(2-3)[unfolded Ball_set[symmetric]] takeWhile_append1 in metis)+

lemma pref_comp_list_all: assumes "u\<cdot>[a] \<bowtie> v\<cdot>[b]" and "list_all P u" and "list_all P v" and "\<not> P a" and "\<not> P b"
  shows "u = v \<and> a = b"
  using pref_comp_takeWhile[OF \<open>u\<cdot>[a] \<bowtie> v\<cdot>[b]\<close>, of P] assms
  unfolding list_all_def Ball_def
  by force

lemmas suf_comp_list_all = pref_comp_list_all[reversed]






lemma long_pow_fac: assumes "u \<le>f w\<^sup>@l" and "\<^bold>|w \<^sup>@ (Suc n)\<^bold>| < \<^bold>|u\<^bold>|"
  shows "w\<^sup>@n \<le>f u"
proof(cases "w = \<epsilon>")
  assume "w \<noteq> \<epsilon>"
  hence "primitive (\<rho> w)"
    by blast

  have "\<^bold>|u\<^bold>| div \<^bold>|w\<^bold>| \<ge> (Suc n)"
    using \<open>\<^bold>|w \<^sup>@ ((Suc n))\<^bold>| < \<^bold>|u\<^bold>|\<close>[unfolded pow_len]
    using \<open>w \<noteq> \<epsilon>\<close> less_eq_div_iff_mult_less_eq less_imp_le_nat by blast
  hence "(\<^bold>|u\<^bold>| div \<^bold>|w\<^bold>| - 1) \<ge> n"
    by linarith

  have "(e\<^sub>\<rho> w)*(\<^bold>|u\<^bold>| div (e\<^sub>\<rho> w * \<^bold>|\<rho> w\<^bold>|)) \<le> (\<^bold>|u\<^bold>| div \<^bold>|\<rho> w\<^bold>|)"
    by (metis div_mult2_eq mult.commute times_div_less_eq_dividend)
  hence "(\<rho> w) \<^sup>@ ((e\<^sub>\<rho> w)*(\<^bold>|u\<^bold>| div \<^bold>|w\<^bold>|)) \<le>p \<rho> w \<^sup>@ (\<^bold>|u\<^bold>| div \<^bold>|\<rho> w\<^bold>|)"
    by (simp add: \<open>w \<noteq> \<epsilon>\<close> le_exps_pref primroot_exp_len)
  hence le1: "(\<rho> w) \<^sup>@ ((e\<^sub>\<rho> w)*(\<^bold>|u\<^bold>| div \<^bold>|w\<^bold>|) - 1) \<le>p \<rho> w \<^sup>@ (\<^bold>|u\<^bold>| div \<^bold>|\<rho> w\<^bold>| - 1)"
    by (meson \<open>w \<noteq> \<epsilon>\<close> diff_le_mono le_exps_pref pref_exp_le primroot_nemp)

  have "e\<^sub>\<rho> w \<ge> 1"
    by (simp add: \<open>w \<noteq> \<epsilon>\<close> leI primroot_exp_nemp)
  hence "(e\<^sub>\<rho> w)*(\<^bold>|u\<^bold>| div \<^bold>|w\<^bold>|) - (e\<^sub>\<rho> w) \<le> (e\<^sub>\<rho> w)*(\<^bold>|u\<^bold>| div \<^bold>|w\<^bold>|) - 1"
    using diff_le_mono2 by blast
  from le_exps_pref[OF this]
  have "(\<rho> w) \<^sup>@ ((e\<^sub>\<rho> w)*(\<^bold>|u\<^bold>| div \<^bold>|w\<^bold>|) - (e\<^sub>\<rho> w)) \<le>p \<rho> w \<^sup>@ (\<^bold>|u\<^bold>| div \<^bold>|\<rho> w\<^bold>| - 1)"
    using le1 prefix_order.dual_order.trans by blast
  hence "w \<^sup>@ (\<^bold>|u\<^bold>| div \<^bold>|w\<^bold>| - 1) \<le>p \<rho> w \<^sup>@ (\<^bold>|u\<^bold>| div \<^bold>|\<rho> w\<^bold>| - 1)"
    using primroot_exp_eq[symmetric]
    by (metis diff_mult_distrib2 mult.comm_neutral pow_mult)

  have "u \<le>f (\<rho> w)\<^sup>@(e\<^sub>\<rho> w * l)"
    by (simp add: pow_mult \<open>w \<noteq> \<epsilon>\<close> assms(1) primroot_exp_eq)
  from fac_pow_div[OF this \<open>primitive (\<rho> w)\<close>]
  show ?thesis
    using \<open>(\<^bold>|u\<^bold>| div \<^bold>|w\<^bold>| - 1) \<ge> n\<close>
    using \<open>w \<^sup>@ (\<^bold>|u\<^bold>| div \<^bold>|w\<^bold>| - 1) \<le>p \<rho> w \<^sup>@ (\<^bold>|u\<^bold>| div \<^bold>|\<rho> w\<^bold>| - 1)\<close> pop_pow' pref_suf_fac sublist_order.order_trans sufI by metis
qed simp



lemma extend_P_fac: assumes "w = p\<cdot>u\<cdot>s" and "list_all P u"
  obtains pp ps sp ss where
    "p = pp\<cdot>ps" "list_all P ps" "pp = \<epsilon> \<or> \<not> P (last pp)"
    "s = sp\<cdot>ss" "list_all P sp" "ss = \<epsilon> \<or> \<not> P (hd ss)"
    "w = pp\<cdot>(ps\<cdot>u\<cdot>sp)\<cdot>ss" "list_all P (ps\<cdot>u\<cdot>sp)" proof-
  define sp where "sp = takeWhile P s"
  define ss where "ss = dropWhile P s"

  define pp where "pp = rev (dropWhile P (rev p))"
  define ps where "ps = rev (takeWhile P (rev p))"

  have "p = pp\<cdot>ps"
    unfolding pp_def ps_def
    unfolding rev_append[symmetric]
    unfolding takeWhile_dropWhile_id
    unfolding rev_rev_ident..

  moreover have "list_all P ps"
    unfolding ps_def
    unfolding Ball_set[symmetric]
    unfolding Ball_def
    unfolding set_rev
    using set_takeWhileD
    by meson

  moreover have "pp = \<epsilon> \<or> \<not> P (last pp)"
    unfolding pp_def
    unfolding last_rev
    using Nil_is_rev_conv hd_dropWhile
    by metis

  moreover have "s = sp\<cdot>ss"
    unfolding sp_def ss_def
    by simp

  moreover have "list_all P sp"
    unfolding sp_def
    unfolding Ball_set[symmetric] Ball_def
    using set_takeWhileD
    by meson

  moreover have "ss = \<epsilon> \<or> \<not> P (hd ss)"
    unfolding ss_def
    using hd_dropWhile by blast

  moreover have "w = pp\<cdot>(ps\<cdot>u\<cdot>sp)\<cdot>ss"
    by (simp add: \<open>p = pp \<cdot> ps\<close> \<open>s = sp \<cdot> ss\<close> assms(1))

  moreover have "list_all P (ps\<cdot>u\<cdot>sp)"
    by (simp add: \<open>list_all P ps\<close> \<open>list_all P sp\<close> assms(2))

  from that[OF calculation this]
  show thesis.
qed

subsection "Morphism additions"

context morphism
begin

lemma core_set_image:
  shows "\<Union> (set ` f\<^sup>\<C> ` \<Union> (set`A)) = \<Union> (set ` f ` A)"
proof
  show "\<Union> (set ` f\<^sup>\<C> ` \<Union> (set ` A)) \<subseteq> \<Union> (set ` f ` A)"
  proof
    fix x
    assume "x \<in> \<Union> (set ` f\<^sup>\<C> ` \<Union> (set ` A))"
              show "x \<in> \<Union> (set ` f ` A)"
    proof(rule UN_E[OF \<open>x \<in> \<Union> (set ` f\<^sup>\<C> ` \<Union> (set ` A))\<close>[unfolded UN_simps, unfolded UN_extend_simps(10)]])
      fix xa
      assume "xa \<in> A" "x \<in> \<Union> (set ` f\<^sup>\<C> ` set xa)"
      thus ?thesis
        using UN_I[OF \<open>xa \<in> A\<close>, of x "set \<circ> f", folded image_comp]
        unfolding comp_apply
        unfolding morph_concat_map[of xa, symmetric]
        unfolding set_concat
        unfolding list.set_map
        by blast
    qed
  qed
  show "\<Union> (set ` f ` A) \<subseteq> \<Union> (set ` f\<^sup>\<C> ` \<Union> (set ` A))"
  proof
    fix x
    assume "x \<in> \<Union> (set ` f ` A)"
    thus "x \<in> \<Union> (set ` f\<^sup>\<C> ` \<Union> (set ` A))"
      unfolding UN_iff[of x "set \<circ> f", folded image_comp] comp_apply
      unfolding arg_cong[OF morph_concat_map, of set, unfolded set_concat list.set_map, symmetric]
      by fast
  qed
qed

lemma image_fac_interp_comp: assumes "morphism g" "p w s \<sim>\<^sub>\<I> (map f\<^sup>\<C> w_pred)" "w_pred \<noteq> \<epsilon>"
  "p' w_pred s' \<sim>\<^sub>\<I> (map g\<^sup>\<C> w_pred')" "w_pred' \<noteq> \<epsilon>"
  shows "(f p')\<cdot>p w s\<cdot>(f s') \<sim>\<^sub>\<I> (map (f o g)\<^sup>\<C> w_pred')"
  unfolding factor_interpretation_def
proof(intro conjI)
  from \<open>p w s \<sim>\<^sub>\<I> (map f\<^sup>\<C> w_pred)\<close>[unfolded factor_interpretation_def]
  have "p <p hd (map f\<^sup>\<C> w_pred)" "s <s last (map f\<^sup>\<C> w_pred)" "p \<cdot> w \<cdot> s = concat (map f\<^sup>\<C> w_pred)" by blast+

  from \<open>p' w_pred s' \<sim>\<^sub>\<I> (map g\<^sup>\<C> w_pred')\<close>[unfolded factor_interpretation_def]
  have "p' <p hd (map g\<^sup>\<C> w_pred')"  "s' <s last (map g\<^sup>\<C> w_pred')" "p' \<cdot> w_pred \<cdot> s' = concat (map g\<^sup>\<C> w_pred')" by blast+

  interpret g: morphism g
    using assms(1).
  interpret fg: morphism "(f o g)"
    by (simp add: assms(1) morph_compose morphism_axioms)

  have "f p' \<cdot> (p\<cdot>w\<cdot>s) \<cdot> f s' = concat (map (f \<circ> g)\<^sup>\<C> w_pred')"
    unfolding \<open>p \<cdot> w \<cdot> s = concat (map f\<^sup>\<C> w_pred)\<close>
    unfolding morph_concat_map
    unfolding morph[symmetric]
    unfolding \<open>p' \<cdot> w_pred \<cdot> s' = concat (map g\<^sup>\<C> w_pred')\<close>
    unfolding g.morph_concat_map fg.morph_concat_map comp_apply..
  thus "(f p' \<cdot> p) \<cdot> w \<cdot> s \<cdot> f s' = concat (map (f \<circ> g)\<^sup>\<C> w_pred')"
    unfolding lassoc.

  show "f p' \<cdot> p <p hd (map (f \<circ> g)\<^sup>\<C> w_pred')"
  proof-
    obtain ps' where "hd (map g\<^sup>\<C> w_pred') = p'\<cdot>ps'" "ps' \<noteq> \<epsilon>"
      using \<open>p' <p hd (map g\<^sup>\<C> w_pred')\<close> strict_prefixE' by blast
    from arg_cong[OF this(1), of f]
    have "hd (map (f \<circ> g)\<^sup>\<C> w_pred') = f (p' \<cdot> ps')"
      by (simp add: assms(5) core_def list.map_sel(1))

    have "hd w_pred = hd ps'"
      using \<open>p' \<cdot> w_pred \<cdot> s' = concat (map g\<^sup>\<C> w_pred')\<close> \<open>p' <p hd (map g\<^sup>\<C> w_pred')\<close>
      unfolding g.morph_concat_map
      unfolding hd_map[OF \<open>w_pred' \<noteq> \<epsilon>\<close>] core_sing g.pop_hd_nemp[OF \<open>w_pred' \<noteq> \<epsilon>\<close>]
                \<open>hd (map g\<^sup>\<C> w_pred') = p' \<cdot> ps'\<close>[unfolded hd_map[OF \<open>w_pred' \<noteq> \<epsilon>\<close>] core_def] rassoc cancel
      using hd_append2 [OF \<open>w_pred \<noteq> \<epsilon>\<close>, symmetric] hd_append2[OF \<open>ps' \<noteq> \<epsilon>\<close>, symmetric] by metis

    show ?thesis
      using \<open>p <p hd (map f\<^sup>\<C> w_pred)\<close>[unfolded  hd_map[OF \<open>w_pred \<noteq> \<epsilon>\<close>] core_sing]
      using \<open>hd (map (f \<circ> g)\<^sup>\<C> w_pred') = f (p' \<cdot> ps')\<close>[unfolded morph, folded arg_cong[OF hd_tl[OF \<open>ps' \<noteq> \<epsilon>\<close>], of f], unfolded morph lassoc]
      unfolding \<open>hd w_pred = hd ps'\<close>
      by auto
  qed

    show "s \<cdot> f s' <s last (map (f \<circ> g)\<^sup>\<C> w_pred')"
  proof-
    obtain sp' where "last (map g\<^sup>\<C> w_pred') = sp'\<cdot>s'" "sp' \<noteq> \<epsilon>"
      using \<open>s' <s last (map g\<^sup>\<C> w_pred')\<close> strict_suffixE' by blast
    from arg_cong[OF this(1), of f]
    have "last (map (f \<circ> g)\<^sup>\<C> w_pred') = f (sp' \<cdot> s')"
      by (simp add: assms(5) comp_core last_map)

    have "last w_pred = last sp'"
      using \<open>p' \<cdot> w_pred \<cdot> s' = concat (map g\<^sup>\<C> w_pred')\<close> \<open>s' <s last (map g\<^sup>\<C> w_pred')\<close>
      unfolding g.morph_concat_map
      unfolding last_map[OF \<open>w_pred' \<noteq> \<epsilon>\<close>] core_sing
      using append_butlast_last_id[OF \<open>w_pred' \<noteq> \<epsilon>\<close>]
      by (metis \<open>\<And>f. last (map f w_pred') = f (last w_pred')\<close> \<open>last (map g\<^sup>\<C> w_pred') = sp' \<cdot> s'\<close> \<open>sp' \<noteq> \<epsilon>\<close> assms(3) assms(5) rassoc core_def g.pop_last_nemp last_appendR same_suffix_suffix suffix_def triv_suf)

    show ?thesis
      using \<open>s <s last (map f\<^sup>\<C> w_pred)\<close>[unfolded  last_map[OF \<open>w_pred \<noteq> \<epsilon>\<close>] core_sing]
      using \<open>last (map (f \<circ> g)\<^sup>\<C> w_pred') = f (sp' \<cdot> s')\<close>
      append_butlast_last_id[OF \<open>sp' \<noteq> \<epsilon>\<close>]
      unfolding \<open>last w_pred = last sp'\<close>
      by (metis morph ssuf_cancel_conv ssuf_ext)
  qed
qed

(*
end *)


lemma long_enough_preimage: assumes fin: "finite (length ` range f\<^sup>\<C>)" and
  "w \<le>f f v" and "(Suc (Suc k))*\<lceil>f\<rceil> \<le> \<^bold>|w\<^bold>|" and "\<lceil>f\<rceil> \<noteq> 0"
  obtains z where "z \<le>f v" and "k \<le> \<^bold>|z\<^bold>|" and "f z \<le>f w"
proof-
  have "w \<noteq> \<epsilon>"
    using assms(3) assms(4) by force
  obtain p w_pred s where "w_pred \<le>f v" "p w s \<sim>\<^sub>\<I> (map f\<^sup>\<C> w_pred)"
    using image_fac_interp'[OF \<open>w \<le>f f v\<close> \<open>w \<noteq> \<epsilon>\<close>, of thesis] by blast
  note fac_interpD[OF \<open>p w s \<sim>\<^sub>\<I> (map f\<^sup>\<C> w_pred)\<close>, unfolded morph_concat_map]
  have "w_pred \<noteq> \<epsilon>"
    using \<open>p \<cdot> w \<cdot> s = f w_pred\<close> \<open>w \<noteq> \<epsilon>\<close> by force
  have "\<^bold>|p\<^bold>| < \<lceil>f\<rceil>"
    using \<open>p <p hd (map f\<^sup>\<C> w_pred)\<close>
      max_im_len_le_sing[OF fin, of "hd w_pred"]
    using le_trans linorder_not_le prefix_length_less
    unfolding core_def hd_map[OF \<open>w_pred \<noteq> \<epsilon>\<close>]
    by fastforce
  have "\<^bold>|s\<^bold>| < \<lceil>f\<rceil>"
    using \<open>s <s last (map f\<^sup>\<C> w_pred)\<close>
      max_im_len_le_sing[OF fin, of "last w_pred"]
    unfolding core_def last_map[OF \<open>w_pred \<noteq> \<epsilon>\<close>]
    by (meson dual_order.trans less_le_not_le suffix_length_less)
  have "(Suc (Suc k)) \<le> \<^bold>|w_pred\<^bold>|"
    using lenarg[OF \<open>p \<cdot> w \<cdot> s = f w_pred\<close>]
    unfolding lenmorph
    using le_trans[OF le_trans[OF \<open>Suc (Suc k) * \<lceil>f\<rceil> \<le> \<^bold>|w\<^bold>|\<close> le_trans[OF le_add1 le_add2]]] max_im_len_le[OF fin] mult_le_cancel2[of _ "\<lceil>f\<rceil>"] \<open>\<lceil>f\<rceil> \<noteq> 0\<close>[folded zero_less_iff_neq_zero]
    by metis
  hence "2 \<le> \<^bold>|w_pred\<^bold>|"
    by linarith
  note hd_middle_last[OF this[folded One_less_Two_le_iff]]
  let ?z = "butlast (tl w_pred)"

  from image_fac_interp_mid[OF \<open>p w s \<sim>\<^sub>\<I> (map f\<^sup>\<C> w_pred)\<close> \<open>2 \<le> \<^bold>|w_pred\<^bold>|\<close>]
  have "f ?z \<le>f w"
    using sublist_appendI[of "f (butlast (tl w_pred))"] by metis

  have "?z \<le>f v"
    by (meson \<open>w_pred \<le>f v\<close> sublist_butlast sublist_order.dual_order.trans sublist_tl)
  have "k \<le> \<^bold>|?z\<^bold>|"
    using \<open>Suc (Suc k) \<le> \<^bold>|w_pred\<^bold>|\<close> by auto

  show thesis
    using that[OF \<open>?z \<le>f v\<close> \<open>k \<le> \<^bold>|?z\<^bold>|\<close> \<open>f ?z \<le>f w\<close>]
    by blast
qed

lemmas long_enough_preimage_dom = long_enough_preimage[OF finite_imageI, OF finite_imageI]

lemma preim_len_ge: assumes "finite (length ` range f\<^sup>\<C>)" and
  "k*\<lceil>f\<rceil> < \<^bold>|f w\<^bold>|"
shows " 0 < \<lceil>f\<rceil>" and "k < \<^bold>|w\<^bold>|"
  using order.strict_trans2[OF assms(2) max_im_len_le[OF \<open>finite (length ` range f\<^sup>\<C>)\<close>, of w]] unfolding mult_less_cancel2 by blast+

lemma preim_len_geq: assumes "finite (length ` range f\<^sup>\<C>)" and "1 \<le> \<lceil>f\<rceil>" and
  "k*\<lceil>f\<rceil> \<le> \<^bold>|f w\<^bold>|"
shows "k \<le> \<^bold>|w\<^bold>|"
    using le_trans[OF \<open>k*\<lceil>f\<rceil> \<le> \<^bold>|f w\<^bold>|\<close> max_im_len_le[OF \<open>finite (length ` range f\<^sup>\<C>)\<close>, of w]]
    unfolding mult_le_cancel2
    using \<open>1 \<le> \<lceil>f\<rceil>\<close>
    by simp

lemmas preim_len_ge_dom = preim_len_ge[OF finite_imageI, OF finite_imageI] and
  preim_len_geq_dom = preim_len_geq[OF finite_imageI, OF finite_imageI]

end

subsection "Endomorphism additions"

context endomorphism
begin
lemma pow_set_union: "{(f^^k) w | k w. w \<in> U \<and> k \<le> n} = \<Union> { (f^^k) ` U | k. k \<le> n}"
  by blast

lemma pow_set_union_all: "{(f^^k) w | k w. w \<in> U} = \<Union> { (f^^k) ` U | k. True}"
  by blast

lemma pow_set_last: "\<Union> {(f ^^ k) ` U |k. k \<le> Suc n} = (\<Union> {(f ^^ k) ` U |k. k \<le> n} \<union> ((f ^^ Suc n) ` U))"
  unfolding set_eq_iff Un_iff
  unfolding Union_iff
  unfolding le_Suc_eq
  unfolding conj_disj_distribL
  by blast

lemma pow_set_finite: assumes "finite U"
  shows "finite {(f^^k) w | k w. w \<in> U \<and> k \<le> n}"
proof(induction n, simp add: assms)
  case (Suc n)
  have "finite ((f ^^ Suc n) ` U)"
    by (simp add: assms)
  then show ?case
    unfolding pow_set_union pow_set_last
    using Suc pow_set_union by auto
qed
end
subsection "Eventually stable sequences"

lemma seq_stable_coin_per: assumes
  stable: "\<And>i j k. seq i = seq j \<Longrightarrow> seq (i+k) = seq (j+k)"
  and coin: "seq l = seq (l+ Suc p)"
shows "\<And>q. seq (l+k) = seq (l+k+q*(Suc p))"
proof(induct k)
  case 0
  then show ?case
  proof(induct q, simp)
    case (Suc q)
    from stable[OF this, of "Suc p"]
    show ?case
      by (simp add: add.commute add.left_commute coin)
  qed
next
  case (Suc k)
  from stable[OF this[of q], of 1]
  show ?case
    by force
qed

lemma funpow_coin_per:  assumes "((f::'x \<Rightarrow> 'x)^^ l) x = (f ^^ (l + Suc p)) x"
  shows "(f ^^ (l + i)) x = (f^^(l+i+k*(Suc p))) x"
  using seq_stable_coin_per[of "\<lambda>i. (f^^i) x ", OF _ assms]
  unfolding funpow_add' comp_apply
  by presburger


corollary funpow_coin_im: assumes "\<And>x. x \<in> X \<Longrightarrow> ((f::'x \<Rightarrow> 'x)^^ l) x = (f ^^ (l + Suc p)) x"
  shows "(f^`UNIV`X) = (f^`{..<l+Suc p}`X)"
proof
  show "(f^`UNIV`X) \<subseteq> (f^`{..<l + Suc p}`X)"
  proof
    fix z
    assume "z \<in> (f^`UNIV`X)"
    then obtain k x where "z = (f^^k) x" "x \<in> X"
      by blast
    show "z \<in> (f^`{..<l + Suc p}`X)"
    proof(cases)
      assume "k < l + Suc p"
      thus ?thesis
        using \<open>x \<in> X\<close> \<open>z = (f ^^ k) x\<close> by blast
    next
      assume "\<not> k < l + Suc p"
      let ?m = "(k - l) mod Suc p"
      let ?d = "(k - l) div Suc p"
      have "k = l + ?m + ?d*Suc p"
        by (metis \<open>\<not> k < l + Suc p\<close> add.assoc add_diff_inverse_nat mod_div_mult_eq trans_less_add1)
      have "l + ?m < l + Suc p"
        using mod_less_divisor nat_add_left_cancel_less zero_less_Suc by presburger
      from funpow_coin_per[OF assms[OF \<open>x \<in> X\<close>], of ?m ?d]
      show ?thesis
        unfolding \<open>k = l + ?m + ?d*Suc p\<close>[symmetric] \<open>z = (f ^^ k) x\<close>
        using \<open>x \<in> X\<close>  \<open>l + ?m < l + Suc p\<close>
        unfolding image_def lessThan_iff
        unfolding Union_iff
        by (metis (mono_tags, lifting) mem_Collect_eq)
    qed
  qed
  show "(f^`{..<l + Suc p}`X) \<subseteq> (f^`UNIV`X)"
    by blast
qed

lemma seq_fin_ran_coin:
  assumes "finite (range (seq::nat \<Rightarrow> 'x))"
  obtains l p where "seq l = seq (l+ Suc p)" "n \<le> l"
proof-
  have "infinite (UNIV::nat set)"
    by simp
  from pigeonhole_infinite[OF this \<open>finite (range seq)\<close>]
  obtain a0 where "infinite {a. seq a = seq a0}"
    by force
  obtain q where
    "q \<in> {a. seq a = seq a0}"
    "n \<le> q"
    by (metis (full_types) \<open>infinite {a. seq a = seq a0}\<close> finite_nat_set_iff_bounded_le nle_le)
  then obtain q' where
    "q' \<in> {a. seq a = seq a0}"
    "q < q'"
    by (metis (full_types) \<open>infinite {a. seq a = seq a0}\<close> finite_nat_set_iff_bounded_le less_le_not_le nat_le_linear)

  obtain p where "q' = q + Suc p"
    using \<open>q < q'\<close> less_iff_Suc_add by auto
  hence "seq q = seq (q + Suc p)"
    using \<open>q \<in> {a. seq a = seq a0}\<close> \<open>q' \<in> {a. seq a = seq a0}\<close> by auto
  from that[OF this \<open>n \<le> q\<close>]
  show thesis.
qed

corollary seq_fin_ran_stable: assumes "finite (range (seq::nat \<Rightarrow> 'x))"
  and stable: "\<And>i j k. seq i = seq j \<Longrightarrow> seq (i+k) = seq (j+k)"
obtains q per_minus_one where "\<And>k n. seq (q+n) = seq (q+n+k*(Suc per_minus_one))"
proof-
  obtain q p where "seq q = seq (q + Suc p)"
    using seq_fin_ran_coin[OF \<open>finite (range seq)\<close>, of 0]
    by auto
  from seq_stable_coin_per[OF stable this] that[of q p]
  show thesis
    by blast
qed

corollary seq_fin_ran_ev_stable:
assumes "finite (range (seq::nat \<Rightarrow> 'x))" and
  eventually_stable: "\<And>i j k. n \<le> i \<Longrightarrow> n \<le> j \<Longrightarrow> seq i = seq j \<Longrightarrow> seq (i+k) = seq (j+k)"
obtains q per_minus_one where "\<And>i k. q \<le> i \<Longrightarrow> seq i = seq (i+k*(Suc per_minus_one))"
proof-
  have "finite (range (\<lambda>i. seq (n + i)))"
    by (meson assms(1) image_subset_iff rangeI rev_finite_subset)
  have "seq (n + i) = seq (n + j) \<Longrightarrow> seq (n + (i + k)) = seq (n + (j + k))" for i j k
    using eventually_stable[of "n+i" "n+j" k] unfolding add.assoc
    using le_add1 by presburger
  then obtain q p where "\<And>k t. seq (n+q+t) = seq (n+q+t+k*(Suc p))"
    using seq_fin_ran_stable[of "\<lambda>i. seq (n + i)",  OF \<open>finite (range (\<lambda>i. seq (n + i)))\<close>]
    unfolding add.assoc
    by blast
  thus thesis
    using that[of "n+q"] unfolding le_iff_add
    by fast
qed

lemma seq_stable_coin_finite:
assumes eventually_stable: "\<And>i j k. n \<le> i \<Longrightarrow> n \<le> j \<Longrightarrow> seq i = seq j \<Longrightarrow> seq (i+k) = seq (j+k)"
and coin: "n \<le> l" "seq l = seq (l+ Suc p)"
shows "finite (range seq)"
proof-
  have "range seq = seq`{0..(l+Suc p)}"
  proof
    show "range seq \<subseteq> seq ` {0..l + Suc p}"
    proof
      fix x
      assume "x \<in> range seq"
      then obtain k where "x = seq k"
        by blast
      show "x \<in> seq ` {0..l + Suc p}"
      proof(cases)
        assume "l \<le> k"

        let ?k = "((k-l) mod (Suc p))"
        let ?l = "((k-l) div (Suc p))"



        have a1: "(\<And>i j k. seq (n + i) = seq (n + j) \<Longrightarrow> seq (n + (i + k)) = seq (n + (j + k)))"
          by (metis eventually_stable group_cancel.add1 le_add1)
        have "seq (n + (l - n)) = seq (n + (l - n + Suc p))"
          using coin by auto
        from seq_stable_coin_per[of "\<lambda>i. seq (n+i)" "l-n" p ?k ?l, OF a1 this]
        have "seq (l+?k) = seq (l + (k-l))"
          by (metis add.assoc le_add_diff_inverse mod_mult_div_eq mult.commute coin(1))
        hence "seq (l+?k) = seq k"
          by (simp add: \<open>l \<le> k\<close>)

        from mod_less_divisor[of "Suc p" "k-l", OF zero_less_Suc]
        have "l+?k \<in> {0..l + Suc p}"
          by simp
        thus "x \<in> seq ` {0..l + Suc p}"
          unfolding \<open>x = seq k\<close> \<open>seq (l+?k) = seq k\<close>[symmetric]
          by blast
      next
        assume "\<not> l \<le> k"
        thus "x \<in> seq ` {0..l + Suc p}"
          by (simp add: \<open>x = seq k\<close>)
      qed
    qed
    show "seq ` {0..l + Suc p} \<subseteq> range seq"
      by blast
  qed
  thus ?thesis
    by simp
qed

lemma finite_ev_per_ev_per: assumes "finite (A::'x set)" and
    all_ev_per: "\<And> w. w \<in> A \<Longrightarrow> \<exists> q p. \<forall>(i::nat) k.  q \<le> i \<longrightarrow> (seq w) i = (seq w) (i+k*(Suc p))"
obtains q per_minus_one where "\<And>w i k. w \<in> A \<Longrightarrow> q \<le> i \<Longrightarrow> (seq w) i = (seq w) (i+k*(Suc per_minus_one))"
proof-
  let ?P_qp = "\<lambda> w qp . (\<forall>i k.  w \<in> A \<and> (fst qp) \<le> i \<longrightarrow> (seq w) i = (seq w) (i+k*(Suc (snd qp))))"
  let ?get_qp = "\<lambda> w. (SOME qp. ?P_qp w qp)"

  have "\<exists>x. ?P_qp w x" for w
    using all_ev_per prod.sel
    by metis
  from someI_ex[OF this]
  have get_qp: "?P_qp w (?get_qp w)" for w
    by blast

    let ?QP = "?get_qp ` A"

  have "finite ?QP"
    using assms(1) by blast

  let ?Q = "fst`?QP"
  have "finite ?Q"
    using \<open>finite ?QP\<close> by blast
  let ?q = "Max ?Q"

  let ?P = "Suc`snd`?QP"
  have "finite ?P"
    using \<open>finite ?QP\<close> by blast
  let ?p = "Lcm ?P"

  have "w \<in> A \<Longrightarrow> ?q \<le> i \<Longrightarrow> (seq w) i = (seq w) (i+k*(Suc (?p-1)))" for w i k
  proof-
    assume "w \<in> A" "?q \<le> i"

    have "(fst (?get_qp w)) \<in> ?Q"
      using \<open>w \<in> A\<close> by blast
    from Max.coboundedI[OF \<open>finite ?Q\<close> this] \<open>?q \<le> i\<close>
    have "(fst (?get_qp w)) \<le> i"
      using order_trans by blast
    hence eq_it: "(seq w) i = (seq w) (i+t*(Suc (snd (?get_qp w))))" for t
      using get_qp \<open>w \<in> A\<close> by blast

    have "Suc (snd (?get_qp w)) \<in> ?P"
      using \<open>w \<in> A\<close> by blast

    from dvd_Lcm_nat[OF this]
    obtain l where "?p = l*(Suc (snd (?get_qp w)))"
      unfolding dvd_def mult.commute[of "Suc (snd (?get_qp w))"]
      by blast
    have "0 < ?p"
      using Lcm_0_iff_nat \<open>finite ?P\<close> by blast
    hence "Suc (?p-1) = ?p"
      using Suc_diff_1 by blast

    show "(seq w) i = (seq w) (i+k*(Suc (?p-1)))"
      unfolding \<open>Suc (?p-1) = ?p\<close>
      unfolding \<open>?p = l*(Suc (snd (?get_qp w)))\<close>
      unfolding eq_it[of "k*l"]
      unfolding mult.assoc[of k]..
  qed

  from that[OF this]
  show thesis
    by blast
qed

corollary finite_ev_per_ev_per_UNIV: assumes "finite (UNIV::'x set)" and
  all_ev_per: "\<And>w::'x. \<exists> q p. \<forall>(i::nat) k.  q \<le> i \<longrightarrow> (seq w) i = (seq w) (i+k*(Suc p))"
obtains q per_minus_one where "\<And>w i k. q \<le> i \<Longrightarrow> (seq w) i = (seq w) (i+k*(Suc per_minus_one))"
  using finite_ev_per_ev_per[OF \<open>finite (UNIV::'x set)\<close>, of seq thesis] UNIV_I
  all_ev_per
  by force

lemma finite_stable_ev_per_all:
  assumes "finite (A::'x set)" and
    "\<And>w. w \<in> A \<Longrightarrow> finite (range ((seq w)::nat \<Rightarrow> 'z))" and
    "\<And>w. w \<in> A \<Longrightarrow> \<exists>n. \<forall>i j k. n \<le> i \<and> n \<le> j \<and> (seq w) i = (seq w) j \<longrightarrow> (seq w) (i+k) = (seq w) (j+k)"
 obtains q per_minus_one where "\<And>w i k. w \<in> A \<Longrightarrow> q \<le> i \<Longrightarrow> (seq w) i = (seq w) (i+k*(Suc per_minus_one))"
  using seq_fin_ran_ev_stable[OF assms(2), of _ _ thesis]
    finite_ev_per_ev_per[OF assms(1), of seq] assms(3)
  by metis


subsection "Strictly growing sequences"

lemma sgrowing_fit: assumes "\<And>j. s j < s (Suc j)" "s 0 < (n::nat)"
  obtains k where "s k < n" "n \<le> s (Suc k)"
  using assms
proof(induct n, blast)
  case (Suc n)
  then show ?case
    using less_antisym[of _ n] nle_le
    unfolding less_eq_Suc_le not_less_eq_eq[symmetric] not_not by metis
qed

subsection "The find"

definition the_find :: "('a \<Rightarrow> bool) \<Rightarrow> 'a list \<Rightarrow> 'a"
  where "the_find P w = the (find P w)"

lemma the_find_hd_dropWhile: "the_find P w = hd (dropWhile (Not o P) w)"
  unfolding the_find_def find_dropWhile hd_def option.the_def
  by (simp add: list.case_eq_if)

lemma the_find_hd_dropWhile': "the_find (Not o P) w = hd (dropWhile P w)"
  using the_find_hd_dropWhile[of "Not o P" for P]
  unfolding comp_def
  by auto

definition the_find_rev :: "('a \<Rightarrow> bool) \<Rightarrow> 'a list \<Rightarrow> 'a"
  where "the_find_rev P w = the_find P (rev w)"

lemma the_find_the_find_rev [reversal_rule]: "the_find P (rev w) = the_find_rev P w"
  unfolding the_find_rev_def..

lemma the_find_rev_the_find [reversal_rule]: "the_find_rev P (rev w) = the_find P w"
  unfolding the_find_rev_def rev_rev_ident..

lemma the_find_takeWhile: "\<not> list_all P w \<Longrightarrow> w = (takeWhile P w) \<cdot> ((the_find (Not o P) w) # tl (dropWhile P w))"
  unfolding the_find_hd_dropWhile'
  by (simp add: Ball_set the_find_def)

lemma the_find_ex: assumes "\<not> list_all P w"
  obtains u v where "w = u\<cdot>[the_find (Not o P) w]\<cdot>v" "\<not> P (the_find (Not o P) w)" "list_all P u"
  using the_find_takeWhile[OF assms] hd_dropWhile
  unfolding the_find_hd_dropWhile'
  unfolding hd_word[of "hd (dropWhile P w)" _, symmetric] the_find_def
  using Ball_set assms dropWhile_eq_Nil_conv set_takeWhileD by metis

lemma (in endomorphism) the_find_conv:
  assumes "\<And>w. list_all P w = list_all P (f w)" "\<not> list_all P w"
  shows "the_find (Not o P) (f [the_find (Not o P) w]) = the_find (Not o P) (f w)"
  using assms
  unfolding the_find_hd_dropWhile'
proof(induction w)
  case Nil
  then show ?case
    by auto
next
  case (Cons a w)
  show ?case
  proof(cases)
    assume "P a"
    hence "the_find (Not o P) (a # w) = the_find (Not o P) (w)"
      by (simp add: the_find_def)
    have "list_all P (f [a])"
      using \<open>P a\<close> assms(1) by auto
    hence "the_find (Not o P) (f (a # w)) = the_find (Not o P) (f w)"
      unfolding hd_word[of a w] morph the_find_hd_dropWhile'
      using Ball_set dropWhile_append2 by metis
    thus ?case
      using Cons.IH Cons.prems(1) Cons.prems(2) \<open>P a\<close> \<open>the_find (Not o P) (a # w) = the_find (Not o P) w\<close>
      by (simp add: the_find_hd_dropWhile')
  next
    assume "\<not> P a"
    hence "the_find (Not o P) (a # w) = a"
      unfolding the_find_def dropWhile.simps
      by simp
    have "\<not> list_all P (f [a])"
      using Cons.prems(1) \<open>\<not> P a\<close> by auto
    hence "the_find (Not o P) (f (a # w)) = the_find (Not o P) (f [a])"
      unfolding hd_word[of a w] morph
      unfolding the_find_hd_dropWhile'
      using dropWhile_eq_Cons_conv list.distinct(1) prefI pref_hd_eq prefix_dropWhile the_find_ex the_find_takeWhile by metis
    thus ?case
      using \<open>the_find (Not o P) (a # w) = a\<close> unfolding the_find_hd_dropWhile'
      by argo
  qed
qed

lemmas the_find_rev_takeWhile = the_find_takeWhile[reversed] and
       the_find_rev_ex = the_find_ex[reversed]







section "Languages"

lemma "finite A \<Longrightarrow> card {w \<in> lists A. \<^bold>|w\<^bold>| = k} = (card A)^k"
  using card_lists_length_eq[unfolded in_lists_conv_set_subset].

lemma "finite (UNIV::'a set) \<Longrightarrow> card {w::'a list. \<^bold>|w\<^bold>| = k} = (card (UNIV::'a set))^k"
  using card_lists_length_eq by fastforce


lemma maxlen_finite: assumes "finite A" "M \<subseteq> lists A" "\<exists>n. \<forall>s\<in>M. \<^bold>|s\<^bold>| \<le> n"
  shows "finite M"
proof-
  obtain n where "\<forall>s \<in> M. \<^bold>|s\<^bold>| \<le> n"
    using assms(3) by blast
  hence "M \<subseteq> {xs. set xs \<subseteq> A \<and> \<^bold>|xs\<^bold>| \<le> n}"
    using \<open>M \<subseteq> lists A\<close> by blast
  thus ?thesis
    using finite_lists_length_le[OF \<open>finite A\<close>]
    by (simp add: finite_subset)
qed

lemma len_lan_infinite: assumes "(\<forall> n. \<exists>w. w \<in> L \<and> n < \<^bold>|w\<^bold>|)" shows "\<not> finite L"
  by (meson assms finite_maxlen not_less_iff_gr_or_eq)


subsection "Languages closed under taking factors"

definition factorial :: "'a list set \<Rightarrow> bool"
  where "factorial L \<equiv> (\<forall>w f. w \<in> L \<and> f \<le>f w \<longrightarrow> f \<in> L)"

lemma factorialD: assumes "factorial L" "w \<in> L" "f \<le>f w"
  shows "f \<in> L"
  using assms
  unfolding factorial_def
  by blast

lemma factorialI[intro]: assumes "\<And>w f. w \<in> L \<Longrightarrow> f \<le>f w \<Longrightarrow> f \<in> L"
  shows "factorial L"
  using assms
  unfolding factorial_def
  by blast

lemma factorial_emp: "factorial L \<Longrightarrow> w \<in> L \<Longrightarrow> \<epsilon> \<in> L"
  unfolding factorial_def by blast

subsection "Subset of language with elements of fixed length"

definition length_subset:: "'a list set \<Rightarrow> nat \<Rightarrow> 'a list set" ("(_)\<^bsub>_\<^esub>" [51,51] 60)
  where "length_subset L n \<equiv> {v \<in> L. \<^bold>|v\<^bold>| = n}"

lemma mem_length_subset:
  "v \<in> L\<^bsub>n\<^esub> \<equiv> (v \<in> L \<and> \<^bold>|v\<^bold>| = n)"
  unfolding length_subset_def
  by auto

lemma length_subset_int: "L\<^bsub>n\<^esub> = L \<inter> {v. \<^bold>|v\<^bold>| = n}"
  unfolding length_subset_def
  by auto

lemma length_subset_sub: "L\<^bsub>n\<^esub> \<subseteq> L"
  by (simp add: mem_length_subset subset_eq)

lemma length_subset_Un: "L = \<Union> {L\<^bsub>n\<^esub> | n. True}"
  unfolding set_eq_iff Union_iff
  unfolding Bex_def
  unfolding mem_Collect_eq
  using mem_length_subset by blast

lemma length_subset_fin: assumes finite_UNIV: "finite (UNIV::'a set)"
  shows "finite ((L::'a list set)\<^bsub>n\<^esub>)"
  by (metis bot_nat_0.extremum iso_tuple_UNIV_I le_add_same_cancel2 lists_UNIV local.finite_UNIV maxlen_finite mem_length_subset subset_eq)

lemma length_subset_0: "L\<^bsub>0\<^esub> \<subseteq> {\<epsilon>}"
  by (metis length_0_conv mem_length_subset singleton_iff subsetI)

subsection "Factors of a (finite) word"

definition factors
  where "factors w = set (sublists w)"

lemma mem_factors:
  "v \<in> factors w \<equiv> v \<le>f w"
  unfolding factors_def
  by simp
lemma factors_eq:
  "factors w = {v. v \<le>f w}"
  unfolding factors_def
  using set_sublists_eq.

lemma prefixes_factors: "set (prefixes w) \<subseteq> factors w"
  unfolding factors_def
  subset_iff
  by simp

subsection "Recurrent, with bounded ocurrences  and uniformly recurrent language"

definition recurrent :: "'a list set \<Rightarrow> bool"
  where "recurrent L \<equiv> factorial L \<and> (\<forall>u v. (u \<in> L \<and> v \<in> L) \<longrightarrow> (\<exists>w. w \<noteq> \<epsilon> \<and> u\<cdot>w\<cdot>v \<in> L))"

lemma recE: assumes "recurrent L" "u \<in> L" "v \<in> L"
  obtains w where "u\<cdot>w\<cdot>v \<in> L" "w \<noteq> \<epsilon>" "w \<in> L"
  using assms unfolding recurrent_def fac_def factorial_def
  by metis

lemma recI[intro]: assumes "factorial L" "\<And>u v. u \<in> L \<Longrightarrow> v \<in> L \<Longrightarrow> \<exists>w. w \<noteq> \<epsilon> \<and> u\<cdot>w\<cdot>v \<in> L"
  shows "recurrent L"
  by (simp add: assms(1) assms(2) recurrent_def)

lemma rec_infin: "recurrent L \<Longrightarrow> w \<in> L \<Longrightarrow>  infinite L"
  by (metis (no_types, opaque_lifting) factorial_def empty_iff recurrent_def self_append_conv2 sublist_code(1) suffix_def suffix_order.finite_has_maximal)

text\<open>A uniformly recurrent language is a recurrent language where all elements occur with bounded gaps. We shall need the latter predicate separately.\<close>


definition has_uniformly_bounded_occurrences
  where "has_uniformly_bounded_occurrences L \<equiv> (\<forall>v \<in> L. \<exists>k. \<forall>w \<in> L. \<^bold>|w\<^bold>| \<ge> k \<longrightarrow> v \<le>f w)"

lemma bounded_occE: assumes "has_uniformly_bounded_occurrences L" "v \<in> L"
  obtains k where "\<And>w. w \<in> L \<Longrightarrow> \<^bold>|w\<^bold>| \<ge> k \<Longrightarrow> v \<le>f w"
  by (meson assms(1) assms(2) has_uniformly_bounded_occurrences_def)

lemma bounded_occI[intro]: assumes "\<And>v. v \<in> L \<Longrightarrow> (\<exists>k. \<forall>w \<in> L. \<^bold>|w\<^bold>| \<ge> k \<longrightarrow> v \<le>f w)"
  shows "has_uniformly_bounded_occurrences L"
  by (simp add: assms has_uniformly_bounded_occurrences_def)

definition uniformly_recurrent :: "'a list set \<Rightarrow> bool"
  where "uniformly_recurrent L \<equiv> recurrent L \<and> has_uniformly_bounded_occurrences L"

lemma un_recE: assumes "uniformly_recurrent L" "v \<in> L"
  obtains k where "\<And>w. w \<in> L \<Longrightarrow> \<^bold>|w\<^bold>| \<ge> k \<Longrightarrow> v \<le>f w"
  using assms
  by (meson bounded_occE uniformly_recurrent_def)

lemma un_recI[intro]: assumes "recurrent L" "\<And>v. v \<in> L \<Longrightarrow> (\<exists>k. \<forall>w \<in> L. \<^bold>|w\<^bold>| \<ge> k \<longrightarrow> v \<le>f w)"
  shows "uniformly_recurrent L"
  by (simp add: assms(1) assms(2) has_uniformly_bounded_occurrences_def uniformly_recurrent_def)

lemma un_recD: assumes "uniformly_recurrent L"
  shows "recurrent L" "factorial L"
  using assms recurrent_def
  unfolding uniformly_recurrent_def by blast+

lemma un_rec_eq: assumes finite_UNIV: "finite (UNIV::'a set)"
  shows "uniformly_recurrent (L::'a list set) \<equiv> recurrent L \<and> (\<forall>n. \<exists>k. \<forall>w \<in> L. \<^bold>|w\<^bold>| \<ge> k \<longrightarrow> L\<^bsub>n\<^esub> \<subseteq> factors w)"
proof-
  have "(\<forall>v\<in>L. \<exists>k. \<forall>w\<in>L. k \<le> \<^bold>|w\<^bold>| \<longrightarrow> v \<le>f w) \<longleftrightarrow> (\<forall>n. \<exists>k. \<forall>w \<in> L. \<^bold>|w\<^bold>| \<ge> k \<longrightarrow> L\<^bsub>n\<^esub> \<subseteq> factors w)"
  proof
    assume as: "\<forall>v\<in>L. \<exists>k. \<forall>w\<in>L. k \<le> \<^bold>|w\<^bold>| \<longrightarrow> v \<le>f w"
    define obt where "obt = (\<lambda>v. (SOME k. (\<forall>w\<in>L. k \<le> \<^bold>|w\<^bold>| \<longrightarrow> v \<le>f w)))"
    have single: "v \<le>f w" if "v \<in> L" "w \<in> L" "obt v \<le> \<^bold>|w\<^bold>|" for v w
    proof-
      obtain k where "obt v = k"
        by simp
      hence  "(\<And>w. w \<in> L \<Longrightarrow> k \<le> \<^bold>|w\<^bold>| \<Longrightarrow> v \<le>f w)"
        unfolding obt_def
        using as[rule_format, OF \<open>v \<in> L\<close>, THEN someI_ex]
        by fast
      from this[OF \<open>w \<in> L\<close> \<open>obt v \<le> \<^bold>|w\<^bold>|\<close>[unfolded \<open>obt v = k\<close>]]
      show ?thesis.
    qed

    show "(\<forall>n. \<exists>k. \<forall>w \<in> L. \<^bold>|w\<^bold>| \<ge> k \<longrightarrow> L\<^bsub>n\<^esub> \<subseteq> factors w)"
    proof
      fix n
      note length_subset_fin[OF finite_UNIV, of L n]

      let ?n = "Max (obt`(L\<^bsub>n\<^esub>))"
      have "t \<in> L\<^bsub>n\<^esub> \<Longrightarrow> obt t \<le> ?n" for t
        by (simp add: \<open>finite (L\<^bsub>n\<^esub>)\<close>)
      hence "w \<in> L \<Longrightarrow> Max (obt ` (L\<^bsub>n\<^esub>)) \<le> \<^bold>|w\<^bold>| \<Longrightarrow> t \<in> L\<^bsub>n\<^esub> \<Longrightarrow> t \<le>f w" for w t
        unfolding subset_iff mem_factors mem_length_subset
        unfolding length_subset_def
        using single[of t w]
        using order.trans by blast
      hence "w \<in> L \<Longrightarrow> \<^bold>|w\<^bold>| \<ge> ?n \<Longrightarrow> L\<^bsub>n\<^esub> \<subseteq> factors w" for w
        using mem_factors by blast
      thus "\<exists>k. \<forall>w\<in>L. k \<le> \<^bold>|w\<^bold>| \<longrightarrow> L\<^bsub>n\<^esub> \<subseteq> factors w"
        by blast
    qed
  next
    show "\<forall>n. \<exists>k. \<forall>w\<in>L. k \<le> \<^bold>|w\<^bold>| \<longrightarrow> L\<^bsub>n\<^esub> \<subseteq> factors w \<Longrightarrow> \<forall>v\<in>L. \<exists>k. \<forall>w\<in>L. k \<le> \<^bold>|w\<^bold>| \<longrightarrow> v \<le>f w"
      by (meson mem_factors mem_length_subset subsetD)
  qed

  thus "uniformly_recurrent L \<equiv> recurrent L \<and> (\<forall>n. \<exists>k. \<forall>w \<in> L. \<^bold>|w\<^bold>| \<ge> k \<longrightarrow> L\<^bsub>n\<^esub> \<subseteq> factors w)"
    by (simp add: has_uniformly_bounded_occurrences_def uniformly_recurrent_def)
qed


lemma un_recE': assumes "uniformly_recurrent (L::'a list set)" and finite_UNIV: "finite (UNIV::'a set)"
  obtains k where "\<And>w. w \<in> L \<Longrightarrow> \<^bold>|w\<^bold>| \<ge> k \<Longrightarrow> L\<^bsub>n\<^esub> \<subseteq> factors w"
  using assms(1)
  unfolding un_rec_eq[OF finite_UNIV]
  by meson

lemma un_rec_infin: assumes "uniformly_recurrent L" "w \<in> L"
  shows "infinite L"
  using assms(1) assms(2) rec_infin un_recD(1) by blast

subsection "Exponent of an element of a language"

text\<open>Given a language, we are interested of its elements of the form @{term "w\<^sup>@k"}. (In full generality, for rational k. Here we consider only integer k.) The maximum such k is called the exponent of @{term w}.\<close>

definition integer_exponent   where "integer_exponent L w \<equiv> (GREATEST k. w\<^sup>@k \<in> L)"

subsection "Unbounded exponent"

text\<open>If the exponent of a word is unbounded, we also speak about an infinite repetition. Considering roots of such infinite repetitions, it is natural to consider only primitive words. As we do not formalize the idea of infinite repetition here, we define these primitive roots directly and refer to them as primitive words/elements of unbounded exponent (with respect to a language L).\<close>

definition primitive_unbounded_exponent
  where "primitive_unbounded_exponent L w \<equiv> primitive w \<and> (\<forall>n. w\<^sup>@n \<in> L)"

lemma prim_unb_expI[intro]: "primitive w \<Longrightarrow> (\<And>n. w\<^sup>@n \<in> L) \<Longrightarrow> primitive_unbounded_exponent L w"
  by (simp add: primitive_unbounded_exponent_def)

lemma prim_unb_expE: assumes "primitive_unbounded_exponent L w"
  shows "primitive w" "w\<^sup>@n \<in> L"
  using assms
  unfolding primitive_unbounded_exponent_def by meson+

lemma prim_unb_exp_len: assumes "primitive_unbounded_exponent L w"
  obtains k where "w\<^sup>@k \<in> L" "n \<le> k"
  by (meson assms primitive_unbounded_exponent_def order_eq_refl)

lemma prim_unb_exp_infinite: assumes "primitive_unbounded_exponent L w"
  shows "infinite L"
  using finite_maxlen prim_unb_expE(2)[OF assms] leD[OF long_pow[OF prim_nemp[OF prim_unb_expE(1)[OF assms]]]]
  by fast

lemma prim_unb_exp_conj: "factorial L \<Longrightarrow> primitive_unbounded_exponent L w \<Longrightarrow> w \<sim> w' \<Longrightarrow> primitive_unbounded_exponent L w'"
  by (meson fac_pow_conjug factorial_def primitive_unbounded_exponent_def prim_conjug sublist_order.eq_refl)


subsection "Repetitive and strongly repetitive language"
definition repetitive  :: "'a list set \<Rightarrow> bool"
  where "repetitive L \<equiv> (\<forall>n. \<exists>w \<in> L. w \<noteq> \<epsilon> \<and> w\<^sup>@n \<in> L)"

definition strongly_repetitive  :: "'a list set \<Rightarrow> bool"
  where "strongly_repetitive L \<equiv> (\<exists>w \<in> L. w \<noteq> \<epsilon> \<and> (\<forall>n. w\<^sup>@n \<in> L))"

subsection "Factor closure"

inductive_set factor_closure:: "'a list set \<Rightarrow> 'a list set" for L
  where "w \<in> L \<Longrightarrow> v \<le>f w \<Longrightarrow> v \<in> factor_closure L"

lemma fac_clo_set: "factor_closure L = {v . \<exists>w. v \<le>f w \<and> w \<in> L}"
  unfolding factor_closure_def factor_closurep.simps
  by blast

lemma fac_clo_sub_id: "L \<subseteq> factor_closure L"
  using factor_closure.simps by auto

lemma fac_clo_sing: "factor_closure {w} = {v. v \<le>f w}"
  unfolding fac_clo_set by auto

lemma fac_clo_insert: "factor_closure (insert w L) = (factor_closure L \<union> factor_closure {w})"
proof-
  have "{v. v \<le>f w} = {v . \<exists>w'. v \<le>f w \<and> w = w'}"
    by presburger

  have "x \<in> factor_closure L \<union> {v. v \<le>f w} \<longleftrightarrow>
    x \<in> {v . \<exists>w'. v \<le>f w' \<and> (w' = w \<or> w' \<in> L)}" for x
    unfolding factor_closure.simps \<open>{v. v \<le>f w} = {v . \<exists>w'. v \<le>f w \<and> w = w'}\<close> fac_clo_set
    by blast
  hence "factor_closure L \<union> {v. v \<le>f w} =
    {v . \<exists>w'. v \<le>f w' \<and> (w' = w \<or> w' \<in> L)}"
    by auto
  thus ?thesis
    unfolding insert_compr factor_closure.simps fac_clo_sing fac_clo_set
    by blast
qed

lemma fac_clo_emp: "factor_closure {\<epsilon>} = {\<epsilon>}"
  by (simp add: fac_clo_sing)

lemma fac_clo_emp': "factor_closure {} = {}"
  by (simp add: fac_clo_set)


lemma fac_cloI_fac: "v \<le>f w \<Longrightarrow> w \<in> factor_closure L \<Longrightarrow> v \<in> factor_closure L"
  by (meson fac_trans factor_closure.simps)

lemma fac_clo_sing_sublists:  "factor_closure {w} = set (sublists w)"
  by (simp add: fac_clo_sing set_sublists_eq)

lemma fac_clo_sub: "L \<subseteq> factor_closure L"
  unfolding fac_clo_set
  by blast

lemma fac_clo_finite: "finite L \<longleftrightarrow> finite (factor_closure L)"
proof
  assume "finite L"
  then show "finite (factor_closure L)"
  proof(induct L, simp add: fac_clo_emp')
    case (insert x F)
    then show ?case
      unfolding fac_clo_insert[of x F] fac_clo_sing_sublists
      by blast
  qed
next
  show "finite (factor_closure L) \<Longrightarrow> finite L"
    by (meson fac_clo_sub infinite_super)
qed

lemma (in morphism) fac_clo_im:  "factor_closure (f ` factor_closure A) = factor_closure (f ` A)"
proof
  show "factor_closure (f ` factor_closure A) \<subseteq> factor_closure (f ` A)"
  proof
    fix x
    assume "x \<in> factor_closure (f ` factor_closure A)"
    then obtain q where "q \<in> factor_closure A" "x \<le>f f q"
      using factor_closure.cases by force
    then obtain a where "a \<in> A" "q \<le>f a"
      using factor_closure.cases by auto
    hence "x \<le>f f a"
      by (meson \<open>x \<le>f f q\<close> fac_mono fac_trans)
    thus "x \<in> factor_closure (f ` A)"
      by (meson \<open>a \<in> A\<close> factor_closure.simps image_eqI)
  qed
  show "factor_closure (f ` A) \<subseteq> factor_closure (f ` factor_closure A)"
    by (meson fac_clo_sub factor_closure.simps image_mono subset_iff)
qed

lemma fac_clo_Un: "factor_closure (A \<union> B) = (factor_closure A) \<union> (factor_closure B)"
  unfolding fac_clo_set
  unfolding set_eq_iff Un_iff
  by blast

lemma un_set_fac_clo: "\<Union> (set ` (factor_closure A)) = \<Union> (set ` A)"
  unfolding set_eq_iff
  unfolding Union_iff Bex_def fac_clo_set
  using set_mono_sublist
  by blast



subsection "Growing letters and strictly growing sequences of words"

definition growing_seq :: "(nat \<Rightarrow> 'a list) \<Rightarrow> bool"
  where "growing_seq wj \<equiv> (\<forall>n0. \<exists>j. \<^bold>|wj j\<^bold>| > n0)"

lemma growing_seqI[intro]: assumes "\<And>n0. \<exists>j. \<^bold>|wj j\<^bold>| > n0"
  shows "growing_seq wj"
  by (simp add: assms growing_seq_def)

lemma growin_seq_lan_infin: assumes "growing_seq wj" "\<And>j. wj j \<in> L"
  shows "infinite L"
  using len_lan_infinite assms
  unfolding growing_seq_def
  by metis

definition strictly_growing_seq :: "(nat \<Rightarrow> 'a list) \<Rightarrow> bool"
  where "strictly_growing_seq wj \<equiv> (\<forall>j. \<^bold>|wj j\<^bold>| < \<^bold>|wj (Suc j)\<^bold>|)"

lemma sgrowing_len: "strictly_growing_seq wj \<Longrightarrow> \<^bold>|wj j\<^bold>| \<ge> j"
proof(induct j, simp)
  case (Suc j)
  then show ?case
    by (metis dual_order.strict_trans2 less_le_not_le not_less_eq_eq strictly_growing_seq_def)
qed

lemma sgrowin_len': assumes "strictly_growing_seq wj"
  obtains j where "\<^bold>|wj j\<^bold>| > n"
  using Suc_le_eq assms sgrowing_len by blast

lemma sgrowingI[intro]: assumes "\<And>j. \<^bold>|wj j\<^bold>| < \<^bold>|wj (Suc j)\<^bold>|"
  shows "strictly_growing_seq wj"
  using assms strictly_growing_seq_def by auto

lemma sgrowing_range_infin: "strictly_growing_seq wj \<Longrightarrow> infinite (range wj)"
  by (metis len_lan_infinite rangeI sgrowin_len')

lemma sgrowing_comp: "morphism f \<Longrightarrow> strictly_growing_seq (f o wj) \<Longrightarrow> strictly_growing_seq wj"
  unfolding strictly_growing_seq_def oops

definition growing :: "('a list \<Rightarrow> 'a list) \<Rightarrow> 'a list \<Rightarrow> bool"
  where "growing f w \<equiv> (\<forall>n. \<exists>k. \<^bold>|(f^^k) w\<^bold>| \<ge> n)"

lemma growingI[intro]: assumes "\<And>n. \<exists>k. \<^bold>|(f^^k) w\<^bold>| \<ge> n"
  shows "growing f w"
  unfolding growing_def
  using assms by blast

lemma sgrowing_growing: "strictly_growing_seq (\<lambda>j. (f^^j) w) \<Longrightarrow> growing f w"
  using sgrowing_len
  by blast

lemma growingE: assumes "growing f w"
  shows "\<exists>k. \<^bold>|(f^^k) w\<^bold>| \<ge> n"
  using assms growing_def by auto

lemma growingE': assumes "growing f w"
  obtains k where "\<^bold>|(f^^k) w\<^bold>| \<ge> n"
  using growingE[OF assms] by blast

context endomorphism
begin

lemma growing_emp: "\<not> growing f \<epsilon>"
  using Suc_n_not_le_n growing_def
  by (metis pow_emp_to_emp)

lemma growing_append: "growing f (v\<cdot>w) \<longleftrightarrow> growing f v \<or> growing f w"
  unfolding growing_def lenmorph pow_morph
proof
  show "\<forall>n. \<exists>k. n \<le> \<^bold>|(f ^^ k) v\<^bold>| + \<^bold>|(f ^^ k) w\<^bold>| \<Longrightarrow>
    (\<forall>n. \<exists>k. n \<le> \<^bold>|(f ^^ k) v\<^bold>|) \<or> (\<forall>n. \<exists>k. n \<le> \<^bold>|(f ^^ k) w\<^bold>|)"
  proof(rule ccontr)
    assume a1: "\<forall>n. \<exists>k. n \<le> \<^bold>|(f ^^ k) v\<^bold>| + \<^bold>|(f ^^ k) w\<^bold>|" and
      a2: "\<not> ((\<forall>n. \<exists>k. n \<le> \<^bold>|(f ^^ k) v\<^bold>|) \<or> (\<forall>n. \<exists>k. n \<le> \<^bold>|(f ^^ k) w\<^bold>|))"

    obtain N1 where "N1 > \<^bold>|(f ^^ k) v\<^bold>|" for k
      by (meson a2 linorder_not_le)
    obtain N2 where "N2 > \<^bold>|(f ^^ k) w\<^bold>|" for k
      by (meson a2 linorder_not_le)
    have "N1 + N2 > \<^bold>|(f ^^ k) v\<^bold>| + \<^bold>|(f ^^ k) w\<^bold>|" for k
      by (simp add: \<open>\<And>k. \<^bold>|(f ^^ k) v\<^bold>| < N1\<close> \<open>\<And>k. \<^bold>|(f ^^ k) w\<^bold>| < N2\<close> add_less_mono order_less_imp_le)
    thus False
      using a1[rule_format, of "N1+N2"] linorder_not_less by blast
  qed
  show "(\<forall>n. \<exists>k. n \<le> \<^bold>|(f ^^ k) v\<^bold>|) \<or> (\<forall>n. \<exists>k. n \<le> \<^bold>|(f ^^ k) w\<^bold>|) \<Longrightarrow>
    \<forall>n. \<exists>k. n \<le> \<^bold>|(f ^^ k) v\<^bold>| + \<^bold>|(f ^^ k) w\<^bold>|"
    using trans_le_add1 trans_le_add2 by blast
qed

lemma growing_sing: "growing f w \<longleftrightarrow> (\<exists>x \<in> set w. growing f [x])"
proof(induct w)
  case Nil
  then show ?case
    using growing_emp by fastforce
next
  case (Cons a w)
  then show ?case
    unfolding hd_word[of a w]
    unfolding growing_append
    by auto
qed

lemma growing_seq_infin: assumes "growing f w"
  shows "infinite {(f^^k) w | k . True }"
proof
  assume "finite {(f ^^ k) w |k. True}"
  from finite_maxlen[OF this]
  obtain n where "s \<in>{(f ^^ k) w |k. True} \<Longrightarrow> \<^bold>|s\<^bold>| < n" for s
    by blast
  thus False
    using growingE[OF \<open>growing f w\<close>, of n] linorder_not_le by blast
qed

lemma seq_infin_growing: assumes "finite(UNIV::'a set)" "infinite {(f^^k) w | k . True }"
  shows "growing f w"
  using maxlen_finite[OF \<open>finite (UNIV::'a set)\<close>, unfolded lists_UNIV, OF subset_UNIV,
      of "{(f^^k) w | k . True }"] \<open>infinite {(f^^k) w | k . True }\<close>
  unfolding growing_def
  using nle_le by auto

lemma growing_maxlen: assumes "finite (UNIV::'a set)" "\<exists>w. growing f w" shows "\<lceil>f\<rceil> > 1"
proof(rule ccontr)
  obtain x where "growing f [x]"
    using assms growing_sing
    by blast

  assume "\<not> 1 < \<lceil>f\<rceil>"

  have "\<lceil>f\<rceil> \<le> 1"
    using \<open>\<not> 1 < \<lceil>f\<rceil>\<close> leI by blast
  hence "\<lceil>f\<rceil> ^ k \<le> 1" for k
    using power_le_one by blast
  hence "\<^bold>|(f^^k) [x]\<^bold>| \<le> 1" for k
    using max_im_len_pow_le'_dom[OF \<open>finite (UNIV::'a set)\<close>, of k "[x]"] le_trans
    unfolding sing_len nat_mult_1
    by blast
  thus False
    using growingE[OF \<open>growing f [x]\<close>] not_less_eq_eq by blast
qed

lemma pow_seq_funpow_image: "{(f^^l) axiom | l. True} = (f^`UNIV`{axiom})"
  unfolding funpow_image_sing
  by simp

end
section "Purely morphic language"

text\<open>The following concept stems from the languages of D0L-systems (specific cases of an L-system introduced in \cite{ LINDENMAYER1968280}. See also \cite{ "book-of-L"}.\<close>

inductive_set purely_morphic_language :: "('a list \<Rightarrow> 'a list) \<Rightarrow> 'a list \<Rightarrow> 'a list set"
  for f axiom where
    "axiom \<in> purely_morphic_language f axiom"
  | "w \<in> purely_morphic_language f axiom \<Longrightarrow> f w \<in> purely_morphic_language f axiom"
  | "w \<in> purely_morphic_language f axiom \<Longrightarrow> u \<le>f w \<Longrightarrow> u \<in> purely_morphic_language f axiom"

lemma pmor_lan_fac: "factorial (purely_morphic_language f axiom)"
  using purely_morphic_language.intros(3)
  by blast


context endomorphism
begin

abbreviation pmor_lan ("\<L> _ " [100])
  where "pmor_lan \<equiv> purely_morphic_language f"

lemma pmor_lan_emp: "x \<in> \<L> \<epsilon> \<Longrightarrow> x = \<epsilon>" for x
  by(induct rule: purely_morphic_language.induct, simp_all)

lemma pmor_lan_emp':  "\<L> \<epsilon> = {\<epsilon>}"
  using pmor_lan_emp purely_morphic_language.intros(1) by fastforce

lemma pmor_lan_pow_in: "(f^^k) axiom \<in> \<L> axiom"
proof(induct k)
  case 0
  then show ?case
    by (simp add: purely_morphic_language.intros(1) power.power.power_0)
next
  case (Suc k)
  from purely_morphic_language.intros(2)[OF this]
  show ?case
    unfolding compow_Suc.
qed

lemma pmor_lan_seq_in: "{(f^^l) axiom | l. True} \<subseteq> \<L> axiom"
  using pmor_lan_pow_in by force

lemma pmor_lan_fac_pow: "w \<in> \<L> axiom \<longleftrightarrow> (\<exists>k. w \<le>f (f^^k) axiom)"
proof
  show " w \<in> \<L> axiom \<Longrightarrow> \<exists>k. w \<le>f (f^^k) axiom"
  proof(induct rule: purely_morphic_language.induct)
    case 1
    then show ?case
      by (metis funpow_0 sublist_order.order_refl)
  next
    case (2 w)
    obtain k where "w \<le>f (f^^k) axiom"
      using "2.hyps"(2)
      by blast
    from fac_mono[OF this]
    show ?case
      unfolding compow_Suc[symmetric]
      using "2"(2)
      by blast
  next
    case (3 w u)
    then show ?case
      by (meson sublist_order.dual_order.trans)
  qed
  show "\<exists>k. w \<le>f (f^^k) axiom \<Longrightarrow> w \<in> \<L> axiom"
    using pmor_lan_pow_in purely_morphic_language.intros(3) by blast
qed

lemma pmor_lan_iff: "w \<in> \<L> axiom \<longleftrightarrow> (w \<in> \<L> (f axiom) \<or> w \<le>f axiom)"
proof
  show "w \<in> \<L> axiom \<Longrightarrow> w \<in> \<L> (f axiom) \<or> w \<le>f axiom"
  proof(induct rule: purely_morphic_language.induct, simp)
    case (2 w)
    then show ?case
      by (meson fac_mono purely_morphic_language.simps)
  next
    case (3 w u)
    then show ?case
      by (meson fac_trans purely_morphic_language.intros(3))
  qed
  show " w \<in> \<L> (f axiom) \<or> w \<le>f axiom \<Longrightarrow> w \<in> \<L> axiom"
    using funpow_0[of f axiom]
    unfolding pmor_lan_fac_pow
    unfolding compow_Suc'[symmetric]
    by metis
qed

lemma pmor_lan_fac_clo_seq: "\<L> axiom = factor_closure {(f^^k) axiom | k. True}"
  unfolding set_eq_iff
  using factor_closure.simps pmor_lan_fac_pow by auto

lemma pmor_lan_fac_sub: assumes "axiom' \<le>f axiom"
  shows "\<L> axiom' \<subseteq> \<L> axiom"
proof
  fix x
  assume "x \<in> \<L> axiom'"
  thus "x \<in> \<L> axiom"
    unfolding pmor_lan_fac_pow
    using assms morphism.fac_mono pow_endomorphism sublist_order.dual_order.trans endomorphism.axioms by metis
qed

lemma pmor_lan_same_pow: assumes "k < k'" "(f^^k) axiom = (f^^k') axiom"
  shows "\<L> axiom = factor_closure {(f^^l) axiom | l. l < k'}"
proof-
  obtain p where "Suc p = k' - k"
    by (metis Suc_diff_Suc assms(1))
  hence "(f^^k) axiom = (f^^(k+Suc p)) axiom"
    by (metis assms(1) assms(2) le_add_diff_inverse less_le_not_le)
  have "(f^`UNIV`{axiom}) = (f^`{..<k'}`{axiom})"
    using funpow_coin_im[of "{axiom}" k f p]
    unfolding \<open>Suc p = k' - k\<close>
    using assms(2) le_add_diff_inverse  less_le_not_le \<open>k < k'\<close>
    by force
  hence "{(f^^l) axiom | l. l < k'} = {(f^^l) axiom | l. True}"
    unfolding funpow_image_sing
    by simp
  thus ?thesis
    unfolding pmor_lan_fac_clo_seq by presburger
qed

lemma fac_pow_seg_finite: "finite (factor_closure {(f^^l) axiom | l. l < k})"
proof-
            have "finite {(f^^l) axiom | l. l < k}"
    using nat_seg_image_imp_finite[of "{(f^^l) axiom | l. l < k}"
        "\<lambda>l. (f^^l) axiom"  k]
    by blast
  from iffD1[OF fac_clo_finite this]
  show ?thesis.
qed

lemma pmor_shift: "\<L> axiom = factor_closure {(f^^l) axiom | l. l < k} \<union> \<L> ((f^^k) axiom)"
  unfolding set_eq_iff Un_iff
  unfolding pmor_lan_fac_pow
  unfolding fac_clo_set mem_Collect_eq
  using add.commute add_diff_inverse_nat
  using funpow_add'' by metis

lemma pmor_lan_infin_seq_infin: "infinite (\<L> axiom) \<longleftrightarrow> infinite {(f^^k) axiom | k. True}"
  using fac_clo_finite pmor_lan_fac_clo_seq by force

lemma pmor_lan_infin_pow_infin: assumes "k \<noteq> 0"
  shows "infinite (\<L> axiom) \<longleftrightarrow> infinite (purely_morphic_language (f^^k) axiom)"
proof
  have "{((f^^k)^^l) axiom | l. True} \<subseteq> {(f^^l) axiom | l. True}"
    unfolding funpow_mult
    by blast
  thus "infinite (purely_morphic_language (f ^^ k) axiom) \<Longrightarrow> infinite \<L> axiom"
    unfolding endomorphism.pmor_lan_infin_seq_infin[OF pow_endomorphism]
      pmor_lan_infin_seq_infin
    using finite_subset by blast

  show "infinite \<L> axiom  \<Longrightarrow> infinite (purely_morphic_language (f ^^ k) axiom)"
  proof(rule ccontr)
    assume "infinite \<L> axiom" "\<not> infinite (purely_morphic_language (f ^^ k) axiom)"

    have "finite (range (\<lambda>l. ((f ^^ k) ^^ l) axiom))"
      using \<open>\<not> infinite (purely_morphic_language (f ^^ k) axiom)\<close>
      unfolding endomorphism.pmor_lan_infin_seq_infin[OF pow_endomorphism]
        pmor_lan_infin_seq_infin
      by (simp add: full_SetCompr_eq)
    from pigeonhole_infinite[OF _ this]
    obtain a0 where "infinite {a \<in> UNIV. ((f ^^ k) ^^ a) axiom = ((f ^^ k) ^^ a0) axiom}"
      by blast
    then obtain a1 where "((f ^^ k) ^^ a1) axiom = ((f ^^ k) ^^ a0) axiom" "a0 < a1"
      by (metis (mono_tags, lifting) finite_nat_set_iff_bounded_le less_or_eq_imp_le linorder_neqE_nat mem_Collect_eq)
    hence "k * (a1 - a0) \<noteq> 0"
      by (simp add: assms)

    have "(f^^(k*a0)) axiom = (f^^(k*a0 + Suc (k*(a1-a0)-1))) axiom"
      using \<open>((f ^^ k) ^^ a1) axiom = ((f ^^ k) ^^ a0) axiom\<close>
      unfolding funpow_mult
      unfolding Suc_minus[OF \<open>k * (a1 - a0) \<noteq> 0\<close>]
      by (metis \<open>a0 < a1\<close> add_mult_distrib2 nat_less_le ordered_cancel_comm_monoid_diff_class.add_diff_inverse)

    from seq_stable_coin_finite[of 0 "\<lambda>i. (f^^i) axiom", OF _ _ this]
    have "finite (range (\<lambda>i. (f ^^ i) axiom))"
      by (metis add.commute bot_nat_0.extremum funpow_add'')
    hence "finite {((f ^^ k)) axiom |k. True}"
      by (simp add: full_SetCompr_eq)
    thus False
      using \<open>infinite \<L> axiom\<close> pmor_lan_infin_seq_infin by auto
  qed
qed


lemma pmor_first_occ: assumes "\<exists>w \<in> \<L> axiom. P w"
  obtains n v where "v \<le>f (f^^n) axiom" "P v" "\<And>u i. u \<le>f (f^^i) axiom \<Longrightarrow> P u \<Longrightarrow> n \<le> i"
proof-
  let ?Pz = "\<lambda>n. \<exists>z. z \<le>f (f^^n) axiom \<and> P z"

  have "\<exists>n. ?Pz n"
    using assms pmor_lan_fac_pow by blast

    define n where "n = (LEAST i. ?Pz i)"
  have "?Pz n"
    unfolding n_def
    using LeastI_ex[OF \<open>\<exists>n. ?Pz n\<close>]
    by blast
  then obtain v where "v \<le>f (f^^n) axiom" "P v"
    by blast

  from Least_le[of ?Pz, folded n_def]
  show ?thesis
    by (meson \<open>\<exists>z. z \<le>f (f ^^ n) axiom \<and> P z\<close> that)
qed





definition preimage where
  "preimage axiom n w wp ws n_floor_pre w_pred p_pred s_pred \<equiv>
       (wp w ws \<sim>\<^sub>\<I> map (f ^^ n)\<^sup>\<C> w_pred) \<and>
       ((f^^(n_floor_pre)) axiom) = p_pred\<cdot>w_pred\<cdot>s_pred \<and>
        (f^^(n_floor_pre+n)) axiom = ((f^^(n)) p_pred)\<cdot>(wp\<cdot>w\<cdot>ws)\<cdot>((f^^(n)) s_pred)"

definition preimage' where
  "preimage' axiom n w wp ws n_floor_pre w_pred p_pred s_pred \<equiv>
       (wp w ws \<sim>\<^sub>\<I> map (f ^^ n)\<^sup>\<C> w_pred) \<and>
       ((f^^(n_floor_pre)) axiom) = p_pred\<cdot>w_pred\<cdot>s_pred"

lemma preimage_def_third_assumption: assumes "preimage' axiom n w wp ws n_floor_pre w_pred p_pred s_pred"
  shows "(f^^(n_floor_pre+n)) axiom = ((f^^n) p_pred)\<cdot>(wp\<cdot>w\<cdot>ws)\<cdot>((f^^n) s_pred)"
proof-
  have int: "wp w ws \<sim>\<^sub>\<I> map (f ^^ n)\<^sup>\<C> w_pred"
    using assms[unfolded preimage'_def] by blast
  have "wp\<cdot>w\<cdot>ws = (f^^n) w_pred"
    using assms[unfolded preimage'_def] fac_interpD(3)[OF int]
    using list_extension_def morphism.core_ext_id pow_morphism by metis
  have  "((f^^n_floor_pre) axiom) = p_pred\<cdot>w_pred\<cdot>s_pred"
    using assms[unfolded preimage'_def] by blast
  have "(f^^(n_floor_pre+n)) axiom = (f^^n) ((f^^n_floor_pre) axiom)"
    by (simp add: add.commute funpow_add'')
  also have "... = (f^^n) p_pred \<cdot> (wp\<cdot>w\<cdot>ws) \<cdot> (f^^n) s_pred"
    unfolding \<open>((f^^n_floor_pre) axiom) = p_pred\<cdot>w_pred\<cdot>s_pred\<close> \<open>wp\<cdot>w\<cdot>ws = (f^^n) w_pred\<close>
    by (simp add: pow_morph)
  ultimately show ?thesis
    by argo
qed


lemma preimageE: assumes "preimage axiom n w wp ws n_floor_pre w_pred p_pred s_pred"
  shows "(wp w ws \<sim>\<^sub>\<I> map (f ^^ n)\<^sup>\<C> w_pred)"
       "((f^^(n_floor_pre)) axiom) = p_pred\<cdot>w_pred\<cdot>s_pred"
        "(f^^(n_floor_pre+n)) axiom = ((f^^(n)) p_pred)\<cdot>(wp\<cdot>w\<cdot>ws)\<cdot>((f^^(n)) s_pred)"
  using assms preimage_def by auto

lemma preimage_fac: assumes "preimage axiom n w wp ws n_floor_pre w_pred p_pred s_pred"
  shows
    "w_pred \<le>f (f^^(n_floor_pre)) axiom"
    "w \<le>f (f^^(n_floor_pre+n)) axiom"
  using assms preimage_def by auto

lemma preimage_nemp: "preimage axiom n w wp ws n_floor_pre w_pred p_pred s_pred \<Longrightarrow> w \<noteq> \<epsilon> \<Longrightarrow> w_pred \<noteq> \<epsilon>"
  using fac_interp_nemp[of w wp ws] list.map_disc_iff
  unfolding preimage_def
  by blast


lemma preimage_from_fac: assumes "w_pred' \<le>f (f^^n_floor_pre) axiom" "w \<le>f (f^^n) w_pred'" "w \<noteq> \<epsilon>"
  obtains wp ws w_pred p_pred s_pred where
    "preimage axiom n w wp ws n_floor_pre w_pred p_pred s_pred"
proof-
  from morphism.image_fac_interp'[OF pow_morphism \<open>w \<le>f (f^^n) w_pred'\<close> \<open>w \<noteq> \<epsilon>\<close>]
  obtain p w_pred s where "w_pred \<le>f w_pred'" "p w s \<sim>\<^sub>\<I> map (f ^^ n)\<^sup>\<C> w_pred"
    by metis
  obtain pp ss where "((f^^(n_floor_pre)) axiom) = pp\<cdot>w_pred\<cdot>ss"
    by (metis \<open>w_pred \<le>f w_pred'\<close> assms(1) facE' fac_trans)
  from arg_cong[OF this, of "f^^n"]
  have "(f^^(n_floor_pre+n)) axiom = ((f^^(n)) pp)\<cdot>(p\<cdot>w\<cdot>s)\<cdot>((f^^(n)) ss)"
    unfolding morphism.morph[OF pow_morphism]
    unfolding fac_interpD(3)[OF \<open>p w s \<sim>\<^sub>\<I> map (f ^^ n)\<^sup>\<C> w_pred\<close>, unfolded morphism.morph_concat_map[OF pow_morphism], symmetric]
    unfolding funpow_add' comp_apply.
  thus ?thesis
    using \<open>(f ^^ n_floor_pre) axiom = pp \<cdot> w_pred \<cdot> ss\<close> \<open>p w s \<sim>\<^sub>\<I> map (f ^^ n)\<^sup>\<C> w_pred\<close> preimage_def that by blast
qed


lemma preimage_preimage: assumes "preimage axiom n w wp ws (N_pred'+N') w_pred p_pred s_pred"
 "preimage axiom N' w_pred wp' ws' N_pred' w_pred' p_pred' s_pred'"
 "w \<noteq> \<epsilon>"
shows "preimage axiom (n+N') w ((f ^^ n) wp' \<cdot> wp) (ws \<cdot> (f ^^ n) ws') N_pred' w_pred' p_pred' s_pred'"
proof-
  note preimageE[OF \<open>preimage axiom n w wp ws (N_pred'+N') w_pred p_pred s_pred\<close>]
  note preimageE[OF \<open>preimage axiom N' w_pred wp' ws' N_pred' w_pred' p_pred' s_pred'\<close>]

  have "w_pred \<noteq> \<epsilon>"
    using assms(1) assms(3) preimage_nemp
    by simp
  hence "w_pred' \<noteq> \<epsilon>"
    using assms(2) preimage_nemp by simp

  have "(f^^n) w_pred = wp\<cdot>w\<cdot>ws"
    by (metis \<open>wp w ws \<sim>\<^sub>\<I> map (f ^^ n)\<^sup>\<C> w_pred\<close> fac_interpD(3) morphism.morph_concat_map pow_morphism)


  have he1: "(f ^^ (N_pred' + (n + N'))) axiom = (f ^^ (n + N')) p_pred' \<cdot> (((f^^n) wp' \<cdot> wp) \<cdot> w \<cdot> (ws \<cdot>(f^^n) ws')) \<cdot> (f ^^ (n + N')) s_pred'"
    using arg_cong[OF \<open>(f ^^ (N_pred' + N')) axiom = (f ^^ N') p_pred' \<cdot> (wp' \<cdot> w_pred \<cdot> ws') \<cdot> (f ^^ N') s_pred'\<close>, of "f^^n"]
    unfolding funpow_add''
        unfolding pow_morph
    unfolding \<open>(f^^n) w_pred = wp\<cdot>w\<cdot>ws\<close> lassoc
    by (simp add: add.left_commute funpow_add'')

  show ?thesis
    using morphism.image_fac_interp_comp[OF pow_morphism pow_morphism \<open>wp w ws \<sim>\<^sub>\<I> map (f ^^ n)\<^sup>\<C> w_pred\<close> \<open>w_pred \<noteq> \<epsilon>\<close> \<open>wp' w_pred ws' \<sim>\<^sub>\<I> map (f ^^ N')\<^sup>\<C> w_pred'\<close> \<open>w_pred' \<noteq> \<epsilon>\<close>]
    unfolding preimage_def funpow_add[symmetric]
    using he1
    using \<open>(f ^^ N_pred') axiom = p_pred' \<cdot> w_pred' \<cdot> s_pred'\<close> by blast
qed


lemma preimage_ex: assumes "w \<noteq> \<epsilon>" "w \<le>f (f^^n) axiom" "n \<ge> n"
  obtains wp ws w_pred p_pred s_pred
  where "preimage axiom n w wp ws (n-n) w_pred p_pred s_pred"
  using diff_self_eq_0 endomorphism.preimage_from_fac[OF endomorphism_axioms _ assms(2) assms(1), of 0, unfolded funpow_0, OF sublist_order.order_refl]
  by metis

lemma preimage_ex': assumes "w \<noteq> \<epsilon>" "(f^^n_floor) axiom = pw\<cdot>w\<cdot>sw" "n_floor \<ge> n"
  obtains wp ws w_pred p_pred s_pred
  where
    "preimage axiom n w wp ws (n_floor-n) w_pred p_pred s_pred"
    "pw = (f^^n) p_pred \<cdot> wp"
    "sw = ws \<cdot> (f^^n) s_pred" \<comment> \<open>consequence\<close>
proof-
  interpret fN: endomorphism "f^^n"
    by (simp add: pow_endomorphism)

  have "pw\<cdot>w\<cdot>sw = (f ^^ n) ((f^^(n_floor-n)) axiom)"
    by (simp add: assms(2) assms(3) funpow_add'')
  from fN.image_fac_interp[OF this \<open>w \<noteq> \<epsilon>\<close>]
  obtain p w_pred s u_pred v_pred where
      "u_pred \<cdot> w_pred \<cdot> v_pred = (f ^^ (n_floor - n)) axiom"
      "p w s \<sim>\<^sub>\<I> map (f ^^ n)\<^sup>\<C> w_pred"
      "pw = (f ^^ n) u_pred \<cdot> p"
      "sw = s \<cdot> (f ^^ n) v_pred"
    by metis
  thus ?thesis
    using that
    unfolding preimage_def
    by (metis append.assoc assms(2) assms(3) le_add_diff_inverse2)
qed

lemma pmor_first_nonsing_preimage: assumes "finite (length`range f\<^sup>\<C>)"   "(f^^m) axiom = pw\<cdot>w\<cdot>sw" "m \<ge> n + Suc Per" "\<^bold>|w\<^bold>| > \<lceil>f\<rceil>^(n+Suc Per)"
  obtains k wp ws w_pred p_pred s_pred
  where
    "m \<ge> n+Suc k*Suc Per" and
    "preimage axiom (n+Suc k*Suc Per) w wp ws (m - (n+Suc k*Suc Per)) w_pred p_pred s_pred" and
    "\<^bold>|w_pred\<^bold>| \<ge> 2" and
    "pw = ((f^^(n+Suc k*Suc Per)) p_pred)\<cdot>wp" and
        "\<And> wp' ws' w_pred' p_pred' s_pred' l. m \<ge> n+((Suc k+Suc l)*Suc Per) \<Longrightarrow> preimage axiom (Suc l*Suc Per) w_pred wp' ws' (m-(n+((Suc k+Suc l)*Suc Per))) w_pred' p_pred' s_pred' \<Longrightarrow> p_pred = ((f^^((Suc l)*Suc Per)) p_pred')\<cdot>wp' \<Longrightarrow> \<^bold>|w_pred'\<^bold>| = 1"
proof-
  have "w \<noteq> \<epsilon>"
    using assms(4) by force


  define P where "P = (\<lambda>k. \<exists>wp ws w_pred p_pred s_pred. preimage axiom (n+Suc k*Suc Per) w wp ws (m - (n+Suc k*Suc Per)) w_pred p_pred s_pred \<and> \<^bold>|w_pred\<^bold>| \<ge> 2 \<and> m \<ge> n+Suc k*Suc Per \<and> pw = ((f^^(n+Suc k*Suc Per)) p_pred)\<cdot>wp)"

  define k where "k = (GREATEST k. P k)"

  show ?thesis
  proof(cases)

    assume "\<exists>k. P k"

    have "(\<And>y. P y \<Longrightarrow> y \<le> Suc m)"
      unfolding P_def
      by auto
    from GreatestI_ex_nat[OF \<open>\<exists>k. P k\<close> this]
    have "P k"
      unfolding k_def
      by blast
    then obtain wp ws w_pred p_pred s_pred where "preimage axiom (n+Suc k*Suc Per) w wp ws (m - (n+Suc k*Suc Per)) w_pred p_pred s_pred"  "\<^bold>|w_pred\<^bold>| \<ge> 2" "m \<ge> n+Suc k*Suc Per" "pw = ((f^^(n+Suc k*Suc Per)) p_pred)\<cdot>wp"
      unfolding P_def
      by blast
    hence "w_pred \<noteq> \<epsilon>"
      by force

    have "(\<And>y. P y \<Longrightarrow> y \<le> m)"
      unfolding P_def
      by force
    have "P t \<Longrightarrow> k \<ge> t" for t
      unfolding \<open>k \<equiv> Greatest P\<close>
      using Greatest_le_nat[of P t m, OF _ \<open>(\<And>y. P y \<Longrightarrow> y \<le> m)\<close>]
      by blast

    have "m \<ge> n+((Suc k+Suc l)*Suc Per) \<Longrightarrow> preimage axiom (Suc l*Suc Per) w_pred wp' ws' (m-(n+((Suc k+Suc l)*Suc Per))) w_pred' p_pred' s_pred' \<Longrightarrow> p_pred = ((f^^((Suc l)*Suc Per)) p_pred')\<cdot>wp' \<Longrightarrow> \<^bold>|w_pred'\<^bold>| = 1" for wp' ws' w_pred' p_pred' s_pred' l
    proof-

      assume "m \<ge> n+((Suc k+Suc l)*Suc Per)" "preimage axiom (Suc l*Suc Per) w_pred wp' ws' (m-(n+((Suc k+Suc l)*Suc Per))) w_pred' p_pred' s_pred'" "p_pred = ((f^^((Suc l)*Suc Per)) p_pred')\<cdot>wp'"

      have he: "m - (n+Suc k*Suc Per) = m - (n + (Suc k + Suc l) * Suc Per) + Suc l * Suc Per"
        by (metis Nat.add_diff_assoc2 \<open>n + (Suc k + Suc l) * Suc Per \<le> m\<close> add_diff_cancel_right add_mult_distrib group_cancel.add1)

      hence "w_pred' \<noteq> \<epsilon>"
        using \<open>preimage axiom (Suc l * Suc Per) w_pred wp' ws' (m - (n + (Suc k + Suc l) * Suc Per)) w_pred' p_pred' s_pred'\<close> \<open>w_pred \<noteq> \<epsilon>\<close> preimage_nemp by simp

      show "\<^bold>|w_pred'\<^bold>| = 1"
      proof(rule ccontr)
        assume "\<^bold>|w_pred'\<^bold>| \<noteq> 1"
        hence "\<^bold>|w_pred'\<^bold>| \<ge> 2"
          using \<open>w_pred' \<noteq> \<epsilon>\<close> nemp_le_len
          by (metis One_less_Two_le_iff le_eq_less_or_eq)
        hence "P (k+Suc l)"
          unfolding P_def
          using preimage_preimage[OF _ \<open>preimage axiom (Suc l*Suc Per) w_pred wp' ws' (m-(n+((Suc k+Suc l)*Suc Per))) w_pred' p_pred' s_pred'\<close> \<open>w \<noteq> \<epsilon>\<close>, OF \<open>preimage axiom (n+Suc k*Suc Per) w wp ws (m - (n+Suc k*Suc Per)) w_pred p_pred s_pred\<close>[unfolded he]]
          using \<open>pw = ((f^^(n+Suc k*Suc Per)) p_pred)\<cdot>wp\<close> \<open>p_pred = ((f^^((Suc l)*Suc Per)) p_pred')\<cdot>wp'\<close>
          unfolding add_mult_distrib[of _ _ "Suc Per"]
          using \<open>n + (Suc k + Suc l) * Suc Per \<le> m\<close>
          unfolding distrib_right add_Suc[symmetric] add.assoc
          unfolding rassoc distrib_right
          by (metis append_assoc funpow_add'' pow_morph)
        moreover have "m > m-(Suc l*Suc Per)"
          using assms(3) by auto
        ultimately show False
          by (meson \<open>\<And>t. P t \<Longrightarrow> t \<le> k\<close> less_add_same_cancel1 linorder_not_le zero_less_Suc)
      qed
    qed
    thus thesis
      using \<open>2 \<le> \<^bold>|w_pred\<^bold>|\<close> \<open>n + Suc k * Suc Per \<le> m\<close> \<open>preimage axiom (n + Suc k * Suc Per) w wp ws (m - (n + Suc k * Suc Per)) w_pred p_pred s_pred\<close> that \<open>pw = (f ^^ (n + Suc k * Suc Per)) p_pred \<cdot> wp\<close> by blast
  next
    assume "\<not> (\<exists>k. P k)"
    hence nok: "preimage axiom (n + Suc k * Suc Per) w wp ws (m - (n + Suc k * Suc Per)) w_pred p_pred s_pred \<Longrightarrow>  n + Suc k * Suc Per \<le> m \<Longrightarrow> pw = (f ^^ (n + Suc k * Suc Per)) p_pred \<cdot> wp
    \<Longrightarrow> \<not> 2 \<le> \<^bold>|w_pred\<^bold>| " for k wp ws w_pred p_pred s_pred
      using P_def by blast

    from preimage_ex'[OF \<open>w \<noteq> \<epsilon>\<close> assms(2) \<open>m \<ge> n + Suc Per\<close>]
    obtain wp ws w_pred p_pred s_pred where
      "preimage axiom (n + Suc Per) w wp ws (m - (n + Suc Per)) w_pred p_pred s_pred"
      "pw = (f ^^ (n + Suc Per)) p_pred \<cdot> wp"
      "sw = ws \<cdot> (f ^^ (n + Suc Per)) s_pred"
      by blast
    hence "\<not> 2 \<le> \<^bold>|w_pred\<^bold>|"
      using nok[of 0]
      by (metis One_nat_def assms(3) lambda_one)
    moreover have "w_pred \<noteq> \<epsilon>"
      using \<open>preimage axiom (n + Suc Per) w wp ws (m - (n + Suc Per)) w_pred p_pred s_pred\<close> \<open>w \<noteq> \<epsilon>\<close> preimage_nemp by simp
    ultimately have "\<^bold>|w_pred\<^bold>| = 1"
      by (metis One_less_Two_le_iff le_eq_less_or_eq nemp_le_len)

    have "wp \<cdot> w \<cdot> ws = (f ^^ (n + Suc Per)) w_pred"
      using \<open>preimage axiom (n + Suc Per) w wp ws (m - (n + Suc Per)) w_pred p_pred s_pred\<close>
      unfolding preimage_def factor_interpretation_def
      unfolding morphism.morph_concat_map[OF pow_morphism]
      by blast
    from le_trans[OF fac_len', of wp w ws, unfolded lenarg[OF this]]
    have "\<^bold>|w\<^bold>| \<le> \<^bold>|w_pred\<^bold>| * \<lceil>f\<rceil>^(n+Suc Per)"
      using max_im_len_pow_le'[OF assms(1)]
      by blast
    hence False
      using \<open>\<^bold>|w\<^bold>| > \<lceil>f\<rceil>^(n+Suc Per)\<close>
      unfolding \<open>\<^bold>|w_pred\<^bold>| = 1\<close>
      by linarith
    thus thesis
      by simp
  qed
qed

lemma pmor_lan_infin_growing: assumes finite_UNIV: "finite (UNIV::'a set)" and
  "infinite (\<L> axiom)"
  shows
    "1 < \<lceil>f\<rceil>"
  using assms
  unfolding pmor_lan_infin_seq_infin
  using growing_maxlen seq_infin_growing by fastforce

lemma growing_pmor_lan_infin: assumes finite_UNIV: "finite (UNIV::'a set)" and
  "growing_seq wj" "\<And>j. wj j \<in> \<L> axiom"
  shows
    "1 < \<lceil>f\<rceil>"
proof-
  have "infinite (\<L> axiom)"
    using assms
    by (meson growing_seq_def len_lan_infinite)
  thus ?thesis
    using local.finite_UNIV pmor_lan_infin_growing by force
qed

lemma pmor_lan_part: "\<L> axiom = (\<Union> i\<in>{0..<Suc p}. (endomorphism.pmor_lan (f^^(Suc p)) ((f^^i) axiom)))" (is "?L = ?R")
proof
  interpret fp: endomorphism "f^^(Suc p)"
    using pow_endomorphism by blast
  have "0 < Suc p"
    by simp
  show "?L \<subseteq> ?R"
  proof
    fix x
    assume "x \<in> ?L"
    then obtain i where "x \<le>f (f^^i) axiom"
      using pmor_lan_fac_pow by blast
    thus "x \<in> ?R"
      using mult_div_mod_eq[of "Suc p" i] mod_less_divisor[OF \<open>0 < Suc p\<close>, of i]
      unfolding atLeast0LessThan
      unfolding fp.pmor_lan_fac_clo_seq fac_clo_set
      unfolding Union_iff Bex_def
      unfolding mem_Collect_eq image_def
      unfolding funpow_mult funpow_add''
      by (metis (mono_tags, lifting) lessThan_iff mem_Collect_eq)
  qed
  show "?R \<subseteq> ?L"
    unfolding fp.pmor_lan_fac_clo_seq fac_clo_set
    unfolding subset_iff
    unfolding Union_iff Bex_def
    unfolding mem_Collect_eq
    unfolding funpow_mult funpow_add''
    unfolding pmor_lan_fac_pow
    by blast
qed

lemma pmor_lan_infin_subset_somewhere:  assumes "range sw \<subseteq> \<L> axiom" "infinite (range sw)"
  obtains I q where "\<And>i. i \<in> I \<Longrightarrow> sw i \<in> purely_morphic_language (f^^Suc p) ((f^^q) axiom)"
  "infinite (sw`I)" "q < Suc p" \<comment> \<open>works for @{term "p=0"} except for the @{term "q < p"}\<close>
proof-
  note Lun = pmor_lan_part[of axiom p]
  hence "range sw = (range sw) \<inter> (\<Union>i\<in>{0..< Suc p}. purely_morphic_language (f ^^ Suc p) ((f ^^ i) axiom))"
    using \<open>range sw \<subseteq> \<L> axiom\<close>
    by (simp add: subset_antisym)
  hence "range sw = (\<Union>i\<in>{0..<Suc p}. range sw \<inter> purely_morphic_language (f ^^ Suc p) ((f ^^ i) axiom))"
    unfolding Int_UN_distrib[of "range sw" "\<lambda> i. purely_morphic_language (f ^^ Suc p) ((f ^^ i) axiom)" "{0..<Suc p}", symmetric].
  then obtain q where
    "infinite ((range sw) \<inter> (purely_morphic_language (f ^^ Suc p) ((f ^^ q) axiom)))" "q < Suc p"
    by (metis (no_types, lifting) assms(2) atLeastLessThan_iff finite_UN_I finite_atLeastLessThan)
  then obtain I where "sw`I = (range sw) \<inter> (purely_morphic_language (f ^^ Suc p) ((f ^^ q) axiom))"
    by (metis inf.cobounded1 subset_imageE)
  hence "infinite (sw`I)"
    using \<open>infinite (range sw \<inter> purely_morphic_language (f ^^ Suc p) ((f ^^ q) axiom))\<close> by presburger
  moreover have "sw i \<in> purely_morphic_language (f^^(Suc p)) ((f^^q) axiom)" if "i \<in> I" for i
    by (metis IntE \<open>sw ` I = range sw \<inter> purely_morphic_language (f ^^ Suc p) ((f ^^ q) axiom)\<close> imageI that)
  ultimately show ?thesis
    using \<open>q < Suc p\<close> that by blast
qed

end
subsection "Factor interpretation"

thm morphism.image_fac_interp

subsection "Bounded letters"

context endomorphism
begin

definition bounded :: "'a list \<Rightarrow> bool"
  where "bounded w \<equiv> (finite (\<L> w))"

lemma growing_unbounded: "growing f w \<Longrightarrow> \<not> bounded w"
  using endomorphism.bounded_def endomorphism_axioms growing_seq_infin pmor_lan_infin_seq_infin by blast

lemma unbounded_growing: "finite (UNIV::'a set) \<Longrightarrow> \<not> bounded w \<longleftrightarrow> growing f w"
  using endomorphism.bounded_def endomorphism_axioms growing_seq_infin pmor_lan_infin_seq_infin seq_infin_growing by blast

lemma bounded_emp: "bounded \<epsilon>"
  by (simp add: bounded_def pmor_lan_emp')

lemma bounded_fin_seq: "bounded w \<longleftrightarrow> finite {(f^^k) w | k. True}"
  using bounded_def pmor_lan_infin_seq_infin by blast

lemma bounded_bounded: assumes "bounded w"
  obtains n where "\<And>i. \<^bold>|(f^^i) w\<^bold>| \<le> n"
  by (meson assms bounded_def growing_def growing_seq_infin nle_le pmor_lan_infin_seq_infin)

lemma bounded_bounded': assumes "finite (UNIV::'a set)" "\<exists>n. \<forall>i. \<^bold>|(f^^i) w\<^bold>| \<le> n"
  shows "bounded w"
  unfolding bounded_def
  using maxlen_finite[OF \<open>finite (UNIV::'a set)\<close>, unfolded lists_UNIV, OF subset_UNIV]
  by (meson assms(2) fac_len order.trans pmor_lan_fac_pow)

lemma bounded_concat: assumes "bounded w" "bounded w'"
  shows "bounded (w\<cdot>w')"
  unfolding bounded_def
proof-
  have "{(f^^k) (w \<cdot> w') | k. True} = {((f^^k) (w)) \<cdot> ((f^^k) (w')) | k. True}"
    unfolding pow_morph
    by blast

  have fin_cart: "finite ({((f^^k) (w)) | k. True} \<times> {((f^^k) (w')) | k. True})"
    using \<open>bounded w\<close> \<open>bounded w'\<close>  pmor_lan_infin_seq_infin
      unfolding bounded_def by force

  have "{(f^^k) w \<cdot> (f^^k) w' |k. True} \<subseteq> (\<lambda>ww. fst ww \<cdot> snd ww) ` ({(f^^k) w |k. True} \<times> {(f^^k) w' |k. True})"
  proof
    fix x
    assume "x \<in> {(f^^k) w \<cdot> (f^^k) w' |k. True}"
    then obtain k where "x = (f^^k) w \<cdot> (f^^k) w'"
      by blast
    have "x = (\<lambda>ww. fst ww \<cdot> snd ww) ((f^^k) w,(f^^k) w')"
      by (simp add: \<open>x = (f^^k) w \<cdot> (f^^k) w'\<close>)
    thus "x \<in> (\<lambda>ww. fst ww \<cdot> snd ww) ` ({(f^^k) w |k. True} \<times> {(f^^k) w' |k. True})"
      by blast
  qed
  from rev_finite_subset[OF _ this, OF finite_imageI, OF fin_cart]
  have "finite ({((f^^k) (w)) \<cdot> ((f^^k) (w')) | k. True})".
  show "finite (\<L> (w \<cdot> w'))"
    using \<open>finite {(f^^k) w \<cdot> (f^^k) w' |k. True}\<close> \<open>{(f^^k) (w \<cdot> w') |k. True} = {(f^^k) w \<cdot> (f^^k) w' |k. True}\<close>
      pmor_lan_infin_seq_infin by presburger
qed

lemma bounded_fac_bounded: "bounded w \<Longrightarrow> u \<le>f w \<Longrightarrow> bounded u"
  using pmor_lan_fac_sub
  by (meson bounded_def infinite_super)

lemma bounded_fac: "factorial (Collect bounded)"
  using bounded_fac_bounded by blast

lemma bounded_concat_conv: "bounded (w\<cdot>w') \<longleftrightarrow> bounded (w) \<and> bounded (w')"
  by (meson bounded_concat endomorphism.bounded_fac_bounded endomorphism_axioms sublist_append_leftI sublist_append_rightI)

lemma bounded_set: "bounded w \<longleftrightarrow> (\<forall> b \<in> set w. bounded [b])"
 by (induct w, simp add: bounded_emp) (subst hd_word, unfold bounded_concat_conv, force)

lemma bounded_pow: "bounded w \<Longrightarrow> bounded (w\<^sup>@k)"
  by(induction k, (simp add: bounded_emp), (simp add: bounded_concat))

lemma bounded_pow_conv: "bounded w \<longleftrightarrow> bounded (w\<^sup>@Suc k)"
  using bounded_concat_conv bounded_pow by auto

lemma bounded_basis: "\<BB> (Collect bounded) = {[a] | a. bounded [a]}"
proof
  show "\<BB> (Collect bounded) \<subseteq> {[a] |a. bounded [a]}"
  proof
    fix x assume "x \<in> \<BB> (Collect bounded)"
    with emp_not_basis[OF this] basis_in[OF this]
    have "x \<in>U \<langle>Collect bounded\<rangle>" "bounded x"  "x \<noteq> \<epsilon>"
      unfolding basis_def using ungen_hull_ungen by blast+
    have x: "x = concat (map (\<lambda> x. [x]) x)"
      by simp
    from ungen_dec_triv'[of "map (\<lambda> x. [x]) x", folded x, OF _ \<open>x \<in>U \<langle>Collect bounded\<rangle>\<close>]
    have "\<^bold>|map (\<lambda>x. [x]) x\<^bold>| = 1"
      using \<open>bounded x\<close>[unfolded bounded_set[of x]] gen_in[of _ "Collect bounded"] by fastforce
    from sing_word[OF this]
    have "x = [hd x]"
      by force
    then show "x \<in> {[a] |a. bounded [a]}"
      unfolding mem_Collect_eq using \<open>bounded x\<close> by auto
  qed
  show "{[a] |a. bounded [a]} \<subseteq> \<BB> (Collect bounded)"
    using ungenerated_sing[of _ "Collect bounded"]
    unfolding basis_def[of "Collect bounded"] by blast
qed

lemma bounded_image: "bounded w \<longleftrightarrow> bounded (f w)"
proof
  assume "bounded w"
  hence "(finite (\<L> (f w)))"
    unfolding bounded_def
    by (meson finite_subset pmor_lan_iff subset_iff)
  thus "bounded (f w)"
    using bounded_def by auto
next
  assume "bounded (f w)"
  have "\<L> w \<subseteq> {v. v \<le>f w} \<union> \<L> (f w)"
    using endomorphism.pmor_lan_iff endomorphism_axioms by auto
  moreover have "finite {v. v \<le>f w}"
    using fac_clo_finite[of "{w}"]
    unfolding fac_clo_sing[of w]
    by blast
  ultimately have "finite (\<L> w)"
    using \<open>bounded (f w)\<close>[unfolded bounded_def] infinite_Un infinite_super
    by fast
  thus "bounded w"
    unfolding bounded_def.
qed

corollary bounded_image_pow: "bounded w \<longleftrightarrow> bounded ((f^^k) w)"
proof(induct k)
  case 0
  then show ?case
    by (simp add: power.power.power_0)
next
  case (Suc k)
  then show ?case
    using bounded_image by auto
qed

lemma bounded_pow_bounded: "bounded w \<longleftrightarrow> endomorphism.bounded (f^^(Suc k)) w"
  using pmor_lan_infin_pow_infin nat.discI
  unfolding endomorphism.bounded_def[OF pow_endomorphism] bounded_def by blast

lemma bounded_rev_map[reversal_rule]: "endomorphism.bounded (rev_map f) (rev x) = bounded x"
  unfolding endomorphism.bounded_fin_seq[OF rev_map_endomorph]
  unfolding bounded_fin_seq
  unfolding rev_map_funpow
  unfolding rev_map_def comp_apply
  unfolding rev_rev_ident
  unfolding finite_image_iff[OF inj_on_rev, of "{(f ^^ k) x |k. True}", symmetric]
  unfolding setcompr_eq_image image_image..

lemma unbounded_letter: "(\<not> bounded w) \<longleftrightarrow> (\<exists>a \<in> set w. \<not> bounded [a])"
proof(induction w, (simp add: bounded_emp))
  case (Cons a w)
  then show ?case
    by (metis Cons_eq_appendI bounded_concat bounded_fac_bounded in_set_conv_decomp_last list.set_intros(2) self_append_conv2 sublist_def)
qed

subsubsection "Fixing of bounded letters"

definition bounded_fixed
  where "bounded_fixed n per_minus_one \<equiv> (\<forall>w i k. bounded w \<longrightarrow> n \<le> i \<longrightarrow> (f^^i) w = (f^^(i+k*(Suc per_minus_one))) w)"

lemma bounded_fixed_rev_map [reversal_rule]: "endomorphism.bounded_fixed (rev_map f) = bounded_fixed"
  unfolding endomorphism.bounded_fixed_def[OF rev_map_endomorph, reversed]
    bounded_fixed_def
  by (simp add: rev_map_arg_rev rev_map_funpow)


lemma bounded_fixedEx: assumes "finite (UNIV::'a set)"
  obtains n per_minus_one where
        "bounded_fixed n per_minus_one"
proof-
  let ?seq = "\<lambda>w i. (f^^i) w"
  let ?A = "{[a] |a. bounded [a]}"

  have fin_b: "finite {[a] | a::'a. bounded [a]}"
    using assms rev_finite_subset by fastforce

  have "bounded [a] \<Longrightarrow> finite (range (?seq [a]))" for a
    by (simp add: bounded_fin_seq full_SetCompr_eq)
  hence fin_range: "\<And>w. w \<in> ?A \<Longrightarrow> finite (range (?seq w))"
    by blast

  have "?seq w i = ?seq w j \<longrightarrow> ?seq w (i + k) = ?seq w (j + k)" for i j k w
    by (metis add.commute funpow_add'')
  hence ev_per_all:
    "\<And>w. w \<in> ?A \<Longrightarrow> \<exists>n. \<forall>i j k. n \<le> i \<and> n \<le> j \<and> (?seq w) i = (?seq w) j \<longrightarrow> (?seq w) (i+k) = (?seq w) (j+k)"
    by blast

  from finite_stable_ev_per_all[OF fin_b, of ?seq, OF fin_range ev_per_all]
  obtain q p where ev_per_all: "\<And>w i k. w \<in> ?A
     \<Longrightarrow> q \<le> i \<Longrightarrow> (?seq w) i = (?seq w) (i+k*(Suc p))"
    by blast

  have "\<And>i k. bounded w \<Longrightarrow> q \<le> i \<Longrightarrow> (?seq w) i = (?seq w) (i+k*(Suc p))" for w
  proof(induct w)
    case Nil
    then show ?case
      using pow_emp_to_emp by force
  next
    case (Cons a w)
    have "bounded [a]"
      using bounded_fac_bounded[OF \<open>bounded (a # w)\<close>]
      by auto
    moreover have "bounded w"
    using bounded_fac_bounded[OF \<open>bounded (a # w)\<close>]
      by (simp add: fac_ext_hd)
    ultimately show ?case
      unfolding morphism.pop_hd[OF pow_morphism, of _ a w]
      using Cons.hyps Cons.prems(2) ev_per_all by auto
  qed
  thus ?thesis
    using that
    unfolding bounded_fixed_def
    by blast
qed

lemmas bounded_fixedEx' = bounded_fixedEx[unfolded bounded_fixed_def, rule_format]

corollary finite_bounded_len:
  assumes "finite (UNIV::'a set)" "finite U" "U \<subseteq> Collect bounded"
  obtains L_bounded where
    "\<And>u k. u \<in> U \<Longrightarrow> \<^bold>|(f^^k) u\<^bold>| \<le> L_bounded"
proof-
  obtain n per_minus_one where
    bounded_fixed: "\<And>w i k.
        bounded w \<Longrightarrow> n \<le> i \<Longrightarrow> (f ^^ i) w = (f ^^ (i + k * Suc per_minus_one)) w"
    using bounded_fixedEx[OF \<open>finite (UNIV::'a set)\<close>]
    unfolding bounded_fixed_def
    by blast

  have "\<And>x. x \<in> U \<Longrightarrow> bounded x"
    using assms(3) by blast
  hence "(f^`UNIV`U) = (f^`{..<n + Suc per_minus_one}`U)"
    using funpow_coin_im[of U n f per_minus_one]
      bounded_fixed[of _ n 1]
    by simp
  hence "finite ((f^`UNIV`U))"
    using funpow_image_fin_fin_fin assms(2) by auto
  then obtain L where "\<And>w. w \<in> (f^`UNIV`U) \<Longrightarrow> \<^bold>|w\<^bold>| \<le> L"
    using len_lan_infinite[of "f^`UNIV`U"]
    by (meson less_le_not_le nle_le)
  thus thesis
    using that[of L]
    by blast
qed





lemma bounded_list_all: "bounded w \<longleftrightarrow> list_all (\<lambda> a. bounded [a]) w"
proof(induct w, simp add: bounded_emp)
  case (Cons a w)
  then show ?case
    by (metis bounded_concat bounded_fac_bounded hd_word list_all_simps(1) sublist_append_leftI sublist_append_rightI)
qed

lemma bounded_rev[reversal_rule]: "endomorphism.bounded (rev_map f) (rev w) = bounded w"
proof(induction w)
  case Nil
  then show ?case
    by (simp add: bounded_emp endomorphism.bounded_emp rev_map_endomorph)
next
case (Cons a w)
  have A1: "{rev ((f ^^ k) [x]) |k. True} = rev ` {(f ^^ k) [x] |k. True}" for x
  unfolding set_eq_iff
  by blast
  have "endomorphism.bounded (rev_map f) [x] = bounded [x]" for x
    unfolding bounded_fin_seq endomorphism.bounded_fin_seq[OF rev_map_endomorph]
      rev_map_funpow
      unfolding rev_map_def rev_sing comp_apply
      unfolding A1
      using finite_imageI[of "rev ` {(f ^^ k) [x] |k. True}" rev, unfolded rev_rev_image_eq]
        finite_imageI[of "{(f ^^ k) [x] |k. True}" rev]
        by argo
  then show ?case
    by (metis (mono_tags, lifting) bounded_list_all endomorphism.bounded_list_all list_all_append list_all_simps(1) local.Cons rev.simps(2) rev_map_endomorph)
qed

lemma unbounded_im_all_long:
  assumes finite_UNIV: "finite (UNIV::'a set)"
  obtains E where "\<And>a i. \<not> bounded [a] \<Longrightarrow> \<^bold>|(f^^(E+i)) [a]\<^bold>| \<ge> n"
proof-
  define P where "P = (\<lambda> a E. \<^bold>|(f^^E) [a]\<^bold>| \<ge> n)"


  have "\<exists>M. \<forall>i. i \<ge> M \<longrightarrow> P a i" if "\<not> bounded [a]" for a
  proof(rule ccontr)
    assume "\<nexists>M. \<forall>i\<ge>M. P a i"
    hence "infinite {i. \<not> P a i}"
      by (metis  finite_nat_set_iff_bounded linorder_not_less mem_Collect_eq)
    have "\<forall>s\<in> (\<lambda>i. (f ^^ i) [a]) ` {i. \<not> P a i}. \<^bold>|s\<^bold>| \<le> n"
      using P_def by auto
    hence "finite ((\<lambda>i. (f ^^ i) [a]) ` {i. \<not> P a i})"
      using finite_UNIV maxlen_finite[OF finite_UNIV, of "((\<lambda>i. (f ^^ i) [a]) ` {i. \<not> P a i})"]
      by blast
    from pigeonhole_infinite[OF \<open>infinite {i. \<not> P a i}\<close> this]
    obtain i0 where "\<not> P a i0" "infinite {aa \<in> {i. \<not> P a i}. (f ^^ aa) [a] = (f ^^ i0) [a]}"
      by blast
    then obtain i1 where "i1 \<in> {aa \<in> {i. \<not> P a i}. (f ^^ aa) [a] = (f ^^ i0) [a]}" "i1 > i0"
      by (meson finite_nat_set_iff_bounded_le leI)
    hence "(f ^^ i1) [a] = (f ^^ i0) [a]"
      by blast

    obtain p where "Suc p = (i1-i0)"
      using Suc_diff_Suc \<open>i0 < i1\<close> by blast
    hence "(f ^^ i0) [a] = (f ^^ (i0 + Suc p)) [a]"
      by (simp add: \<open>(f ^^ i1) [a] = (f ^^ i0) [a]\<close>)

    have "finite (range (\<lambda>i. (f ^^ i) [a]))"
      using seq_stable_coin_finite[of 0 "(\<lambda>i. (f ^^ i) [a])",OF _ bot_nat_0.extremum \<open>(f ^^ i0) [a] = (f ^^ (i0 + Suc p)) [a]\<close>]
      unfolding funpow_add' comp_apply
      by presburger
     thus False
      using that[unfolded arg_cong[OF bounded_fin_seq, of Not]] unfolding full_SetCompr_eq[symmetric]
      by presburger
  qed

    let ?Q = "(\<lambda>a. (SOME M. \<forall>i. i \<ge> M \<longrightarrow> P a i))"
  define T where "T = ?Q ` {a. \<not> bounded [a]}"


  have "?Q a \<le> Max T" if "\<not> bounded [a]" for a
    unfolding T_def
    using finite_UNIV rev_finite_subset that by fastforce
  have "\<forall>i. i \<ge> ?Q a \<longrightarrow> P a i" if "\<not> bounded [a]" for a
    using \<open>\<And>a. \<not> bounded [a] \<Longrightarrow> \<exists>M. \<forall>i\<ge>M. P a i\<close>[OF that, THEN someI_ex]
    by argo

  have "\<forall>i. i \<ge> Max T \<longrightarrow> P a i" if "\<not> bounded [a]" for a
    by (meson \<open>\<And>a. \<not> bounded [a] \<Longrightarrow> (SOME M. \<forall>i\<ge>M. P a i) \<le> Max T\<close> \<open>\<And>a. \<not> bounded [a] \<Longrightarrow> \<forall>i\<ge>SOME M. \<forall>i\<ge>M. P a i. P a i\<close> le_trans that)
  thus thesis
    using that[of "Max T"]
    unfolding P_def
    by simp
qed

subsubsection "Ininitial bounded words"

abbreviation initial_bounded
  where "initial_bounded axiom u \<equiv> bounded u \<and> (u \<le>f axiom \<or>
      (\<exists>a. ([a] \<in> \<L> axiom \<and> u \<le>f f [a] \<and> \<not> bounded [a])))"

lemma initial_bounded_finite: assumes "finite (UNIV::'a set)"
    shows "finite {u. initial_bounded axiom u}"
proof(rule maxlen_finite[OF \<open>finite (UNIV::'a set)\<close>])
  show "{u. initial_bounded axiom u} \<subseteq> lists UNIV"
    by blast
  have "s \<in> {u. initial_bounded axiom u} \<Longrightarrow> \<^bold>|s\<^bold>| \<le> max \<^bold>|axiom\<^bold>| \<lceil>f\<rceil>" for s
  proof-
    assume "s \<in> {u. initial_bounded axiom u}"
    then consider "s \<le>f axiom" | "\<exists>a. s \<le>f f [a]"
      by blast
    then show ?thesis
    proof(cases)
      case 1
      then show ?thesis
        by (simp add: fac_len max.coboundedI1)
    next
      case 2
      then show ?thesis
        by (metis (no_types, lifting) One_nat_def assms dual_order.trans fac_len length_Cons list.size(3) max.coboundedI2 morphism.max_im_len_le_dom morphism_axioms mult.commute nat_mult_1_right)
    qed
  qed
  thus "\<exists>n. \<forall>s\<in>{u. initial_bounded axiom u}. \<^bold>|s\<^bold>| \<le> n "
    by meson
qed

lemma initial_bounded_pow_len: assumes finite_UNIV: "finite (UNIV::'a set)"
  obtains C_initial where "\<And>u k. initial_bounded axiom u \<Longrightarrow> \<^bold>|(f^^k) u\<^bold>| \<le> C_initial"
proof-
  note rule0 = finite_induct_select[OF initial_bounded_finite[OF finite_UNIV, of axiom],
      of "\<lambda> U. (finite ( \<Union> { (f^^k) ` U | k . True }))", folded pow_set_union_all,
      unfolded mem_Collect_eq]

  let ?A = "{(f^^k) u | k u. initial_bounded axiom u}"
  have "finite ?A"
  proof(rule rule0)
    show "finite {(f ^^ k) u |k u. u \<in> {}}"
      by simp
  next
    fix S
    assume "S \<subset> {u. initial_bounded axiom u}"
           "finite {(f ^^ k) u |k u. u \<in> S}"
    have "{(f ^^ k) u |k u. u \<in> insert x S} = {(f ^^ k) u |k u. u \<in> S} \<union> {(f ^^ k) x |k . True}" if "x \<in>{u. initial_bounded axiom u}" for x
      unfolding insert_compr mem_Collect_eq
      by blast
    thus "\<exists>s\<in>{u. initial_bounded axiom u} - S. finite {(f ^^ k) u |k u. u \<in> insert s S}"
      using finite_UnI[OF \<open>finite {(f ^^ k) u |k u. u \<in> S}\<close>] \<open>S \<subset> {u. initial_bounded axiom u}\<close> bounded_fin_seq by auto
  qed
  then obtain n where "\<And>w. w \<in> ?A \<Longrightarrow> \<^bold>|w\<^bold>| \<le> n"
    using len_lan_infinite[of ?A]
    by (meson less_le_not_le nle_le)
  thus ?thesis
    unfolding mem_Collect_eq
    using that[of n]
    by blast
qed

end

subsection "Immortal words"
definition immortal :: "('a list \<Rightarrow> 'a list) \<Rightarrow> 'a list \<Rightarrow> bool"
  where "immortal f w \<equiv> \<forall>k. (f^^k) w \<noteq> \<epsilon>"

abbreviation (input) mortal :: "('a list \<Rightarrow> 'a list) \<Rightarrow> 'a list \<Rightarrow> bool"
  where "mortal f w \<equiv> \<not> immortal f w"

context endomorphism
begin

lemma mortal_bounded: "mortal f w \<Longrightarrow> bounded w"
  by (metis bounded_emp bounded_image_pow immortal_def)

text\<open>The other implication does not hold, even if under the hypothesis of finite alphabet.\<close>

lemma mortalE: assumes "mortal f w"
  obtains k where "\<And>i. (f^^(k+i)) w = \<epsilon>"
  using assms unfolding immortal_def funpow_add' comp_apply
  using pow_emp_to_emp
  by metis

lemma mortal_concat: assumes "mortal f v" "mortal f w"
  shows "mortal f (v\<cdot>w)"
  using mortalE[OF assms(1)] mortalE[OF assms(2)]
  unfolding immortal_def pow_morph
  using append_Nil2 funpow_add'' by metis

lemma mortal_fac: assumes "mortal f w" "v \<le>f w"
  shows "mortal f v"
  by (metis (full_types) assms(1) assms(2) immortal_def morphism.fac_mono pow_morphism sublist_Nil_right)

lemma mortal_list_all: "mortal f w \<longleftrightarrow> list_all (\<lambda>x. mortal f [x]) w"
proof(induction w)
  case Nil
  then show ?case
    by (simp add: immortal_def pow_emp_to_emp)
next
  case (Cons a w)
  then show ?case
    by (metis hd_word list_all_simps(1) mortal_concat mortal_fac sublist_append_leftI sublist_append_rightI)
qed

lemma assumes "finite (UNIV::'a set)"
  obtains M where "\<And>w. mortal f w \<Longrightarrow> (f^^M) w = \<epsilon>"
proof-
  let ?oN = "\<lambda>x. SOME n. (f^^n) [x] = \<epsilon>"
  have "mortal f [x] \<Longrightarrow> (f^^(?oN x)) [x] = \<epsilon>" for x
    by (meson immortal_def tfl_some)
  hence "i \<ge> ?oN x \<Longrightarrow> mortal f [x] \<Longrightarrow> (f^^i) [x] = \<epsilon>" for x i
    using le_add_diff_inverse[of "?oN x" i] funpow_add''[of "i-(?oN x)" _ "?oN x"] pow_emp_to_emp
    unfolding add.commute[of "?oN x"]
    by metis

  let ?A = "?oN ` {x. mortal f [x]}"
  have "finite ?A"
    using assms rev_finite_subset by fastforce
  define M where "M = Max ?A"
  have "mortal f [x] \<Longrightarrow> (f^^M) [x] = \<epsilon>" for x
    unfolding M_def
    using Max.coboundedI[OF \<open>finite ?A\<close>]
    using \<open>\<And>x i. \<lbrakk>(SOME n. (f ^^ n) [x] = \<epsilon>) \<le> i; \<not> immortal f [x]\<rbrakk> \<Longrightarrow> (f ^^ i) [x] = \<epsilon>\<close> by blast

  have "mortal f w \<Longrightarrow> (f^^M) w = \<epsilon>" for w
  proof(induction w)
    case Nil
    then show ?case
      using pow_emp_to_emp by auto
  next
    case (Cons a w)
    show ?case
      unfolding hd_word[of a w] pow_morph
      using mortal_fac[OF Cons.prems]
      by (metis Cons.IH Cons.prems \<open>\<And>x. \<not> immortal f [x] \<Longrightarrow> (f ^^ M) [x] = \<epsilon>\<close> append_is_Nil_conv endomorphism.mortal_list_all endomorphism_axioms list_all_simps(1))
  qed
  thus thesis
    using that by blast
qed

lemma growing_immortal: "growing f w \<Longrightarrow> immortal f w"
  using growing_unbounded mortal_bounded by blast

subsection "Pushy morphic languages"

definition pushy :: "'a list \<Rightarrow> bool"
  where "pushy axiom \<equiv> \<not> finite ((\<L> axiom) \<inter> (Collect bounded))"

lemma pushy_infinite: "pushy axiom \<Longrightarrow> infinite (\<L> axiom)"
  unfolding pushy_def
  by blast

lemma pushy_pow_not_bounded: "pushy axiom \<Longrightarrow> \<not> bounded ((f^^k) axiom)"
proof(induction k)
  case 0
  then show ?case
    using pushy_infinite eq_id_iff
    unfolding funpow_0 bounded_def
    by auto
next
  case (Suc k)
  then show ?case
    using bounded_image[of "(f^^k) axiom"]
    unfolding compow_Suc
    by fast
qed

text\<open>If the system is pushy, then there is a word over bounded letter of arbitrary length.\<close>
lemma pushy_maxlen: assumes "finite (UNIV::'a set)" "pushy axiom"
  obtains u where "u \<in> \<L> axiom" "bounded u" "n < \<^bold>|u\<^bold>|"
  using maxlen_finite[OF \<open>finite UNIV\<close> ] \<open>pushy axiom\<close>
  unfolding pushy_def
  using IntD1 IntD2 linorder_not_less lists_UNIV subset_UNIV CollectD by metis

lemma pushyI: assumes "\<forall>n. \<exists>u. u \<in> \<L> axiom \<and> bounded u \<and> n < \<^bold>|u\<^bold>|"
  shows "pushy axiom"
  using len_lan_infinite assms IntI mem_Collect_eq
  unfolding conj_assoc[symmetric] Int_iff[symmetric] pushy_def by metis

lemma pushy_shift: shows "pushy axiom = pushy ((f^^k) axiom)"
  unfolding pushy_def pmor_shift[of axiom k]
  using fac_pow_seg_finite[of axiom k]
  unfolding Int_Un_distrib2
  by blast

lemma pushy_strong_shift:
  assumes "finite (UNIV::'a set)"
  shows "pushy axiom = endomorphism.pushy (f^^Suc p) axiom"
proof(cases)
  assume "1 < \<lceil>f\<rceil>"
  show ?thesis
  proof
    show "pushy axiom" if "endomorphism.pushy (f ^^ Suc p) axiom"
    proof-
      have "purely_morphic_language (f ^^ Suc p) axiom \<subseteq> \<L> axiom"
        by (metis (no_types, lifting) endomorphism.pmor_lan_fac_pow funpow_mult pmor_lan_pow_in pow_endomorphism purely_morphic_language.simps subsetI)
      show ?thesis
        unfolding pushy_def
      proof(rule infinite_super)
        show "(purely_morphic_language (f ^^ Suc p) axiom) \<inter> (Collect bounded) \<subseteq> \<L> axiom  \<inter> Collect bounded"
          using \<open>purely_morphic_language (f ^^ Suc p) axiom \<subseteq> \<L> axiom\<close> by fastforce
        show " infinite (purely_morphic_language (f ^^ Suc p) axiom \<inter> Collect bounded)"
          using that
          unfolding endomorphism.pushy_def[OF pow_endomorphism] bounded_pow_bounded[symmetric].
      qed
    qed

    interpret fp: endomorphism "f^^(Suc p)"
      using pow_endomorphism by blast

    show "fp.pushy axiom" if "pushy axiom"
    proof(rule ccontr)
      assume "\<not> fp.pushy axiom"
      then obtain la where la_def: "\<And>w. w \<in> fp.pmor_lan axiom \<Longrightarrow> fp.bounded w \<Longrightarrow> \<^bold>|w\<^bold>| < la"
        by (meson fp.pushyI not_less_eq)

      define na where "na = (Suc (Suc (Suc la))) * \<lceil>f\<rceil>^(Suc p)"

      obtain wa where "wa \<in> \<L> axiom" "bounded wa" "\<^bold>|wa\<^bold>| > na"
        using \<open>pushy axiom\<close> assms(1) pushy_maxlen by blast

      have "wa \<noteq> \<epsilon>"
        using \<open>na < \<^bold>|wa\<^bold>|\<close> by force

      obtain t where "wa \<le>f (f^^t) axiom"
        using \<open>wa \<in> \<L> axiom\<close> pmor_lan_fac_pow by blast
      hence "wa \<le>f (f^^(t mod (Suc p))) (((f^^(Suc p))^^(t div (Suc p))) axiom)"
        unfolding funpow_mult funpow_add'' mod_mult_div_eq.

      let ?f = "f^^(t mod (Suc p))"
      interpret ft: endomorphism "?f"
        using pow_endomorphism by auto

      from ft.image_fac_interp'[OF \<open>wa \<le>f (f^^(t mod (Suc p))) (((f^^(Suc p))^^(t div (Suc p))) axiom)\<close> \<open>wa \<noteq> \<epsilon>\<close>]
      obtain pw w_pred sw where
        "w_pred \<le>f ((f ^^ Suc p) ^^ (t div Suc p)) axiom" and
        "pw wa sw \<sim>\<^sub>\<I> map (f ^^ (t mod Suc p))\<^sup>\<C> w_pred"
        by metis
      note fac_interpD[OF this(2), unfolded ft.morph_concat_map]

      have "w_pred \<in> fp.pmor_lan axiom"
        using \<open>w_pred \<le>f ((f ^^ Suc p) ^^ (t div Suc p)) axiom\<close> fp.pmor_lan_fac_pow by blast

      have "w_pred \<noteq> \<epsilon>"
        using \<open>pw \<cdot> wa \<cdot> sw = (f ^^ (t mod Suc p)) w_pred\<close> \<open>wa \<noteq> \<epsilon>\<close> by force

      have "t mod Suc p < Suc p"
        using mod_less_divisor by blast

      have "(Suc (Suc (Suc la))) \<le> \<^bold>|w_pred\<^bold>|"
      proof-
        have "(Suc (Suc (Suc la))) * \<lceil>f\<rceil> ^ Suc p < \<^bold>|w_pred\<^bold>| * \<lceil>f\<rceil> ^ Suc p"
          using le_trans[OF fac_len', of pw wa sw, unfolded lenarg[OF \<open>pw \<cdot> wa \<cdot> sw = ?f w_pred\<close>], OF ft.max_im_len_le_dom[OF \<open>finite UNIV\<close>, of w_pred]]  max_im_len_pow_le_dom[OF \<open>finite UNIV\<close>, of "(t mod Suc p)"]
          using \<open>\<^bold>|wa\<^bold>| > na\<close>
          unfolding na_def
          using \<open>t mod Suc p < Suc p\<close>[folded power_strict_increasing_iff[OF \<open>1 < \<lceil>f\<rceil>\<close>, of "t mod Suc p"]]
          by (meson less_imp_le_nat mult_le_mono2 order.strict_trans2)
        thus ?thesis
          by (meson less_imp_le_nat mult_less_cancel2)
      qed
      hence "2 \<le> \<^bold>|w_pred\<^bold>|"
        by linarith
      note wbutl =  hd_middle_last[OF this[folded One_less_Two_le_iff]]

      have "Suc la \<le> \<^bold>|butlast (tl w_pred)\<^bold>|"
        using \<open>Suc (Suc (Suc la)) \<le> \<^bold>|w_pred\<^bold>|\<close> by force
      hence "\<not> bounded (butlast (tl w_pred))"
        using \<open>w_pred \<in> fp.pmor_lan axiom\<close> la_def wbutl facI
        by (metis Suc_leD bounded_pow_bounded linorder_not_le purely_morphic_language.intros(3))

      from ft.image_fac_interp_mid[OF \<open>pw wa sw \<sim>\<^sub>\<I> map (f ^^ (t mod Suc p))\<^sup>\<C> w_pred\<close> \<open>2 \<le> \<^bold>|w_pred\<^bold>|\<close>]
      have "?f (butlast (tl w_pred)) \<le>f wa"
        by (metis sublist_def)
      thus False
        using \<open>\<not> bounded (butlast (tl w_pred))\<close> \<open>bounded wa\<close> endomorphism.bounded_fac_bounded endomorphism.bounded_image_pow endomorphism_axioms by metis
    qed
  qed
next
  assume "\<not> 1 < \<lceil>f\<rceil>"
  thus ?thesis
    using assms bounded_def bounded_pow_bounded endomorphism.pushy_pow_not_bounded pmor_lan_infin_growing pow_endomorphism pushy_infinite by blast
qed







subsection "Iterated prefixes and suffixes"

fun iterated_prefix where
  "iterated_prefix u 0 = \<epsilon>" |
  "iterated_prefix u (Suc i) = (f (iterated_prefix u i))\<cdot>u"

fun iterated_suffix where
  "iterated_suffix u 0 = \<epsilon>" |
  "iterated_suffix u (Suc i) = u\<cdot>(f (iterated_suffix u i))"

lemma iterated_prefix_suffix[reversal_rule]: "endomorphism.iterated_prefix (rev_map f) (rev u) k = rev (iterated_suffix u k)"
proof(induct k)
  case 0
  then show ?case
    unfolding endomorphism.iterated_prefix.simps[OF rev_map_endomorph]
      iterated_suffix.simps
    by simp
next
  case (Suc k)
  show ?case
    unfolding endomorphism.iterated_prefix.simps[OF rev_map_endomorph]
      iterated_suffix.simps
    unfolding rev_map_def comp_apply
    unfolding Suc[unfolded rev_map_def comp_apply]
    unfolding rev_append rev_map_arg rev_rev_ident..
qed



lemma iterated_suffix_prefix[reversal_rule]: "(endomorphism.iterated_suffix (rev_map f) (rev u) k) = rev (iterated_prefix u k)"
  by (metis endomorphism.iterated_prefix_suffix rev_map_endomorph rev_map_idemp rev_swap)

lemma iterated_prefix_switch: "f (iterated_prefix u l) \<cdot> u = (f ^^ l) u \<cdot> iterated_prefix u l"
proof(induct l, simp)
  case (Suc l)
  then show ?case
    using morph by auto
qed

lemma iterated_prefix_split: "iterated_prefix u (k+l) = (iterated_prefix ((f^^l) u) k)\<cdot>(iterated_prefix u l)"
proof(induct k)
  case 0
  then show ?case
    by simp
next
  case (Suc k)
  show ?case
    unfolding add_Suc
    unfolding iterated_prefix.simps
    unfolding Suc morph rassoc
    unfolding cancel
    using iterated_prefix_switch.
qed

lemma iterated_prefix_mono:  "k \<le> l \<Longrightarrow> iterated_prefix u k \<le>s iterated_prefix u l"
  using iterated_prefix_split[of u "l-k" k]
  by (simp add: suffixI)

lemma image_iterated_prefix: "f w = u\<cdot>w\<cdot>v \<Longrightarrow> (iterated_prefix u k)\<cdot>w \<le>p (f^^k) w"
proof(induct k)
  case 0
  then show ?case
    unfolding iterated_prefix.simps
    by simp
next
  case (Suc k)
  then show ?case
    unfolding iterated_prefix.simps
    using append_eq_appendI funpow_swap1 compow_Suc' morph pref_mono pref_shorten triv_pref by metis
qed

lemma image_iterated_prefix': "f w = u\<cdot>w \<Longrightarrow> (iterated_prefix u k)\<cdot>w = (f^^k) w"
proof(induct k)
  case 0
  then show ?case
    unfolding iterated_prefix.simps
    by simp
next
  case (Suc k)
  then show ?case
    unfolding iterated_prefix.simps
    unfolding append_assoc compow_Suc
    using morph by metis
qed

lemma iterated_prefix_bounded: "bounded u \<Longrightarrow> bounded (iterated_prefix u k)"
proof(induction k)
  case 0
  then show ?case
    unfolding iterated_prefix.simps
    using bounded_emp by auto
next
  case (Suc k)
  then show ?case
    unfolding iterated_prefix.simps
    using bounded_concat bounded_image by presburger
qed

lemmas iterated_suffix_switch = endomorphism.iterated_prefix_switch[OF rev_map_endomorph, reversed] and
  iterated_suffix_split = endomorphism.iterated_prefix_split[OF rev_map_endomorph, reversed] and
  iterated_suffix_mono = endomorphism.iterated_prefix_mono[OF rev_map_endomorph, reversed] and
  image_iterated_suffix = endomorphism.image_iterated_prefix[OF rev_map_endomorph, reversed] and
  image_iterated_suffix' = endomorphism.image_iterated_prefix'[OF rev_map_endomorph, reversed] and
  iterated_suffix_bounded = endomorphism.iterated_prefix_bounded[OF rev_map_endomorph, reversed]

lemma iterated_suffix_per: assumes coin: "(f^^n) u = (f^^(n+Suc p)) u"
  shows "iterated_suffix u (n+k*(Suc p)) = (iterated_suffix u n)\<cdot>(iterated_suffix ((f^^n) u) (Suc p))\<^sup>@k"
proof(induct k, simp)
  case (Suc k)
  note f_rew = funpow_coin_per[OF coin, of 0 k, unfolded add_0_right]
  have rew: "n + Suc k * Suc p = (n+k*Suc p) + Suc p"
    by simp
  show ?case
    unfolding rew add.commute[of _ "Suc p"]
    unfolding iterated_suffix_split[of _ "Suc p"]
    unfolding Suc rassoc
    unfolding cancel
    unfolding f_rew pow_Suc'..
qed

lemma iterated_suffix_fix: assumes coin: "(f^^n) u = (f^^(n+Suc p)) u"
  shows "(f^^(Suc p)) (iterated_suffix ((f^^n) u) k) = (iterated_suffix ((f^^n) u) k)"
proof(induction k)
  case 0
  show ?case
    unfolding iterated_suffix.simps
    by (simp add: pow_emp_to_emp)
  case (Suc k)
  then show ?case
    unfolding iterated_suffix.simps
    unfolding morphism.morph[OF pow_morphism, of "Suc p"]
    unfolding assms[unfolded funpow_add' comp_apply, symmetric]
    unfolding cancel
    by (simp add: funpow_swap1)
qed

lemmas iterated_prefix_per = endomorphism.iterated_suffix_per[OF rev_map_endomorph, reversed] and
  iterated_prefix_fix = endomorphism.iterated_suffix_fix[OF rev_map_endomorph, reversed]

end

section "Morphic languages"


text\<open>Morphic language is the factorial language of an HD0L-system.\<close>

definition morphic_language :: "('a list \<Rightarrow> 'a list) \<Rightarrow> ('a list \<Rightarrow> 'b list) \<Rightarrow> 'a list \<Rightarrow> 'b list set"
  where "morphic_language f h axiom \<equiv> factor_closure (h`purely_morphic_language f axiom)"

lemma mor_lan_fac: "factorial (morphic_language f h axiom)"
  by (simp add: fac_cloI_fac factorialI morphic_language_def)

lemma mor_lan_im_in: "w \<in> purely_morphic_language f axiom \<Longrightarrow> h w \<in> morphic_language f h axiom"
  unfolding morphic_language_def
  using fac_clo_sub_id imageI[of _ _ h] in_mono by blast


subsection "Locale"

locale endomorphism_morphism = endomorphism f + H:morphism h
  for f::"'a list \<Rightarrow> 'a list" and h::"'a list \<Rightarrow> 'b list"
begin

abbreviation mor_lan ("\<H> _ " [100])
  where "mor_lan \<equiv> morphic_language f h"

lemma morphic_language_cases[consumes 1, case_names axiom image factor]: assumes
    "x \<in> morphic_language f h axiom"
  obtains "x = h axiom" |
  w where "w \<in> purely_morphic_language f axiom" "x = h (f w)" |
  w where "w \<in> morphic_language f h axiom" "x \<le>f w" "x \<noteq> w"
proof-
  obtain v where "v \<in> h ` purely_morphic_language f axiom" "x \<le>f v"
    using factor_closure.cases[OF assms[unfolded morphic_language_def], of thesis]
    by presburger
  obtain q where "v = h q" "q \<in> purely_morphic_language f axiom"
    using \<open>v \<in> h ` purely_morphic_language f axiom\<close> by blast
  show thesis
  proof(cases rule: purely_morphic_language.cases[OF \<open>q \<in> purely_morphic_language f axiom\<close>])
    case 1
    then show ?thesis
      by (metis \<open>v = h q\<close> \<open>v \<in> h ` purely_morphic_language f axiom\<close> \<open>x \<le>f v\<close> factor_closure.intros morphic_language_def sublist_order.order_refl that(1) that(3))
  next
    case (2 w)
    then show ?thesis
      by (metis \<open>v = h q\<close> \<open>v \<in> h ` purely_morphic_language f axiom\<close> \<open>x \<le>f v\<close> factor_closure.intros morphic_language_def sublist_order.order_eq_iff that(2) that(3))
  next
    case (3 w u)
    from \<open>w \<in> \<L> axiom\<close>[unfolded pmor_lan_fac_pow]
    obtain k where "w \<le>f (f ^^ k) axiom"
      by blast
    show ?thesis
    proof(cases)
      assume "k = 0"
      hence "w \<le>f axiom"
        using \<open>w \<le>f (f ^^ k) axiom\<close> by auto
      show thesis
        using that
        using "3"(3) H.fac_mono \<open>v = h q\<close> \<open>w \<le>f axiom\<close> \<open>x \<le>f v\<close>   sublist_order.dual_order.trans sublist_order.order.eq_iff
        unfolding \<open>q = u\<close> morphic_language_def factor_closure.simps
        using imageI[OF purely_morphic_language.intros(1),of h axiom f] by metis
    next
      assume "k \<noteq> 0"
      have "w \<le>f f ((f ^^ (k-1)) axiom)"
        using  \<open>w \<le>f (f ^^ k) axiom\<close>
        unfolding compow_Suc[of "k-1" f, unfolded Suc_minus[OF \<open>k \<noteq> 0\<close>]].
      have "(f ^^ (k-1)) axiom \<in> \<L> axiom"
        using pmor_lan_pow_in by auto
      from that(2)[OF this] that(3)[of "h (f ((f ^^ (k - 1)) axiom))"]
      show thesis
        using
          H.fac_mono[OF fac_trans[OF \<open>u \<le>f w\<close> \<open>w \<le>f f ((f ^^ (k - 1)) axiom)\<close>]]
          \<open>(f ^^ (k - 1)) axiom \<in> \<L> axiom\<close>
          fac_trans[OF \<open>x \<le>f v\<close>]
          unfolding \<open>q = u\<close>[symmetric] \<open>v = h q\<close>[symmetric]
          unfolding compow_Suc[symmetric]
          using factor_closure.simps image_eqI morphic_language_def pmor_lan_pow_in sublist_order.order_refl by metis
    qed
  qed
qed

lemma morphic_language_induct[consumes 1]: assumes
    "x \<in> morphic_language f h axiom"
    "P (h axiom)" and
    ih2: "\<And>w. h w \<in> morphic_language f h axiom \<Longrightarrow> P (h w) \<Longrightarrow> P (h (f w))" and
    ih3: "\<And>w u. w \<in> morphic_language f h axiom \<Longrightarrow> P w \<Longrightarrow> u \<le>f w \<Longrightarrow> P u"
    shows "P x"
proof-
  obtain v where "v \<in> h ` purely_morphic_language f axiom" "x \<le>f v"
    using factor_closure.cases[OF \<open>x \<in> morphic_language f h axiom\<close>[unfolded morphic_language_def], of thesis]
    by presburger
  obtain q where "v = h q" "q \<in> purely_morphic_language f axiom"
    using \<open>v \<in> h ` purely_morphic_language f axiom\<close> by blast
  show ?thesis
    using \<open>q \<in> purely_morphic_language f axiom\<close> \<open>v = h q\<close> \<open>x \<le>f v\<close>
  proof(induct q arbitrary: v x)
    case 1
    then show ?case
      by (metis assms(2) assms(4) factor_closure.intros imageI morphic_language_def purely_morphic_language.intros(1) sublist_order.order_eq_iff)
  next
    case (2 w)
    show ?case
    proof(rule ih3[OF _ _ \<open>x \<le>f v\<close>])
      show "v \<in> \<H> axiom"
        using mor_lan_im_in "2.hyps"(1) "2.prems"(1) purely_morphic_language.intros(2)
        by blast
      show "P v"
        using ih2[of w] mor_lan_im_in "2.hyps"(1) "2.hyps"(2) "2.prems"(1) by blast
    qed
  next
    case (3 w u)
    then show ?case
      by (meson H.fac_mono fac_trans)
  qed
qed

lemma mor_lanE: assumes "w \<in> \<H> axiom"
  obtains k where "w \<le>f h ((f^^k) axiom)"
  using assms
  unfolding morphic_language_def factor_closure.simps image_def
  unfolding mem_Collect_eq
  using pmor_lan_fac_pow H.fac_mono fac_trans by metis

lemma mor_lanE': assumes "h w \<in> \<H> axiom"
  obtains v where "v \<in> \<L> axiom" "h w \<le>f h v"
  using assms
  by (meson mor_lanE pmor_lan_pow_in)

lemma mor_lan_emp: "x \<in> \<H> \<epsilon> \<Longrightarrow> x = \<epsilon>" for x
  by (metis H.emp_to_emp' mor_lanE pow_emp_to_emp sublist_Nil_right)

lemma mor_lan_emp': "\<H> \<epsilon> = {\<epsilon>}"
  by (simp add: fac_clo_emp morphic_language_def pmor_lan_emp')

lemma mor_lan_pow_in: "h ((f^^k) axiom) \<in> \<H> axiom"
  unfolding morphic_language_def factor_closure.simps
  by (meson imageI pmor_lan_pow_in sublist_order.order_refl)

lemma mor_lan_seq_in: "{h ((f^^l) axiom) | l. True} \<subseteq> \<H> axiom"
  using mor_lan_pow_in by force

lemma mor_lan_fac_pow: "w \<in> \<H> axiom \<longleftrightarrow> (\<exists>k. w \<le>f h ((f^^k) axiom))"
  by (metis factor_closure.intros image_eqI mor_lanE morphic_language_def pmor_lan_pow_in)

lemma mor_lan_iff: "w \<in> \<H> axiom \<longleftrightarrow> (w \<in> \<H> (f axiom) \<or> w \<le>f h axiom)"
  unfolding mor_lan_fac_pow
  unfolding funpow_swap1[symmetric]
  unfolding compow_Suc[symmetric]
  using Suc_minus funpow_0 by metis

lemma mor_lan_fac_clo_seq: "\<H> axiom = factor_closure {h ((f^^k) axiom) | k. True}"
  unfolding set_eq_iff
  using factor_closure.simps mor_lan_fac_pow by auto

lemma mor_lan_fac_sub: assumes "axiom' \<le>f axiom"
  shows "\<H> axiom' \<subseteq> \<H> axiom"
  using pmor_lan_fac_sub
  unfolding morphic_language_def
  by (meson assms factor_closure.simps image_mono subsetD subsetI)

lemma mor_lan_same_pow: assumes "k < k'" "(f^^k) axiom = (f^^k') axiom"
  shows "\<H> axiom = factor_closure (h`{(f^^l) axiom | l. l < k'})"
  unfolding morphic_language_def pmor_lan_same_pow[OF assms]
  unfolding H.fac_clo_im..

lemma mor_lan_same_pow': assumes "k < k'" "h ((f^^k) axiom) = h ((f^^k') axiom)"
  shows "\<H> axiom = factor_closure (h`{(f^^l) axiom | l. l < k'})"
  oops
lemma mor_lan_infin_iff: "infinite (\<H> axiom) = infinite {h ((f^^k) axiom) | k. True}"
  using fac_clo_finite mor_lan_fac_clo_seq by auto


lemma bound_occ_prim_unb_exp: assumes "has_uniformly_bounded_occurrences (\<L> axiom)" "primitive_unbounded_exponent (\<H> axiom) w" "finite (UNIV::'a set)" "\<lceil>h\<rceil> \<noteq> 0"
  shows "\<H> axiom = {v. \<exists>k. v \<le>f w\<^sup>@k}"
proof
  show "{v. \<exists>k. v \<le>f w \<^sup>@ k} \<subseteq> \<H> axiom"
    unfolding subset_iff mem_Collect_eq
    using prim_unb_expE(2)[OF assms(2)] fac_cloI_fac
    unfolding mor_lan_fac_clo_seq by blast
  show "\<H> axiom  \<subseteq> {v. \<exists>k. v \<le>f w \<^sup>@ k}"
  proof
    fix x
    assume "x \<in> \<H> axiom"
    obtain v where "v \<in> \<L> axiom" "x \<le>f h v"
      by (meson \<open>x \<in> \<H> axiom\<close> mor_lan_fac_pow pmor_lan_pow_in)
    obtain k where k_def: "\<And>w. w \<in> \<L> axiom \<Longrightarrow> \<^bold>|w\<^bold>| \<ge> k \<Longrightarrow> v \<le>f w"
      by (meson \<open>v \<in> \<L> axiom\<close> assms(1) bounded_occE)

    let ?w = "w\<^sup>@((Suc (Suc k))*\<lceil>h\<rceil>)"
    obtain z' where "?w \<le>f h z'" "z' \<in> \<L> axiom"
      using prim_unb_expE(2)[OF \<open>primitive_unbounded_exponent (\<H> axiom) w\<close>, of "Suc (Suc k)*\<lceil>h\<rceil>"]
      by (meson mor_lanE pmor_lan_pow_in)

    have "w \<noteq> \<epsilon>"
      using prim_unb_expE(1)[OF \<open>primitive_unbounded_exponent (\<H> axiom) w\<close>] emp_not_prim by blast
    hence "(Suc (Suc k))*\<lceil>h\<rceil> \<le> \<^bold>|?w\<^bold>|"
      unfolding pow_len
      using nemp_le_len by auto
    from H.long_enough_preimage[OF finite_imageI, OF finite_imageI, OF \<open>finite UNIV\<close> \<open>?w \<le>f h z'\<close> this \<open>\<lceil>h\<rceil> \<noteq> 0\<close>]
    obtain z where "z \<in> \<L> axiom" "\<^bold>|z\<^bold>| \<ge> k" "h z \<le>f ?w"
      using \<open>z' \<in> \<L> axiom\<close>
      by (metis purely_morphic_language.intros(3))

    note k_def[OF \<open>z \<in> \<L> axiom\<close> \<open>\<^bold>|z\<^bold>| \<ge> k\<close>]
    hence "h v \<le>f h z"
      by (simp add: H.fac_mono)
    from \<open>x \<le>f h v\<close> this \<open>h z \<le>f ?w\<close>
    have "x \<le>f ?w"
      by (meson fac_trans)
    thus "x \<in> {v. \<exists>k. v \<le>f w \<^sup>@ k}"
      by blast
  qed
qed

lemma mor_lan_fac_interp: assumes
  "w \<in> \<H> axiom" "w \<noteq> \<epsilon>"
  obtains p w_pred s where "w_pred \<in> \<L> axiom" "p w s \<sim>\<^sub>\<I> (map h\<^sup>\<C> w_pred)"
  using H.image_fac_interp'[OF _ assms(2)] assms(1)  pmor_lan_fac_pow
  unfolding mor_lan_fac_pow by metis

end
end
