(*  Title:      Morphic_Language_Unbounded_Exponent
    File:       CoW_Infinite.Morphic_Language_Unbounded_Exponent
    Author:     Štěpán Starosta, Czech Technical University in Prague

Part of Combinatorics on Words Formalized. See https://gitlab.com/formalcow/combinatorics-on-words-formalized/
*)

theory Morphic_Language_Unbounded_Exponent
  imports Languages
begin

context endomorphism
begin

section "Subalphabet of image powers"

abbreviation pow_subalph_l :: "nat \<Rightarrow> 'a \<Rightarrow> 'a set"
  where "pow_subalph_l i a \<equiv> set ((f^^i) ([a]))"

lemma pow_subalph_l_core:  "pow_subalph_l i a = set ((f^^i)\<^sup>\<C> a)"
  by (simp add: core_sing)

lemma pow_subalph_l_eq: "pow_subalph_l i = (set) \<circ> ((f^^i)\<^sup>\<C>)"
  unfolding comp_apply core_def..

lemma pow_subalph_l_add: "(pow_subalph_l (i+k) a) = \<Union> (set`((f^^k)\<^sup>\<C>`(pow_subalph_l i a)))"
  by (metis add.commute funpow_add'' morphism.set_core_set pow_morphism)


lemma pow_subalph_l_mult: "(pow_subalph_l (i*k) a) = endomorphism.pow_subalph_l (f^^k) i a"
  by (simp add: funpow_mult mult.commute)


lemma pow_subalph_l_add': "(pow_subalph_l (i+k) a) = \<Union> ((pow_subalph_l k)`(pow_subalph_l i a))"
  unfolding pow_subalph_l_eq[of k]
  by (simp add: pow_subalph_l_add)


lemma pow_subalph_l_sub: "(pow_subalph_l i a) \<subseteq> (pow_subalph_l j a) \<Longrightarrow>
  (pow_subalph_l (i+k) a) \<subseteq> (pow_subalph_l (j+k) a)"
  unfolding pow_subalph_l_add
  by blast

lemma pow_subalph_l_el_sub: "a \<in> (pow_subalph_l i b) \<Longrightarrow> (pow_subalph_l k a) \<subseteq> (pow_subalph_l (i+k) b)"
  by (metis add.commute fac_mono_pow set_mono_sublist split_list' sublist_appendI)


lemma pow_subalph_l_pow_same: "pow_subalph_l i a = pow_subalph_l j a \<Longrightarrow> pow_subalph_l (i+k) a = pow_subalph_l (j+k) a"
proof(induction k, simp)
  case (Suc k)
  from pow_sets_im[OF Suc.prems, of "Suc k"]
  show ?case
    unfolding funpow_add'' add_Suc
    by (simp add: add.commute)
qed

lemma pow_subalph_l_pow_same_per: "pow_subalph_l i a = pow_subalph_l (i+p) a \<Longrightarrow>
  pow_subalph_l i a = pow_subalph_l (i+k*p) a"
proof(induct k, simp)
  case (Suc k)
  from pow_subalph_l_pow_same[OF Suc.hyps[OF Suc.prems], of p]
  show ?case
    unfolding Suc.prems add_ac(1)[of i] mult_Suc add.commute[of p].
qed

definition pow_subalph_fixes
  where "pow_subalph_fixes Q p \<equiv>
        \<forall>a i k. Q \<le> i \<longrightarrow> pow_subalph_l i a = pow_subalph_l (i+k*p) a"

lemma pow_subalph_fixesE: assumes "pow_subalph_fixes Q p"
  shows "\<And>a i k. Q \<le> i \<Longrightarrow> pow_subalph_l i a = pow_subalph_l (i+k*p) a"
  using assms pow_subalph_fixes_def by blast

corollary pow_subalph_l_ev_per_all: assumes "finite (UNIV::'a set)"
    obtains Q p where "pow_subalph_fixes Q p" "p > Q" "p > 0"
proof-
  let ?seq = "\<lambda>a i. pow_subalph_l i a"

  have fin_range: "w \<in> UNIV \<Longrightarrow> finite (range (?seq w))" for w
    by (meson assms finite_UnionD rev_finite_subset subset_UNIV)

  have ev_per_all: "\<exists>n. \<forall>i j k. n \<le> i \<and> n \<le> j \<and> (?seq w) i = (?seq w) j \<longrightarrow> (?seq w) (i+k) = (?seq w) (j+k)" for w
    using pow_subalph_l_pow_same[of _ w]
    by blast

  from finite_stable_ev_per_all[OF \<open>finite UNIV\<close>, of ?seq,
      OF fin_range ev_per_all]
  obtain q p where
      qp: "\<And>a i k. q \<le> i \<Longrightarrow> pow_subalph_l i a = pow_subalph_l (i + k * p) a" "p > 0"
    by (meson UNIV_I zero_less_Suc)

  have "q < Suc q * p"
    using \<open>p > 0\<close> Suc_le_eq less_numeral_extra(3) mult.commute quotient_smaller by metis
  have "0 < Suc q * p"
    by (simp add: qp(2))

  show thesis
    using that[of q "(Suc q *  p)"]
    unfolding pow_subalph_fixes_def
    using qp(1)[of _ _ "k*Suc q" for k] \<open>0 < Suc q * p\<close> \<open>q < Suc q * p\<close>
    unfolding mult.assoc
    by presburger
qed

lemma pow_subalph_fixes_pow: assumes "pow_subalph_fixes Q p" "p > 0"
  shows "endomorphism.pow_subalph_fixes (f^^p) Q 1"
      proof-
  have subfix: "Q \<le> i \<Longrightarrow> pow_subalph_l i a = pow_subalph_l (i + k * p) a" for i a k
    using assms pow_subalph_fixes_def by auto



  interpret fp: endomorphism "f^^p"
    using pow_endomorphism by blast

  have he: "p*i + k*p = p * (i + k * 1)" for i k
    by (simp add: add_mult_distrib2)
  show "fp.pow_subalph_fixes Q 1"
    using subfix[of "p*i" for i]
    unfolding fp.pow_subalph_fixes_def
    unfolding funpow_mult he
    using assms(2) le_trans quotient_smaller by blast
qed

lemma pow_subalph_l_fixes_all: assumes "pow_subalph_fixes Q p" "Q \<le> p" "p > 0" "k > 0"
  shows "(pow_subalph_l p a) = (pow_subalph_l (k*p) a)"
proof-
  have pa_fixes: "\<And>a i k. Q \<le> i \<Longrightarrow> pow_subalph_l i a = pow_subalph_l (i+k*p) a"
    using assms(1) unfolding pow_subalph_fixes_def by blast
  show ?thesis
    using pa_fixes[OF \<open>Q \<le> p\<close>, of a "k-1"]
    using assms(3) assms(4) mult_eq_if by auto
qed

lemma pow_subalph_fixes_pow': assumes "pow_subalph_fixes Q p" "Q \<le> p" "p > 0"
  shows "endomorphism.pow_subalph_fixes (f^^p) 1 1"
proof-
        note subfix = pow_subalph_l_fixes_all[OF assms]

  interpret fp: endomorphism "f^^p"
    using pow_endomorphism by blast

  show "fp.pow_subalph_fixes 1 1"
    unfolding fp.pow_subalph_fixes_def
    unfolding funpow_mult mult.commute[of p]
    using subfix subfix[of "(i + k * 1)" for i k]
    by (metis add_pos_nonneg ge1_pos_conv le_add1 le_add_same_cancel1)
qed















section "Invariant subalphabets"

definition invariant_subalphabet where
  "invariant_subalphabet p B = (((\<Union> (pow_subalph_l p ` B) ) = B) \<and> (\<exists>a. a \<in> B \<and> \<not> bounded [a]))"

lemma inv_subI[intro]: assumes "((\<Union> (pow_subalph_l p ` B) ) = B)" "(\<exists>a. a \<in> B \<and> \<not> bounded [a])"
  shows "invariant_subalphabet p B"
  unfolding invariant_subalphabet_def
  using assms(1) assms(2) by blast

lemma inv_subI': assumes "((\<Union> (pow_subalph_l p ` B) ) = B)" " a \<in> B" "\<not> bounded [a]"
  shows "invariant_subalphabet p B"
  unfolding invariant_subalphabet_def
  using assms(1) assms(2) assms(3) by blast

lemma inv_sub_eq: "invariant_subalphabet p B = (((\<Union> (set ` (((f^^p)\<^sup>\<C>) ` B))) = B) \<and> (\<exists>a. a \<in> B \<and> \<not> bounded [a]))"
  using invariant_subalphabet_def pow_subalph_l_core by auto

lemma inv_subE1: assumes "invariant_subalphabet p B" "a \<in> B"
  shows "set ( (f^^p) [a]) \<subseteq> B"
  using assms unfolding invariant_subalphabet_def core_def
  using  Sup_upper image_eqI
  by blast

lemma inv_subE2: assumes "invariant_subalphabet p B"
  obtains a where "a \<in> B" "\<not> bounded [a]"
  using assms unfolding invariant_subalphabet_def
  by blast

lemma inv_sub_nemp: assumes "invariant_subalphabet p B"
  shows "B \<noteq> {}"
  using assms inv_subE2 by auto

lemma pow_subalph_inv_sub: assumes "\<not> bounded [a]" "pow_subalph_fixes Q p" "Q \<le> p"
  shows "invariant_subalphabet p (pow_subalph_l p a)"
  using One_nat_def \<open>\<not> bounded [a]\<close>
    bounded_image_pow
    pow_subalph_l_add
    unbounded_letter
    lambda_one
    pow_subalph_fixesE[OF \<open>pow_subalph_fixes Q p\<close> \<open>Q \<le> p\<close>]
  unfolding inv_sub_eq
  by metis

lemma inv_sub_ex: assumes finite_UNIV: "finite (UNIV::'a set)" and "\<not> bounded [a]" "pow_subalph_fixes Q p" "Q \<le> p"
  obtains B b where "invariant_subalphabet p B" "b \<in> B" "\<not> bounded [b]" "b \<in> set ((f^^p) [a])"
  by (meson assms(2) assms(3) assms(4) inv_subE2 pow_subalph_inv_sub)





lemma inv_sub_un: "invariant_subalphabet p A \<Longrightarrow> invariant_subalphabet p B \<Longrightarrow> invariant_subalphabet p (A \<union> B)"
  unfolding invariant_subalphabet_def
  by auto

definition invariant_subalphabet_bot where
  "invariant_subalphabet_bot p B \<equiv> (invariant_subalphabet p B \<and> (\<forall>C. C \<subset> B \<longrightarrow> \<not> invariant_subalphabet p C))"

lemma inv_sub_botE: assumes "invariant_subalphabet_bot p B"
  shows "invariant_subalphabet p B" "\<And>C. C \<subset> B \<Longrightarrow>  \<not> invariant_subalphabet p C"
  using assms invariant_subalphabet_bot_def by blast+

lemma inv_sub_botI[intro]: assumes "invariant_subalphabet p B" "\<And>C. C \<subset> B \<Longrightarrow>  \<not> invariant_subalphabet p C"
  shows "invariant_subalphabet_bot p B"
  using assms invariant_subalphabet_bot_def by blast

lemma inv_sub_bot_ex:
  assumes finite_UNIV: "finite (UNIV::'a set)" and "invariant_subalphabet p A"
  obtains B where "invariant_subalphabet_bot p B" "B \<subseteq> A"
proof-
  define C where "C = Collect (\<lambda>x. invariant_subalphabet p x \<and> x \<subseteq> A)"
  have "finite C"
    by (meson finite_UnionD finite_UNIV rev_finite_subset subset_UNIV)
  have "C \<noteq> {}"
    using \<open>invariant_subalphabet p A\<close> subset_refl[of A]
    unfolding C_def by blast
  from finite_has_minimal[OF \<open>finite C\<close> this]
  obtain M where "M \<in> C" and Mmin: "Q \<in> C \<Longrightarrow> Q \<subseteq> M \<longrightarrow> M = Q" for Q
    by blast
  have "invariant_subalphabet_bot p M"
    using Mmin psubsetE subset_trans \<open>M \<in> C\<close>
    unfolding invariant_subalphabet_bot_def mem_Collect_eq C_def
    by metis
  have "M \<subseteq> A"
    using C_def \<open>M \<in> C\<close> by blast
  show thesis
    using \<open>M \<subseteq> A\<close> \<open>invariant_subalphabet_bot p M\<close> that by blast
qed


lemma inv_sub_bot_ex':
  assumes finite_UNIV: "finite (UNIV::'a set)" and "\<not> bounded [a]" "pow_subalph_fixes Q p" "Q \<le> p"
  obtains B where "invariant_subalphabet_bot p B" "B \<subseteq> pow_subalph_l p a"
  using inv_sub_bot_ex[OF finite_UNIV pow_subalph_inv_sub[OF \<open>\<not> bounded [a]\<close> \<open>pow_subalph_fixes Q p\<close> \<open>Q \<le> p\<close>]].






lemma inv_sub_bot_pow_subalph:
  assumes finite_UNIV: "finite (UNIV::'a set)" and "invariant_subalphabet_bot p B"
  "\<not> bounded [a]" "a \<in> B" "pow_subalph_fixes Q p" "Q \<le> p"
shows "pow_subalph_l p a = B"
  using assms pow_subalph_inv_sub
  unfolding  invariant_subalphabet_bot_def inv_sub_eq
  using psubsetI
  by (metis (mono_tags, opaque_lifting) Sup_upper core_def imageI)





lemma non_pushy_subset_unbounded_image:
  assumes finite_UNIV: "finite (UNIV::'a set)" and
    "W \<subseteq> \<L> axiom" and
    la: "\<And>w v. w \<in> W \<Longrightarrow> v \<le>f w \<Longrightarrow>  bounded v \<Longrightarrow> \<^bold>|v\<^bold>| \<le> la"
  obtains n0 where
    "\<And>w. w \<in> W \<Longrightarrow> n0 \<le> \<^bold>|w\<^bold>| \<Longrightarrow> (\<exists>b. \<not> bounded [b] \<and> [b] \<in> \<L> axiom \<and> (f^^r) [b] \<le>f w)"
proof(cases "finite W")
  assume "finite W"
              then show ?thesis
      using finite_maxlen[of W] linorder_not_less that
      by metis
                                      next
  assume "infinite W"

  let ?f = "(f^^r)"
  interpret fr: endomorphism "?f"
    using pow_endomorphism by blast

  have "\<not> bounded axiom"
    unfolding bounded_def
    using \<open>infinite W\<close> assms(2) infinite_super by blast
  hence "axiom \<noteq> \<epsilon>"
    using bounded_emp by blast

  have "1 < \<lceil>f\<rceil>"
    using \<open>\<not> bounded axiom\<close> growing_maxlen local.finite_UNIV unbounded_growing by blast


  note frle = max_im_len_pow_le_dom[OF finite_UNIV, of r]

  define qa where "qa = Suc (2*\<lceil>f\<rceil>^r*\<^bold>|axiom\<^bold>| + la)"
            hence "qa \<ge> 2"
    using Suc_le_eq \<open>1 < \<lceil>f\<rceil>\<close>
    by (simp add: \<open>axiom \<noteq> \<epsilon>\<close>)



  have "(\<exists>b. \<not> bounded [b] \<and> [b] \<in> \<L> axiom \<and> ?f [b] \<le>f w)" if "w \<in> W" "qa \<le> \<^bold>|w\<^bold>|" for w
  proof-
    have "\<^bold>|w\<^bold>| \<ge> 2"
      using \<open>qa \<ge> 2\<close> \<open>qa \<le> \<^bold>|w\<^bold>|\<close>
      by force
    hence "w \<noteq> \<epsilon>" by auto
            have  "2*\<lceil>f\<rceil>^r* \<^bold>|axiom\<^bold>| < \<^bold>|w\<^bold>|"
      using \<open>qa \<le> \<^bold>|w\<^bold>|\<close> add_leE
      unfolding qa_def
      by linarith
    hence qa'': "\<lceil>f\<rceil>^r * \<^bold>|axiom\<^bold>| < \<^bold>|w\<^bold>|"
      using \<open>2 \<le> \<^bold>|w\<^bold>|\<close>
      by linarith

    have qa': "Suc (2*\<lceil>f\<rceil>^r) \<le> \<^bold>|w\<^bold>|"
      using \<open>qa \<le> \<^bold>|w\<^bold>|\<close> qa''
      unfolding qa_def
      using \<open>2 * \<lceil>f\<rceil> ^ r * \<^bold>|axiom\<^bold>| < \<^bold>|w\<^bold>|\<close> \<open>axiom \<noteq> \<epsilon>\<close> le_trans length_0_conv mult.commute not_less not_less_eq_eq quotient_smaller by metis

    have "\<not> bounded w"
      using la[OF \<open>w \<in> W\<close>, of w] \<open>qa \<le> \<^bold>|w\<^bold>|\<close> \<open>1 < \<lceil>f\<rceil>\<close>
      unfolding qa_def
      by (meson not_less_eq_eq sublist_order.order.eq_iff trans_le_add2)

    obtain t where "w \<le>f (f^^t) axiom"
      by (meson \<open>w \<in> W\<close> assms(2) pmor_lan_fac_pow subset_eq)
    from fac_len[OF this]
    have "r < t"
      using qa'' morphism.max_im_len_le_dom[OF pow_morphism finite_UNIV, of t axiom]
        max_im_len_pow_le_dom[OF finite_UNIV, of t] power_strict_increasing_iff[OF \<open>1 < \<lceil>f\<rceil>\<close>]
      by (smt (verit, best) linorder_not_le mult.commute mult_le_mono1 order.strict_trans2)
    hence "w \<le>f ?f ((f^^(t-r)) axiom)"
      using \<open>w \<le>f (f ^^ t) axiom\<close>
      by (simp add: funpow_add'')
    from fr.image_fac_interp'[OF this \<open>w \<noteq> \<epsilon>\<close>]
    obtain pw w_pred sw where
      "w_pred \<le>f (f ^^ (t-r)) axiom" and
      "pw w sw \<sim>\<^sub>\<I> map ?f\<^sup>\<C> w_pred"
      by metis
    note fE = fac_interpD[OF this(2), unfolded fr.morph_concat_map]

    have "w_pred \<noteq> \<epsilon>"
      using fE(3) \<open>w \<noteq> \<epsilon>\<close> pow_emp_to_emp by force
    moreover have "\<^bold>|w\<^bold>| \<le> \<^bold>|?f w_pred\<^bold>|"
      by (metis fE(3) fac_len')
    moreover have "\<^bold>|w_pred\<^bold>| \<noteq> 1"
      using max_im_len_le_dom[OF finite_UNIV, of w_pred] qa' Suc_1 Suc_n_not_le_n calculation(2) dual_order.trans endomorphism.max_im_len_pow_le'_dom endomorphism_axioms local.finite_UNIV mult_Suc trans_le_add2 by metis
    ultimately have "2 \<le> \<^bold>|w_pred\<^bold>|"
      by (metis One_less_Two_le_iff nat_less_le nemp_le_len)
    note butl = hd_middle_last[OF this[folded One_less_Two_le_iff]]

    obtain pw' sw' where
      w_butl: "w = pw' \<cdot> ?f (butlast (tl w_pred)) \<cdot> sw'" and
      pw': "pw \<cdot> pw' = ?f [hd w_pred]" and
      sw': "sw'\<cdot> sw = ?f [last w_pred]"
      using fr.image_fac_interp_mid[OF \<open>pw w sw \<sim>\<^sub>\<I> map ?f\<^sup>\<C> w_pred\<close> \<open>2 \<le> \<^bold>|w_pred\<^bold>|\<close>]
      by blast
    from lenarg[OF this(1)]  le_trans[OF pref_len[OF prefI[OF this(3)]]] le_trans[OF suf_len[OF sufI[OF this(2)]]]
    have "\<^bold>|w\<^bold>| \<le> \<lceil>?f\<rceil> + (\<^bold>|?f (butlast (tl w_pred))\<^bold>| + \<lceil>?f\<rceil>)"
      using fr.max_im_len_le_sing_dom[OF finite_UNIV]
      unfolding lenmorph
      by (simp add: add_mono)
    hence wle: "\<^bold>|w\<^bold>| \<le> 2*\<lceil>?f\<rceil> + \<^bold>|?f (butlast (tl w_pred))\<^bold>|"
      by linarith

    have "\<not> bounded (butlast (tl w_pred))"
    proof(rule ccontr)
      assume " \<not> \<not> bounded (butlast (tl w_pred))"
      hence "bounded (?f (butlast (tl w_pred)))"
        using bounded_image_pow by blast
      hence "\<^bold>|?f (butlast (tl w_pred))\<^bold>| \<le> la"
        using la[OF \<open>w \<in> W\<close> facI'[OF w_butl[symmetric]]] by blast
      hence "\<^bold>|w\<^bold>| \<le> 2*\<lceil>?f\<rceil> + la"
        using wle
        by linarith
      hence "\<^bold>|w\<^bold>| \<le> 2*\<lceil>f\<rceil>^r + la"
        using frle
        by linarith
      from le_trans[OF \<open>qa \<le> \<^bold>|w\<^bold>|\<close> this]
      show False
        unfolding qa_def
        by (metis \<open>axiom \<noteq> \<epsilon>\<close> add_le_mono le_refl length_0_conv mult.commute not_less_eq_eq quotient_smaller)
    qed
    then obtain p'' b s'' where "butlast (tl w_pred) = p'' \<cdot> [b] \<cdot> s''" "\<not> bounded [b]"
      by (meson split_list' unbounded_letter)
    hence "?f [b] \<le>f w"
      by (metis fac_ext fr.fac_mono sublist_appendI w_butl)
    moreover have "[b] \<in> \<L> axiom"
      using \<open>butlast (tl w_pred) = p'' \<cdot> [b] \<cdot> s''\<close> w_butl \<open>w \<le>f (f ^^ t) axiom\<close>
      by (metis \<open>w_pred \<le>f (f ^^ (t-r)) axiom\<close> factorial_def pmor_lan_fac pmor_lan_pow_in sublist_appendI sublist_butlast sublist_tl)
    ultimately show ?thesis
      using \<open>\<not> bounded [b]\<close> by blast
  qed
  thus ?thesis
    using that by blast
qed


lemma inv_sub_bot_bounded_occ: assumes finite_UNIV: "finite (UNIV::'a set)" and
  "invariant_subalphabet_bot p B" "\<not> bounded [a]" "a \<in> B" "pow_subalph_fixes Q p" "Q \<le> p" "p > 0"
  "\<not> pushy [a]"
shows "has_uniformly_bounded_occurrences (purely_morphic_language (f^^p) ((f^^t) [a]))"
proof
  let ?f = "f^^p"
  let ?axiom = "((f^^t) [a])"
  interpret f: endomorphism ?f
    using pow_endomorphism by blast
  note strong = pow_subalph_l_fixes_all[OF \<open>pow_subalph_fixes Q p\<close> \<open>Q \<le> p\<close> \<open>p > 0\<close>]
  note bot = inv_sub_bot_pow_subalph[OF finite_UNIV \<open>invariant_subalphabet_bot p B\<close> \<open>\<not> bounded [a]\<close> \<open>a \<in> B\<close> \<open>pow_subalph_fixes Q p\<close> \<open>Q \<le> p\<close>]

  have "\<not> pushy ?axiom"
    using pushy_shift assms(8) by blast
  then obtain l0 where l0_def: "\<And>w. w \<in> \<L> ?axiom \<Longrightarrow> bounded w \<Longrightarrow> \<^bold>|w\<^bold>| \<le> l0"
    by (meson linorder_not_le pushyI)

  obtain la where la_def: "\<And>w. w \<in> \<L> [a] \<Longrightarrow> bounded w \<Longrightarrow> \<^bold>|w\<^bold>| \<le> la"
    by (meson assms(8) linorder_not_le pushyI)

  have "\<^bold>|?axiom\<^bold>| \<noteq> 0"
    by (metis assms(3) bounded_emp bounded_image_pow nemp_len_not0)
        have bounded_bounded: "bounded w = f.bounded w" for w
    using bounded_pow_bounded \<open>p > 0\<close> less_imp_Suc_add by blast
  hence "\<lceil>?f\<rceil> > 1"
    using assms(3) f.growing_maxlen f.unbounded_growing local.finite_UNIV by fastforce


  have "set ((?f^^i) [a]) \<subseteq> B" for i
  proof(cases "i = 0")
    case True
    then show ?thesis
      using assms(4) by auto
  next
    case False
    then show ?thesis
      using strong[of i a]
      unfolding bot pow_subalph_l_mult
      by blast
  qed


  have "(\<And>w v. w \<in> f.pmor_lan [a] \<Longrightarrow> v \<le>f w \<Longrightarrow> f.bounded v \<Longrightarrow> \<^bold>|v\<^bold>| \<le> la)"
    using bounded_bounded endomorphism.pmor_lan_fac_pow endomorphism_axioms f.endomorphism_axioms funpow_mult la_def purely_morphic_language.intros(3) by (metis (no_types, opaque_lifting))
  then obtain n0 where
    n0_def: "(\<And>w. w \<in> f.pmor_lan [a] \<Longrightarrow> n0 \<le> \<^bold>|w\<^bold>| \<Longrightarrow> \<exists>b. \<not> f.bounded [b] \<and> [b] \<in> f.pmor_lan [a] \<and> ?f [b] \<le>f w)"
    using f.non_pushy_subset_unbounded_image[OF finite_UNIV, of "f.pmor_lan [a]" "[a]" la 1, unfolded funpow_1]
    by blast

  have a_ev: "[a] \<le>f w" if "w \<in> f.pmor_lan [a]" "n0 \<le> \<^bold>|w\<^bold>|" for w
  proof-
    obtain b where "\<not> f.bounded [b]" "[b] \<in> f.pmor_lan [a]" "?f [b] \<le>f w"
      using n0_def[OF \<open>w \<in> f.pmor_lan [a]\<close> \<open>n0 \<le> \<^bold>|w\<^bold>|\<close>] by blast
    hence "\<not> bounded [b]"
      using bounded_bounded by blast
    have "b \<in> B"
      using \<open>[b] \<in> f.pmor_lan [a]\<close> \<open>\<And>i. set (((f ^^ p) ^^ i) [a]) \<subseteq> B\<close>
      unfolding f.pmor_lan_fac_pow
      by (metis (no_types, lifting) dual_order.trans insert_subset list.set(2) set_mono_sublist)
    show ?thesis
      using inv_sub_bot_pow_subalph[OF finite_UNIV \<open>invariant_subalphabet_bot p B\<close> \<open>\<not> bounded [b]\<close> \<open>b \<in> B\<close> \<open>pow_subalph_fixes Q p\<close> \<open>Q \<le> p\<close>]
      by (metis \<open>(f ^^ p) [b] \<le>f w\<close> assms(4) split_list' sublist_def sublist_order.dual_order.trans)
  qed

 show "\<exists>k. \<forall>w\<in>f.pmor_lan ?axiom. k \<le> \<^bold>|w\<^bold>| \<longrightarrow> v \<le>f w" if "v \<in> f.pmor_lan ?axiom" for v
  proof-
    obtain l where "v \<le>f (?f^^l) ?axiom"
      using \<open>v \<in> f.pmor_lan ((f ^^ t) [a])\<close> f.pmor_lan_fac_pow by blast

    let ?k = " \<lceil>?f\<rceil>^l * \<lceil>f^^t\<rceil> * (Suc (Suc n0))"
    note morphism.max_im_len_le_dom[OF pow_morphism finite_UNIV, of "t" "[a]", unfolded sing_len mult_1]

    have "?k \<noteq> 0"
      using \<open>1 < \<lceil>f ^^ p\<rceil>\<close> \<open>\<^bold>|(f ^^ t) [a]\<^bold>| \<le> \<lceil>f ^^  t\<rceil>\<close> \<open>\<^bold>|(f ^^ t) [a]\<^bold>| \<noteq> 0\<close> bot_nat_0.extremum_unique nat.discI nat_less_le no_zero_divisors power_not_zero
      by metis

    have "\<lceil>f ^^ p\<rceil> ^ l * \<^bold>|(f ^^ t) [a]\<^bold>| \<le> \<lceil>f ^^ p\<rceil> ^ l * \<lceil>f ^^ t\<rceil>"
      using \<open>\<^bold>|(f ^^ t) [a]\<^bold>| \<le> \<lceil>f ^^  t\<rceil>\<close> mult_le_mono2 by blast
    moreover have "0 < \<lceil>f ^^ p\<rceil> ^ l * \<lceil>f ^^ t\<rceil>"
      using \<open>\<lceil>f ^^  p\<rceil> ^ l * \<lceil>f ^^ t\<rceil> * Suc (Suc n0) \<noteq> 0\<close> by force
    ultimately have hek: "\<lceil>?f\<rceil>^l * \<^bold>|(f^^t) [a]\<^bold>| < ?k"
      using n_less_n_mult_m[OF \<open>0 < \<lceil>f ^^  p\<rceil> ^ l * \<lceil>f ^^ t\<rceil>\<close>, of "Suc (Suc n0)"]
      by linarith

    have "v \<le>f w" if "w \<in> f.pmor_lan ?axiom" "?k \<le> \<^bold>|w\<^bold>|" for w
    proof-
      obtain t' where "w \<le>f (?f^^t') ?axiom"
        using \<open>w \<in> f.pmor_lan ?axiom\<close> f.pmor_lan_fac_pow by blast

        have "w \<noteq> \<epsilon>"
          using \<open>?k \<le> \<^bold>|w\<^bold>|\<close>
          by (metis \<open>?k \<noteq> 0\<close> le_0_eq list.size(3))

      have "l \<le> t'"
        using
          le_trans[OF \<open>?k \<le> \<^bold>|w\<^bold>|\<close>
            le_trans[OF fac_len[OF \<open>w \<le>f (?f^^t') ?axiom\<close>]
              morphism.max_im_len_le_dom[OF pow_morphism finite_UNIV, of "p*t'", folded funpow_mult]]]
          \<open>\<^bold>|?axiom\<^bold>| \<noteq> 0\<close>
          \<open>\<lceil>?f\<rceil> > 1\<close>
        using hek
        mult_le_mono1[OF f.max_im_len_pow_le_dom[OF finite_UNIV, of t'],  of "\<^bold>|(f ^^t) [a]\<^bold>|" ]
        unfolding mult.commute[of "\<^bold>|(f ^^ t) [a]\<^bold>|"]
        by (meson le_trans less_imp_le_nat mult_le_cancel2 neq0_conv power_increasing_iff)
      hence "w \<le>f (?f^^(l)) ((?f^^(t'-l)) ?axiom)"
        using \<open>w \<le>f ((f ^^  p) ^^ t') ((f ^^  t) [a])\<close>
        by (simp add: funpow_add'')
      hence "w \<le>f (f^^(l* p + t)) ((?f^^(t'-l)) [a])"
        unfolding funpow_mult
        using add.commute funpow_add'' mult.commute by metis
      from morphism.image_fac_interp'[OF pow_morphism this \<open>w \<noteq> \<epsilon>\<close>]
      obtain pw w_pred sw where
        "w_pred \<le>f ((f ^^  p) ^^ (t' - l)) [a]" and
        "pw w sw \<sim>\<^sub>\<I> map (f ^^ (l *  p +  t))\<^sup>\<C> w_pred"
        by metis
      note fac_interpD[OF this(2), unfolded morphism.morph_concat_map[OF pow_morphism]]
      have "Suc (Suc n0) \<le> \<^bold>|w_pred\<^bold>|"
      proof(rule ccontr)
        assume "\<not> Suc (Suc n0) \<le> \<^bold>|w_pred\<^bold>|"
        hence "\<^bold>|w_pred\<^bold>| <  Suc (Suc n0)"
          by simp
        have "\<^bold>|(f ^^ (l * p + t)) w_pred\<^bold>| < ?k"
          unfolding funpow_add comp_apply
          using  mult_le_mono1[OF f.max_im_len_pow_le_dom[OF finite_UNIV, of l]]
                 le_trans[OF morphism.max_im_len_le_dom[OF pow_morphism finite_UNIV, of "l*p", unfolded mult.commute[of l], folded funpow_mult, of "((f ^^t) w_pred)"]
                 mult_le_mono1[OF morphism.max_im_len_le_dom[OF pow_morphism finite_UNIV, of "t" w_pred]]]
          unfolding mult.commute[of l]
          unfolding funpow_mult[symmetric]
          unfolding mult.commute[of "\<^bold>|w_pred\<^bold>|"] mult.assoc
          unfolding mult.commute[of _ "\<lceil>(f ^^  p) ^^ l\<rceil>"] mult.assoc[symmetric]
          using mult_less_mono2[OF \<open>\<^bold>|w_pred\<^bold>| <  Suc (Suc n0)\<close>]
          by (smt (verit, del_insts) \<open>\<lceil>f ^^  p\<rceil> ^ l * \<lceil>f ^^ t\<rceil> * Suc (Suc n0) \<noteq> 0\<close> \<open>\<not> Suc (Suc n0) \<le> \<^bold>|w_pred\<^bold>|\<close> dual_order.trans length_greater_0_conv list.size(3) mult_le_mono nat_0_less_mult_iff nle_le not_less)
                    thus False
          by (metis (no_types, opaque_lifting) \<open>pw \<cdot> w \<cdot> sw = (f ^^ (l * p + t)) w_pred\<close> dual_order.trans fac_len not_less sublist_appendI that(2))
      qed
      hence "w_pred = [hd w_pred] \<cdot> butlast (tl w_pred) \<cdot> [last w_pred]"
        using hd_middle_last[symmetric] by force
      have "n0 \<le> \<^bold>|butlast (tl w_pred)\<^bold>| "
        using \<open>Suc (Suc n0) \<le> \<^bold>|w_pred\<^bold>|\<close> by force
      from a_ev[OF _ this]
      have "[a] \<le>f butlast (tl w_pred)"
        by (metis \<open>w_pred = [hd w_pred] \<cdot> butlast (tl w_pred) \<cdot> [last w_pred]\<close> \<open>w_pred \<le>f ((f ^^  p) ^^ (t' - l)) [a]\<close> f.pmor_lan_pow_in purely_morphic_language.intros(3) sublist_appendI)

                    have "(f ^^ (l * p +  t)) (butlast (tl w_pred)) \<le>f w"
        using fac_interp_mid_fac[OF \<open>pw w sw \<sim>\<^sub>\<I> map (f ^^ (l *  p + t))\<^sup>\<C> w_pred\<close>]
        by (metis endomorphism.pow_morphism endomorphism_axioms map_butlast map_tl morphism.morph_concat_map)
      from fac_trans[OF morphism.fac_mono[OF pow_morphism \<open>[a] \<le>f butlast (tl w_pred)\<close>] this]
      show ?thesis
        using fac_trans[OF \<open>v \<le>f (?f^^l) ?axiom\<close>]
        unfolding funpow_mult mult.commute[of l]
        unfolding funpow_add comp_apply
        by blast
    qed
    thus ?thesis
      by blast
  qed
qed

end


section "Graphs of unbounded letters"

text\<open>Give a letter a, it is of interest to see the first (from the left or the right) unbounded letter that occurs in the image @{term "f [a]"}, if there it exists.

\<close>

context endomorphism
begin

definition first_unbounded_left :: "'a \<Rightarrow> 'a"
  where "first_unbounded_left a \<equiv> the_find (\<lambda> x. \<not> bounded [x]) (f [a])"

definition first_unbounded_right :: "'a \<Rightarrow> 'a"
 where "first_unbounded_right a \<equiv> the_find_rev (\<lambda> x. \<not> bounded (rev [x])) (f [a])"

lemma first_unbounded_right_left[reversal_rule]: "endomorphism.first_unbounded_right (rev_map f) = first_unbounded_left"
  unfolding endomorphism.first_unbounded_right_def[OF rev_map_endomorph] first_unbounded_left_def
  unfolding the_find_rev_def
  unfolding bounded_rev
  by (simp add: rev_map_sing)

lemma first_unbounded_left_right[reversal_rule]: "endomorphism.first_unbounded_left (rev_map f) = first_unbounded_right"
  using endomorphism.first_unbounded_right_left[OF rev_map_endomorph] by fastforce

lemma first_unbounded_left_ex: assumes "\<not> bounded [a]"
  obtains u v where "bounded u" "\<not> bounded [first_unbounded_left a]" "f [a] = u\<cdot>[first_unbounded_left a]\<cdot>v"
  using the_find_ex[OF assms[unfolded bounded_image[of "[a]"],
      unfolded bounded_list_all[of "f [a]"]]]
  unfolding bounded_list_all[symmetric] first_unbounded_left_def comp_def
  by metis

lemma first_unbounded_left_pow_ex: assumes "\<not> bounded [a]"
  obtains u v where "bounded u" "\<not> bounded [(first_unbounded_left^^k) a]" "(f^^k) [a] = u\<cdot>[(first_unbounded_left^^k) a]\<cdot>v"
proof(induction k arbitrary: thesis)
  case 0
  then show ?case
    unfolding funpow.simps
    using \<open>\<not> bounded [a]\<close> bounded_emp by force
next
  case (Suc k)
  obtain u v where
    "bounded u" and
    "\<not> bounded [(first_unbounded_left ^^ k) a]" and
    "(f ^^ k) [a] = u \<cdot> [(first_unbounded_left ^^ k) a] \<cdot> v"
    using Suc(1)
    by blast
  show ?case
    using Suc(2)
    unfolding funpow.simps comp_apply
    using first_unbounded_left_ex[OF \<open>\<not> bounded [(first_unbounded_left ^^ k) a]\<close>]
    unfolding \<open>(f ^^ k) [a] = u \<cdot> [(first_unbounded_left ^^ k) a] \<cdot> v\<close>
    using \<open>bounded u\<close> append_assoc bounded_image endomorphism.bounded_concat[OF endomorphism_axioms] unfolding morph by smt
qed

lemma first_unbounded_left_pow: "\<not> bounded [a] \<Longrightarrow> ((endomorphism.first_unbounded_left (f^^Suc k)) a) = ((first_unbounded_left^^Suc k) a)"
  unfolding endomorphism.first_unbounded_left_def[OF pow_endomorphism] first_unbounded_left_def
  unfolding bounded_pow_bounded[symmetric]
proof(induction k)
  case 0
  show ?case
    unfolding funpow.simps comp_apply
    by simp
next
  case (Suc k)
  show ?case
    unfolding funpow.simps(2)[of "Suc k"] comp_apply
    using the_find_conv[of "\<lambda>w. bounded [w]" "(f ^^ Suc k) [a]", unfolded bounded_list_all[symmetric]]
    using Suc.IH Suc.prems bounded_image bounded_image_pow
    unfolding comp_def
    by force
qed

lemma first_unbounded_leftI: assumes "\<not> bounded [a]" "\<not> bounded [b]" "bounded u" "f [a] = u\<cdot>[b]\<cdot>v"
  shows "first_unbounded_left a = b"
  unfolding first_unbounded_left_def \<open>f [a] = u\<cdot>[b]\<cdot>v\<close> the_find_hd_dropWhile
  using \<open>bounded u\<close>[unfolded bounded_list_all[of u], folded Ball_set, unfolded Ball_def, rule_format]
    dropWhile_append2[of u "(\<lambda>x. bounded [x])" "[b]\<cdot>v"]
   \<open>\<not> bounded [b]\<close>
  by force


lemma first_unbounded_leftI_pow: assumes "\<not> bounded [a]" "\<not> bounded [b]" "bounded u" "(f^^k) [a] = u\<cdot>[b]\<cdot>v"
  shows "(first_unbounded_left^^k) a = b"
proof(cases rule: Nat.nat.exhaust[of k])
  case zero
  then show ?thesis
    by (metis assms(4) funpow_0 hd_word nemp_comm snoc_sublist_snoc sublist_appendI sublist_code(2))
next
  case (Suc k')
  show ?thesis
    unfolding \<open>k = Suc k'\<close>
    using endomorphism.first_unbounded_leftI[OF pow_endomorphism, of "Suc k'", unfolded bounded_pow_bounded[symmetric], OF assms[unfolded \<open>k = Suc k'\<close>]]
    endomorphism.first_unbounded_leftI[OF pow_endomorphism, of "Suc k'",  unfolded bounded_pow_bounded[symmetric]]
    using  first_unbounded_left_pow_ex assms(1) bounded_pow_bounded by metis
qed

lemma first_unbounded_left_word: assumes "\<not> bounded w"
  shows "the_find (\<lambda> x. \<not> bounded [x]) (f w) = first_unbounded_left (the_find (\<lambda> x. \<not> bounded [x]) w)"
  using assms
proof(induction w, (simp add: bounded_emp))
  case (Cons a w)
  show ?case
    unfolding hd_word[of a w] morph
  proof(cases)
    assume "bounded [a]"
    hence "bounded (f [a])"
      using bounded_image by auto
    have "\<not> bounded w"
      using \<open>bounded [a]\<close> bounded_concat local.Cons(2) by fastforce
    have "dropWhile (\<lambda>a. bounded [a]) (f [a] \<cdot> f w) = dropWhile (\<lambda>a. bounded [a]) (f w)"
      using \<open>bounded (f [a])\<close>[unfolded bounded_list_all[of "f [a]"] Ball_set[symmetric]]
        dropWhile_append2[of "f [a]" "\<lambda>a. bounded [a]"]
      by blast
    thus "the_find (\<lambda>x. \<not> bounded [x]) (f [a] \<cdot> f w) = first_unbounded_left (the_find (\<lambda>x. \<not> bounded [x]) ([a] \<cdot> w))"
      using Cons.IH[OF \<open>\<not> bounded w\<close>]
          \<open>bounded [a]\<close>[unfolded bounded_list_all[of "[a]"] Ball_set[symmetric]]
      unfolding the_find_hd_dropWhile comp_def
      by simp
  next
    assume "\<not> bounded [a]"
    hence "\<not> bounded (f [a])"
      using bounded_image by auto
    hence "\<not> list_all ((\<lambda>x. bounded [x])) (f [a])"
      using bounded_list_all by blast
    hence da: "the_find (\<lambda>x. \<not> bounded [x]) (f [a] \<cdot> f w) = the_find (\<lambda>x. \<not> bounded [x]) (f [a])"
      unfolding the_find_hd_dropWhile comp_def
      by (metis the_find_takeWhile list.sel(2) not_Cons_self2 pref_hd_eq prefix_def prefix_dropWhile self_append_conv takeWhile_dropWhile_id)
    have db: "the_find (\<lambda>x. \<not> bounded [x]) ([a] \<cdot> w) = a"
      unfolding the_find_def
      by (simp add: \<open>\<not> bounded [a]\<close>)
    show "the_find (\<lambda>x. \<not> bounded [x]) (f [a] \<cdot> f w) = first_unbounded_left (the_find (\<lambda>x. \<not> bounded [x]) ([a] \<cdot> w))"
      unfolding da db first_unbounded_left_def..
  qed
qed

lemma first_unbounded_left_pow_conv: assumes "\<not> bounded [a]"
  shows "endomorphism.first_unbounded_left (f^^Suc k) a = (first_unbounded_left^^Suc k) a"
  unfolding endomorphism.first_unbounded_left_def[OF pow_endomorphism] first_unbounded_left_def
    bounded_pow_bounded[of _ k, symmetric]
proof(induction k)
  case 0
  then show ?case
    unfolding funpow.simps comp_def id_def..
next
  case (Suc k)
  obtain u v where "bounded u" "\<not> bounded [(first_unbounded_left ^^ Suc k) a]"
    "(f ^^ Suc k) [a] = u \<cdot> [(first_unbounded_left ^^ Suc k) a] \<cdot> v"
    using first_unbounded_left_pow_ex[OF \<open>\<not> bounded [a]\<close>, of "Suc k"] by blast
  have "bounded (f u)"
    using \<open>bounded u\<close> bounded_image by auto
  have dw: "dropWhile (\<lambda>x. bounded [x]) (f u \<cdot> w) = dropWhile (\<lambda>x. bounded [x]) w"
      "dropWhile (\<lambda>x. bounded [x]) (u \<cdot> w) = dropWhile (\<lambda>x. bounded [x]) w" for w
    using \<open>bounded (f u)\<close>[unfolded bounded_list_all[of "f u"]]
          \<open>bounded (u)\<close>[unfolded bounded_list_all[of "u"]]
          dropWhile_append2
    unfolding Ball_set[symmetric]
    by force+
  show ?case
    unfolding funpow.simps(2)[of "Suc k"]
    unfolding Suc[symmetric] comp_apply
    unfolding the_find_def
    unfolding \<open>(f ^^ Suc k) [a] = u \<cdot> [(first_unbounded_left ^^ Suc k) a] \<cdot> v\<close>
    unfolding morph
    unfolding dw
    unfolding morph[symmetric]
    using first_unbounded_left_def first_unbounded_left_word \<open>(f ^^ Suc k) [a] = u \<cdot> [(first_unbounded_left ^^ Suc k) a] \<cdot> v\<close> \<open>bounded u\<close>
      assms bounded_concat the_find_def bounded_image_pow by metis qed

lemma first_unbounded_left_powI: assumes "\<not> bounded [a]" "\<not> bounded [b]" "bounded u" "(f^^k) [a] = u\<cdot>[b]\<cdot>v"
  shows "(first_unbounded_left^^k) a = b"
  using assms(2-4)
proof(induction k arbitrary: u b v)
  case 0
  then show ?case
    by (metis append_eq_Cons_conv funpow_0 list.distinct(1) list.inject  suf_nemp)
next
  case (Suc k)
  then show ?case
    using first_unbounded_left_pow_ex[of a "Suc k"] assms(1) endomorphism.first_unbounded_leftI[OF pow_endomorphism, of "Suc k"]
    unfolding bounded_pow_bounded[of _ k, symmetric]
      by metis
qed

lemma first_unbounded_right_ex: assumes "\<not> bounded [a]"
  obtains u v where "bounded u" "\<not> bounded [first_unbounded_right a]" "f [a] = v\<cdot>[first_unbounded_right a]\<cdot>u"
  using endomorphism.first_unbounded_left_ex[OF rev_map_endomorph, reversed, OF \<open>\<not> bounded [a]\<close>]
  unfolding rassoc
  by auto

lemma first_unbounded_rigth_pow_ex: assumes "\<not> bounded [a]"
  obtains u v where "bounded u" "\<not> bounded [(first_unbounded_right^^k) a]" "(f^^k) [a] = v\<cdot>[(first_unbounded_right^^k) a]\<cdot>u"
  using endomorphism.first_unbounded_left_pow_ex[OF rev_map_endomorph, reversed, OF \<open>\<not> bounded [a]\<close>]
  unfolding rassoc
  by auto





lemma first_unbounded_left_fixes: assumes "finite (UNIV::'a set)"
  obtains G p where
    "\<And>a i k. G \<le> i \<Longrightarrow> (first_unbounded_left^^i) a = (first_unbounded_left^^(i+k*(Suc p))) a"
proof-
  let ?seq = "\<lambda>a i. (first_unbounded_left^^i) a"

  have fr: "finite (range (?seq a))" for a
    using assms rev_finite_subset by auto

  have "?seq a i = ?seq a j \<Longrightarrow> ?seq a (i+k) = ?seq a (j+k)" for a i j k
    by (smt (verit, ccfv_threshold) add.commute comp_apply funpow_add)
  hence ev_all: "\<exists>n. \<forall>i j k. n \<le> i \<and> n \<le> j \<and> ?seq w i = ?seq w j \<longrightarrow> ?seq w (i + k) = ?seq w (j + k)" for w
    by blast

  from  finite_stable_ev_per_all[OF \<open>finite (UNIV::'a set)\<close>, of ?seq thesis,OF fr ev_all]
  show thesis
    using that by blast
qed


lemmas
first_unbounded_right_pow_conv = endomorphism.first_unbounded_left_pow_conv[OF rev_map_endomorph, reversed,
  unfolded endomorphism.first_unbounded_left_right[OF pow_endomorphism]]  andfirst_unbounded_right_fixes =  endomorphism.first_unbounded_left_fixes[OF rev_map_endomorph, reversed]

definition first_unbounded_both_fixed
  where "first_unbounded_both_fixed G p \<equiv> \<forall>a i k. G \<le>i \<longrightarrow> (
              ((first_unbounded_left^^i) a = (first_unbounded_left^^(i+k*(Suc p))) a) \<and>
              ((first_unbounded_right^^i) a = (first_unbounded_right^^(i+k*(Suc p))) a))"

lemma first_unbounded_both_fixedE: assumes "first_unbounded_both_fixed G p"
  shows "\<And> a i k. G \<le>i \<Longrightarrow> (first_unbounded_left^^i) a = (first_unbounded_left^^(i+k*(Suc p))) a"
        "\<And> a i k. G \<le>i \<Longrightarrow> (first_unbounded_right^^i) a = (first_unbounded_right^^(i+k*(Suc p))) a"
  using first_unbounded_both_fixed_def assms by blast+


lemma first_unbounded_both_fixedEx: assumes "finite (UNIV::'a set)"
  obtains G p where
            "first_unbounded_both_fixed G p"
proof-
  obtain G p where
    left: "\<And>a i k. G \<le> i \<Longrightarrow> (first_unbounded_left^^i) a = (first_unbounded_left^^(i+k*(Suc p))) a"
    using first_unbounded_left_fixes assms by blast
  obtain G' p' where
    right: "\<And>a i k. G' \<le> i \<Longrightarrow> (first_unbounded_right^^i) a = (first_unbounded_right^^(i+k*(Suc p'))) a"
    using first_unbounded_right_fixes assms by blast

  have "Suc p * Suc p' \<noteq> 0"
    by simp
  show thesis
    using that[of "G+G'" "(Suc p * Suc p' - 1)"]
    unfolding first_unbounded_both_fixed_def
    unfolding Suc_minus[OF \<open>Suc p * Suc p' \<noteq> 0\<close>]
    using left[where ?k = "k*Suc p'" for k]
          right[where ?k = "k*Suc p" for k]
    unfolding mult.assoc mult.commute[of "Suc p"]
    by force
qed

lemmas first_unbounded_both_fixedEx' = first_unbounded_both_fixedEx[unfolded first_unbounded_both_fixed_def, rule_format]

end
section "Endomorphism with fixed bound"


locale endomorphism_fixed = endomorphism +
  fixes bf_N bf_p g_N g_p
  assumes
  finite_UNIV: "finite (UNIV::'a set)" and
  bounded_fix: "bounded_fixed bf_N bf_p" and
  first_unbounded_both_fix: "first_unbounded_both_fixed g_N g_p"
begin


lemmas
  bounded_fixed = bounded_fix[unfolded bounded_fixed_def, rule_format] and
  first_unbounded_both_fixed = first_unbounded_both_fix[unfolded first_unbounded_both_fixed_def, rule_format]

lemma rev_map_endomorph:  "endomorphism_fixed (rev_map f) bf_N bf_p g_N g_p"
  using first_unbounded_both_fixed first_unbounded_left_right first_unbounded_right_left bounded_fix bounded_fixed_rev_map endomorphism.first_unbounded_both_fixed_def endomorphism_axioms endomorphism_fixed.finite_UNIV endomorphism_fixed.intro endomorphism_fixed_axioms endomorphism_fixed_axioms_def rev_map_endomorph by fastforce

definition fixed_unbounded_left
  where "fixed_unbounded_left \<equiv> (first_unbounded_left^`{g_N..}`(Collect (\<lambda>x. \<not> bounded [x])))"

definition fixed_unbounded_right
  where "fixed_unbounded_right \<equiv> endomorphism_fixed.fixed_unbounded_left (rev_map f) g_N"

lemmas fixed_unbounded_right_def[symmetric, reversal_rule]
lemma [reversal_rule]: "endomorphism_fixed.fixed_unbounded_right (rev_map f) g_N  = fixed_unbounded_left"
  using endomorphism_fixed.fixed_unbounded_right_def[OF rev_map_endomorph]
  by auto

lemma fixed_unbounded_left_unbouded: "a \<in> fixed_unbounded_left \<Longrightarrow> \<not> bounded [a]"
  unfolding fixed_unbounded_left_def
  using first_unbounded_left_pow_ex by blast

lemma fixed_unbounded_left_fixed: assumes
  "a \<in> fixed_unbounded_left"
  shows "(first_unbounded_left^^(k * Suc g_p)) a = a"
proof-
  obtain i y where "a = ((first_unbounded_left^^i) y)" "g_N \<le> i" "\<not> bounded [y]"
    using assms unfolding atLeast_def mem_Collect_eq
    unfolding fixed_unbounded_left_def
    by blast
  from first_unbounded_both_fixed[OF \<open>g_N \<le> i\<close>, of y]
  show ?thesis
    unfolding add.commute[of i] funpow_add comp_apply
    unfolding \<open>a = ((first_unbounded_left^^i) y)\<close>[symmetric]
    by presburger
qed

lemma unbounded_fixed_unbounded_left: assumes "\<not> bounded [a]" "g_N \<le> i"
  shows "(first_unbounded_left^^i) a \<in> fixed_unbounded_left"
  using assms(1) assms(2) fixed_unbounded_left_def by auto


lemma shows "first_unbounded_left ` fixed_unbounded_left = fixed_unbounded_left"
    unfolding fixed_unbounded_left_def
    unfolding atLeast_def
    unfolding mem_Collect_eq
proof
  show "first_unbounded_left ` \<Union> {(first_unbounded_left ^^ i) ` {x. \<not> bounded [x]} |i. g_N \<le> i}
    \<subseteq> \<Union> {(first_unbounded_left ^^ i) ` {x. \<not> bounded [x]} |i. g_N \<le> i}" (is "?A \<subseteq> ?B")
  proof
    fix x
    assume "x \<in> ?A"
    then obtain i y where "x = first_unbounded_left ((first_unbounded_left^^i) y)" "g_N \<le> i" "\<not> bounded [y]"
      by blast
    thus "x \<in> ?B"
      unfolding comp_apply[symmetric, of first_unbounded_left] funpow.simps[symmetric]
      unfolding Union_iff
      unfolding image_def
      using le_SucI[OF \<open>g_N \<le> i\<close>]
      by (metis (mono_tags, lifting) mem_Collect_eq)
  qed
  show "?B \<subseteq> ?A"
  proof
    fix x
    assume "x \<in> ?B"
    then obtain i y where "x = ((first_unbounded_left^^i) y)" "g_N \<le> i"  "\<not> bounded [y]"
      by blast
    have "x = (first_unbounded_left ^^ (i + 1 * Suc (g_p))) y"
      using first_unbounded_both_fixed[OF\<open>g_N \<le> i\<close>, of y 1]
      unfolding \<open>x = ((first_unbounded_left^^i) y)\<close>
      by blast
    hence "x = first_unbounded_left ((first_unbounded_left ^^ (i + 1 * Suc (g_p) - 1)) y)"
      by simp
    have "g_N \<le> (i + 1 * Suc (g_p) - 1)"
      by (simp add: \<open>g_N \<le> i\<close> trans_le_add1)
    thus "x \<in> ?A"
      using \<open>x = first_unbounded_left ((first_unbounded_left ^^ (i + 1 * Suc g_p - 1)) y)\<close>
        \<open>\<not> bounded [y]\<close>
      unfolding image_iff
      by blast
  qed
qed

subsection "Pushy factors produced by unbounded letters"

lemma fixed_unbounded_left_per:
  obtains U where
    "finite U"
    "U \<subseteq> {x. bounded x}"
    "\<And>a. a \<in> fixed_unbounded_left \<Longrightarrow> \<exists>z w.
        z \<in> U \<and> w \<in> U \<and>
        (\<forall>k. z\<^sup>@k \<cdot> w \<cdot> [a] \<le>p (f^^( (Suc (g_p)*(bf_N))+k*(Suc g_p * Suc bf_p))) [a])  \<and>
                  (f^^(Suc g_p * Suc bf_p)) z = z \<and> z\<cdot>w\<cdot>[a] \<le>p (f^^(Suc g_p * Suc bf_p)) (w\<cdot>[a])"
proof-
  define SQ where "SQ = Suc (g_p)"
  let ?P = "\<lambda> a z w. bounded z \<and> bounded w \<and>
                  (\<forall>k. z\<^sup>@k \<cdot> w \<cdot> [a] \<le>p (f^^( SQ*bf_N+k*(SQ * Suc bf_p))) [a]) \<and>
                  (f^^(SQ * Suc bf_p)) z = z \<and> z\<cdot>w\<cdot>[a] \<le>p (f^^(SQ * Suc bf_p)) (w\<cdot>[a])"

  let ?Pr = "\<lambda>a r. ?P a (fst r) (snd r)"
  define obt
    where "obt a = (SOME r. ?Pr a r)" for a

  have allPr: "\<And>a. a \<in> fixed_unbounded_left \<Longrightarrow> ?Pr a (obt a)"
  proof-
    fix a
    assume "a \<in> fixed_unbounded_left"
    then obtain z w where "?P a z w"
    proof-

      have "\<not> bounded [a]"
        using fixed_unbounded_left_unbouded[OF \<open>a \<in> fixed_unbounded_left\<close>].

      note a_fixed = fixed_unbounded_left_fixed[OF \<open>a \<in> fixed_unbounded_left\<close>]

      obtain u v where "bounded u" "((f^^SQ) [a]) = u\<cdot>[a]\<cdot>v"
        using first_unbounded_left_pow_ex[OF \<open>\<not> bounded [a]\<close>]
          a_fixed[of 1, unfolded mult_1]
        unfolding SQ_def
        by metis

      let ?f = "f ^^ SQ"
      interpret fp: endomorphism ?f
        using pow_endomorphism.

      have "(?f^^bf_N) u = (?f^^(bf_N + Suc bf_p)) u"
        using bounded_fixed[OF \<open>bounded u\<close>, of "SQ*bf_N" "SQ"]
        unfolding add_mult_distrib2[symmetric]
        unfolding funpow_mult SQ_def
        by fastforce
      note ip = fp.iterated_prefix_per[OF this] and it = fp.iterated_prefix_fix[OF this, of "Suc bf_p"]

      let ?z = "fp.iterated_prefix (((f ^^ SQ) ^^ bf_N) u) (Suc bf_p)"
      let ?w = "fp.iterated_prefix u bf_N"

      have im: "\<And>k. (?z \<^sup>@ k \<cdot> ?w) \<cdot> [a] \<le>p (f ^^ (SQ * bf_N  + k * SQ * Suc bf_p)) [a]"
        using fp.image_iterated_prefix[OF \<open>((f^^SQ) [a]) = u\<cdot>[a]\<cdot>v\<close>,
            of "bf_N + k * Suc bf_p" for k]
        unfolding ip funpow_mult add_mult_distrib2
        unfolding mult.commute[of _ SQ] mult.assoc.



      have "fp.bounded u"
        using bounded_pow_bounded SQ_def \<open>bounded u\<close> by blast
      hence b1: "bounded ?z"
        using SQ_def bounded_pow_bounded fp.bounded_image_pow fp.iterated_prefix_bounded by blast

      have b2: "bounded ?w"
        using SQ_def \<open>fp.bounded u\<close> bounded_pow_bounded fp.iterated_prefix_bounded by blast
      hence "bounded ((f^^(SQ * Suc bf_p)) ?w)"
        using bounded_image_pow by auto

      have b3: "bounded (fp.iterated_prefix u (Suc bf_p))"
        using SQ_def \<open>fp.bounded u\<close> bounded_pow_bounded fp.iterated_prefix_bounded by blast

      have "(f^^(SQ * Suc bf_p)) (?w\<cdot>[a]) \<bowtie> ?z\<cdot>?w\<cdot>[a]"
        using morphism.pref_mono[OF pow_morphism im[of 0], of "SQ * Suc bf_p"] im[of 1]
        unfolding mult_0 add_0_right emp_simps mult_1 exp_simps
        by (simp add: add.commute funpow_add'' prefix_comparable_def prefix_same_cases)

      have "\<not> bounded ((f^^(SQ * Suc bf_p)) (?w\<cdot>[a]))"
        using \<open>\<not> bounded [a]\<close> bounded_fac_bounded bounded_image_pow by blast
      hence "\<not> (f^^(SQ * Suc bf_p)) (?w\<cdot>[a]) \<le>p ?z\<cdot>?w"
        by (meson b1 b2 bounded_concat bounded_fac_bounded prefix_imp_sublist)
      hence iw: "?z\<cdot>?w\<cdot>[a] \<le>p (f^^(SQ * Suc bf_p)) (?w\<cdot>[a])"
        by (metis \<open>(f ^^ (SQ * Suc bf_p)) (fp.iterated_prefix u bf_N \<cdot> [a]) \<bowtie> fp.iterated_prefix (((f ^^ SQ) ^^ bf_N) u) (Suc bf_p) \<cdot> fp.iterated_prefix u bf_N \<cdot> [a]\<close> last_no_split rassoc pref_compE)

      show thesis
        using that[of "(fp.iterated_prefix (((f ^^ SQ) ^^ bf_N) u) (Suc bf_p))" "(fp.iterated_prefix u bf_N)"] b1 b2 im it iw
        unfolding mult.assoc
        by (metis rassoc funpow_mult)
    qed
    hence "?Pr a (z,w)"
      by simp
    hence "\<exists> r. ?Pr a r"
      by blast
    from someI_ex[OF this]
    show "?Pr a (obt a)"
      unfolding obt_def
      by blast
  qed

  have fin: "finite fixed_unbounded_left"
    by (meson local.finite_UNIV rev_finite_subset top_greatest)
  hence finR: "finite (obt ` fixed_unbounded_left)" (is "finite ?R")
    by blast

  define U where "U = (fst`?R \<union> snd`?R)"
  have "finite U"
    unfolding U_def using finR by blast

  have "\<And>a. a \<in> fixed_unbounded_left \<Longrightarrow> bounded (fst (obt a))"
       "\<And>a. a \<in> fixed_unbounded_left \<Longrightarrow> bounded (snd (obt a))"
    using allPr by blast+
  hence "U \<subseteq> Collect bounded"
    unfolding Ball_Collect[symmetric] Ball_def
    unfolding U_def
    by blast

  have Na_good: "\<And>a. a \<in> fixed_unbounded_left \<Longrightarrow> \<exists>z w.
        z \<in> U \<and> w \<in> U \<and> (\<forall>k. z\<^sup>@k \<cdot> w \<cdot> [a] \<le>p (f^^(SQ*bf_N+k*(SQ*Suc bf_p))) [a]) \<and>
                  (f^^(SQ * Suc bf_p)) z = z \<and> z\<cdot>w\<cdot>[a] \<le>p (f^^(SQ * Suc bf_p)) (w\<cdot>[a])"
  proof-
    fix a
    assume "a \<in> fixed_unbounded_left"
    have "fst (obt a) \<in> U"
      unfolding U_def
      using \<open>a \<in> fixed_unbounded_left\<close> by blast
    have "snd (obt a) \<in> U"
      unfolding U_def
      using \<open>a \<in> fixed_unbounded_left\<close> by blast

    have "(\<forall>k. (fst (obt a)) \<^sup>@ k \<cdot> (snd (obt a)) \<cdot> [a] \<le>p (f ^^ (SQ * bf_N + k * (SQ * Suc bf_p))) [a]) \<and>
                  (f^^(SQ * Suc bf_p)) (fst (obt a)) = (fst (obt a)) \<and> (fst (obt a))\<cdot>(snd (obt a))\<cdot>[a] \<le>p (f^^(SQ * Suc bf_p)) ((snd (obt a))\<cdot>[a])"
      using allPr[OF \<open>a \<in> fixed_unbounded_left\<close>]
      by presburger
    thus "\<exists>z w. z \<in> U \<and> w \<in> U \<and> (\<forall>k. z \<^sup>@ k \<cdot> w \<cdot> [a] \<le>p (f ^^ (SQ * bf_N + k * (SQ * Suc bf_p))) [a]) \<and>
                  (f^^(SQ * Suc bf_p)) z = z \<and> z\<cdot>w\<cdot>[a] \<le>p (f^^(SQ * Suc bf_p)) (w\<cdot>[a])"
      using \<open>fst (obt a) \<in> U\<close> \<open>snd (obt a) \<in> U\<close> by blast
  qed

  from that[OF \<open>finite U\<close> \<open>U \<subseteq> Collect bounded\<close>] Na_good
  show thesis
    unfolding SQ_def by blast
qed

corollary unbounded_left_per:
  obtains U where
    "finite U"
    "U \<subseteq> {x. bounded x}"
    "\<And>b. \<not> bounded [b] \<Longrightarrow> \<exists>z w v a.
        z \<in> U \<and> w \<in> U \<and> a \<in> fixed_unbounded_left \<and> v \<in> U \<and>
        (\<forall>k. v \<cdot> z\<^sup>@k \<cdot> w \<cdot> [a] \<le>p (f^^(g_N+ (Suc (g_p)*(bf_N))+k*(Suc g_p * Suc bf_p))) [b])  \<and>
        (\<forall>k. z\<^sup>@k \<cdot> w \<cdot> [a] \<le>p (f^^((Suc (g_p)*(bf_N))+k*(Suc g_p * Suc bf_p))) [a]) \<and>
                  (f^^(Suc g_p * Suc bf_p)) z = z \<and> z\<cdot>w\<cdot>[a] \<le>p (f^^(Suc g_p * Suc bf_p)) (w\<cdot>[a])"
proof-
  obtain U where
    "finite U" and
    "U \<subseteq> {x. bounded x}" and
    ful: "\<And>a. a \<in> fixed_unbounded_left \<Longrightarrow> \<exists>z w.
        z \<in> U \<and> w \<in> U \<and>
        (\<forall>k. z\<^sup>@k \<cdot> w \<cdot> [a] \<le>p (f^^( (Suc (g_p)*(bf_N))+k*(Suc g_p * Suc bf_p))) [a])  \<and>
                  (f^^(Suc g_p * Suc bf_p)) z = z \<and> z\<cdot>w\<cdot>[a] \<le>p (f^^(Suc g_p * Suc bf_p)) (w\<cdot>[a])"
    using fixed_unbounded_left_per[of thesis] by blast

  let ?luP = "\<lambda>b w. bounded w \<and> w\<cdot>[(first_unbounded_left^^g_N) b] \<le>p (f^^g_N) [b]"
  let ?lu = "\<lambda>b. (SOME w. ?luP b w )"

  have "\<not> bounded [b] \<Longrightarrow> \<exists>w. ?luP b w" for b
    using first_unbounded_left_pow_ex[of b] rassoc prefI
    by metis
  hence luEx: "\<not> bounded [b] \<Longrightarrow> ?luP b ( ?lu b )" for b
    by (metis (no_types, lifting) tfl_some)

  have "finite {x. \<not> bounded [x]}"
    using infinite_super local.finite_UNIV top_greatest by blast
  hence "finite (?lu`{x. \<not> bounded [x]})" (is "finite ?L")
    by blast


  have "?L \<subseteq> {x. bounded x}"
    by (simp add: image_subset_iff luEx)

  have "finite ((f^^(Suc (g_p)*(bf_N)))`?L)" (is "finite ?L1")
    using \<open>finite ?L\<close>
    by blast

  have "?L1 \<subseteq> {x. bounded x}"
    using \<open>(\<lambda>b. SOME w. bounded w \<and> w \<cdot> [(first_unbounded_left ^^ g_N) b] \<le>p (f ^^ g_N) [b]) ` {x. \<not> bounded [x]} \<subseteq> Collect bounded\<close> endomorphism.bounded_image_pow endomorphism_axioms by blast


  define U1 where "U1 = U \<union> ?L1"
  have "U1 \<subseteq> {x. bounded x}"
    using U1_def \<open>(f ^^ (Suc g_p * bf_N)) ` (\<lambda>b. SOME w. bounded w \<and> w \<cdot> [(first_unbounded_left ^^ g_N) b] \<le>p (f ^^ g_N) [b]) ` {x. \<not> bounded [x]} \<subseteq> Collect bounded\<close> \<open>U \<subseteq> Collect bounded\<close> by auto
  have "finite U1"
    by (metis (mono_tags, lifting) U1_def \<open>finite U\<close> finite_Un finite_imageI infinite_super local.finite_UNIV subset_UNIV)


  have "\<not> bounded [b] \<Longrightarrow> \<exists>z w v a.
        z \<in> U1 \<and> w \<in> U1 \<and> a \<in> fixed_unbounded_left \<and> v \<in> U1 \<and>
        (\<forall>k. v \<cdot> z\<^sup>@k \<cdot> w \<cdot> [a] \<le>p (f^^(g_N+ (Suc (g_p)*(bf_N))+k*(Suc g_p * Suc bf_p))) [b])  \<and>
        (\<forall>k. z\<^sup>@k \<cdot> w \<cdot> [a] \<le>p (f^^((Suc (g_p)*(bf_N))+k*(Suc g_p * Suc bf_p))) [a]) \<and>
                  (f^^(Suc g_p * Suc bf_p)) z = z \<and> z\<cdot>w\<cdot>[a] \<le>p (f^^(Suc g_p * Suc bf_p)) (w\<cdot>[a])" for b
  proof-
    assume "\<not> bounded [b]"
    define v where "v = ?lu b"
    have "bounded v"
      unfolding v_def
      using \<open>\<And>b. \<not> bounded [b] \<Longrightarrow> bounded (SOME w. bounded w \<and> w \<cdot> [(first_unbounded_left ^^ g_N) b] \<le>p (f ^^ g_N) [b]) \<and> (SOME w. bounded w \<and> w \<cdot> [(first_unbounded_left ^^ g_N) b] \<le>p (f ^^ g_N) [b]) \<cdot> [(first_unbounded_left ^^ g_N) b] \<le>p (f ^^ g_N) [b]\<close> \<open>\<not> bounded [b]\<close> by force

    define a where "a = (first_unbounded_left^^g_N) b"
    hence "a \<in> fixed_unbounded_left"
      by (simp add: \<open>\<not> bounded [b]\<close> unbounded_fixed_unbounded_left)

    have "v\<cdot>[a] \<le>p (f^^g_N) [b]"
      by (simp add: \<open>\<And>b. \<not> bounded [b] \<Longrightarrow> bounded (SOME w. bounded w \<and> w \<cdot> [(first_unbounded_left ^^ g_N) b] \<le>p (f ^^ g_N) [b]) \<and> (SOME w. bounded w \<and> w \<cdot> [(first_unbounded_left ^^ g_N) b] \<le>p (f ^^ g_N) [b]) \<cdot> [(first_unbounded_left ^^ g_N) b] \<le>p (f ^^ g_N) [b]\<close> \<open>\<not> bounded [b]\<close> a_def v_def)
    then obtain s where "(f^^g_N) [b] = v\<cdot>[a]\<cdot>s"
      by (metis rassoc prefix_def)

    have "v \<in> ?L"
      using \<open>\<not> bounded [b]\<close> v_def by blast
    define v1 where "v1 = (f^^( (Suc (g_p)*(bf_N)))) v"
    have "v1 \<in> ?L1"
      unfolding v1_def using \<open>v \<in> ?L\<close>
      by blast
    have "v1 \<in> U1"
      using U1_def \<open>v1 \<in> (f ^^ (Suc g_p * bf_N)) ` (\<lambda>b. SOME w. bounded w \<and> w \<cdot> [(first_unbounded_left ^^ g_N) b] \<le>p (f ^^ g_N) [b]) ` {x. \<not> bounded [x]}\<close> by blast

    obtain z w where "z \<in> U" "w \<in> U"  and
      a_all: "(\<forall>k. z\<^sup>@k \<cdot> w \<cdot> [a] \<le>p (f^^( (Suc (g_p)*(bf_N))+k*(Suc g_p * Suc bf_p))) [a])" and
      "(f^^(Suc g_p * Suc bf_p)) z = z" "z\<cdot>w\<cdot>[a] \<le>p (f^^(Suc g_p * Suc bf_p)) (w\<cdot>[a])"
      using ful[OF \<open>a \<in> fixed_unbounded_left\<close>]
      by blast
    have "z \<in> U1" "w \<in> U1"
      using \<open>z \<in> U\<close> \<open>w \<in> U\<close> unfolding U1_def by blast+


    have "(\<forall>k. ((f^^( (Suc (g_p)*(bf_N))+k*(Suc g_p * Suc bf_p))) v) \<cdot> z\<^sup>@k \<cdot> w \<cdot> [a] \<le>p (f^^( (Suc (g_p)*(bf_N))+k*(Suc g_p * Suc bf_p))) ((f^^g_N) [b]))"
      unfolding \<open>(f^^g_N) [b] = v\<cdot>[a]\<cdot>s\<close> morphism.morph[OF pow_morphism]
      using a_all by auto
    hence "(\<forall>k. v1 \<cdot> z\<^sup>@k \<cdot> w \<cdot> [a] \<le>p (f^^( (Suc (g_p)*(bf_N))+k*(Suc g_p * Suc bf_p))) ((f^^g_N) [b]))"
      unfolding v1_def
      by (metis (no_types, lifting) \<open>bounded v\<close> bounded_fixed le_add1 mult.assoc mult_Suc)
    hence "(\<forall>k. v1 \<cdot> z\<^sup>@k \<cdot> w \<cdot> [a] \<le>p (f^^(g_N + (Suc (g_p)*(bf_N))+k*(Suc g_p * Suc bf_p))) [b])"
      unfolding funpow_add' comp_apply.
    thus ?thesis
      using \<open>a \<in> fixed_unbounded_left\<close> \<open>(f^^(Suc g_p * Suc bf_p)) z = z\<close> \<open>z\<cdot>w\<cdot>[a] \<le>p (f^^(Suc g_p * Suc bf_p)) (w\<cdot>[a])\<close> \<open>z \<in> U1\<close> \<open>w \<in> U1\<close> \<open>v1 \<in> U1\<close> a_all by blast
  qed
  from that[OF \<open>finite U1\<close> \<open>U1 \<subseteq> Collect bounded\<close> this]
  show thesis
    by blast
qed

lemmas fixed_unbounded_right_unbouded =  endomorphism_fixed.fixed_unbounded_left_unbouded[OF rev_map_endomorph, reversed] and
    unbounded_fixed_unbounded_right = endomorphism_fixed.unbounded_fixed_unbounded_left[OF rev_map_endomorph, reversed] and
  fixed_unbounded_right_per =
      endomorphism_fixed.fixed_unbounded_left_per[OF rev_map_endomorph, reversed,
        unfolded finite_image_iff[OF inj_on_rev] rassoc] and
  unbounded_right_per =
      endomorphism_fixed.unbounded_left_per[OF rev_map_endomorph, reversed,
        unfolded finite_image_iff[OF inj_on_rev] rassoc]


thm pmor_first_nonsing_preimage

lemma long_factor:
  assumes
    w_factor: "w \<in> \<L> axiom" and
    w_long: "\<^bold>|w\<^bold>| > \<lceil>f\<rceil>^(m + Suc Per)*\<^bold>|axiom\<^bold>|" (is "\<^bold>|w\<^bold>| > ?length") and
    f_growing: "1 < \<lceil>f\<rceil>" and
    w_P: "list_all P w" and
    P_not_all: "\<And>k. \<not> list_all P ((f^^k) axiom)"
  obtains n pp ps sp ss k int_p int_s w_pred pp_pred ss_pred int_p' int_s'
  where
                 "\<And>i. w \<le>f (f^^i) axiom \<Longrightarrow> n \<le> i"
        "m + Suc Per < n"
        "(f ^^ n) axiom = pp\<cdot>(ps\<cdot>w\<cdot>sp)\<cdot>ss"
        "list_all P ps" "pp = \<epsilon> \<or> \<not> P (last pp)"
        "list_all P sp" "ss = \<epsilon> \<or> \<not> P (hd ss)"
        "list_all P (ps\<cdot>w\<cdot>sp)"

         "m + Suc k * Suc Per \<le> n"
         "preimage axiom (m + Suc k * Suc Per) (ps \<cdot> w \<cdot> sp) int_p int_s (n - (m + Suc k * Suc Per)) w_pred pp_pred ss_pred"
                        "1 < \<^bold>|w_pred\<^bold>|"
        "\<^bold>|w_pred\<^bold>| \<le> \<lceil>f\<rceil> ^ (Suc Per) * \<^bold>|axiom\<^bold>|"
                "(f^^(m + Suc k * Suc Per)) [hd w_pred] = int_p\<cdot>int_p'"
        "int_p' \<noteq> \<epsilon>"         "(f^^(m + Suc k * Suc Per)) [last w_pred] = int_s'\<cdot>int_s"
        "int_s' \<noteq> \<epsilon>"         "ps \<cdot> w \<cdot> sp = int_p'\<cdot> ((f^^(m + Suc k * Suc Per)) (butlast (tl w_pred))) \<cdot> int_s'"
        "pp_pred\<cdot>w_pred\<cdot>ss_pred = ((f ^^ (n-(m + Suc k * Suc Per))) axiom)"
        "pp = ((f^^(m + Suc k * Suc Per)) pp_pred)\<cdot>int_p"         "ss = int_s\<cdot>((f^^(m + Suc k * Suc Per)) ss_pred)"     proof-
  have "1 \<le> \<lceil>f\<rceil>^(m + Suc Per)"
    using f_growing by simp
  hence "?length \<ge> \<^bold>|axiom\<^bold>|"
    by simp
  have axiom_nemp: "axiom \<noteq> \<epsilon>"
    using assms(2) pmor_lan_emp w_factor by fastforce
  hence "1 \<le> \<^bold>|axiom\<^bold>|"
    using nemp_le_len by blast

  let ?Pp = "\<lambda>z. w \<le>f z"
  have "\<exists>z \<in> \<L> axiom. ?Pp z"
    using w_factor by blast
  from pmor_first_occ[OF this]
  obtain n z where "z \<le>f (f^^n) axiom" "?Pp z" and
    n_least: "\<And>u i. u \<le>f (f^^i) axiom \<Longrightarrow> ?Pp u \<Longrightarrow> n \<le> i"
    by metis
  hence "w \<le>f (f^^n) axiom"
    by blast
  have "\<And>i. w \<le>f (f^^i) axiom \<Longrightarrow> n \<le> i"
    using n_least by blast

  have "w \<noteq> axiom"
    using \<open>\<^bold>|axiom\<^bold>| \<le> \<lceil>f\<rceil> ^ (m + Suc Per) * \<^bold>|axiom\<^bold>|\<close> less_le_not_le w_long by blast
  hence "\<not> w \<le>f axiom"
    using \<open>?length \<ge> \<^bold>|axiom\<^bold>|\<close> w_long
    by (meson dual_order.trans fac_len less_le_not_le)
  hence "0 \<noteq> n"
    using \<open>w \<le>f (f ^^ n) axiom\<close> by force
  hence "Suc (n - 1) = n"
    by auto

  have lengthN: "k \<le> m+Suc Per \<Longrightarrow> \<^bold>|(f^^k) axiom\<^bold>| \<le> \<^bold>|axiom\<^bold>| *  \<lceil>f\<rceil>^(m + Suc Per)" for k
    using  \<open>1 < \<lceil>f\<rceil>\<close> dual_order.trans max_im_len_pow_le'_dom mult_le_mono2 power_increasing_iff finite_UNIV
    by (metis (full_types))
  hence "m + Suc Per < n"
    using max_im_len_pow_le_dom[OF finite_UNIV]
    by (smt (verit, best) \<open>\<lceil>f\<rceil> ^ (m + Suc Per) * \<^bold>|axiom\<^bold>| < \<^bold>|w\<^bold>|\<close> \<open>w \<le>f (f ^^ n) axiom\<close> fac_len le_trans linorder_not_le mult.commute)


 obtain p s where "(f^^n) axiom = p\<cdot>w\<cdot>s"
    using \<open>w \<le>f (f ^^ n) axiom\<close> by blast
    from extend_P_fac[OF this \<open>list_all P w\<close>]
  obtain pp ps sp ss where
    "p = pp\<cdot>ps" "list_all P ps" "pp = \<epsilon> \<or> \<not> P (last pp)"
    "s = sp\<cdot>ss" "list_all P sp" "ss = \<epsilon> \<or> \<not> P (hd ss)"
    "(f ^^ n) axiom = pp\<cdot>(ps\<cdot>w\<cdot>sp)\<cdot>ss" "list_all P (ps\<cdot>w\<cdot>sp)"
    by blast

  have "\<lceil>f\<rceil> ^ (m + Suc Per) < \<^bold>|ps \<cdot> w \<cdot> sp\<^bold>|"
    by (metis \<open>1 \<le> \<^bold>|axiom\<^bold>|\<close> dual_order.trans fac_len' mult.right_neutral mult_le_mono2 not_less w_long)

  from pmor_first_nonsing_preimage[OF finite_imageI, OF finite_imageI, OF finite_UNIV \<open>(f ^^ n) axiom = pp\<cdot>(ps\<cdot>w\<cdot>sp)\<cdot>ss\<close> less_imp_le[OF \<open>m + Suc Per < n\<close>] this]
  obtain k int_p int_s w_pred pp_pred ss_pred where
    "m + Suc k * Suc Per \<le> n"
    "preimage axiom (m + Suc k * Suc Per) (ps \<cdot> w \<cdot> sp) int_p int_s (n - (m + Suc k * Suc Per)) w_pred pp_pred ss_pred"
    "2 \<le> \<^bold>|w_pred\<^bold>|"
    "pp = (f ^^ (m + Suc k * Suc Per)) pp_pred \<cdot> int_p" and
    last: "(\<And>wp' ws' w_pred' p_pred' s_pred' l. m + (Suc k + Suc l) * Suc Per \<le> n \<Longrightarrow> preimage axiom (Suc l * Suc Per) w_pred wp' ws' (n - (m + (Suc k + Suc l) * Suc Per)) w_pred' p_pred' s_pred' \<Longrightarrow> pp_pred = (f ^^ (Suc l * Suc Per)) p_pred' \<cdot> wp' \<Longrightarrow> \<^bold>|w_pred'\<^bold>| = 1)"
    by blast


  note preimageE[OF \<open>preimage axiom (m + Suc k * Suc Per) (ps \<cdot> w \<cdot> sp) int_p int_s (n - (m + Suc k * Suc Per)) w_pred pp_pred ss_pred\<close>]

  have fna: "(f ^^ n) axiom =
    (f ^^ (m + Suc k * Suc Per)) pp_pred \<cdot> (int_p \<cdot> (ps \<cdot> w \<cdot> sp) \<cdot> int_s) \<cdot> (f ^^ (m + Suc k * Suc Per)) ss_pred"
    by (metis \<open>(f ^^ (n - (m + Suc k * Suc Per) + (m + Suc k * Suc Per))) axiom = (f ^^ (m + Suc k * Suc Per)) pp_pred \<cdot> (int_p \<cdot> (ps \<cdot> w \<cdot> sp) \<cdot> int_s) \<cdot> (f ^^ (m + Suc k * Suc Per)) ss_pred\<close> \<open>m + Suc k * Suc Per \<le> n\<close> le_add_diff_inverse2)




  have fwp: "(f ^^ (m + Suc k * Suc Per)) w_pred = int_p \<cdot> (ps \<cdot> w \<cdot> sp) \<cdot> int_s"
    by (metis \<open>int_p ps \<cdot> w \<cdot> sp int_s \<sim>\<^sub>\<I> map (f ^^ (m + Suc k * Suc Per))\<^sup>\<C> w_pred\<close> endomorphism.pow_morphism endomorphism_axioms fac_interpD(3) morphism.morph_concat_map)

  have "ss = int_s \<cdot> (f ^^ (m + Suc k * Suc Per)) ss_pred"
    using fna
    unfolding \<open>(f ^^ n) axiom = pp\<cdot>(ps\<cdot>w\<cdot>sp)\<cdot>ss\<close>
      \<open>pp = (f ^^ (m + Suc k * Suc Per)) pp_pred \<cdot> int_p\<close>
      rassoc
    unfolding cancel.

  let ?Nk = "n + Suc k * Suc Per"

  interpret fn: endomorphism "f ^^ n"
    using endomorphism.intro pow_morphism by blast
  interpret fNk: endomorphism "f ^^ ?Nk"
    using endomorphism.intro pow_morphism by blast

  have "\<^bold>|w\<^bold>| \<le> \<^bold>|axiom\<^bold>| * \<lceil>f ^^ n\<rceil>"
    by (meson \<open>w \<le>f (f ^^ n) axiom\<close> dual_order.trans fac_len fn.max_im_len_le_dom local.finite_UNIV)

  have "pp \<noteq> \<epsilon> \<or> ss \<noteq> \<epsilon>"
  proof(rule ccontr)
    assume "\<not> (pp \<noteq> \<epsilon> \<or> ss \<noteq> \<epsilon>)"
    hence "(f ^^ n) axiom = ps\<cdot>w\<cdot>sp"
      by (simp add: \<open>(f ^^ n) axiom = p \<cdot> w \<cdot> s\<close> \<open>p = pp \<cdot> ps\<close> \<open>s = sp \<cdot> ss\<close>)
    hence "list_all P ((f ^^ n) axiom)"
      using \<open>list_all P (ps \<cdot> w \<cdot> sp)\<close> by auto
    thus False
      using P_not_all
      by blast
  qed

  have "ps \<cdot> w \<cdot> sp \<noteq> \<epsilon>"
    using \<open>\<not> w \<le>f axiom\<close> by blast



  have "int_p \<le>s pp"
    by (simp add: \<open>pp = (f ^^ (m + Suc k * Suc Per)) pp_pred \<cdot> int_p\<close> sufI)
  have "int_s \<le>p ss"
    using \<open>ss = int_s \<cdot> (f ^^ (m + Suc k * Suc Per)) ss_pred\<close>
    by force

  have "w_pred \<noteq> \<epsilon>"
    using \<open>2 \<le> \<^bold>|w_pred\<^bold>|\<close> by fastforce

  have "int_p <p (f ^^ (m + Suc k * Suc Per))\<^sup>\<C> (hd w_pred)"
    by (metis \<open>int_p ps \<cdot> w \<cdot> sp int_s \<sim>\<^sub>\<I> map (f ^^ (m + Suc k * Suc Per))\<^sup>\<C> w_pred\<close> \<open>w_pred \<noteq> \<epsilon>\<close> fac_interpD(1) list.map_sel(1))
  hence "int_p <p (f^^(m + Suc k * Suc Per)) [hd w_pred]"
    unfolding core_def.
  have "int_s <s (f ^^ (m + Suc k * Suc Per))\<^sup>\<C> (last w_pred)"
    by (metis \<open>int_p ps \<cdot> w \<cdot> sp int_s \<sim>\<^sub>\<I> map (f ^^ (m + Suc k * Suc Per))\<^sup>\<C> w_pred\<close> \<open>w_pred \<noteq> \<epsilon>\<close> fac_interpD(2) last_map)
  hence "int_s <s (f^^(m + Suc k * Suc Per)) [last w_pred]"
    unfolding core_def.

  have "\<^bold>|w_pred\<^bold>| \<noteq> 1"
    using \<open>2 \<le> \<^bold>|w_pred\<^bold>|\<close> by linarith
  hence "w_pred = [hd w_pred]\<cdot>(butlast (tl w_pred))\<cdot>[last w_pred]"
    by (metis One_nat_def \<open>w_pred \<noteq> \<epsilon>\<close> append_butlast_last_id hd_word last_tl length_Cons list.collapse list.size(3))

  obtain int_p' where "(f^^(m + Suc k * Suc Per)) [hd w_pred] = int_p\<cdot>int_p'" "int_p' \<noteq> \<epsilon>"
    using \<open>int_p <p (f^^(m + Suc k * Suc Per)) [hd w_pred]\<close>
    by (meson neq_Nil_conv strict_prefixE')
  obtain int_s' where "(f^^(m + Suc k * Suc Per)) [last w_pred] = int_s'\<cdot>int_s" "int_s' \<noteq> \<epsilon>"
    using \<open>int_s <s (f^^(m + Suc k * Suc Per)) [last w_pred]\<close>
    unfolding strict_suffix_def suffix_def
    by force

  have  "ps \<cdot> w \<cdot> sp = int_p'\<cdot> ((f^^(m + Suc k * Suc Per)) (butlast (tl w_pred))) \<cdot> int_s'"
    by (smt (verit) \<open>(f ^^ (m + Suc k * Suc Per)) [hd w_pred] = int_p \<cdot> int_p'\<close> \<open>(f ^^ (m + Suc k * Suc Per)) [last w_pred] = int_s' \<cdot> int_s\<close> \<open>w_pred = [hd w_pred] \<cdot> butlast (tl w_pred) \<cdot> [last w_pred]\<close> append_assoc cancel_right fwp pow_morph same_append_eq)

  have "1 < \<^bold>|w_pred\<^bold>|"
    using \<open>\<^bold>|w_pred\<^bold>| \<noteq> 1\<close> \<open>w_pred \<noteq> \<epsilon>\<close> nat_neq_iff by auto

  have "\<^bold>|w_pred\<^bold>| \<le> \<lceil>f\<rceil> ^ (Suc Per) * \<^bold>|axiom\<^bold>|"
  proof(cases)
    assume "m + (Suc k + Suc 0) * Suc Per \<le> n"
    hence "Suc Per \<le> n - (m + Suc k * Suc Per)"
      by (simp add: add_le_imp_le_diff)

    have he: "n - (m + (Suc k + 1) * Suc Per) = n - (m + Suc k * Suc Per) - Suc Per"
      by (metis Suc_eq_plus1 diff_diff_left diff_right_commute mult.commute mult_Suc_right)

    obtain wpd wsd w_predd p_predd s_predd where
      "preimage axiom (Suc Per) w_pred wpd wsd (n - (m + Suc k * Suc Per) - Suc Per) w_predd p_predd s_predd"
      "pp_pred = (f ^^ Suc Per) p_predd \<cdot> wpd"
      "ss_pred = wsd \<cdot> (f ^^ Suc Per) s_predd"
      using preimage_ex'[OF \<open>w_pred \<noteq> \<epsilon>\<close> \<open>(f ^^ (n - (m + Suc k * Suc Per))) axiom = pp_pred \<cdot> w_pred \<cdot> ss_pred\<close> \<open>Suc Per \<le> n - (m + Suc k * Suc Per)\<close>] by blast

    have "\<^bold>|w_predd\<^bold>| = 1"
      using last[OF \<open>m + (Suc k + Suc 0) * Suc Per \<le> n\<close>,
          unfolded One_nat_def[symmetric] mult.left_neutral he,
          OF \<open>preimage axiom (Suc Per) w_pred wpd wsd (n - (m + Suc k * Suc Per) - Suc Per) w_predd p_predd s_predd\<close> \<open>pp_pred = (f ^^ Suc Per) p_predd \<cdot> wpd\<close>].

    show ?thesis
      using  max_im_len_pow_le'_dom[OF finite_UNIV, of "Suc Per" w_predd]
        fac_len'[THEN le_trans, of wpd w_pred wsd]
      unfolding \<open>\<^bold>|w_predd\<^bold>| = 1\<close> mult_1
      unfolding fac_interpD(3)[OF preimageE(1)[OF \<open>preimage axiom (Suc Per) w_pred wpd wsd (n - (m + Suc k * Suc Per) - Suc Per) w_predd p_predd s_predd\<close>], unfolded morphism.morph_concat_map[OF pow_morphism]]
      by (metis \<open>1 \<le> \<^bold>|axiom\<^bold>|\<close> mult.commute mult_1 mult_le_mono)
  next
    assume "\<not> m + (Suc k + Suc 0) * Suc Per \<le> n"
    hence "n - (m + Suc k * Suc Per) < Suc Per"
      by force
    hence "\<lceil>f\<rceil> ^ (n - (m + Suc k * Suc Per)) \<le>  \<lceil>f\<rceil> ^ (Suc Per)"
      by (meson f_growing less_or_eq_imp_le power_increasing_iff)
    thus "\<^bold>|w_pred\<^bold>| \<le> \<lceil>f\<rceil> ^ (Suc Per) * \<^bold>|axiom\<^bold>|"
      using max_im_len_pow_le'_dom[OF finite_UNIV, of "n - (m + Suc k * Suc Per)" axiom]
      unfolding \<open>(f ^^ (n - (m + Suc k * Suc Per))) axiom = pp_pred \<cdot> w_pred \<cdot> ss_pred\<close>
      using fac_len'[THEN le_trans, of pp_pred w_pred ss_pred]
      by (metis dual_order.trans mult.commute mult_le_mono2)
  qed


  show thesis using that[OF  \<open>\<And>i. w \<le>f (f^^i) axiom \<Longrightarrow> n \<le> i\<close> \<open>m + Suc Per < n\<close> \<open>(f ^^ n) axiom = pp\<cdot>(ps\<cdot>w\<cdot>sp)\<cdot>ss\<close> \<open>list_all P ps\<close> \<open>pp = \<epsilon> \<or> \<not> P (last pp)\<close> \<open>list_all P sp\<close> \<open>ss = \<epsilon> \<or> \<not> P (hd ss)\<close>  \<open>list_all P (ps\<cdot>w\<cdot>sp)\<close> \<open>m + Suc k * Suc Per \<le> n\<close>
        \<open>preimage axiom (m + Suc k * Suc Per) (ps \<cdot> w \<cdot> sp) int_p int_s (n - (m + Suc k * Suc Per)) w_pred pp_pred ss_pred\<close> \<open>1 < \<^bold>|w_pred\<^bold>|\<close> \<open>\<^bold>|w_pred\<^bold>| \<le> \<lceil>f\<rceil> ^ (Suc Per) * \<^bold>|axiom\<^bold>|\<close> \<open>(f^^(m + Suc k * Suc Per)) [hd w_pred] = int_p\<cdot>int_p'\<close> \<open>int_p' \<noteq> \<epsilon>\<close> \<open>(f^^(m + Suc k * Suc Per)) [last w_pred] = int_s'\<cdot>int_s\<close> \<open>int_s' \<noteq> \<epsilon>\<close> \<open>ps \<cdot> w \<cdot> sp = int_p'\<cdot> ((f^^(m + Suc k * Suc Per)) (butlast (tl w_pred))) \<cdot> int_s'\<close> \<open>(f ^^ (n - (m + Suc k * Suc Per))) axiom = pp_pred \<cdot> w_pred \<cdot> ss_pred\<close>[symmetric] \<open>pp = ((f^^(m + Suc k * Suc Per)) pp_pred)\<cdot>int_p\<close> \<open>ss = int_s\<cdot>((f^^(m + Suc k * Suc Per)) ss_pred)\<close>].
qed

lemma long_bounded_factor:
  obtains W U where "finite W" and "finite U" and
           "\<And>w. w \<in> \<L> axiom \<Longrightarrow> bounded w \<Longrightarrow>
    (\<exists>w1 w2 w3 u1 u2 k1 k2. w1 \<in> W \<and> w2 \<in> W \<and> w3 \<in> W \<and> u1 \<in> U \<and> u2 \<in> U \<and> w \<le>f w1\<cdot>u1\<^sup>@k1\<cdot>w2\<cdot>u2\<^sup>@k2\<cdot>w3)"
proof(cases)
  assume "1 \<ge> \<lceil>f\<rceil> \<or> bounded axiom"
  hence "bounded axiom"
    using growing_maxlen local.finite_UNIV unbounded_growing linorder_not_le
    by metis
  thus ?thesis
    using bounded_def sublist_append_rightI that[of "\<L> axiom"  "\<L> axiom"]
    by blast
next
  assume "\<not> (1 \<ge> \<lceil>f\<rceil> \<or> bounded axiom)"
  hence "1 < \<lceil>f\<rceil>" "\<not> bounded axiom"
    by linarith+


       define Q where "Q = (\<lambda>W U (w::'a list).
    (\<exists>w1 w2 w3 u1 u2 k1 k2. w1 \<in> W \<and> w2 \<in> W \<and> w3 \<in> W \<and> u1 \<in> U \<and> u2 \<in> U \<and> w \<le>f w1\<cdot>u1\<^sup>@k1\<cdot>w2\<cdot>u2\<^sup>@k2\<cdot>w3))"

  let ?N = "g_N+ (Suc (g_p)*(bf_N))"
  let ?Per = "(Suc g_p * Suc bf_p)"
  obtain Per where Per_def: "?Per = Suc Per"
    by simp


  define N where "N = ?N + Suc Per"
  define Ntk where "Ntk = (\<lambda>k. ?N + Suc Per + k*Suc Per)"

  have "Ntk 0 = N"
    unfolding N_def Ntk_def by simp

  define U_long where "U_long = {w. bounded w \<and> \<^bold>|w\<^bold>| \<le> \<lceil>f\<rceil> ^ N * \<^bold>|axiom\<^bold>|}"
  have "finite U_long"
    using maxlen_finite[OF finite_UNIV, of U_long]
    unfolding U_long_def
    by blast


  have U_long_fac_clo: "x \<in> U_long \<Longrightarrow> y \<le>f x \<Longrightarrow> y \<in> U_long" for x y
    unfolding U_long_def mem_Collect_eq
    using bounded_fac_bounded
    by (meson fac_len le_trans)

  obtain UL where "finite UL" "UL \<subseteq> Collect bounded"
    and left_per: "(\<And>b. \<not> bounded [b] \<Longrightarrow> \<exists>z w v a. z \<in> UL \<and> w \<in> UL \<and> a \<in> fixed_unbounded_left \<and> v \<in> UL \<and> (\<forall>k. v \<cdot> z \<^sup>@ k \<cdot> w \<cdot> [a] \<le>p (f ^^ (g_N + Suc g_p * bf_N + k * (Suc g_p * Suc bf_p))) [b]) \<and> (\<forall>k. z \<^sup>@ k \<cdot> w \<cdot> [a] \<le>p (f ^^ (Suc g_p * bf_N + k * (Suc g_p * Suc bf_p))) [a]) \<and> (f ^^ (Suc g_p * Suc bf_p)) z = z \<and> z \<cdot> w \<cdot> [a] \<le>p (f ^^ (Suc g_p * Suc bf_p)) (w \<cdot> [a]))" using unbounded_left_per[of thesis]
    by metis

  have UL_Ball: "x \<in> UL \<Longrightarrow> bounded x" for x
    using \<open>UL \<subseteq> Collect bounded\<close> by blast

  obtain UR where "finite UR" "UR \<subseteq> Collect bounded" and
    right_per: "(\<And>b. \<not> bounded [b] \<Longrightarrow> \<exists>xa xb xc a. xa \<in> UR \<and> xb \<in> UR \<and> a \<in> fixed_unbounded_right \<and> xc \<in> UR \<and> (\<forall>k. [a] \<cdot> xb \<cdot> xa \<^sup>@ k \<cdot> xc \<le>s (f ^^ (g_N + Suc g_p * bf_N + k * (Suc g_p * Suc bf_p))) [b]) \<and> (\<forall>k. [a] \<cdot> xb \<cdot> xa \<^sup>@ k \<le>s (f ^^ (Suc g_p * bf_N + k * (Suc g_p * Suc bf_p))) [a]) \<and> (f ^^ (Suc g_p * Suc bf_p)) xa = xa \<and> [a] \<cdot> xb \<cdot> xa \<le>s (f ^^ (Suc g_p * Suc bf_p)) ([a] \<cdot> xb))"
    using unbounded_right_per[of thesis] by metis

  have UR_Ball: "x \<in> UR \<Longrightarrow> bounded x" for x
    using \<open>UR \<subseteq> Collect bounded\<close> by blast

  define U where "U = UL \<union> UR \<union> {\<epsilon>}"
  define Wpred where "Wpred = (\<lambda>x. (\<exists>l r m. l \<in> U \<and> r \<in> U \<and> m \<in> U_long \<and> x = l\<cdot>m\<cdot>r))"
  define W where "W = Collect Wpred"

  have "\<epsilon> \<in> U"
    using U_def by auto
  have "\<epsilon> \<in> U_long"
    by (simp add: U_long_def bounded_emp)
  hence "\<epsilon> \<in> W"
    unfolding W_def Wpred_def U_def
    by blast
  have "x \<in> U_long \<Longrightarrow> x \<in> W" for x
    unfolding W_def Wpred_def
    using \<open>\<epsilon> \<in> U\<close> by fastforce

  have "finite U"
    by (simp add: U_def \<open>finite UL\<close> \<open>finite UR\<close>)
  have "finite W"
  proof-
    have "finite (U \<times> U_long \<times> U)"
      by (simp add: \<open>finite U\<close> \<open>finite U_long\<close>)
    let ?map = "(\<lambda> x. (fst(x))\<cdot>(fst(snd(x)))\<cdot>(snd(snd(x))))"
    have "?map`(U \<times> U_long \<times> U) = W"
    proof
      show "?map`(U \<times> U_long \<times> U) \<subseteq> W"
        unfolding W_def Wpred_def
        by fastforce
      show "W \<subseteq> ?map`(U \<times> U_long \<times> U)"
      proof
        fix x
        assume "x \<in> W"
        then obtain l r m where "l \<in> U" "r \<in> U" "m \<in> U_long" "x = l\<cdot>m\<cdot>r"
          using W_def Wpred_def by blast
        have "(l,m,r) \<in> U \<times> U_long \<times> U"
          by (simp add: \<open>l \<in> U\<close> \<open>m \<in> U_long\<close> \<open>r \<in> U\<close>)
        moreover have "?map (l,m,r) = x"
          by (simp add: \<open>x = l \<cdot> m \<cdot> r\<close>)
        ultimately show "x \<in> ?map ` (U \<times> U_long \<times> U)" by blast
      qed
    qed
    thus ?thesis
      using \<open>finite (U \<times> U_long \<times> U)\<close> by force
  qed


  have "Q W U w" if "w \<in> \<L> axiom" "bounded w" for w
  proof(cases)
    assume "w \<in> U_long"
    then show ?thesis
      unfolding Q_def W_def
      using W_def \<open>\<And>x. x \<in> U_long \<Longrightarrow> x \<in> W\<close> \<open>\<epsilon> \<in> U\<close> by blast
   next
    assume "w \<notin> U_long"
    hence long_w: "\<lceil>f\<rceil> ^ N * \<^bold>|axiom\<^bold>| < \<^bold>|w\<^bold>|"
      unfolding U_long_def
      using leI that(2) by blast


    let ?Pb = "\<lambda>x . bounded [x]"
    have "list_all ?Pb w"
      using bounded_list_all that(2) by blast

    have "(\<And>k. \<not> bounded ((f ^^ k) axiom))"
      using \<open>\<not> bounded axiom\<close> bounded_image_pow by auto


    have "axiom \<noteq> \<epsilon>"
      using \<open>\<not> bounded axiom\<close> bounded_emp by auto




    have "bf_N \<le> N"
      unfolding N_def
      by simp



    have sing_U_long: "(f^^(N + l*Suc Per)) [x] \<in> U_long" if "bounded [x]" for l x
    proof-
      have "(f ^^ N) [x] = (f ^^ (N + l * Suc Per)) [x]"
        using bounded_fixed[OF \<open>bounded [x]\<close> \<open>bf_N \<le> N\<close>, of"l*Suc g_p"]
        unfolding Per_def[symmetric] mult.assoc.
      moreover have "\<^bold>|(f ^^ N) [x]\<^bold>| \<le> \<lceil>f\<rceil> ^ N * \<^bold>|axiom\<^bold>|"
        using endomorphism.max_im_len_pow_le'_dom[OF endomorphism_axioms finite_UNIV, of N "[x]", unfolded sing_len mult.left_neutral]
        using dual_order.trans mult_le_mono2 nat_mult_1_right nemp_le_len[OF \<open>axiom \<noteq> \<epsilon>\<close>] by metis
      moreover have "bounded ((f^^N) [x])"
        using bounded_image_pow that by blast
      ultimately show ?thesis
        by (simp add: U_long_def)
    qed

    have "\<And>k. \<lceil>f\<rceil> ^ (?N+k*?Per) * \<^bold>|axiom\<^bold>| < \<lceil>f\<rceil> ^ (?N+(Suc k)*?Per) * \<^bold>|axiom\<^bold>|"
      using \<open>1 < \<lceil>f\<rceil>\<close>
      by (metis add_strict_left_mono \<open>\<not> bounded axiom\<close> endomorphism.bounded_emp endomorphism_axioms length_greater_0_conv lessI mult_less_mono1 nat_0_less_mult_iff power_strict_increasing_iff zero_less_Suc)
    hence "\<And>k. \<lceil>f\<rceil> ^ (Ntk k) * \<^bold>|axiom\<^bold>| < \<lceil>f\<rceil> ^ (Ntk (Suc k)) * \<^bold>|axiom\<^bold>|"
      unfolding Ntk_def
      by (metis \<open>1 < \<lceil>f\<rceil>\<close> add_strict_left_mono \<open>\<not> bounded axiom\<close> endomorphism.bounded_emp endomorphism_axioms length_greater_0_conv lessI mult_less_mono1 power_strict_increasing_iff zero_less_Suc)



    obtain k where "\<lceil>f\<rceil> ^ (Ntk k) * \<^bold>|axiom\<^bold>| < \<^bold>|w\<^bold>|" "\<lceil>f\<rceil> ^ (Ntk (Suc k)) * \<^bold>|axiom\<^bold>| \<ge> \<^bold>|w\<^bold>|"
      using sgrowing_fit[of "\<lambda>k.  \<lceil>f\<rceil> ^ (Ntk k) * \<^bold>|axiom\<^bold>|", unfolded \<open>Ntk 0 = N\<close>,
          OF \<open>\<And>k. \<lceil>f\<rceil> ^ (Ntk k) * \<^bold>|axiom\<^bold>| < \<lceil>f\<rceil> ^ (Ntk (Suc k)) * \<^bold>|axiom\<^bold>|\<close> long_w]
      by blast

    define Nper where "Nper = (?N + k* Suc Per) + Suc Per"
    have "Nper = Ntk k"
      by (simp add: Nper_def Ntk_def)

    have "\<lceil>f\<rceil> ^ (Nper) * \<^bold>|axiom\<^bold>| < \<^bold>|w\<^bold>|"
      unfolding \<open>Nper = Ntk k\<close>
      by (simp add: \<open>\<lceil>f\<rceil> ^ Ntk k * \<^bold>|axiom\<^bold>| < \<^bold>|w\<^bold>|\<close>)

    obtain n pp ps sp ss ka int_p int_s w_pred pp_pred ss_pred int_p' int_s' where
      "(\<And>i. w \<le>f (f ^^ i) axiom \<Longrightarrow> n \<le> i)"
      "g_N + Suc g_p * bf_N + k * Suc Per + Suc Per < n"
      "(f ^^ n) axiom = pp \<cdot> (ps \<cdot> w \<cdot> sp) \<cdot> ss"
      "bounded ps"
      "pp = \<epsilon> \<or> \<not> bounded [last pp]"
      "bounded sp"
      "ss = \<epsilon> \<or> \<not> bounded [hd ss]"
      "bounded (ps \<cdot> w \<cdot> sp)"
      "g_N + Suc g_p * bf_N + k * Suc Per + Suc ka * Suc Per \<le> n" and
      preim: "preimage axiom (g_N + Suc g_p * bf_N + k * Suc Per + Suc ka * Suc Per) (ps \<cdot> w \<cdot> sp) int_p int_s (n - (g_N + Suc g_p * bf_N + k * Suc Per + Suc ka * Suc Per)) w_pred pp_pred ss_pred" and
      "1 < \<^bold>|w_pred\<^bold>|"
      "\<^bold>|w_pred\<^bold>| \<le> \<lceil>f\<rceil> ^ (Suc Per) * \<^bold>|axiom\<^bold>|" and
      w_hd: "(f ^^ (g_N + Suc g_p * bf_N + k * Suc Per + Suc ka * Suc Per)) [hd w_pred] = int_p \<cdot> int_p'" and "int_p' \<noteq> \<epsilon>" and
      w_last: "(f ^^ (g_N + Suc g_p * bf_N + k * Suc Per + Suc ka * Suc Per)) [last w_pred] = int_s' \<cdot> int_s" and "int_s' \<noteq> \<epsilon>" and
      w_int: "ps \<cdot> w \<cdot> sp = int_p' \<cdot> (f ^^ (g_N + Suc g_p * bf_N + k * Suc Per + Suc ka * Suc Per)) (butlast (tl w_pred)) \<cdot> int_s'" and
      "pp_pred \<cdot> w_pred \<cdot> ss_pred = (f ^^ (n - (g_N + Suc g_p * bf_N + k * Suc Per + Suc ka * Suc Per))) axiom"
      "pp = (f ^^ (g_N + Suc g_p * bf_N + k * Suc Per + Suc ka * Suc Per)) pp_pred \<cdot> int_p"
      "ss = int_s \<cdot> (f ^^ (g_N + Suc g_p * bf_N + k * Suc Per + Suc ka * Suc Per)) ss_pred"
      using long_factor[OF \<open>w \<in> \<L> axiom\<close> \<open>\<lceil>f\<rceil> ^ (Nper) * \<^bold>|axiom\<^bold>| < \<^bold>|w\<^bold>|\<close>[unfolded Nper_def] \<open>1 < \<lceil>f\<rceil>\<close> \<open>list_all ?Pb w\<close>, folded bounded_list_all, OF \<open>(\<And>k. \<not> bounded ((f ^^ k) axiom))\<close>, of thesis]
      by blast

    note preimageE[OF preim]
    define Nper_k where  "Nper_k = ?N + k* Suc Per + Suc ka * Suc Per"


    have "bounded (butlast (tl w_pred))"
      using \<open>bounded (ps \<cdot> w \<cdot> sp)\<close> \<open>ps \<cdot> w \<cdot> sp = int_p' \<cdot> (f ^^ (g_N + Suc g_p * bf_N + k * Suc Per + Suc ka * Suc Per)) (butlast (tl w_pred)) \<cdot> int_s'\<close> bounded_concat_conv bounded_image_pow
      by metis
    from bounded_fixed[OF this]
    have "(f^^(Ntk l)) (butlast (tl w_pred)) = (f^^(g_N + Suc g_p * bf_N)) (butlast (tl w_pred))" for l
      unfolding Ntk_def N_def
      by (metis \<open>Suc g_p * Suc bf_p = Suc Per\<close> add_leE funpow_coin_per le_add2 mult_Suc)



    have "\<^bold>|(f ^^ (g_N + Suc g_p * bf_N)) w_pred\<^bold>| \<le> \<lceil>f\<rceil> ^ (g_N + Suc g_p * bf_N + Suc Per) * \<^bold>|axiom\<^bold>|"
      unfolding U_long_def mem_Collect_eq
      unfolding N_def
      using le_trans[OF morphism.max_im_len_le_dom[OF pow_morphism finite_UNIV, of "g_N + Suc g_p * bf_N" w_pred] mult_le_mono[OF \<open>\<^bold>|w_pred\<^bold>| \<le> \<lceil>f\<rceil> ^ (Suc Per) * \<^bold>|axiom\<^bold>|\<close> max_im_len_pow_le_dom[OF finite_UNIV]]]
      by (simp add: monoid_mult_class.power_add mult.commute)
    hence "\<^bold>|(f ^^ (g_N + Suc g_p * bf_N)) (butlast (tl w_pred))\<^bold>| \<le> \<lceil>f\<rceil> ^ (g_N + Suc g_p * bf_N + Suc Per) * \<^bold>|axiom\<^bold>|"
      using morphism.fac_mono[OF pow_morphism fac_trans[OF sublist_butlast sublist_tl]]
        fac_len le_trans by blast
    moreover have "bounded ((f ^^ (g_N + Suc g_p * bf_N)) (butlast (tl w_pred)))"
      using \<open>bounded (butlast (tl w_pred))\<close> bounded_image_pow by auto
    ultimately have "(f^^(g_N + Suc g_p * bf_N)) (butlast (tl w_pred)) \<in> U_long"
      using N_def U_long_def by force
    hence  "(f^^(Ntk l)) (butlast (tl w_pred)) \<in> U_long" for l
      using \<open>\<And>l. (f ^^ Ntk l) (butlast (tl w_pred)) = (f ^^ (g_N + Suc g_p * bf_N)) (butlast (tl w_pred))\<close> by auto
    hence but_tl_U_long: "(f^^(g_N + Suc g_p * bf_N + k * Suc Per + Suc ka * Suc Per)) (butlast (tl w_pred)) \<in> U_long"
      unfolding add_mult_distrib Suc_eq_plus1[of ka] mult.left_neutral Ntk_def
      by (metis (no_types, opaque_lifting) add.commute add_mult_distrib group_cancel.add2)


    have "int_p <p hd (map (f ^^ (Nper_k))\<^sup>\<C> w_pred)" "int_s <s last (map (f ^^ (Nper_k))\<^sup>\<C> w_pred)" "int_p \<cdot> (ps \<cdot> w \<cdot> sp) \<cdot> int_s =  concat (map (f ^^ (Nper_k))\<^sup>\<C> w_pred)"
      unfolding Nper_k_def
      using \<open>int_p ps \<cdot> w \<cdot> sp int_s \<sim>\<^sub>\<I> map (f ^^ (g_N + Suc g_p * bf_N + k * Suc Per + Suc ka * Suc Per))\<^sup>\<C> w_pred\<close> factor_interpretation_def by blast+


    have "(f ^^ (Nper_k)) w_pred = int_p \<cdot> (ps \<cdot> w \<cdot> sp) \<cdot> int_s"
      by (metis \<open>int_p \<cdot> (ps \<cdot> w \<cdot> sp) \<cdot> int_s = concat (map (f ^^ Nper_k)\<^sup>\<C> w_pred)\<close> endomorphism.pow_morphism endomorphism_axioms morphism.morph_concat_map)



    have sing_U_long': "(f^^(g_N + Suc g_p * bf_N + k * Suc Per + Suc ka * Suc Per)) [x] \<in> U_long" if "bounded [x]" for x
      using sing_U_long[OF \<open>bounded [x]\<close>, of "k+ka", unfolded N_def]
      unfolding add_mult_distrib Suc_eq_plus1[of ka] mult.left_neutral
      by (simp add: add.commute add.left_commute)

    have int_p_emp: "bounded [hd w_pred]" "int_p' \<in> W" if "int_p = \<epsilon>"
    proof-
      show "bounded [hd w_pred]"
        using \<open>int_p = \<epsilon>\<close> \<open>(f ^^ (g_N + Suc g_p * bf_N + k * Suc Per + Suc ka * Suc Per)) [hd w_pred] = int_p \<cdot> int_p'\<close> \<open>bounded (ps \<cdot> w \<cdot> sp)\<close> \<open>ps \<cdot> w \<cdot> sp = int_p' \<cdot> (f ^^ (g_N + Suc g_p * bf_N + k * Suc Per + Suc ka * Suc Per)) (butlast (tl w_pred)) \<cdot> int_s'\<close> bounded_concat_conv bounded_image_pow
        by (metis append_Nil)

      show  "int_p' \<in> W"
        using sing_U_long'[OF \<open>bounded [hd w_pred]\<close>]
        unfolding w_hd that emp_simps
        using \<open>\<And>x. x \<in> U_long \<Longrightarrow> x \<in> W\<close> by auto
    qed

    have int_s_emp:  "bounded [last w_pred]" "int_s' \<in> W" if "int_s = \<epsilon>"
    proof-
      show "bounded [last w_pred]"
      using that \<open>(f ^^ (g_N + Suc g_p * bf_N + k * Suc Per + Suc ka * Suc Per)) [last w_pred] = int_s' \<cdot> int_s\<close> \<open>bounded (ps \<cdot> w \<cdot> sp)\<close> \<open>ps \<cdot> w \<cdot> sp = int_p' \<cdot> (f ^^ (g_N + Suc g_p * bf_N + k * Suc Per + Suc ka * Suc Per)) (butlast (tl w_pred)) \<cdot> int_s'\<close> bounded_concat_conv bounded_image_pow
      by (metis append_Nil2)

      show  "int_s' \<in> W"
        using sing_U_long'[OF \<open>bounded [last w_pred]\<close>]
        unfolding w_last that emp_simps
        using \<open>\<And>x. x \<in> U_long \<Longrightarrow> x \<in> W\<close> by auto
    qed


    have "list_all ?Pb int_s'" "list_all ?Pb int_p'"
      using \<open>bounded (ps \<cdot> w \<cdot> sp)\<close> \<open>ps \<cdot> w \<cdot> sp = int_p' \<cdot> (f ^^ (g_N + Suc g_p * bf_N + k * Suc Per + Suc ka * Suc Per)) (butlast (tl w_pred)) \<cdot> int_s'\<close> bounded_concat_conv bounded_list_all by metis+



    have int_s_nemp: "\<not> bounded [last w_pred]" "\<not> bounded [hd int_s]"
      "\<exists>vl zl wl. vl \<in> UL \<and> zl \<in> UL \<and> wl \<in> UL \<and> int_s' = vl \<cdot> zl \<^sup>@ (k+Suc ka) \<cdot> wl" if "int_s \<noteq> \<epsilon>"
    proof-
      show "\<not> bounded [last w_pred]"
        using \<open>ss = \<epsilon> \<or> \<not> bounded [hd ss]\<close> \<open>ss = int_s \<cdot> (f ^^ (g_N + Suc g_p * bf_N + k * Suc Per + Suc ka * Suc Per)) ss_pred\<close>
        by (metis Nil_is_append_conv \<open>(f ^^ (g_N + Suc g_p * bf_N + k * Suc Per + Suc ka * Suc Per)) [last w_pred] = int_s' \<cdot> int_s\<close> bounded_concat_conv bounded_image_pow hd_append2 hd_word list.collapse that)
      show "\<not> bounded [hd int_s]"
        using \<open>ss = \<epsilon> \<or> \<not> bounded [hd ss]\<close>
        by (simp add: \<open>ss = int_s \<cdot> (f ^^ (g_N + Suc g_p * bf_N + k * Suc Per + Suc ka * Suc Per)) ss_pred\<close> that)
      show "\<exists>vl zl wl. vl \<in> UL \<and> zl \<in> UL \<and> wl \<in> UL \<and> int_s' = vl \<cdot> zl \<^sup>@ (k+Suc ka) \<cdot> wl"
      proof-
        from left_per[OF \<open>\<not> bounded [last w_pred]\<close>]
        obtain zl wl vl al where  "zl \<in> UL" "wl \<in> UL" "al \<in> fixed_unbounded_left"
          "vl \<in> UL"
          "(\<forall>k. vl \<cdot> zl \<^sup>@ k \<cdot> wl \<cdot> [al] \<le>p
          (f ^^ (g_N + Suc g_p * bf_N + k * (Suc g_p * Suc bf_p))) [last w_pred])"
          "(\<forall>k. zl \<^sup>@ k \<cdot> wl \<cdot> [al] \<le>p (f ^^ (Suc g_p * bf_N + k * (Suc g_p * Suc bf_p))) [al])"
          "(f ^^ (Suc g_p * Suc bf_p)) zl = zl \<and> zl \<cdot> wl \<cdot> [al] \<le>p (f ^^ (Suc g_p * Suc bf_p)) (wl \<cdot> [al])"
          by blast

        have "vl \<cdot> zl \<^sup>@ (k+Suc ka) \<cdot> wl \<cdot> [al] \<le>p (f ^^ Nper_k) [last w_pred]"
          unfolding Nper_k_def Per_def[symmetric]
          by (metis \<open>\<forall>k. vl \<cdot> zl \<^sup>@ k \<cdot> wl \<cdot> [al] \<le>p (f ^^ (g_N + Suc g_p * bf_N + k * (Suc g_p * Suc bf_p))) [last w_pred]\<close> add.assoc add_mult_distrib)


        have "bounded (vl \<cdot> zl \<^sup>@ (k+Suc ka) \<cdot> wl)"
          using UL_Ball[OF \<open>zl \<in> UL\<close>] UL_Ball[OF \<open>wl \<in> UL\<close>] UL_Ball[OF \<open>vl \<in> UL\<close>]
          unfolding bounded_concat_conv
          unfolding bounded_pow_conv[symmetric]
          using bounded_pow by blast


        have "int_s' = vl \<cdot> zl \<^sup>@ (k+Suc ka) \<cdot> wl"
        proof-
          have "(int_s' \<cdot> [hd int_s]) \<bowtie> (vl \<cdot> zl \<^sup>@ (k+Suc ka) \<cdot> wl) \<cdot> [al]"
            unfolding rassoc
            using \<open>(f ^^ (g_N + Suc g_p * bf_N + k * Suc Per + Suc ka * Suc Per)) [last w_pred] = int_s' \<cdot> int_s\<close>
            by (metis Nper_k_def \<open>vl \<cdot> zl \<^sup>@ (k + Suc ka) \<cdot> wl \<cdot> [al] \<le>p (f ^^ Nper_k) [last w_pred]\<close> hd_pref ruler' same_prefix_prefix that)
          from pref_comp_list_all[OF this \<open>list_all ?Pb int_s'\<close> _ \<open>\<not> bounded [hd int_s]\<close>] \<open>bounded (vl \<cdot> zl \<^sup>@ (k+Suc ka) \<cdot> wl)\<close> bounded_list_all fixed_unbounded_left_unbouded[OF \<open>al \<in> fixed_unbounded_left\<close>]
          have "int_s' = vl \<cdot> zl \<^sup>@ (k+Suc ka) \<cdot> wl \<and> hd int_s = al"
            by blast
          thus ?thesis
            by blast
        qed
        thus ?thesis
          using \<open>vl \<in> UL\<close> \<open>wl \<in> UL\<close> \<open>zl \<in> UL\<close> by blast
      qed
    qed

    have int_p_nemp: "\<not> bounded [hd w_pred]" "\<not> bounded [last int_p]"
      "\<exists>vl zl wl. vl \<in> UR \<and> zl \<in> UR \<and> wl \<in> UR \<and> int_p' = vl \<cdot> zl \<^sup>@ (k+Suc ka) \<cdot> wl" if "int_p \<noteq> \<epsilon>"     proof-
      have "\<not> bounded [last pp]"
        using \<open>pp = (f ^^ (g_N + Suc g_p * bf_N + k * Suc Per + Suc ka * Suc Per)) pp_pred \<cdot> int_p\<close> \<open>pp = \<epsilon> \<or> \<not> bounded [last pp]\<close> that by force
      hence "\<not> bounded int_p"
        by (metis \<open>pp = (f ^^ (g_N + Suc g_p * bf_N + k * Suc Per + Suc ka * Suc Per)) pp_pred \<cdot> int_p\<close> last_appendR last_in_set that unbounded_letter)
      thus "\<not> bounded [hd w_pred]"
        using bounded_concat_conv bounded_image_pow w_hd by metis
      show "\<not> bounded [last int_p]"
        using \<open>\<not> bounded [last pp]\<close> \<open>pp = (f ^^ (g_N + Suc g_p * bf_N + k * Suc Per + Suc ka * Suc Per)) pp_pred \<cdot> int_p\<close> that by auto
      show "\<exists>vl zl wl. vl \<in> UR \<and> zl \<in> UR \<and> wl \<in> UR \<and> int_p' = vl \<cdot> zl \<^sup>@ (k+Suc ka) \<cdot> wl"
      proof-
        from right_per[OF \<open>\<not> bounded [hd w_pred]\<close>]
        obtain zl wl vl al where  "zl \<in> UR" "wl \<in> UR" "al \<in> fixed_unbounded_right"
          "vl \<in> UR"
          "(\<forall>k. [al] \<cdot> wl \<cdot> zl \<^sup>@ k \<cdot> vl \<le>s (f ^^ (g_N + Suc g_p * bf_N + k * (Suc g_p * Suc bf_p))) [hd w_pred])" "(\<forall>k. [al] \<cdot> wl \<cdot> zl \<^sup>@ k \<le>s (f ^^ (Suc g_p * bf_N + k * (Suc g_p * Suc bf_p))) [al])"
          "(f ^^ (Suc g_p * Suc bf_p)) zl = zl \<and> [al] \<cdot> wl \<cdot> zl \<le>s (f ^^ (Suc g_p * Suc bf_p)) ([al] \<cdot> wl)"
          by blast

        have "[al] \<cdot> wl \<cdot> zl \<^sup>@ (k + Suc ka) \<cdot> vl \<le>s (f ^^ Nper_k) [hd w_pred]"
          unfolding Nper_k_def Ntk_def
          by (metis Per_def \<open>\<forall>k. [al] \<cdot> wl \<cdot> zl \<^sup>@ k \<cdot> vl \<le>s (f ^^ (g_N + Suc g_p * bf_N + k * (Suc g_p * Suc bf_p))) [hd w_pred]\<close> add.assoc add_mult_distrib)

        have "bounded (wl \<cdot> zl \<^sup>@  (k + Suc ka) \<cdot> vl)"
          using UR_Ball[OF \<open>zl \<in> UR\<close>] UR_Ball[OF \<open>wl \<in> UR\<close>] UR_Ball[OF \<open>vl \<in> UR\<close>]
          unfolding bounded_concat_conv
          unfolding bounded_pow_conv[symmetric]
          using bounded_pow by blast


        have "int_p' = wl \<cdot> zl \<^sup>@  (k + Suc ka) \<cdot> vl"
        proof-
          have "([last int_p] \<cdot> int_p') \<bowtie>\<^sub>s [al] \<cdot>(wl \<cdot> zl \<^sup>@ (k + Suc ka) \<cdot> vl)"
            unfolding rassoc
            using \<open>[al] \<cdot> wl \<cdot> zl \<^sup>@  (k + Suc ka) \<cdot> vl \<le>s (f ^^ Nper_k) [hd w_pred]\<close>
            unfolding Nper_k_def \<open>(f ^^ (g_N + Suc g_p * bf_N + k * Suc Per + Suc ka * Suc Per)) [hd w_pred] = int_p \<cdot> int_p'\<close>
            using ruler'[reversed, OF hd_pref[reversed, OF \<open>int_p \<noteq> \<epsilon>\<close>, folded same_suffix_suffix[of "[last int_p]" int_p' int_p]]]
            by blast
          from suf_comp_list_all[OF this \<open>list_all ?Pb int_p'\<close> _ \<open>\<not> bounded [last int_p]\<close>]
            \<open>bounded (wl \<cdot> zl \<^sup>@  (k + Suc ka) \<cdot> vl)\<close> bounded_list_all fixed_unbounded_right_unbouded[OF \<open>al \<in> fixed_unbounded_right\<close>]
          have "int_p' = wl \<cdot> zl \<^sup>@  (k + Suc ka) \<cdot> vl \<and> last int_p = al"
            by blast
          thus ?thesis
            by blast
        qed
        thus ?thesis
          using \<open>vl \<in> UR\<close> \<open>wl \<in> UR\<close> \<open>zl \<in> UR\<close> by blast
      qed
    qed



        obtain w1 u1 k1 w2 u2 k2 w3
        where "w1 \<in> W \<and> w2 \<in> W \<and> w3 \<in> W \<and> u1 \<in> U \<and> u2 \<in> U \<and> w \<le>f w1 \<cdot> u1 \<^sup>@ k1 \<cdot> w2 \<cdot> u2 \<^sup>@ k2 \<cdot> w3"
    proof(cases rule: case_split[OF case_split case_split])
      assume "int_p = \<epsilon>" "int_s = \<epsilon>"
      hence "bounded w_pred"
        by (metis \<open>(f ^^ Nper_k) w_pred = int_p \<cdot> (ps \<cdot> w \<cdot> sp) \<cdot> int_s\<close> \<open>bounded (ps \<cdot> w \<cdot> sp)\<close> endomorphism.bounded_image_pow endomorphism_axioms nemp_comm self_append_conv)
      have "bf_N \<le> Nper_k - ?Per"
        unfolding Nper_k_def Per_def[symmetric]
        by simp
       have "(f ^^ (Nper_k - ?Per)) w_pred = (f ^^ Nper_k) w_pred"
        using bounded_fixed[OF \<open>bounded w_pred\<close> \<open>bf_N \<le> Nper_k - ?Per\<close>, of "Suc g_p"]
        by (metis Nper_k_def Per_def add_leE le_add2 le_add_diff_inverse2 mult_Suc)
      hence "w \<le>f (f^^(n - ?Per)) axiom"
      proof-
        have "n - Nper_k + (Nper_k - ?Per) = n - ?Per"
          using Nper_k_def Per_def \<open>g_N + Suc g_p * bf_N + k * Suc Per + Suc ka * Suc Per \<le> n\<close> by fastforce
        have "w_pred \<le>f (f ^^ (n - Nper_k)) axiom"
          using Nper_k_def \<open>pp_pred \<cdot> w_pred \<cdot> ss_pred = (f ^^ (n - (g_N + Suc g_p * bf_N + k * Suc Per + Suc ka * Suc Per))) axiom\<close> facI' by blast
        hence "(f ^^ (Nper_k - ?Per)) w_pred \<le>f (f ^^ (Nper_k - ?Per)) ((f ^^ (n - Nper_k)) axiom)"
          using fac_mono_pow
          by (simp add: funpow_add'')
        thus ?thesis
          unfolding \<open>(f ^^ (Nper_k - ?Per)) w_pred = (f ^^ Nper_k) w_pred\<close> \<open>(f ^^ Nper_k) w_pred = int_p \<cdot> (ps \<cdot> w \<cdot> sp) \<cdot> int_s\<close>
          unfolding funpow_add'[symmetric] comp_apply[symmetric, of "(f ^^ (Nper_k - ?Per))"]
          unfolding \<open>n - Nper_k + (Nper_k - ?Per) = n - ?Per\<close>
          by (meson sublist_def sublist_order.order_trans)
      qed
      from \<open>(\<And>i. w \<le>f (f ^^ i) axiom \<Longrightarrow> n \<le> i)\<close>[OF this]
      have False
        using \<open>g_N + Suc g_p * bf_N + k * Suc Per + Suc ka * Suc Per \<le> n\<close> by force
      thus ?thesis
        by simp
    next
      assume "int_p = \<epsilon>" "int_s \<noteq> \<epsilon>"
      from int_s_nemp(3)[OF this(2)]
      obtain vl zl wl where "vl \<in> UL" "zl \<in> UL" "wl \<in> UL" "int_s' = vl \<cdot> zl \<^sup>@ (k + Suc ka) \<cdot> wl"
        by auto

      have "vl \<in> U"
        using U_def \<open>vl \<in> UL\<close> by blast
      have "zl \<in> U"
        using U_def \<open>zl \<in> UL\<close> by blast
      have "wl \<in> W"
        using W_def Wpred_def U_def \<open>wl \<in> UL\<close> \<open>\<epsilon> \<in> U_long\<close> by blast

      have "(f ^^ (g_N + Suc g_p * bf_N + k * Suc Per + Suc ka * Suc Per)) (butlast (tl w_pred)) \<cdot> vl \<in> W"
        using but_tl_U_long \<open>vl \<in> U\<close>
        unfolding W_def Wpred_def
        using \<open>\<epsilon> \<in> U\<close> by blast
      thus ?thesis
        using int_p_emp(2)[OF \<open>int_p = \<epsilon>\<close>]
        that[of int_p' "(f ^^ (g_N + Suc g_p * bf_N + k * Suc Per + Suc ka * Suc Per)) (butlast (tl w_pred)) \<cdot> vl" wl \<epsilon> zl _ "k+Suc ka", unfolded emp_simps, unfolded rassoc, folded \<open>int_s' = vl \<cdot> zl \<^sup>@ (k + Suc ka) \<cdot> wl\<close>, folded  w_int]
        \<open>vl \<in> U\<close> \<open>zl \<in> U\<close> \<open>wl \<in> W\<close> \<open>\<epsilon> \<in> U\<close>
        by blast
    next
      assume "int_p \<noteq> \<epsilon>" "int_s = \<epsilon>"
      from int_p_nemp(3)[OF this(1)]
      obtain vr zr wr where "vr \<in> UR" "zr \<in> UR" "wr \<in> UR" "int_p' = vr \<cdot> zr \<^sup>@ (k + Suc ka) \<cdot> wr"
        by auto

      have "wr \<in> U"
        using U_def \<open>wr \<in> UR\<close> by blast
      have "zr \<in> U"
        using U_def \<open>zr \<in> UR\<close> by blast
      have "vr \<in> W"
        using W_def Wpred_def U_def \<open>vr \<in> UR\<close> \<open>\<epsilon> \<in> U_long\<close> by blast

      have "wr \<cdot> (f ^^ (g_N + Suc g_p * bf_N + k * Suc Per + Suc ka * Suc Per)) (butlast (tl w_pred)) \<in> W"
        using but_tl_U_long \<open>wr \<in> U\<close>
        unfolding W_def Wpred_def
        using \<open>\<epsilon> \<in> U\<close> by blast
      thus ?thesis
        using int_s_emp(2)[OF \<open>int_s = \<epsilon>\<close>]
        that[of vr "wr \<cdot> (f ^^ (g_N + Suc g_p * bf_N + k * Suc Per + Suc ka * Suc Per)) (butlast (tl w_pred))" int_s'  zr \<epsilon> "k+Suc ka", unfolded emp_simps, unfolded rassoc, folded  w_int[unfolded \<open>int_p' = vr \<cdot> zr \<^sup>@ (k + Suc ka) \<cdot> wr\<close>, unfolded rassoc]]
        \<open>vr \<in> W\<close> \<open>zr \<in> U\<close> \<open>wr \<in> U\<close> \<open>\<epsilon> \<in> U\<close>
        by blast
    next
      assume "int_p \<noteq> \<epsilon>" "int_s \<noteq> \<epsilon>"
      from int_p_nemp(3)[OF this(1)]
      obtain vr zr wr where "vr \<in> UR" "zr \<in> UR" "wr \<in> UR" "int_p' = vr \<cdot> zr \<^sup>@ (k + Suc ka) \<cdot> wr"
        by auto

      have "wr \<in> U"
        using U_def \<open>wr \<in> UR\<close> by blast
      have "zr \<in> U"
        using U_def \<open>zr \<in> UR\<close> by blast
      have "vr \<in> W"
        using W_def Wpred_def U_def \<open>vr \<in> UR\<close> \<open>\<epsilon> \<in> U_long\<close> by blast

      from int_s_nemp(3)[OF \<open>int_s \<noteq> \<epsilon>\<close>]
      obtain vl zl wl where "vl \<in> UL" "zl \<in> UL" "wl \<in> UL" "int_s' = vl \<cdot> zl \<^sup>@ (k + Suc ka) \<cdot> wl"
        by auto

      have "vl \<in> U"
        using U_def \<open>vl \<in> UL\<close> by blast
      have "zl \<in> U"
        using U_def \<open>zl \<in> UL\<close> by blast
      have "wl \<in> W"
        using W_def Wpred_def U_def \<open>wl \<in> UL\<close> \<open>\<epsilon> \<in> U_long\<close> by blast

      have "wr \<cdot> (f ^^ (g_N + Suc g_p * bf_N + k * Suc Per + Suc ka * Suc Per)) (butlast (tl w_pred)) \<cdot> vl  \<in> W"
        using but_tl_U_long \<open>wr \<in> U\<close> \<open>vl \<in> U\<close>
        unfolding W_def Wpred_def
        using \<open>\<epsilon> \<in> U\<close> by blast
      thus ?thesis
        using that[of vr "wr \<cdot> (f ^^ (g_N + Suc g_p * bf_N + k * Suc Per + Suc ka * Suc Per)) (butlast (tl w_pred)) \<cdot> vl" wl zr zl "k + Suc ka" "k + Suc ka"]
        unfolding rassoc w_int[unfolded \<open>int_p' = vr \<cdot> zr \<^sup>@ (k + Suc ka) \<cdot> wr\<close> \<open>int_s' = vl \<cdot> zl \<^sup>@ (k + Suc ka) \<cdot> wl\<close>, unfolded rassoc, symmetric]
        using \<open>vr \<in> W\<close> \<open>wl \<in> W\<close> \<open>zl \<in> U\<close> \<open>zr \<in> U\<close> by blast
    qed
    thus ?thesis
      using Q_def by blast
  qed
  thus ?thesis
    using that[of W U]
    unfolding Q_def
    using \<open>finite U\<close> \<open>finite W\<close> by blast
qed

end
subsection "Long bounded factor"

context endomorphism
begin

lemma long_bounded_factor:
  assumes "finite (UNIV::'a set)"
  obtains W U where "finite W" and "finite U" and
  "\<And>w. w \<in> \<L> axiom \<Longrightarrow> bounded w \<Longrightarrow>
    (\<exists>w1 w2 w3 u1 u2 k1 k2. w1 \<in> W \<and> w2 \<in> W \<and> w3 \<in> W \<and> u1 \<in> U \<and> u2 \<in> U \<and> w \<le>f w1\<cdot>u1\<^sup>@k1\<cdot>w2\<cdot>u2\<^sup>@k2\<cdot>w3)"
    using endomorphism_fixed.intro[OF endomorphism_axioms endomorphism_fixed_axioms.intro, THEN
      endomorphism_fixed.long_bounded_factor, OF \<open>finite (UNIV::'a set)\<close> _ _]
    first_unbounded_both_fixedEx[OF \<open>finite (UNIV::'a set)\<close>] bounded_fixedEx[OF \<open>finite (UNIV::'a set)\<close>]
    by metis








end




section "Counting the number of primitive elements of unbounded exponent"

definition (in endomorphism) pushy_sequence
  where "pushy_sequence sw \<equiv> (\<forall>n. \<exists>i w. \<^bold>|w\<^bold>| > n \<and> bounded w \<and> w \<le>f sw i)"


context endomorphism_morphism
begin

definition is_repetition_embed_sequence
  where "is_repetition_embed_sequence axiom u seq \<equiv> ((\<forall>j. seq j \<in> \<L> axiom \<and> (\<exists>k. h (seq j) \<le>f u\<^sup>@k)) \<and> strictly_growing_seq seq)"

lemma rep_em_seqE: assumes "is_repetition_embed_sequence axiom u sw"
  shows
    "\<And>j. sw j \<in> \<L> axiom" and
    "\<And>j. \<exists>k. h (sw j) \<le>f u\<^sup>@k" and
    "strictly_growing_seq sw"
  using assms is_repetition_embed_sequence_def by blast+

lemma rep_em_seq_nemp: assumes "is_repetition_embed_sequence axiom u sw" "nonerasing_morphism h"
  shows "u \<noteq> \<epsilon>"
proof-
  have "\<^bold>|sw 1\<^bold>| \<ge> 1"
    using rep_em_seqE(3)[OF \<open>is_repetition_embed_sequence axiom u sw\<close>]
    by (simp add: sgrowing_len)
  hence "\<^bold>|h (sw 1)\<^bold>| \<ge> 1"
    using assms(2) nonerasing_morphism.im_len_le order.trans by blast
  thus ?thesis
    using rep_em_seqE(2)[OF \<open>is_repetition_embed_sequence axiom u sw\<close>]
    by force
qed

lemma rep_em_seq_prim_unb_exp:
  assumes "is_repetition_embed_sequence axiom u sw" "nonerasing_morphism h"
  shows "primitive_unbounded_exponent (\<H> axiom) (\<rho> u)"
proof-
  note rep_em_seqE[OF \<open>is_repetition_embed_sequence axiom u sw\<close>] rep_em_seq_nemp[OF \<open>is_repetition_embed_sequence axiom u sw\<close> \<open>nonerasing_morphism h\<close>]
  have "(\<rho> u) \<^sup>@ n \<in> \<H> axiom" for n
  proof-
    let ?j = "((Suc n))*\<^bold>|u\<^bold>|"
    have "\<^bold>|sw (Suc ?j)\<^bold>| > ?j"
      by (meson Suc_le_eq \<open>strictly_growing_seq sw\<close> sgrowing_len)
    hence "\<^bold>|h (sw (Suc ?j))\<^bold>| > ?j"
      by (meson assms(2) dual_order.trans linorder_not_less nonerasing_morphism.im_len_le)
    obtain l where "h (sw (Suc ?j)) \<le>f u\<^sup>@l"
      using \<open>\<And>j. \<exists>k. h (sw j) \<le>f u \<^sup>@ k\<close> by blast
    from long_pow_fac[unfolded pow_len, OF this \<open>\<^bold>|h (sw (Suc ?j))\<^bold>| > ?j\<close>]
    have "u\<^sup>@n \<le>f (h (sw (Suc ?j)))".
    hence "((\<rho> u)\<^sup>@(e\<^sub>\<rho> u))\<^sup>@n \<le>f (h (sw (Suc ?j)))"
      by (simp add: \<open>u \<noteq> \<epsilon>\<close>)
    moreover have "((\<rho> u))\<^sup>@n \<le>p ((\<rho> u)\<^sup>@(e\<^sub>\<rho> u))\<^sup>@n"
      unfolding pow_mult[symmetric]
      using primroot_exp_nemp[OF \<open>u \<noteq> \<epsilon>\<close>]
      by (simp add: le_exps_pref)
    ultimately have "((\<rho> u))\<^sup>@n \<le>f (h (sw (Suc ?j)))"
      by (meson prefix_imp_sublist sublist_order.order_trans)
    thus ?thesis
      by (metis \<open>\<And>j. sw j \<in> \<L> axiom\<close> factor_closure.intros imageI morphic_language_def)
  qed
  thus ?thesis
    using \<open>u \<noteq> \<epsilon>\<close> by blast
qed

lemma rep_em_seq_prim_unb_exp':
  assumes "is_repetition_embed_sequence axiom u sw" "nonerasing_morphism h" "primitive u"
  shows "primitive_unbounded_exponent (\<H> axiom) u"
  by (metis assms(1) assms(2) assms(3) prim_primroot rep_em_seq_prim_unb_exp)


context
begin

private definition is_repetition_embed_sequence_next_element
  where  "is_repetition_embed_sequence_next_element axiom u l w \<equiv> l < \<^bold>|w\<^bold>| \<and> w \<in> \<L> axiom \<and> (\<exists>k. h w \<le>f u\<^sup>@k) "

private lemma rep_em_seq_next_ex: assumes "finite (UNIV::'a set)" "\<And>k. u\<^sup>@k \<in> \<H> axiom" "u \<noteq> \<epsilon>"
  obtains w where "is_repetition_embed_sequence_next_element axiom u l w"
  unfolding is_repetition_embed_sequence_next_element_def
proof-
  have "infinite \<H> axiom"
    by (metis assms(2-3) finite_maxlen linorder_not_le long_pow)
  hence "infinite \<L> axiom"
    by (metis fac_clo_finite finite_imageI morphic_language_def)
  have "1 \<le> \<lceil>h\<rceil>"
  proof(rule ccontr)
    assume " \<not> 1 \<le> \<lceil>h\<rceil>"
                    hence "h w = \<epsilon>" for w
      using H.max_im_len_le_dom H.max_im_len_emp assms(1) by fastforce
    hence "h`(\<L> axiom) = {\<epsilon>}"
      by (metis assms(2) assms(3) fac_len length_greater_0_conv linorder_not_le list.size(3) mor_lanE nat_mult_1 pow_len)
    hence "\<H> axiom = {\<epsilon>}"
      by (metis \<open>infinite \<H> axiom\<close> fac_clo_finite finite.emptyI finite.insertI morphic_language_def)
    thus False
      using \<open>infinite \<H> axiom\<close> by force
  qed
  have "\<exists>w. l < \<^bold>|w\<^bold>| \<and> w \<in> \<L> axiom \<and> h w \<le>f u \<^sup>@ ((Suc (Suc l))*\<lceil>h\<rceil>)"
  proof-
    obtain v where "v \<in> \<L> axiom" "u \<^sup>@ ((Suc (Suc l))*\<lceil>h\<rceil>) \<le>f h v"
      by (meson assms(2) mor_lan_fac_pow pmor_lan_pow_in)
    from H.image_fac_interp'[OF this(2)]
    obtain p w_pred s where
      "w_pred \<le>f v" and
      "p (u \<^sup>@ ((Suc (Suc l))*\<lceil>h\<rceil>)) s \<sim>\<^sub>\<I> map h\<^sup>\<C> w_pred"
      by (metis One_nat_def Suc_le_eq \<open>1 \<le> \<lceil>h\<rceil>\<close> assms(3) linorder_not_le list.size(3) long_pow nat_0_less_mult_iff zero_less_Suc)
    note fac_interpD[OF this(2), unfolded H.morph_concat_map]
    have "((Suc (Suc l))*\<lceil>h\<rceil>) * \<^bold>|u\<^bold>| \<le> \<^bold>|h w_pred\<^bold>|"
      using lenarg[OF \<open>p \<cdot> u \<^sup>@ ((Suc (Suc l))*\<lceil>h\<rceil>) \<cdot> s = h w_pred\<close>]
      unfolding lenmorph pow_len
      by linarith
    hence "Suc (Suc l) \<le> \<^bold>|w_pred\<^bold>|"
      using \<open>1 \<le> \<lceil>h\<rceil>\<close> quotient_smaller[OF nemp_len_not0[OF assms(3)], unfolded mult.commute[of "\<^bold>|u\<^bold>|"]]  H.preim_len_geq_dom[OF assms(1)] le_trans by blast
    hence "1 < \<^bold>|w_pred\<^bold>|"
      by linarith
    hence "w_pred \<noteq> \<epsilon>"
      by force

    have "w_pred \<in> \<L> axiom"
      using \<open>v \<in> \<L> axiom\<close> \<open>w_pred \<le>f v\<close> purely_morphic_language.intros(3) by blast


    show ?thesis
    proof(cases)
      assume "((Suc (Suc l))*\<lceil>h\<rceil>) * \<^bold>|u\<^bold>| = \<^bold>|h w_pred\<^bold>|"
      hence "u \<^sup>@ (Suc (Suc l) * \<lceil>h\<rceil>) = h w_pred"
        using lenarg[OF \<open>p\<cdot> u \<^sup>@ (Suc (Suc l) * \<lceil>h\<rceil>) \<cdot> s = h w_pred\<close>]
        unfolding lenmorph pow_len
        using \<open>p \<cdot> u \<^sup>@ (Suc (Suc l) * \<lceil>h\<rceil>) \<cdot> s = h w_pred\<close> by auto
      hence "l < \<^bold>|w_pred\<^bold>| \<and> w_pred \<in> \<L> axiom  \<and> h w_pred \<le>f u \<^sup>@ ((Suc (Suc l))*\<lceil>h\<rceil>)"
        using \<open>w_pred \<in> \<L> axiom\<close> \<open>Suc (Suc l) \<le> \<^bold>|w_pred\<^bold>|\<close> by auto
      thus ?thesis
        by blast
    next
      assume "((Suc (Suc l))*\<lceil>h\<rceil>) * \<^bold>|u\<^bold>| \<noteq> \<^bold>|h w_pred\<^bold>|"
      hence "((Suc (Suc l))*\<lceil>h\<rceil>) * \<^bold>|u\<^bold>| < \<^bold>|h w_pred\<^bold>|"
        using \<open>Suc (Suc l) * \<lceil>h\<rceil> * \<^bold>|u\<^bold>| \<le> \<^bold>|h w_pred\<^bold>|\<close> le_neq_implies_less by blast

      note hw_pred = arg_cong[OF hd_middle_last[OF \<open>1 < \<^bold>|w_pred\<^bold>|\<close>], of h, symmetric]
      have "h (butlast (tl w_pred)) \<le>f u \<^sup>@ ((Suc (Suc l))*\<lceil>h\<rceil>)"
        using pref_suf_mid[OF \<open>p \<cdot> u \<^sup>@ ((Suc (Suc l))*\<lceil>h\<rceil>) \<cdot> s = h w_pred\<close>[unfolded hw_pred, unfolded H.morph]]
          sprefD1[OF \<open>p <p hd (map h\<^sup>\<C> w_pred)\<close>, unfolded hd_map[OF \<open>w_pred \<noteq> \<epsilon>\<close>] core_sing]
          ssufD1[OF \<open>s <s last (map h\<^sup>\<C> w_pred)\<close>, unfolded last_map[OF \<open>w_pred \<noteq> \<epsilon>\<close>] core_sing]
        by blast
      moreover have "l*\<lceil>h\<rceil> < \<^bold>|h (butlast (tl w_pred))\<^bold>|"
      proof(rule ccontr)
        assume "\<not> l * \<lceil>h\<rceil> < \<^bold>|h (butlast (tl w_pred))\<^bold>|"
        hence "\<^bold>|h (butlast (tl w_pred))\<^bold>| \<le> l* \<lceil>h\<rceil>"
          using linorder_not_le by blast
        hence "\<^bold>|h w_pred\<^bold>| \<le> \<lceil>h\<rceil> + l*\<lceil>h\<rceil> + \<lceil>h\<rceil>"
          unfolding lenarg[OF hw_pred]
          unfolding H.morph lenmorph
          by (metis H.max_im_len_le_dom One_nat_def add.commute add_mono_thms_linordered_semiring(1) assms(1) length_Cons list.size(3) nat_mult_1)
        thus False
          using \<open>Suc (Suc l) * \<lceil>h\<rceil> * \<^bold>|u\<^bold>| \<le> \<^bold>|h w_pred\<^bold>|\<close> \<open>Suc (Suc l) * \<lceil>h\<rceil> * \<^bold>|u\<^bold>| \<noteq> \<^bold>|h w_pred\<^bold>|\<close> \<open>\<^bold>|h w_pred\<^bold>| \<le> \<lceil>h\<rceil> + l * \<lceil>h\<rceil> + \<lceil>h\<rceil>\<close> add.commute assms(3) le_antisym le_trans long_pow mult_Suc pow_len by metis
      qed
      moreover have "butlast (tl w_pred) \<in> \<L> axiom"
        using \<open>w_pred \<in> \<L> axiom\<close> factorialD pmor_lan_fac sublist_butlast sublist_tl
        by metis
      ultimately have "l < \<^bold>|butlast (tl w_pred)\<^bold>| \<and> (butlast (tl w_pred)) \<in> \<L> axiom  \<and> h (butlast (tl w_pred)) \<le>f u \<^sup>@ (Suc (Suc l) * \<lceil>h\<rceil>)"
        using  H.max_im_len_le_dom assms(1) le_trans linorder_not_le mult.commute mult_le_cancel1 by smt
      thus ?thesis
        by blast
    qed
  qed
  thus ?thesis
    using is_repetition_embed_sequence_next_element_def that by blast
qed

private fun repetition_embed_sequence_next_element where
  "repetition_embed_sequence_next_element axiom u prev =
  (if (\<exists>w. is_repetition_embed_sequence_next_element axiom u \<^bold>|prev\<^bold>| w) then (SOME w. is_repetition_embed_sequence_next_element axiom u \<^bold>|prev\<^bold>| w) else undefined)"

private lemma rep_em_seq_next: assumes "finite (UNIV::'a set)" "\<And>k. u\<^sup>@k \<in> \<H> axiom" "u \<noteq> \<epsilon>"
  shows "is_repetition_embed_sequence_next_element axiom u \<^bold>|prev\<^bold>| (repetition_embed_sequence_next_element axiom u prev)"
  using rep_em_seq_next_ex[OF assms]
  unfolding repetition_embed_sequence_next_element.simps
  using someI[of "\<lambda>w. is_repetition_embed_sequence_next_element axiom u \<^bold>|prev\<^bold>| w"]
  by auto

private fun repetition_embed_sequence where
  "repetition_embed_sequence axiom u 0 = \<epsilon>" |
  "repetition_embed_sequence axiom u (Suc k) = repetition_embed_sequence_next_element axiom u (repetition_embed_sequence axiom u k)"


private lemma rep_em_seq: assumes "finite (UNIV::'a set)" "\<And>k. u\<^sup>@k \<in> \<H> axiom" "u \<noteq> \<epsilon>"
  shows "is_repetition_embed_sequence axiom u (repetition_embed_sequence axiom u)"
proof-
  have "repetition_embed_sequence axiom u j \<in> \<L> axiom  \<and> (\<exists>k. h (repetition_embed_sequence axiom u j) \<le>f u \<^sup>@ k)" for j
  proof(cases rule: nat.exhaust[of j], simp add: pmor_lan_fac_pow)
    case (Suc x2)
    show ?thesis
      unfolding \<open>j = Suc x2\<close>
      unfolding repetition_embed_sequence.simps
      using rep_em_seq_next[OF assms, of "(repetition_embed_sequence axiom u x2)"]
      unfolding is_repetition_embed_sequence_next_element_def
      by blast
  qed
  moreover have "\<^bold>|repetition_embed_sequence axiom u j\<^bold>| < \<^bold>|repetition_embed_sequence axiom u (Suc j)\<^bold>|" for j
    using rep_em_seq_next[OF assms]
    unfolding repetition_embed_sequence.simps(2) is_repetition_embed_sequence_next_element_def
    by blast
  ultimately show "?thesis"
    unfolding is_repetition_embed_sequence_def strictly_growing_seq_def
    by blast
qed

lemma rep_em_seq_ex: assumes "finite (UNIV::'a set)" "primitive_unbounded_exponent (\<H> axiom) w"
          obtains sw where "is_repetition_embed_sequence axiom w sw"
  by (metis assms prim_unb_expE(1) prim_unb_expE(2) prim_nemp rep_em_seq)

end
lemma pushy_rem_em_seq_finite: assumes finite_UNIV: "finite (UNIV::'a set)" and "nonerasing_morphism h"
  obtains HU where
    "finite HU" and
    "\<And>u. primitive u \<and> (\<exists> sw. is_repetition_embed_sequence axiom u sw \<and> pushy_sequence sw) \<longrightarrow> u \<in> HU"
proof-
  obtain W U where
    "finite W" and
    "finite U" and
    UW: "(\<And>w. w \<in> \<L> axiom \<Longrightarrow> bounded w \<Longrightarrow> \<exists>w1 w2 w3 u1 u2 k1 k2. w1 \<in> W \<and> w2 \<in> W \<and> w3 \<in> W \<and> u1 \<in> U \<and> u2 \<in> U \<and> w \<le>f w1 \<cdot> u1 \<^sup>@ k1 \<cdot> w2 \<cdot> u2 \<^sup>@ k2 \<cdot> w3)"
    using long_bounded_factor[OF finite_UNIV]
    by metis

  obtain lW where lW_def: "\<And>w. w \<in> W \<Longrightarrow> \<^bold>|w\<^bold>| \<le> lW"
    by (meson \<open>finite W\<close> finite_maxlen le_eq_less_or_eq)
  obtain lU where lU_def: "\<And>w. w \<in> U \<Longrightarrow> \<^bold>|w\<^bold>| \<le> lU"
    by (meson \<open>finite U\<close> finite_maxlen le_eq_less_or_eq)


  have le2: "w1 \<in> W \<Longrightarrow> u1 \<in> U \<Longrightarrow> \<^bold>|w1\<cdot>u1\<^sup>@k1\<^bold>| \<le> lW + k1*lU" for w1 u1 k1
    unfolding lenmorph pow_len
    using lW_def lU_def
    by (simp add: add_mono_thms_linordered_semiring(1))
  hence le3: "w1 \<in> W \<Longrightarrow> u1 \<in> U \<Longrightarrow> w2 \<in> W \<Longrightarrow> \<^bold>|w1\<cdot>u1\<^sup>@k1\<cdot>w2\<^bold>| \<le> 2*lW + k1*lU" for w1 u1 k1 w2
    by (simp add: add.assoc add.commute add_mono_thms_linordered_semiring(1) lW_def mult_2)
  hence le4: "w1 \<in> W \<Longrightarrow> u1 \<in> U \<Longrightarrow> w2 \<in> W \<Longrightarrow> u2 \<in> U \<Longrightarrow> \<^bold>|w1\<cdot>u1\<^sup>@k1\<cdot>w2\<cdot>u2\<^sup>@k2\<^bold>| \<le> 2*lW + (k1+k2)*lU" for w1 u1 k1 w2 u2 k2
    unfolding lassoc lenmorph[of _ "u2\<^sup>@k2"] pow_len[of u2]
    using lU_def[of u2]
    unfolding add_mult_distrib add.assoc[symmetric]
    using add_le_mono nat_mult_le_cancel_disj by presburger
  from this[THEN add_mono_thms_linordered_semiring(1)[OF conjI, of _ _ _ lW], unfolded add.commute[of _ lW], unfolded add.assoc[symmetric]]
  have le5: "w1 \<in> W \<Longrightarrow> u1 \<in> U \<Longrightarrow> w2 \<in> W \<Longrightarrow> u2 \<in> U \<Longrightarrow> w3 \<in> W \<Longrightarrow> \<^bold>|w1\<cdot>u1\<^sup>@k1\<cdot>w2\<cdot>u2\<^sup>@k2\<cdot>w3\<^bold>| \<le> 3*lW + (k1+k2)*lU" for w1 u1 k1 w2 u2 k2 w3
    unfolding lassoc lenmorph[of _ "w3"]
    using lW_def[of w3]
    unfolding numeral_3_eq_3 Suc_1[symmetric] One_nat_def
    unfolding Suc_eq_plus1
    unfolding add_mult_distrib mult_1 mult_0 add_0
    unfolding add.assoc
    by force

    define HU where "HU = {r. \<exists>r' \<in> primitive_root`h`U. r \<sim> r'}"

  have "u \<in> HU" if "primitive u" "(\<exists> sw. is_repetition_embed_sequence axiom u sw \<and> pushy_sequence sw)" for u
  proof-
    obtain sw where "is_repetition_embed_sequence axiom u sw" "pushy_sequence sw"
      using \<open>\<exists>sw. is_repetition_embed_sequence axiom u sw \<and> pushy_sequence sw\<close> by blast

    note rep_em_seqE[OF \<open>is_repetition_embed_sequence axiom u sw\<close>]

    have "primitive_unbounded_exponent (\<H> axiom) u"
      using \<open>primitive u\<close> \<open>is_repetition_embed_sequence axiom u sw\<close> assms(2) rep_em_seq_prim_unb_exp' by auto

        define q where "q = 2*\<^bold>|u\<^bold>|"
    hence "q \<noteq> 0"
      using prim_nemp[OF \<open>primitive u\<close>] max_nat.eq_neutr_iff nemp_len
      by simp

    define n where "n = 3*lW + (2*q)*lU"

    obtain w j where "bounded w" "\<^bold>|w\<^bold>| > n" "w \<le>f sw j"
      by (meson \<open>pushy_sequence sw\<close> pushy_sequence_def)
    then obtain l where "h w \<le>f u\<^sup>@l"
      by (meson H.fac_mono \<open>\<And>j. \<exists>k. h (sw j) \<le>f u \<^sup>@ k\<close> fac_trans)
    have "w \<in> \<L> axiom"
      using \<open>\<And>j. sw j \<in> \<L> axiom\<close> \<open>w \<le>f sw j\<close> purely_morphic_language.intros(3) by blast


    from UW[OF \<open>w \<in> \<L> axiom\<close> \<open>bounded w\<close>]
    obtain w1 w2 w3 u1 u2 k1 k2 where
     "w1 \<in> W" "w2 \<in> W" "w3 \<in> W"
     "u1 \<in> U" "u2 \<in> U"
     "w \<le>f w1 \<cdot> u1 \<^sup>@ k1 \<cdot> w2 \<cdot> u2 \<^sup>@ k2 \<cdot> w3"
      by meson


    have qin: "(u1\<^sup>@q \<le>f w \<and> u1 \<noteq> \<epsilon>) \<or> (u2\<^sup>@q \<le>f w  \<and> u2 \<noteq> \<epsilon>)"
    proof-
      obtain p s where pws: "p\<cdot>w\<cdot>s = w1 \<cdot> u1 \<^sup>@ k1 \<cdot> w2 \<cdot> u2 \<^sup>@ k2 \<cdot> w3"
        by (metis \<open>w \<le>f w1 \<cdot> u1 \<^sup>@ k1 \<cdot> w2 \<cdot> u2 \<^sup>@ k2 \<cdot> w3\<close> facE)

      have "n \<ge> \<^bold>|u1\<^sup>@q\<^bold>|"
        unfolding pow_len n_def
        using lU_def[OF \<open>u1 \<in> U\<close>]
        by (simp add: trans_le_add2)

      \<comment> \<open>aux. lemma for case analysis below\<close>
      have lem: "u0\<^sup>@q \<le>f w" if "w \<le>p p'\<cdot>w0" "p' \<le>s u0\<^sup>@k0" "w0 \<in> W" "u0 \<in> U" "u0 \<noteq> \<epsilon>"
        for u0 p' w0 k0
      proof-
        obtain m where
          "u0 \<^sup>@ m \<le>s p'" and
          "p' <s u0 \<cdot> u0 \<^sup>@ m"
          using pref_mod_pow[reversed, OF \<open>p' \<le>s u0\<^sup>@k0\<close> \<open>u0 \<noteq> \<epsilon>\<close>] cancel_right same_suffix_suffix sufI unfolding suffix_order.less_le by metis
        have "\<^bold>|p'\<cdot>w0\<^bold>| < lW + lU + m*lU"
          using lU_def[OF \<open>u0 \<in> U\<close>] suffix_length_less[OF \<open>p' <s u0 \<cdot> u0 \<^sup>@ m\<close>]
          unfolding lenmorph pow_len
          using lW_def[OF \<open>w0 \<in> W\<close>]
          unfolding lenmorph[of _ w0] add.commute[of lW] add.assoc
          unfolding  add.assoc[symmetric]
          by (smt (verit, ccfv_SIG) add_le_less_mono add_le_mono add_right_imp_eq le_antisym le_trans mult_le_mono2 nat_less_le)
        hence "... > n"
          using \<open>\<^bold>|w\<^bold>| > n\<close> pref_len[OF \<open>w \<le>p p'\<cdot>w0\<close>]
          by linarith
        moreover have "n \<ge> lW + lU + q * lU"
          unfolding n_def mult_2 add_mult_distrib
          unfolding numeral_3_eq_3
          using  add_mono le_add1 quotient_smaller[OF \<open>q \<noteq> 0\<close>] trans_le_add2
          add.assoc mult.commute mult_Suc_right add_le_cancel_right by metis
        ultimately have "m > q"
          by (meson leI le_trans less_le_not_le mult_le_mono1 nat_add_left_cancel_le)

        obtain p'' where "p' = p'' \<cdot> u0 \<^sup>@ m" "p'' \<le>s u0"
          by (metis \<open>p' <s u0 \<cdot> u0 \<^sup>@ m\<close> \<open>u0\<^sup>@ m \<le>s p'\<close> same_suffix_suffix ssufD suffix_def)
        have "p' = p'' \<cdot> u0 \<^sup>@ q \<cdot> u0 \<^sup>@ (m-q)"
          by (metis \<open>m > q\<close> \<open>p' = p'' \<cdot> u0 \<^sup>@ m\<close> less_or_eq_imp_le pop_pow)
        hence "p'' \<cdot> u0 \<^sup>@ q \<bowtie> w"
          by (metis (no_types, opaque_lifting) append_eq_appendI prefix_def ruler_pref'' that(1))
        moreover have "\<^bold>|p'' \<cdot> u0 \<^sup>@ q\<^bold>| \<le> n"
          unfolding lenmorph pow_len
          using lU_def[OF \<open>u1 \<in> U\<close>] \<open>p'' \<le>s u0\<close>
          by (meson \<open>lW + lU + q * lU \<le> n\<close> add_mono_thms_linordered_semiring(1) dual_order.trans lU_def le_add2 mult_le_mono2 suffix_length_le that(4))
        ultimately have "p'' \<cdot> u0 \<^sup>@ q \<le>p w"
          by (meson \<open>n < \<^bold>|w\<^bold>|\<close> comp_shorter le_trans less_or_eq_imp_le)
        thus "u0 \<^sup>@ q \<le>f w"
          by (meson fac_pref)
      qed

      consider
        "p \<le>p w1" |
        "\<not> p \<le>p w1 \<and> p \<le>p w1\<cdot>u1 \<^sup>@ k1" |
        "\<not> p \<le>p w1\<cdot>u1 \<^sup>@ k1 \<and> p \<le>p w1 \<cdot> u1 \<^sup>@ k1 \<cdot> w2" |
        "\<not> p \<le>p w1 \<cdot> u1 \<^sup>@ k1 \<cdot> w2 \<and> p \<le>p w1 \<cdot> u1 \<^sup>@ k1 \<cdot> w2 \<cdot> u2 \<^sup>@ k2" |
        "\<not> p \<le>p w1 \<cdot> u1 \<^sup>@ k1 \<cdot> w2 \<cdot> u2 \<^sup>@ k2"
        by blast
      then show ?thesis
      proof(cases)
        case 1

        consider  "k1 > q" "u1 \<noteq> \<epsilon>" |
          "k1 \<le> q \<or> u1 = \<epsilon>" "k2 > q" "u2 \<noteq> \<epsilon>"
        proof(cases)
          assume "k1 \<le> q \<or> u1 = \<epsilon>"
          hence "\<^bold>|u1\<^sup>@k1\<^bold>| \<le> q*lU"
            using lU_def[OF \<open>u1 \<in> U\<close>]
            unfolding pow_len
            by (metis bot_nat_0.extremum list.size(3) mult.commute mult_0 mult_le_mono)
          hence "\<^bold>|w\<^bold>| \<le> 3*lW + (q + k2)*lU"
            using fac_len[OF \<open>w \<le>f w1 \<cdot> u1 \<^sup>@ k1 \<cdot> w2 \<cdot> u2 \<^sup>@ k2 \<cdot> w3\<close>]
              le3[OF \<open>w2 \<in> W\<close> \<open>u2 \<in> U\<close> \<open>w3 \<in> W\<close>, of k2]
              lW_def[OF \<open>w1 \<in> W\<close>]
            unfolding lenmorph[of w1] lenmorph[of "u1\<^sup>@k1"] rassoc
            unfolding add_mult_distrib
            by linarith
          hence "k2 > q"
            by (smt (verit, ccfv_threshold) \<open>n < \<^bold>|w\<^bold>|\<close> add_less_cancel_left le_square linorder_neqE_nat mult_2 mult_less_cancel2 n_def order_less_le_trans order_less_trans)
          moreover have "u2 \<noteq> \<epsilon>"
          proof(rule ccontr)
            assume "\<not> u2 \<noteq> \<epsilon>" hence "u2 = \<epsilon>" by blast
            have "w \<le>f w1 \<cdot> u1 \<^sup>@ k1 \<cdot> w2 \<cdot> w3"
              using \<open>w \<le>f w1 \<cdot> u1 \<^sup>@ k1 \<cdot> w2 \<cdot> u2 \<^sup>@ k2 \<cdot> w3\<close>
              unfolding \<open>u2 = \<epsilon>\<close> emp_simps.
            from fac_len[OF this]
            have "\<^bold>|w\<^bold>| \<le> 3*lW + q*lU"
              unfolding lenmorph
              using lW_def[OF \<open>w1 \<in> W\<close>] lW_def[OF \<open>w2 \<in> W\<close>] lW_def[OF \<open>w3 \<in> W\<close>] \<open>\<^bold>|u1\<^sup>@k1\<^bold>| \<le> q*lU\<close>
              by linarith
            thus False
              using \<open>n < \<^bold>|w\<^bold>|\<close> n_def by linarith
          qed
          ultimately show ?thesis
            using \<open>k1 \<le> q \<or> u1 = \<epsilon>\<close> that(2) by blast
        next
          assume "\<not> (k1 \<le> q \<or> u1 = \<epsilon>)"
          thus ?thesis
            using linorder_not_le that(1) by blast
        qed
        then show ?thesis
        proof(cases)
          assume "k1 > q" "u1 \<noteq> \<epsilon>"
          hence "k1 = q + (k1 - q)"
            by linarith
          hence "p \<cdot> w \<cdot> s = w1 \<cdot> u1 \<^sup>@ (q + (k1 - q)) \<cdot> w2 \<cdot> u2 \<^sup>@ k2 \<cdot> w3"
            by (metis pws)
          hence "p\<cdot>w \<bowtie> w1 \<cdot> u1 \<^sup>@ q"
            unfolding add_exps unfolding lassoc
            by (metis append_assoc eqd_comp)

          from le2[OF \<open>w1 \<in> W\<close> \<open>u1 \<in> U\<close>, of q]
          have "\<^bold>|w1 \<cdot> u1 \<^sup>@ q\<^bold>| \<le> n"
            unfolding n_def mult_2 add_mult_distrib
            by linarith
          hence "\<^bold>|w\<^bold>| \<ge> \<^bold>|w1 \<cdot> u1 \<^sup>@ q\<^bold>|"
            using \<open>n < \<^bold>|w\<^bold>|\<close>
            by linarith
          hence "w1 \<cdot> u1 \<^sup>@ q \<le>p p\<cdot>w"
            by (meson \<open>p \<cdot> w \<bowtie> w1 \<cdot> u1 \<^sup>@ q\<close> comp_shorter dual_order.trans pref_comp_sym suf_len')
          hence "u1 \<^sup>@ q \<le>f w"
            using prefE[OF \<open>p \<le>p w1\<close>] pref_cancel[of p]
            using append_assoc fac_trans prefix_imp_sublist sublist_append_leftI by smt
          thus ?thesis
            using \<open>u1 \<noteq> \<epsilon>\<close> by blast
        next
          assume "k1 \<le> q \<or> u1 = \<epsilon>" "k2 > q" "u2 \<noteq> \<epsilon>"
          hence "k2 = q + (k2 - q)"
            by linarith
          hence "p \<cdot> w \<cdot> s = w1 \<cdot> u1 \<^sup>@ k1 \<cdot> w2 \<cdot> u2 \<^sup>@ (q + (k2 - q)) \<cdot> w3"
            by (metis pws)
          hence "p\<cdot>w \<bowtie> w1 \<cdot> u1 \<^sup>@ k1 \<cdot> w2 \<cdot> u2 \<^sup>@ q"
            unfolding add_exps unfolding lassoc
            by (metis append_assoc prefI ruler_pref'')

          have "\<^bold>|u1\<^sup>@k1\<^bold>| \<le> q*lU"
            using \<open>k1 \<le> q \<or> u1 = \<epsilon>\<close> lU_def[OF \<open>u1 \<in> U\<close>]
            by (metis bot_nat_0.extremum list.size(3) mult_is_0 mult_le_mono pow_len)
          hence "\<^bold>|w1 \<cdot> u1 \<^sup>@ k1 \<cdot> w2 \<cdot> u2 \<^sup>@ q\<^bold>| \<le> 2*lW + (2*q)*lU"
            unfolding lenmorph add_mult_distrib mult_2
            using lW_def[OF \<open>w1 \<in> W\<close>] lW_def[OF \<open>w2 \<in> W\<close>] lU_def[OF \<open>u2 \<in> U\<close>]
            unfolding pow_len[of u2]
            by (smt (verit, del_insts) add.assoc add.commute add_le_mono nat_mult_le_cancel_disj)
          also have "... \<le> n"
            unfolding n_def add_mult_distrib
            by linarith
          finally have "\<^bold>|w1 \<cdot> u1 \<^sup>@ k1 \<cdot> w2 \<cdot> u2 \<^sup>@ q\<^bold>| \<le> n".
          hence "\<^bold>|w\<^bold>| \<ge> \<^bold>|w1 \<cdot> u1 \<^sup>@ k1 \<cdot> w2 \<cdot> u2 \<^sup>@ q\<^bold>|"
            using \<open>n < \<^bold>|w\<^bold>|\<close>
            by linarith
          hence "w1 \<cdot> u1 \<^sup>@ k1 \<cdot> w2 \<cdot> u2 \<^sup>@ q \<le>p p\<cdot>w"
            by (meson \<open>p \<cdot> w \<bowtie> w1 \<cdot> u1 \<^sup>@ k1 \<cdot> w2 \<cdot> u2 \<^sup>@ q\<close> comp_shorter dual_order.trans pref_comp_sym suf_len')
          hence "u1 \<^sup>@ k1 \<cdot> w2 \<cdot> u2 \<^sup>@ q \<le>f w"
            using prefE[OF \<open>p \<le>p w1\<close>] pref_cancel[of p]
            using append_assoc fac_trans prefix_imp_sublist sublist_append_leftI by smt
          hence "u2\<^sup>@q \<le>f w"
            by (meson sublist_append_leftI sublist_order.dual_order.trans)
          thus ?thesis
            using \<open>u2 \<noteq> \<epsilon>\<close>
            by blast
        qed
      next
        case 2
        then obtain p' where "p\<cdot>p' = w1 \<cdot> u1 \<^sup>@ k1"
          by (metis prefixE)
        have "w \<cdot> s = p' \<cdot> w2 \<cdot> u2 \<^sup>@ k2 \<cdot> w3"
          using pws
          unfolding lassoc \<open>p\<cdot>p' = w1 \<cdot> u1 \<^sup>@ k1\<close>[symmetric]
          unfolding rassoc cancel
          unfolding lassoc.

        show ?thesis
        proof(cases)
          assume "\<^bold>|p'\<^bold>| \<ge> \<^bold>|u1\<^sup>@q\<^bold>|"
          have "p' \<le>s u1\<^sup>@k1"
            by (metis "2" \<open>p \<cdot> p' = w1 \<cdot> u1 \<^sup>@ k1\<close> eqd_pref_suf_iff ruler_pref')
          hence "u1\<^sup>@q \<le>f p'"
            by (smt (verit, ccfv_SIG) \<open>\<^bold>|u1 \<^sup>@ q\<^bold>| \<le> \<^bold>|p'\<^bold>|\<close> fac_pref le_exps_pref nle_le sublist_length_le suf_exps_pow suffix_def suffix_imp_sublist suffix_length_suffix)

          have "p' \<bowtie> w"
            by (metis \<open>w \<cdot> s = p' \<cdot> w2 \<cdot> u2 \<^sup>@ k2 \<cdot> w3\<close> pref_compI1 pref_compI2 ruler_pref' triv_pref)
          thus ?thesis
          proof(cases rule: pref_compE)
            assume "p' \<le>p w"
            thus ?thesis
              by (metis "2" \<open>u1 \<^sup>@ q \<le>f p'\<close> append.right_neutral emp_pow_emp fac_trans prefix_imp_sublist)
          next
            assume "w \<le>p p'"
            hence "w \<le>p p'\<cdot>w2"
              by auto
            have "u1 \<noteq> \<epsilon>"
              using "2" by force
            thus ?thesis
              using lem[OF \<open>w \<le>p p'\<cdot>w2\<close> \<open>p' \<le>s u1\<^sup>@k1\<close> \<open>w2 \<in> W\<close> \<open>u1 \<in> U\<close> \<open>u1 \<noteq> \<epsilon>\<close>]
              by blast
          qed
        next
          assume "\<not>\<^bold>|p'\<^bold>| \<ge> \<^bold>|u1\<^sup>@q\<^bold>|"
          hence "\<^bold>|p'\<^bold>| < q*lU"
            unfolding pow_len
            using lU_def[OF \<open>u1 \<in> U\<close>]
            by (meson dual_order.trans linorder_not_less mult_le_mono2)


          from lenarg[OF \<open>w \<cdot> s = p' \<cdot> w2 \<cdot> u2 \<^sup>@ k2 \<cdot> w3\<close>]
          have "n \<le> \<^bold>|p' \<cdot> w2 \<cdot> u2 \<^sup>@ k2 \<cdot> w3\<^bold>|"
            unfolding lenmorph[of w]
            using \<open>n < \<^bold>|w\<^bold>|\<close> by linarith
          also have "... \<le> 2 * lW + (k2 + q) * lU"
            unfolding lenmorph[of p']
            using le3[OF \<open>w2 \<in> W\<close> \<open>u2 \<in> U\<close> \<open>w3 \<in> W\<close>, of k2] \<open>\<^bold>|p'\<^bold>| < q*lU\<close>
            unfolding add_mult_distrib
            unfolding add.commute[of "\<^bold>|p'\<^bold>|"] add.assoc[symmetric]
            by (meson add_mono_thms_linordered_semiring(1) less_or_eq_imp_le)
          finally have "k2+q \<ge> 2*q"
            unfolding n_def
            by (smt (verit, del_insts) One_nat_def Suc_1 \<open>\<^bold>|p' \<cdot> w2 \<cdot> u2 \<^sup>@ k2 \<cdot> w3\<^bold>| \<le> 2 * lW + (k2 + q) * lU\<close> \<open>\<^bold>|w \<cdot> s\<^bold>| = \<^bold>|p' \<cdot> w2 \<cdot> u2 \<^sup>@ k2 \<cdot> w3\<^bold>|\<close> \<open>n < \<^bold>|w\<^bold>|\<close> add_le_mono le_add2 linorder_not_less max.absorb2 max.coboundedI1 mult_le_mono1 n_def nle_le numeral_3_eq_3 plus_1_eq_Suc prefI prefix_length_le)           hence "k2 \<ge> q"
            by linarith

          have "\<^bold>|p' \<cdot> w2 \<cdot> u2 \<^sup>@ q\<^bold>| < lW + 2*q*lU"
            unfolding lenmorph[of p']
            using \<open>\<^bold>|p'\<^bold>| < q*lU\<close> le2[OF \<open>w2 \<in> W\<close> \<open>u2 \<in> U\<close>, of q]
            unfolding mult_2
            unfolding add_mult_distrib add.assoc[symmetric]
            by linarith
          also have "... \<le> n"
            unfolding n_def
            by linarith
          finally have "\<^bold>|p' \<cdot> w2 \<cdot> u2 \<^sup>@ q\<^bold>| < n".
          hence "\<^bold>|p' \<cdot> w2 \<cdot> u2 \<^sup>@ q\<^bold>| < \<^bold>|w\<^bold>|"
            using \<open>n < \<^bold>|w\<^bold>|\<close> order_less_trans by blast


          have "u2\<^sup>@k2 = u2\<^sup>@q\<cdot>u2\<^sup>@(k2-q)"
            using \<open>q \<le> k2\<close>
            by (metis pop_pow)
          have "p'\<cdot>w2\<cdot>u2\<^sup>@q \<bowtie> w"
            using \<open>w \<cdot> s = p' \<cdot> w2 \<cdot> u2 \<^sup>@ k2 \<cdot> w3\<close>
            unfolding \<open>u2\<^sup>@k2 = u2\<^sup>@q\<cdot>u2\<^sup>@(k2-q)\<close> rassoc
            by (metis append_eq_appendI eqd_comp)
          hence "p'\<cdot>w2\<cdot>u2\<^sup>@q \<le>p w"
            by (meson \<open>\<^bold>|p' \<cdot> w2 \<cdot> u2 \<^sup>@ q\<^bold>| < \<^bold>|w\<^bold>|\<close> linorder_not_less pref_compE prefix_length_le)
          hence "u2\<^sup>@q \<le>f w"
            by (meson fac_trans prefix_imp_sublist sublist_append_leftI)
          moreover have "u2 \<noteq> \<epsilon>"
          proof(rule ccontr)
            assume "\<not> u2 \<noteq> \<epsilon>"
            hence "\<^bold>|u2\<^bold>| = 0"
              by blast
            have "\<^bold>|p' \<cdot> w2 \<cdot> u2 \<^sup>@ k2 \<cdot> w3\<^bold>| \<le> 2*lW + q*lU"
              using lW_def[OF \<open>w2 \<in> W\<close>] lW_def[OF \<open>w3 \<in> W\<close>]
              unfolding lenmorph n_def mult_2 add_mult_distrib pow_len \<open>\<^bold>|u2\<^bold>| = 0\<close>
              unfolding mult_0_right add_0
              using \<open>\<^bold>|p'\<^bold>| < q * lU\<close> by linarith
            from le_trans[OF \<open>n \<le> \<^bold>|p' \<cdot> w2 \<cdot> u2 \<^sup>@ k2 \<cdot> w3\<^bold>|\<close> this]
            show False
              unfolding n_def
              using \<open>\<^bold>|p'\<^bold>| < q * lU\<close> by linarith
          qed
          ultimately show ?thesis
            by meson
        qed
      next
        case 3
        then obtain p' s' where "p = w1 \<cdot> u1 \<^sup>@ k1 \<cdot> p'" "w2 = p'\<cdot>s'"
          using append_assoc pref_cancel pref_compE prefix_def ruler_pref'' by smt

        have "\<^bold>|s'\<^bold>| \<le> lW"
          using \<open>w2 = p' \<cdot> s'\<close> \<open>w2 \<in> W\<close> lW_def le_trans suf_len' by blast

        have "w \<cdot> s = s' \<cdot> u2 \<^sup>@ k2 \<cdot> w3"
          using pws
          unfolding \<open>w2 = p'\<cdot>s'\<close> \<open>p = w1 \<cdot> u1 \<^sup>@ k1 \<cdot> p'\<close> rassoc
          unfolding cancel.
        hence "n < \<^bold>|s' \<cdot> u2 \<^sup>@ k2 \<cdot> w3\<^bold>|"
          by (metis \<open>n < \<^bold>|w\<^bold>|\<close> order_less_le_trans suf_len' swap_len)
        also have "... \<le> 2*lW + k2 * lU"
          using le3[OF \<open>w2 \<in> W\<close> \<open>u2 \<in> U\<close> \<open>w3 \<in> W\<close>, of k2]
          unfolding \<open>w2 = p'\<cdot>s'\<close> rassoc
          unfolding lenmorph[of p']
          using add_leE by blast
        finally have "n < 2 * lW + k2 * lU ".
        hence "k2 \<ge> q"
          unfolding n_def
          unfolding numeral_3_eq_3 mult_2
          unfolding add_mult_distrib[of _ 1, unfolded mult_1, folded Suc_eq_plus1] mult_0 add_0
          unfolding add.assoc
          unfolding nat_add_left_cancel_less[of lW]
          unfolding less_le_not_le
          by (metis mult_le_cancel2 nat_le_linear trans_le_add2)
        note pop_pow[OF this, of u2]
        hence "s'\<cdot>u2\<^sup>@q \<bowtie> w"
          using \<open>w \<cdot> s = s' \<cdot> u2 \<^sup>@ k2 \<cdot> w3\<close>
          by (metis append_assoc eqd_comp)
        moreover have "\<^bold>|s'\<cdot>u2\<^sup>@q\<^bold>| \<le> lW + q*lU"
          unfolding lenmorph pow_len
          using lU_def[OF \<open>u2 \<in> U\<close>] \<open>\<^bold>|s'\<^bold>| \<le> lW\<close>
          by (simp add: add_le_mono)
        hence "\<^bold>|s'\<cdot>u2\<^sup>@q\<^bold>| < \<^bold>|w\<^bold>|"
          using \<open>n < \<^bold>|w\<^bold>|\<close>
          unfolding n_def
          by linarith
        ultimately have  "s'\<cdot>u2\<^sup>@q \<le>p w"
          by (meson comp_shorter less_or_eq_imp_le)
        hence "u2\<^sup>@q \<le>f w"
          by (meson fac_pref)
        moreover have "u2 \<noteq> \<epsilon>"
        proof(rule ccontr)
          assume "\<not> u2 \<noteq> \<epsilon>" hence "u2 = \<epsilon>" by blast
          show False
            using \<open>n < \<^bold>|s' \<cdot> u2 \<^sup>@ k2 \<cdot> w3\<^bold>|\<close> \<open>\<^bold>|s'\<^bold>| \<le> lW\<close> lW_def[OF \<open>w3 \<in> W\<close>]
            unfolding n_def \<open>u2 = \<epsilon>\<close> emp_simps
            unfolding lenmorph
            by linarith
        qed
        ultimately show ?thesis
          by blast
      next
        case 4
        then obtain p' s' where "p = w1 \<cdot> u1 \<^sup>@ k1 \<cdot> w2  \<cdot>  p'" "u2 \<^sup>@ k2 = p'\<cdot>s'"
          unfolding lassoc
          using pref_cancel pref_compE prefixE ruler_pref'' by smt


        have "u2 \<noteq> \<epsilon>"
          using "4" by fastforce

        have "w \<cdot> s = s' \<cdot>  w3"
          using pws
          unfolding \<open>u2 \<^sup>@ k2 = p'\<cdot>s'\<close> \<open>p = w1 \<cdot> u1 \<^sup>@ k1 \<cdot> w2  \<cdot>  p'\<close> rassoc
          unfolding cancel.
        hence "w \<le>p s'\<cdot>w3"
          by blast
        from lem[OF this sufI[OF \<open>u2 \<^sup>@ k2 = p'\<cdot>s'\<close>[symmetric]] \<open>w3 \<in> W\<close> \<open>u2 \<in> U\<close> \<open>u2 \<noteq> \<epsilon>\<close>]
        show ?thesis
          using \<open>u2 \<noteq> \<epsilon>\<close>
          by blast
      next
        case 5
        hence "w\<cdot>s \<le>s w3"
          using eqd_pref_suf_iff[of p "w\<cdot>s" "w1 \<cdot> u1 \<^sup>@ k1 \<cdot> w2 \<cdot> u2 \<^sup>@ k2" w3] ruler_suf'[of w3 p "w\<cdot>s"]
          unfolding emp_simps rassoc
          unfolding pws
          by fastforce
        hence "\<^bold>|w\<^bold>| \<le> lW"
          using lW_def[OF \<open>w3 \<in> W\<close>]
          by (meson le_trans pref_len' suffix_length_le)
        hence False
          using \<open>\<^bold>|w\<^bold>| > n\<close>[unfolded n_def]
          by linarith
        thus ?thesis
          by blast
      qed
    qed

    let ?u = "if (u1\<^sup>@q \<le>f w \<and> u1 \<noteq> \<epsilon>) then u1 else u2"
    have "?u\<^sup>@q \<le>f w" "?u \<noteq> \<epsilon>"
      using qin by presburger+
    have "?u \<in> U"
      using \<open>u1 \<in> U\<close> \<open>u2 \<in> U\<close> by presburger

    define hu where "hu = h ?u"

    have "hu\<^sup>@q \<le>f (h w)"
      unfolding hu_def
      by (smt (verit, ccfv_threshold) H.fac_mono H.pow_morph qin)
    hence "hu\<^sup>@q \<le>f u\<^sup>@l"
      using \<open>h w \<le>f u \<^sup>@ l\<close> by blast

    have "\<^bold>|hu\<^bold>| \<ge> 1"
      unfolding hu_def
      using nemp_le_len[OF \<open>?u \<noteq> \<epsilon>\<close>] nonerasing_morphism.im_len_le[OF assms(2), of ?u]
      by linarith
    hence "\<^bold>|hu\<^sup>@q\<^bold>| \<ge> 2*\<^bold>|u\<^bold>|"
      unfolding q_def
      by (simp add: pow_len)
    from le_trans[OF this  fac_len[OF \<open>hu \<^sup>@ q \<le>f u \<^sup>@ l\<close>]]
    have "2 \<le> l"
      unfolding pow_len
      unfolding mult_le_cancel2
      using \<open>primitive u\<close> emp_not_prim by blast

    have "2 \<le> q"
      using \<open>q \<noteq> 0\<close> nemp_le_len q_def by auto

    from fac_pow_conjug_primroot[OF \<open>hu\<^sup>@q \<le>f u\<^sup>@l\<close> \<open>\<^bold>|hu\<^sup>@q\<^bold>| \<ge> 2*\<^bold>|u\<^bold>|\<close> \<open>2 \<le> q\<close>]
    have "\<rho> hu \<sim> \<rho> u"
      using \<open>1 \<le> \<^bold>|hu\<^bold>|\<close> by fastforce
    hence "\<rho> hu \<sim> u"
      by (simp add: \<open>primitive u\<close> prim_primroot)
    thus ?thesis
      unfolding HU_def mem_Collect_eq
      using  \<open>u1 \<in> U\<close> \<open>u2 \<in> U\<close> conjug_swap hu_def image_eqI by smt
  qed
  moreover have "finite HU"
  proof-
    have "finite (\<rho> ` h ` U)"
      using \<open>finite U\<close> by blast
    from finite_Bex_conjug[OF this]
    show ?thesis
      unfolding HU_def.
  qed
  ultimately show ?thesis
    using that by presburger
qed


theorem inv_sub_bot_fin_prim_unb_exp: assumes finite_UNIV: "finite (UNIV::'a set)" and "pow_subalph_fixes Q p" "Q \<le> p" "p > 0" "invariant_subalphabet_bot p A" "a \<in> A" "\<not> bounded [a]" "nonerasing_morphism h"
  shows "finite(Collect (primitive_unbounded_exponent (morphic_language (f^^p) h [a])))"
proof-
  interpret fp: endomorphism "f^^p"
    using pow_endomorphism by blast

  interpret hp: endomorphism_morphism "f^^p" h
    using endomorphism.pow_endomorphism endomorphism_morphism_axioms endomorphism_morphism_def by blast

  let ?axiom = "[a]"

  let ?P = "\<lambda>u sw. primitive u \<and> hp.is_repetition_embed_sequence ?axiom u sw \<and> \<not> fp.pushy_sequence sw"
  define L where "L = {u. \<exists>sw. ?P u sw}"



  have "finite L"
  proof(cases "L = {}", simp)
    assume "L \<noteq> {}"
    then obtain u where "u \<in> L"
      by blast

    obtain sw where "primitive u" "hp.is_repetition_embed_sequence ?axiom u sw"  "\<not> fp.pushy_sequence sw"
      using L_def \<open>u \<in> L\<close> by blast

    have "range sw \<subseteq> fp.pmor_lan ?axiom"
      using H.morphism_axioms \<open>hp.is_repetition_embed_sequence [a] u sw\<close> endomorphism.pow_endomorphism endomorphism_axioms endomorphism_morphism.rep_em_seqE(1) endomorphism_morphism_def by blast

    have "infinite (range sw)"
      using H.morphism_axioms \<open>hp.is_repetition_embed_sequence [a] u sw\<close> endomorphism.pow_endomorphism endomorphism_axioms endomorphism_morphism.rep_em_seqE(3) endomorphism_morphism_def sgrowing_range_infin by blast

    have "\<not> pushy [a]"
    proof
      assume "pushy [a]"
      hence "fp.pushy [a]"
        using pushy_strong_shift[OF finite_UNIV, of _ "p-1"] \<open>p > 0\<close>
        by simp

      obtain la where la_def: "\<And>w v. w \<in> range sw \<Longrightarrow> v \<le>f w \<Longrightarrow> fp.bounded v \<Longrightarrow> \<^bold>|v\<^bold>| \<le> la"
        using \<open>\<not> fp.pushy_sequence sw\<close> unfolding fp.pushy_sequence_def
        by (metis linorder_not_le rangeE)

      obtain ka wa where "fp.bounded wa" "wa \<le>f ((f^^p)^^ka) ?axiom" "la < \<^bold>|wa\<^bold>|"
        by (meson \<open>fp.pushy [a]\<close> fp.pmor_lan_fac_pow fp.pushy_maxlen local.finite_UNIV)

      obtain n0 where
        n0_def: "(\<And>w. w \<in> range sw \<Longrightarrow> n0 \<le> \<^bold>|w\<^bold>| \<Longrightarrow> \<exists>b. \<not> fp.bounded [b] \<and> [b] \<in> fp.pmor_lan [a] \<and> ((f ^^ p) ^^ Suc ka) [b] \<le>f w)"
        using fp.non_pushy_subset_unbounded_image[OF finite_UNIV \<open>range sw \<subseteq> fp.pmor_lan ?axiom\<close>, of la "Suc ka"] la_def
        by blast


      obtain w0 where "w0 \<in> range sw" "n0 \<le> \<^bold>|w0\<^bold>|"
        by (metis \<open>infinite (range sw)\<close> lists_UNIV local.finite_UNIV maxlen_finite nat_le_linear subset_UNIV)
      from n0_def[OF this]
      obtain b where "\<not> fp.bounded [b]" "[b] \<in> fp.pmor_lan [a]" "((f ^^ p) ^^ Suc ka) [b] \<le>f w0"
        by blast

      note bot = inv_sub_bot_pow_subalph[OF finite_UNIV \<open>invariant_subalphabet_bot p A\<close> _ _ \<open>pow_subalph_fixes Q p\<close> \<open>Q \<le> p\<close>]
      note subfix = pow_subalph_l_fixes_all[OF \<open>pow_subalph_fixes Q p\<close> \<open>Q \<le> p\<close> \<open>p > 0\<close>]

      have "b \<in> A"
        using \<open>[b] \<in> fp.pmor_lan [a]\<close> subfix[of _ a, OF zero_less_Suc] \<open>a \<in> A\<close>
        unfolding bot[OF \<open>\<not> bounded [a]\<close> \<open>a \<in> A\<close>]
        unfolding fp.pmor_lan_iff[of "[b]" "[a]"]
        unfolding fp.pmor_lan_fac_pow
        unfolding funpow_mult
        unfolding funpow_add''
        unfolding pow_subalph_l_mult
        unfolding add_mult_distrib2[of _ _ 1, folded Suc_eq_plus1, unfolded nat_mult_1_right, symmetric]
        unfolding mult.commute[of "p"] funpow_mult
        using insert_subset list.set(1) list.set(2) set_mono_sublist singleton_iff
        by metis
      hence "a \<in> set (((f^^p) [b]))"
        by (metis Suc_pred \<open>\<not> fp.bounded [b]\<close> assms(4) assms(6) bot endomorphism.bounded_pow_bounded endomorphism_axioms)
      moreover have "((f ^^ p) ^^ Suc ka) [b] = ((f ^^ p) ^^ ka) ((f ^^ p) [b])"
        using compow_Suc'[of _ "f ^^ p"] by auto
      ultimately have "wa \<le>f ((f ^^ p) ^^ Suc ka) [b]"
        by (smt (verit, best) \<open>wa \<le>f ((f ^^ p) ^^ ka) [a]\<close> fac_mono_pow funpow_mult funpow_add'' split_list' sublist_appendI sublist_order.order_trans)
      hence "wa \<le>f w0"
        using \<open>((f ^^ p) ^^ Suc ka) [b] \<le>f w0\<close> by blast
      from la_def[OF _ this]
      show False
        using \<open>fp.bounded wa\<close> \<open>la < \<^bold>|wa\<^bold>|\<close> \<open>w0 \<in> range sw\<close> less_le_not_le by blast
    qed

    have "\<exists>l. w \<le>f u\<^sup>@l" if "w \<in> hp.mor_lan ((f^^p) [a])" for w
    proof-
      obtain r where "r \<in> fp.pmor_lan ((f^^p) [a])" "w \<le>f h r"
        by (meson \<open>w \<in> hp.mor_lan ((f^^p) [a])\<close> fp.pmor_lan_pow_in hp.mor_lan_fac_pow)

      from inv_sub_bot_bounded_occ[OF finite_UNIV \<open>invariant_subalphabet_bot p A\<close> \<open>\<not> bounded [a]\<close> \<open>a \<in> A\<close> \<open>pow_subalph_fixes Q p\<close> \<open>Q \<le> p\<close> \<open>p > 0\<close> \<open>\<not> pushy [a]\<close>, of p, unfolded has_uniformly_bounded_occurrences_def, rule_format, OF \<open>r \<in> fp.pmor_lan ((f^^p) [a])\<close>]
      obtain k where k_def: "z \<in> fp.pmor_lan ((f^^p) [a]) \<Longrightarrow> k \<le> \<^bold>|z\<^bold>| \<Longrightarrow> r \<le>f z" for z
        by meson
      obtain j where "sw j \<in> fp.pmor_lan ((f^^p) [a])" "k \<le> \<^bold>|sw j\<^bold>|"
      proof-
        have "finite (factor_closure {((f ^^ p) ^^ l) [a] |l. l < 1})" (is "finite ?T")
          using fp.fac_pow_seg_finite by blast
        then obtain tz where "\<And>w. w \<in> ?T \<Longrightarrow> \<^bold>|w\<^bold>| < tz"
          by (meson finite_maxlen)
        obtain j where "(max k tz) < \<^bold>|sw j\<^bold>|"
          using \<open>hp.is_repetition_embed_sequence [a] u sw\<close> endomorphism_morphism.rep_em_seqE(3) hp.endomorphism_morphism_axioms sgrowin_len' by blast
        hence "sw j \<notin> ?T"
          using \<open>\<And>w. w \<in> factor_closure {((f ^^ p) ^^ l) [a] |l. l < 1} \<Longrightarrow> \<^bold>|w\<^bold>| < tz\<close> by fastforce
        hence "sw j \<in> fp.pmor_lan ((f^^ p) [a])"
          using fp.pmor_shift[of "[a]" "1", unfolded funpow_1] \<open>range sw \<subseteq> fp.pmor_lan [a]\<close> by auto
        from that[OF this]
        show ?thesis
          using \<open>max k tz < \<^bold>|sw j\<^bold>|\<close> by auto
      qed
      from k_def[OF this]
      have "r \<le>f sw j".
      obtain l where "h (sw j) \<le>f u\<^sup>@l"
        by (meson \<open>hp.is_repetition_embed_sequence [a] u sw\<close> endomorphism_morphism.rep_em_seqE(2) hp.endomorphism_morphism_axioms)
      hence "h r \<le>f u\<^sup>@l"
        using H.fac_mono \<open>r \<le>f sw j\<close> by blast
      hence "w \<le>f u\<^sup>@l"
        using \<open>w \<le>f h r\<close> by blast
      thus ?thesis
        by blast
    qed

    define Z where "Z = factor_closure (h`factor_closure {((f ^^ p) ^^ l) [a] |l. l < 1})"
    have "w \<in> hp.mor_lan [a] = ((w \<in> Z) \<or> (w \<in> hp.mor_lan ((f ^^ p) [a])))" for w
      using arg_cong[OF fp.pmor_shift[of "[a]", of 1, unfolded funpow_1], of "factor_closure o (image h)", unfolded comp_apply]
      unfolding Z_def fac_clo_Un image_Un
      unfolding morphic_language_def Un_iff[symmetric] set_eq_iff..

    have "finite Z"
    proof-
      have "finite ({((f ^^ p) ^^ l) [a] |l. l < 1})"
        using fac_clo_finite fp.fac_pow_seg_finite by blast
      thus ?thesis
        unfolding Z_def
        by (meson fac_clo_finite finite_imageI)
    qed
    then obtain lq where "\<And>q. q \<in> Z \<Longrightarrow> \<^bold>|q\<^bold>| < lq"
      by (meson finite_maxlen)

    have "u \<sim> u'" if "u' \<in> L" for u'
    proof-
      obtain sw' where "primitive u'" "hp.is_repetition_embed_sequence ?axiom u' sw'"  "\<not> fp.pushy_sequence sw'"
        using L_def \<open>u' \<in> L\<close> by blast
      note hp.rep_em_seqE[OF this(2)]
      obtain j where "lq + 2*\<^bold>|u\<^bold>| + 2*\<^bold>|u'\<^bold>| < \<^bold>|sw' j\<^bold>|"
        using \<open>strictly_growing_seq sw'\<close> sgrowin_len' by blast
      obtain k' where "h (sw' j) \<le>f u' \<^sup>@ k'"
        using \<open>\<And>j. \<exists>k. h (sw' j) \<le>f u' \<^sup>@ k\<close> by presburger

      have "lq < \<^bold>|sw' j\<^bold>|"
        using \<open>lq + 2*\<^bold>|u\<^bold>| + 2*\<^bold>|u'\<^bold>| < \<^bold>|sw' j\<^bold>|\<close> by linarith
      hence "h (sw' j) \<in> hp.mor_lan ((f^^p) [a])"
        by (meson \<open>\<And>q. q \<in> Z \<Longrightarrow> \<^bold>|q\<^bold>| < lq\<close> \<open>\<And>w. (w \<in> hp.mor_lan [a]) = (w \<in> Z \<or> w \<in> hp.mor_lan ((f ^^ p) [a]))\<close> \<open>hp.is_repetition_embed_sequence [a] u' sw'\<close> assms(8) endomorphism_morphism.rep_em_seqE(1) hp.endomorphism_morphism_axioms mor_lan_im_in nonerasing_morphism.im_len_le not_less_iff_gr_or_eq order.strict_trans2)
      then obtain k where "h (sw' j) \<le>f u \<^sup>@ k"
        using \<open>\<And>w. w \<in> hp.mor_lan ((f ^^ p) [a]) \<Longrightarrow> \<exists>l. w \<le>f u \<^sup>@ l\<close> by presburger

      have "2*\<^bold>|u\<^bold>| < \<^bold>|h (sw' j)\<^bold>|"
        by (meson \<open>lq + 2 * \<^bold>|u\<^bold>| + 2 * \<^bold>|u'\<^bold>| < \<^bold>|sw' j\<^bold>|\<close> add_leD2 assms(8) le_add1 le_trans linorder_not_le nonerasing_morphism.im_len_le)
      hence "2 < k"
        by (metis \<open>h (sw' j) \<le>f u \<^sup>@ k\<close> dual_order.strict_trans1 fac_len mult_less_cancel2 pow_len)
      have "2*\<^bold>|u'\<^bold>| < \<^bold>|h (sw' j)\<^bold>|"
        by (meson \<open>lq + 2 * \<^bold>|u\<^bold>| + 2 * \<^bold>|u'\<^bold>| < \<^bold>|sw' j\<^bold>|\<close> assms(8) le_trans linorder_not_le nonerasing_morphism.im_len_le trans_le_add2)
      hence "2 < k'"
        by (metis \<open>h (sw' j) \<le>f u' \<^sup>@ k'\<close> dual_order.strict_trans1 fac_len mult_less_cancel2 pow_len)

      have "h (sw' j) \<noteq> \<epsilon>"
        using \<open>2 * \<^bold>|u'\<^bold>| < \<^bold>|h (sw' j)\<^bold>|\<close> by force

      note per_fac[OF \<open>h (sw' j) \<noteq> \<epsilon>\<close> \<open>h (sw' j) \<le>f u \<^sup>@ k\<close>] per_fac[OF \<open>h (sw' j) \<noteq> \<epsilon>\<close> \<open>h (sw' j) \<le>f u' \<^sup>@ k'\<close>]
      have pe1: "(h (sw' j)) \<le>p (take \<^bold>|u\<^bold>| (h (sw' j))) \<cdot> (h (sw' j))"
        using \<open>period (h (sw' j)) \<^bold>|u\<^bold>|\<close> per_pref by blast
      have pe2: "(h (sw' j)) \<le>p (take \<^bold>|u'\<^bold>| (h (sw' j))) \<cdot> (h (sw' j))"
        using \<open>period (h (sw' j)) \<^bold>|u'\<^bold>|\<close> per_pref by blast
      from two_pers[OF pe1 pe2]
      have co: "take \<^bold>|u\<^bold>| (h (sw' j)) \<cdot> take \<^bold>|u'\<^bold>| (h (sw' j)) = take \<^bold>|u'\<^bold>| (h (sw' j)) \<cdot> take \<^bold>|u\<^bold>| (h (sw' j))"
        using \<open>2 * \<^bold>|u'\<^bold>| < \<^bold>|h (sw' j)\<^bold>|\<close> \<open>2 * \<^bold>|u\<^bold>| < \<^bold>|h (sw' j)\<^bold>|\<close> by fastforce

      have "take \<^bold>|u\<^bold>| (h (sw' j)) \<sim> u"
        by (smt (verit) \<open>2 * \<^bold>|u\<^bold>| < \<^bold>|h (sw' j)\<^bold>|\<close> \<open>h (sw' j) \<le>f u \<^sup>@ k\<close> add_leD2 conjug_sym fac_pow_len_conjug less_imp_le_nat mult_2 pref_fac sublist_order.dual_order.trans take_is_prefix take_len)
      hence "primitive (take \<^bold>|u\<^bold>| (h (sw' j)))"
        by (simp add: \<open>primitive u\<close> conjug_prim_iff)

      have "take \<^bold>|u'\<^bold>| (h (sw' j)) \<sim> u'"
        by (smt (verit) \<open>2 * \<^bold>|u'\<^bold>| < \<^bold>|h (sw' j)\<^bold>|\<close> \<open>h (sw' j) \<le>f u' \<^sup>@ k'\<close> add_leD2 conjug_sym fac_pow_len_conjug le_simps(1) mult_2 prefix_imp_sublist sublist_order.dual_order.trans take_is_prefix take_len)
      hence "primitive (take \<^bold>|u'\<^bold>| (h (sw' j)))"
        by (simp add: \<open>primitive u'\<close> conjug_prim_iff)

      have "(take \<^bold>|u\<^bold>| (h (sw' j))) =  take \<^bold>|u'\<^bold>| (h (sw' j))"
        using co
        by (meson \<open>primitive (take \<^bold>|u'\<^bold>| (h (sw' j)))\<close> \<open>primitive (take \<^bold>|u\<^bold>| (h (sw' j)))\<close> comm_prim)
      thus "u \<sim> u'"
        by (metis \<open>take \<^bold>|u'\<^bold>| (h (sw' j)) \<sim> u'\<close> \<open>take \<^bold>|u\<^bold>| (h (sw' j)) \<sim> u\<close> conjug_swap conjug_trans)
    qed
    hence "L \<subseteq> Collect ((\<sim>) u)"
      by (simp add: subsetI)
    thus ?thesis
      using card_conjug[OF prim_nemp[OF \<open>primitive u\<close>]] finite_subset
      using \<open>primitive u\<close> prim_primroot by fastforce
  qed



  obtain HU where
    "finite HU" and
    HU_def: "(\<And>u. primitive u \<and> (\<exists>sw. hp.is_repetition_embed_sequence [a] u sw \<and> fp.pushy_sequence sw) \<longrightarrow> u \<in> HU)"
    using hp.pushy_rem_em_seq_finite[OF finite_UNIV \<open>nonerasing_morphism h\<close>] by blast

  have "w \<in> L \<union> HU" if "primitive_unbounded_exponent (hp.mor_lan ?axiom) w" for w
   proof(cases)
    assume "\<exists>sw. hp.is_repetition_embed_sequence ?axiom w sw \<and> fp.pushy_sequence sw"
    hence "w \<in> HU" using HU_def[of w]
      using primitive_unbounded_exponent_def that by blast
    thus ?thesis
      by blast
  next
    assume "\<nexists>sw. hp.is_repetition_embed_sequence ?axiom w sw \<and> fp.pushy_sequence sw"

    have "primitive w"
      using prim_unb_expE(1) that by blast

    obtain sw where
      "hp.is_repetition_embed_sequence ?axiom w sw"
      using hp.rep_em_seq_ex[OF finite_UNIV \<open>primitive_unbounded_exponent (hp.mor_lan ?axiom) w\<close>]
      by blast
    hence "\<not> fp.pushy_sequence sw"
      using \<open>\<nexists>sw. hp.is_repetition_embed_sequence [a] w sw \<and> fp.pushy_sequence sw\<close> by blast
    hence "w \<in> L"
      unfolding L_def
      using \<open>primitive w\<close> \<open>hp.is_repetition_embed_sequence ?axiom w sw\<close> by blast
    thus ?thesis
      by blast
  qed
  thus ?thesis
    using \<open>finite HU\<close> \<open>finite L\<close> finite_Un infinite_super mem_Collect_eq subsetI by metis
qed

end
context endomorphism
begin

lemma non_pushy_inv_sub_bot_ex: assumes finite_UNIV: "finite (UNIV::'a set)"
 and "\<not> pushy_sequence sw" "\<And>j. sw j \<in> \<L> axiom" "infinite (range sw)"
 and "pow_subalph_fixes Q p" "Q \<le> p" "p > 0"
shows
  "\<And>n. \<exists>b B j k. invariant_subalphabet_bot p B \<and> b \<in> B \<and> \<not> bounded [b] \<and> ((f^^p)^^k) [b] \<le>f sw j \<and> n < \<^bold>|((f^^p)^^k) [b]\<^bold>|"
proof-
  let ?W = "range sw"
  have "?W \<subseteq> \<L> axiom"
    using \<open>\<And>j. sw j \<in> \<L> axiom\<close> by blast

  obtain la where la_def: "\<And>w v. w \<in> ?W \<Longrightarrow> v \<le>f w \<Longrightarrow> bounded v \<Longrightarrow> \<^bold>|v\<^bold>| \<le> la"
    by (metis \<open>\<not> pushy_sequence sw\<close> leI pushy_sequence_def rangeE)

  interpret fp: endomorphism "f^^p"
    using pow_endomorphism by presburger

  fix n
  obtain e where
    "(\<And>a i. \<not> fp.bounded [a] ==> Suc n \<le> \<^bold>|((f ^^ p) ^^ (e + i)) [a]\<^bold>|)"
    using fp.unbounded_im_all_long local.finite_UNIV by blast

  interpret fpe: endomorphism "((f^^p)^^(Suc e))"
    unfolding funpow_mult
    using pow_endomorphism by presburger


  have "0 < p * Suc e"
    using \<open>p > 0\<close>
    by simp

  obtain I q where
    "(\<And>i. i \<in> I \<Longrightarrow> sw i \<in> fpe.pmor_lan ((f ^^ q) axiom))" and
    "infinite (sw ` I)" and
    "q < p * Suc e"
    using pmor_lan_infin_subset_somewhere[OF \<open>?W \<subseteq> \<L> axiom\<close> \<open>infinite ?W\<close>, of "(p * (Suc e)) - 1", unfolded Suc_diff_1[OF \<open>0 < p * Suc e\<close>], folded funpow_mult] by blast
  define W' where "W' = sw`I"
  hence "infinite W'"
    using \<open>infinite (sw ` I)\<close>
    by blast

  have "W' \<subseteq> ?W"
    unfolding W'_def
    by blast
  hence "W' \<subseteq> \<L> axiom"
    using \<open>?W \<subseteq> \<L> axiom\<close> by blast

  have "W' \<subseteq> fpe.pmor_lan ((f^^q) axiom)"
    using W'_def \<open>\<And>i. i \<in> I \<Longrightarrow> sw i \<in> fpe.pmor_lan ((f ^^ q) axiom)\<close> by blast


  have la_def': "\<And>w v. w \<in> W' \<Longrightarrow> v \<le>f w \<Longrightarrow> fpe.bounded v \<Longrightarrow> \<^bold>|v\<^bold>| \<le> la"
    using \<open>W' \<subseteq> ?W\<close> bounded_pow_bounded fp.bounded_pow_bounded la_def subset_eq \<open>p > 0\<close>
    by (metis less_imp_Suc_add)

  obtain n0 where
    n0_def: "(\<And>w. w \<in> W' \<Longrightarrow> n0 \<le> \<^bold>|w\<^bold>| \<Longrightarrow> \<exists>b. \<not> fpe.bounded [b] \<and> [b] \<in> fpe.pmor_lan ((f ^^ q) axiom) \<and> ((f ^^ p) ^^ Suc e) [b] \<le>f w)"
    using fpe.non_pushy_subset_unbounded_image[OF finite_UNIV \<open>W' \<subseteq> fpe.pmor_lan ((f^^q) axiom)\<close>, of _ 1, unfolded funpow_1]
      la_def'
    by blast

  obtain w where "w \<in> W'" "n0 \<le> \<^bold>|w\<^bold>|"
    by (metis \<open>infinite W'\<close> lists_UNIV local.finite_UNIV maxlen_finite nat_le_linear subset_UNIV)
  from n0_def[OF this]
  obtain b where "\<not> fpe.bounded [b]" "[b] \<in> fpe.pmor_lan ((f ^^ q) axiom)" "((f ^^ p) ^^ Suc e) [b] \<le>f w"
    by blast
  hence "((f ^^ p) ^^ e ) ((f ^^ p) [b]) \<le>f w"
    using compow_Suc'[of _ "f ^^ p"]
    by auto

  obtain wj where "w = sw wj"
    using \<open>W' \<subseteq> range sw\<close> \<open>w \<in> W'\<close> by blast

  have "fp.pow_subalph_fixes 1 1"
    using pow_subalph_fixes_pow'[OF \<open>pow_subalph_fixes Q p\<close> \<open>Q \<le> p\<close> \<open>p > 0\<close>].
  from fp.inv_sub_bot_ex'[OF finite_UNIV _ this, of b]
  obtain B where
    "fp.invariant_subalphabet_bot 1 B" and
    "B \<subseteq> set (((f ^^ p) ^^ 1) [b])"
    using \<open>\<not> fpe.bounded [b]\<close> fp.bounded_pow_bounded[of "[b]" e]
    by blast

  have "invariant_subalphabet_bot p B"
    using \<open>fp.invariant_subalphabet_bot 1 B\<close>
    unfolding invariant_subalphabet_bot_def fp.invariant_subalphabet_bot_def
    unfolding invariant_subalphabet_def fp.invariant_subalphabet_def
    unfolding bounded_pow_bounded[of _ p, symmetric]
    unfolding funpow_mult One_nat_def[symmetric] nat_mult_1_right
    by (metis Suc_pred assms(7) endomorphism.bounded_pow_bounded endomorphism_axioms)
  then obtain b' where "\<not> bounded [b']" "b' \<in> B"
    by (meson inv_subE2 inv_sub_botE(1))
  hence "[b'] \<le>f (((f ^^ p) ^^ 1) [b])"
    by (metis \<open>B \<subseteq> set (((f ^^ p) ^^ 1) [b])\<close> split_list' sublist_def subset_eq)
  from fac_mono_pow[OF this[unfolded funpow_mult One_nat_def[symmetric] nat_mult_1_right], of "p * e"]
  have "((f ^^ p) ^^ e ) [b'] \<le>f w"
    using \<open>((f ^^ p) ^^ e ) ((f ^^ p) [b]) \<le>f w\<close>
    unfolding funpow_mult One_nat_def[symmetric] nat_mult_1_right
    unfolding funpow_add comp_apply
    by blast
  moreover have "n < \<^bold>|((f ^^ p) ^^ e ) [b']\<^bold>|"
    using Suc_le_eq \<open>\<And>i a. \<not> fp.bounded [a] \<Longrightarrow> Suc n \<le> \<^bold>|((f ^^ p) ^^ (e + i)) [a]\<^bold>|\<close>[of "b'" 0] \<open>\<not> bounded [b']\<close> bounded_pow_bounded[of "[b']" "p-1"]
    unfolding add.right_neutral Suc_pred'[OF \<open>p > 0\<close>, symmetric]
    by blast
  ultimately show "\<exists>b B j k.
            invariant_subalphabet_bot p B \<and>
            b \<in> B \<and> \<not> bounded [b] \<and> ((f ^^ p) ^^ k) [b] \<le>f sw j \<and> n < \<^bold>|((f ^^ p) ^^ k) [b]\<^bold>|"
    using \<open>\<not> bounded [b']\<close> \<open>b' \<in> B\<close> \<open>invariant_subalphabet_bot p B\<close> \<open>w = sw wj\<close> by blast
qed

lemma non_pushy_inv_sub_bot_ex_one: assumes finite_UNIV: "finite (UNIV::'a set)"
 and "\<not> pushy_sequence sw" "\<And>j. sw j \<in> \<L> axiom" "infinite (range sw)" and "pow_subalph_fixes Q p" "Q \<le> p" "p > 0"
obtains B b where "invariant_subalphabet_bot p B" "b \<in> B" "\<not> bounded [b]"
  "\<And>n. \<exists>j k. ((f^^p)^^k) [b] \<le>f sw j \<and> n < \<^bold>|((f^^p)^^k) [b]\<^bold>|"
proof-
  define P where "P = (\<lambda> B b j k n. invariant_subalphabet_bot p B \<and>
   b \<in> B \<and> \<not> bounded [b] \<and> ((f ^^p) ^^ k) [b] \<le>f sw j \<and> n < \<^bold>|((f ^^ p) ^^ k) [b]\<^bold>|)"
  define Pp where "Pp = (\<lambda> Bb jk n. P (fst Bb) (snd Bb) (fst jk) (snd jk) n)"

    define obt_Bbjk where "obt_Bbjk = (\<lambda>n. SOME (Bb,jk). Pp Bb jk n)"

  have "\<exists> Bbjk. Pp (fst Bbjk) (snd Bbjk) n" for n
    using non_pushy_inv_sub_bot_ex[OF assms, of n]
    unfolding Pp_def P_def
    using prod.sel
    by auto
  from someI_ex[OF this]
  have obtPp: "Pp (fst (obt_Bbjk n)) (snd (obt_Bbjk n)) n" for n
    unfolding obt_Bbjk_def
    by (simp add: case_prod_unfold)

  define all_bots where "all_bots = { (B,b). invariant_subalphabet_bot p B \<and>
   b \<in> B \<and> \<not> bounded [b]}"
  hence "finite all_bots"
    by (meson Finite_Set.finite_set finite_Prod_UNIV local.finite_UNIV rev_finite_subset subset_UNIV)

  have "fst (obt_Bbjk n) \<in> all_bots" for n
    using obtPp[of n] unfolding Pp_def P_def all_bots_def
    by (simp add: case_prod_beta')
  hence "fst`(range obt_Bbjk) \<subseteq> all_bots"
    by blast
  hence "finite (fst`(range obt_Bbjk))"
    by (meson \<open>finite all_bots\<close> finite_subset)

  interpret fp: endomorphism "f^^p"
    using pow_endomorphism by blast

  have "infinite (\<L> axiom)"
    by (meson assms(3) assms(4) finite_subset image_subsetI)
  hence "1 < \<lceil>f ^^ p\<rceil>"
    using bounded_def endomorphism.bounded_pow_bounded endomorphism.pow_endomorphism endomorphism.unbounded_growing endomorphism_axioms fp.growing_maxlen local.finite_UNIV \<open>p > 0\<close>
    fp.pmor_lan_infin_growing pmor_lan_infin_pow_infin by force

  have "infinite (range obt_Bbjk)"
  proof-
    have "\<exists>n. (snd (snd (obt_Bbjk n))) > k0" for k0
    proof
      define n0 where "n0 = \<lceil>f ^^ (p)\<rceil>^k0"
      have "n0 < \<^bold>|((f ^^ p) ^^ (snd (snd (obt_Bbjk n0)))) [(snd (fst (obt_Bbjk n0)))]\<^bold>|"
        using P_def Pp_def obtPp by blast
      hence "n0 <  \<lceil>f ^^ p\<rceil> ^ (snd (snd (obt_Bbjk n0)))"
        using morphism.max_im_len_le_dom[OF pow_morphism finite_UNIV, of "p * (snd (snd (obt_Bbjk n0)))" "[(snd (fst (obt_Bbjk n0)))]"]
              fp.max_im_len_pow_le_dom[OF finite_UNIV, of "(snd (snd (obt_Bbjk n0)))"]
        unfolding funpow_mult[symmetric]
        unfolding sing_len
        unfolding mult_1
        unfolding Suc_le_eq[symmetric]
        using le_trans
        by linarith
      thus "k0 < snd (snd (obt_Bbjk n0))"
        unfolding n0_def using \<open>1 < \<lceil>f ^^ p\<rceil>\<close>
        using power_less_imp_less_exp by blast
    qed
    hence "infinite (snd`snd`(range obt_Bbjk))"
      by (metis (mono_tags, opaque_lifting) Suc_le_lessD UNIV_I finite_nat_set_iff_bounded image_eqI nat_le_linear not_less_eq)
    thus ?thesis
      by blast
  qed


  from pigeonhole_infinite[OF this \<open>finite (fst`(range obt_Bbjk))\<close>]
  obtain Bbjk where "Bbjk \<in> range obt_Bbjk" "infinite {a \<in> range obt_Bbjk. fst a = fst Bbjk}"
    by blast

  have "\<exists>n. fst (obt_Bbjk n) = fst Bbjk \<and> n0 < n" for n0
  proof(rule ccontr)
    assume "\<nexists>n. fst (obt_Bbjk n) = fst Bbjk \<and> n0 < n"
    hence "\<forall>n. fst (obt_Bbjk n) \<noteq> fst Bbjk \<or> n0 \<ge> n"
      using leI by blast
    hence "\<forall>n. fst (obt_Bbjk n) = fst Bbjk \<longrightarrow> n0 \<ge> n"
      by blast
    hence "finite {n. fst (obt_Bbjk n) = fst Bbjk }"
      using finite_nat_set_iff_bounded_le by auto
    note finite_imageI[OF this, of obt_Bbjk]
    moreover have "obt_Bbjk ` {n. fst (obt_Bbjk n) = fst Bbjk} = {a \<in> range obt_Bbjk. fst a = fst Bbjk}"
      by blast
    ultimately show False
      using \<open>infinite {a \<in> range obt_Bbjk. fst a = fst Bbjk}\<close>
      by argo
  qed

  define obt where "obt = (\<lambda>n. (if (fst (obt_Bbjk n) = fst Bbjk) then (obt_Bbjk n) else (obt_Bbjk (SOME n'. fst (obt_Bbjk n') = fst Bbjk \<and> n < n'))))"
  have "Pp (fst (obt n)) (snd (obt n)) n \<and> fst (obt n) = fst Bbjk" for n
  proof(cases)
    assume "fst (obt_Bbjk n) = fst Bbjk"
    thus ?thesis
      by (metis obtPp obt_def)
  next
    assume "fst (obt_Bbjk n) \<noteq> fst Bbjk"
    hence "obt n = obt_Bbjk (SOME n'. fst (obt_Bbjk n') = fst Bbjk \<and> n < n')"
      using obt_def by presburger
    thus ?thesis
      using someI_ex[OF \<open>\<exists>n'. fst (obt_Bbjk n') = fst Bbjk \<and> n < n'\<close>] obtPp[of "(SOME n'. fst (obt_Bbjk n') = fst Bbjk \<and> n < n')"]
      unfolding P_def Pp_def
      by auto
  qed
  thus ?thesis
    unfolding Pp_def P_def
    using that
    by metis
qed

end
context endomorphism_morphism
begin

theorem mor_lan_fin_prim_unb_exp:
  assumes "finite (UNIV::'a set)"
    and "nonerasing_morphism h"
  shows "finite {u. primitive_unbounded_exponent (\<H> axiom) u}"
                                proof-

  obtain Q p where "pow_subalph_fixes Q p" "Q \<le> p" "p > 0"
    using pow_subalph_l_ev_per_all[OF \<open>finite (UNIV::'a set)\<close>, of thesis]
    by simp

  let ?P = "\<lambda>w B b. invariant_subalphabet_bot p B \<and> b \<in> B \<and> \<not> bounded [b] \<and> primitive_unbounded_exponent (morphic_language (f^^p) h [b]) w"
  let ?map = "\<lambda>w. (SOME (B,b). ?P w B b)"

  \<comment> \<open>We first show that a repetition having pushy embed sequence is in fact a repetition over a subsystem generated by some @{term "invariant_subalphabet_bot"} and @{term "f^^p"}.\<close>

  have rep_np: "?P w (fst (?map w)) (snd (?map w))"
    if "primitive w" "is_repetition_embed_sequence axiom w sw" "\<not> pushy_sequence sw" for w sw
  proof-
    have "primitive_unbounded_exponent (\<H> axiom) w"
      using rep_em_seq_prim_unb_exp'[OF \<open>is_repetition_embed_sequence axiom w sw\<close> \<open>nonerasing_morphism h\<close> \<open>primitive w\<close>].
    have "infinite (range sw)"
      using sgrowing_range_infin[OF rep_em_seqE(3)[OF \<open>is_repetition_embed_sequence axiom w sw\<close>]].
    obtain B b where
      "invariant_subalphabet_bot p B" and
      "b \<in> B" and
      "\<not> bounded [b]" and
      obt: "\<And>n. \<exists>j k. ((f ^^ p) ^^ k) [b] \<le>f sw j \<and> n < \<^bold>|((f ^^ p) ^^ k) [b]\<^bold>|"
      using non_pushy_inv_sub_bot_ex_one[OF \<open>finite (UNIV::'a set)\<close> \<open>\<not> pushy_sequence sw\<close> _ \<open>infinite (range sw)\<close> \<open>pow_subalph_fixes Q p\<close> \<open>Q \<le> p\<close> \<open>p > 0\<close>]
        rep_em_seqE(1)[OF \<open>is_repetition_embed_sequence axiom w sw\<close>]
      by metis
    have "primitive_unbounded_exponent (morphic_language (f^^p) h [b]) w"
    proof
      show "primitive w" using prim_unb_expE(1)[OF \<open>primitive_unbounded_exponent (\<H> axiom) w\<close>].
      show "w \<^sup>@ n \<in> morphic_language (f ^^ p) h [b]" for n
      proof-
        obtain j k where "((f ^^ p) ^^ k) [b] \<le>f sw j" "\<^bold>|w \<^sup>@ Suc (Suc n)\<^bold>| < \<^bold>|((f ^^ p) ^^ k) [b]\<^bold>|"
          using obt[rule_format, of "\<^bold>|w\<^sup>@(Suc (Suc n))\<^bold>|"] by blast
        obtain l where "h (((f ^^ p) ^^ k) [b]) \<le>f w\<^sup>@l"
          using H.fac_mono[OF \<open>((f ^^ p) ^^ k) [b] \<le>f sw j\<close>] \<open>is_repetition_embed_sequence axiom w sw\<close>
          unfolding is_repetition_embed_sequence_def
          by blast
        hence "\<^bold>|w \<^sup>@ (Suc (Suc n))\<^bold>| < \<^bold>|h (((f ^^ p) ^^ k) [b])\<^bold>|"
          using \<open>\<^bold>|w \<^sup>@ (Suc (Suc n))\<^bold>| < \<^bold>|(((f ^^ p) ^^ k) [b])\<^bold>|\<close> nonerasing_morphism.im_len_le[OF \<open>nonerasing_morphism h\<close>]
          by (meson le_trans linorder_not_le)
        hence "w\<^sup>@n \<le>f h (((f ^^ p) ^^ k) [b])"
          using long_pow_fac[OF \<open>h (((f ^^ p) ^^ k) [b]) \<le>f w\<^sup>@l\<close>]
          by simp
        have "(((f ^^ p) ^^ k) [b]) \<in> purely_morphic_language (f ^^ p) [b]"
          using endomorphism.pmor_lan_pow_in endomorphism.pow_endomorphism endomorphism_axioms by blast
        thus ?thesis
          unfolding morphic_language_def
          by (meson \<open>w \<^sup>@ n \<le>f h (((f ^^ p) ^^ k) [b])\<close> factor_closure.intros imageI)
      qed
    qed
    have "\<exists> B b. ?P w B b"
      using \<open>\<not> bounded [b]\<close> \<open>b \<in> B\<close> \<open>primitive_unbounded_exponent (morphic_language (f ^^ p) h [b]) w\<close> \<open>invariant_subalphabet_bot p B\<close> by blast
    thus ?thesis
      by (metis (mono_tags, lifting) some_eq_imp split_beta split_conv)
  qed

  \<comment> \<open>There is only finitely many such subsystems and each subsystem has finitely many repetitions due to @{thm inv_sub_bot_fin_prim_unb_exp}.\<close>

  let ?L = "{w. \<exists>B b. ?P w B b}"
  have "finite ?L"
  proof-

    have L_UN: "?L = \<Union> { {w. primitive_unbounded_exponent (morphic_language (f^^p) h [b]) w} | b. \<exists>B. (invariant_subalphabet_bot p B \<and> b \<in> B \<and> \<not> bounded [b])}"
      unfolding set_eq_iff Union_iff Bex_def mem_Collect_eq
      by (metis mem_Collect_eq)

    have "finite {b. \<exists>B. invariant_subalphabet_bot p B \<and> b \<in> B \<and> \<not> bounded [b]}" (is "finite ?allb")
      using infinite_super \<open>finite (UNIV::'a set)\<close> subset_UNIV by blast
    let ?q = "\<lambda> b. {w. primitive_unbounded_exponent (morphic_language (f^^p) h [b]) w}"
    have L_UN': "\<Union>(?q ` ?allb) = ?L"
      unfolding set_eq_iff Union_iff Bex_def mem_Collect_eq
      by blast
    have "b \<in> ?allb \<Longrightarrow> finite (?q b)" for b
      using  inv_sub_bot_fin_prim_unb_exp[OF \<open>finite (UNIV::'a set)\<close> \<open>pow_subalph_fixes Q p\<close> \<open>Q \<le> p\<close> \<open>p > 0\<close>] \<open>nonerasing_morphism h\<close>
      by blast
    thus ?thesis
      using finite_UN[OF \<open>finite ?allb\<close>, of ?q]
      unfolding L_UN L_UN'
      by blast
  qed

  \<comment> \<open>The other case when a repetition has a non pushy embed sequence is covered by @{thm pushy_rem_em_seq_finite}.\<close>

  obtain HU where
    "finite HU" and
    HU_def: "(\<And>u. primitive u \<and> (\<exists>sw. is_repetition_embed_sequence axiom u sw \<and> pushy_sequence sw) \<longrightarrow> u \<in> HU)"
    using pushy_rem_em_seq_finite[OF \<open>finite (UNIV::'a set)\<close> \<open>nonerasing_morphism h\<close>]
    by blast

  \<comment> \<open>No other case occurs.\<close>

  have "w \<in> ?L \<union> HU" if "primitive_unbounded_exponent (\<H> axiom) w" for w
  proof(cases)
    assume "\<exists>sw. is_repetition_embed_sequence axiom w sw \<and> pushy_sequence sw"
    hence "w \<in> HU" using HU_def[of w]
      using primitive_unbounded_exponent_def that by blast
    thus ?thesis
      by blast
  next
    assume "\<nexists>sw. is_repetition_embed_sequence axiom w sw \<and> pushy_sequence sw"

    have "primitive w"
      using prim_unb_expE(1) that by blast

    obtain sw where
      "is_repetition_embed_sequence axiom w sw"
      using \<open>primitive_unbounded_exponent (\<H> axiom) w\<close> rep_em_seq_ex \<open>finite (UNIV::'a set)\<close> by blast
    hence "\<not> pushy_sequence sw"
      using \<open>\<nexists>sw. is_repetition_embed_sequence axiom w sw \<and> pushy_sequence sw\<close> by blast
    hence "w \<in> ?L"
      using rep_np \<open>is_repetition_embed_sequence axiom w sw\<close> \<open>primitive w\<close> by auto
    thus ?thesis
      by blast
  qed
  hence "Collect (primitive_unbounded_exponent (\<H> axiom)) \<subseteq> (?L \<union> HU)"
    by blast
  thus ?thesis
    using \<open>finite HU\<close> \<open>finite ?L\<close> finite_subset by fastforce
qed

end
lemmas morphic_language_finite_unbounded_exponent = endomorphism_morphism.mor_lan_fin_prim_unb_exp[OF endomorphism_morphism.intro, unfolded primitive_unbounded_exponent_def]


end
