theory CoW_Collection
  imports 
          CoW.Equations_Basic
          CoW.Lyndon_Schutzenberger
          CoW.Binary_Code_Morphisms
          CoW.Border_Array
          CoW.Periodicity_Lemma
          CoW_Graph_Lemma.Graph_Lemma
          CoW_Lyndon.Lyndon
          CoW_Lyndon.Lyndon_Addition 
          CoW_Interpretations.Binary_Code_Imprimitive
          CoW_Interpretations.Binary_Imprimitive_Decision
          CoW_Equations.BEW_Three_bs
          CoW_Binary_Monoids_Intersection.CoW_Binary_Monoids_Intersection
          CoW_PCP.Double_Periodic_GPCP2
          CoW_Infinite.Morphic_Language_Unbounded_Exponent
          CoW_Infinite.Sturmian_Words 
begin
 
end